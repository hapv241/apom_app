﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Security.Claims;
using Repositories.Services.Administrative;
using Repositories.Models.Administrative;
using System.Threading.Tasks;
using Repositories.Models;
using System.Net;

namespace Apom_API_Province.Common
{
    /// <summary>
    /// An action filter to make sure the token is present in each and every request
    /// </summary>
    public class TokenValidationAttribute: ActionFilterAttribute
    {
        /// <summary>
        /// Action	IActionFilter	ActionFilterAttribute	Chạy trước và sau phương thức action
        /// Result  IResultFilter   ActionFilterAttribute   Chạy trước và sau action result thực thi
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (Constant.AUTH_TOKEN)
            {
                string token;

                try
                {
                    token = actionContext.Request.Headers.GetValues(Constant.ACCESS_TOKEN).First();
                }
                catch (Exception)
                {
                    actionContext.Response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        Content = new StringContent(Messages.TOKEN_MISSING)
                    };
                    return;
                }

                try
                {
                    ClaimsIdentity tokenIdetity = TokenManager.GetTokenInfo(token);
                    string email = tokenIdetity.FindFirst(ClaimTypes.Email).Value;
                    string pwd = tokenIdetity.FindFirst(Constant.CLAIM_TYPES_PWD).Value;
                    string pass = Libs.Utils.Encrypts.Encrypt(String.Concat(email, "_", Common.Constant.KEYENDE), pwd);

                    // check user
                    user userLogin = userLocalServiceUtil.Instance.getUserbyEmailPwd(email, pass);

                    if (userLogin == null)
                    {
                        actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent(Messages.TOKEN_INVALID_AUTHORIZED)
                        };
                        return;
                    }

                    base.OnActionExecuting(actionContext);
                }
                catch (Exception)
                {
                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                    {
                        Content = new StringContent(Messages.TOKEN_INVALID_AUTHORIZED)
                    };
                    return;
                }
            }
            else
                base.OnActionExecuting(actionContext);
        }
    }
}