﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Apom_API_Province.Common
{
    public class Log
    {
        public static readonly log4net.ILog LOG4NET = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string getFileNameLog(DateTime dateLog)
        {
            string fileName = "apom.api." + dateLog.Year.ToString() + "." + dateLog.Month.ToString() + "." + dateLog.Day + ".log";
            return fileName;
        }

        public static void writeLog(string msg)
        {
            string fileName = @"C:\APOM_APP\Logs" + "\\" + getFileNameLog(DateTime.Now);
            if (!File.Exists(fileName))
            {
                // auto refresh object
            }
            try
            {
                StreamWriter sw = File.AppendText(fileName);
                sw.WriteLine(msg);
                sw.Flush();
                sw.Close();
            }
            catch { }
        }
    }
}