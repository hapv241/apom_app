﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Apom_API_Province.Common
{
    public class Messages
    {
        public static string USER_INVALID_DATA = "Thông tin người dùng không hợp lệ";
        public static string USER_INVALID_DATA_EN = "Thông tin người dùng không hợp lệ";

        public static string USER_INVALID_AUTHENTICATION = "Thông tin đăng nhập không chính xác";

        public static string TOKEN_MISSING = "Không tồn tại thông tin xác thực";
        public static string TOKEN_INVALID_AUTHORIZED = "Thông tin xác thực không chính xác";
    }

    public class Constant
    {
        public static int PAGE_SIZE = GET_CONFIG("page_size");

        #region token
        static string expireToken = WebConfigurationManager.AppSettings["expire_token"];
        static string authToken = WebConfigurationManager.AppSettings["auth_token"];
        public static string ACCESS_TOKEN = "access_token";
        public static bool AUTH_TOKEN = String.IsNullOrEmpty(authToken) ? false : (authToken.Trim() == "0" ? false : true);
        public static string KEYENDE = WebConfigurationManager.AppSettings["KeyEnDe"];
        public static int EXPIRE_TOKEN = String.IsNullOrEmpty(expireToken) ? 0 : Convert.ToInt16(expireToken.Trim());
        public static string SECRET_TOKEN = WebConfigurationManager.AppSettings["secret_token"];
        #endregion

        #region claim in token
        public static string CLAIM_TYPES_UID = "uid";
        public static string CLAIM_TYPES_PWD = "password";
        public static string CLAIM_TYPES_LANG = "language";
        #endregion

        #region groupcomponent_id
        public static string TEMP_GROUPCOMPONENT_ID = WebConfigurationManager.AppSettings["temp_group_component_id"];
        public static string RH_GROUPCOMPONENT_ID = WebConfigurationManager.AppSettings["rh_group_component_id"];
        #endregion

        private static string _connectionStr = "";
        public static string CONNECTIONSTR
        {
            get
            {
                if (String.IsNullOrEmpty(_connectionStr))
                {
                    Models.DataApomAppConnection ApomAppConnection = new Models.DataApomAppConnection();
                    _connectionStr = ApomAppConnection.Database.Connection.ConnectionString;
                }
                return _connectionStr;
            }
        }
        public static int GET_CONFIG(string appName)
        {
            int value = 0;
            if (int.TryParse(WebConfigurationManager.AppSettings[appName].ToString(), out value))
            {
                return value;
            }

            return value;

        }

        public static List<UserEvent> USER_EVENT = new List<UserEvent>();

        #region info user for events
        public class UserEvent
        {
            public string user_id { get; }
            // no. to change pinned_location in a day of user
            public int num_pinnedlocation_in_a_day { get; set; }
            public DateTime date_change_pinnedlocation_in_a_day { get; set; }

            public UserEvent(string user_id)
            {
                this.user_id = user_id;
            }

            public bool checkOverLimit()
            {
                if (this.num_pinnedlocation_in_a_day >= LIMIT_NUM_PINNEDLOCATION_IN_A_DAY) return true;
                return false;
            }

            public bool checkOverDate()
            {
                DateTime now = DateTime.Now;
                if (date_change_pinnedlocation_in_a_day.Year == now.Year && date_change_pinnedlocation_in_a_day.Month == now.Month && date_change_pinnedlocation_in_a_day.Day == now.Day) return false;
                return true;
            }

            // limit no. to change pinned_location in a day of user
            public static int LIMIT_NUM_PINNEDLOCATION_IN_A_DAY = 200;
        }
        #endregion
    }
}