﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apom_API_Province.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseParameters
    {
        /// <summary>
        /// ngày yêu cầu để lấy dữ liệu: yyyy-mm-dd
        /// </summary>
        public string date_request { get; set; }
        /// <summary>
        /// vi || en
        /// </summary>
        public string lang_id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Parameters : BaseParameters
    {
        public string group_id { get; set; }
        public string comp_id { get; set; }
        /// <summary>
        /// số ngày trước đó so với date_request
        /// </summary>
        public int predays { get; set; }
        /// <summary>
        /// số ngày sau đó so với date_request
        /// </summary>
        public int nextdays { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GeoTiffParameters
    {
        public string url { get; set; }
        public string featuretype { get; set; }
        public string featurebase { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GeoTiffIdentifyParameters : BaseParameters
    {
        public double x { get; set; }
        public double y { get; set; }
        public string groupcomponent_id { get; set; }
        //public int numdays { get; set; }
        /// <summary>
        /// số ngày trước đó so với date_request
        /// </summary>
        public int predays { get; set; }
        /// <summary>
        /// số ngày sau đó so với date_request
        /// </summary>
        public int nextdays { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GeoTiffIdentifyProvinceParameters : BaseParameters
    {
        public string province_id { get; set; }
        public string groupcomponent_id { get; set; }
        //public int numdays { get; set; }
        /// <summary>
        /// số ngày trước đó so với date_request
        /// </summary>
        public int predays { get; set; }
        /// <summary>
        /// số ngày sau đó so với date_request
        /// </summary>
        public int nextdays { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GeoTiffIdentifyDistrictParameters : BaseParameters
    {
        public string district_id { get; set; }
        public string groupcomponent_id { get; set; }
        //public int numdays { get; set; }
        /// <summary>
        /// số ngày trước đó so với date_request
        /// </summary>
        public int predays { get; set; }
        /// <summary>
        /// số ngày sau đó so với date_request
        /// </summary>
        public int nextdays { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GeoTiffRankingParameters
    {
        public string group_id { get; set; }
        public string component_id { get; set; }
        /// <summary>
        /// vi || en
        /// </summary>
        public string lang_id { get; set; }
    }

    public class GeoTiffRankingParametersVN : GeoTiffRankingParameters
    {
        public string date_shooting { get; set; }
        public string date_shooting_pre { get; set; }
    }

    public class GeoTiffRankingParametersProvince : GeoTiffRankingParametersVN
    {
        public string province_id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GeoTiffStatisticParameters
    {
        public string province_id { get; set; }
        public string district_id { get; set; }
        /// <summary>
        /// ngày bắt đầu yêu cầu để lấy dữ liệu
        /// </summary>
        public string fromday { get; set; }
        /// <summary>
        /// ngày kết thúc yêu cầu để lấy dữ liệu
        /// </summary>
        public string today { get; set; }
        /// <summary>
        /// pm25 || aqi_pm25
        /// </summary>
        public string type_product { get; set; }
        /// <summary>
        /// vi || en
        /// </summary>
        public string lang_id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PinnedLocationParameters
    {
        public string province_id { get; set; }
        public string user_id { get; set; }
        public string group_id { get; set; }
        public string component_id { get; set; }
        /// <summary>
        /// vi || en
        /// </summary>
        public string lang_id { get; set; }
        /// <summary>
        /// true: thêm mới, false: gỡ bỏ
        /// </summary>
        public bool is_add { get; set; }
        /// <summary>
        /// true: province
        /// false: district 
        /// </summary>
        public bool is_province { get; set; } = true;
    }

    /// <summary>
    /// 
    /// </summary>
    public class AnalysisParameters
    {
        public string id { get; set; }
        /// <summary>
        /// ngày bắt đầu yêu cầu để lấy dữ liệu
        /// </summary>
        public string from_date { get; set; }
        /// <summary>
        /// ngày kết thúc yêu cầu để lấy dữ liệu
        /// </summary>
        public string to_date { get; set; }
        public string component_id { get; set; }
        /// <summary>
        /// vi || en
        /// </summary>
        public string lang_id { get; set; }
    }

}