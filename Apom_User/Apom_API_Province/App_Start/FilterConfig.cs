﻿using System.Web;
using System.Web.Mvc;

namespace Apom_API_Province
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
