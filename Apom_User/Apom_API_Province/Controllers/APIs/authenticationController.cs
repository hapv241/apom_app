﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using Repositories.Models.Administrative;
using Repositories.Services.Administrative;

namespace Apom_API_Province.Controllers.APIs
{
    /// <summary>
    /// Xác thực tài khoản
    /// </summary>
    public class authenticationController : ApiController
    {
        /// <summary>
        /// đăng nhập để lấy token key
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/auth/login")]
        public async Task<IHttpActionResult> LoginUserOrganization([FromBody]LoginViewModel model)
        {
            string email = model.auth_email;
            string pass = model.auth_password;
            
            pass = Libs.Utils.Encrypts.Encrypt(String.Concat(email, "_", Common.Constant.KEYENDE), pass);
            user userLogin = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(email, pass);
            if (userLogin == null)
            {
                return this.Ok(Common.Messages.USER_INVALID_AUTHENTICATION);
            }

            string displayName = String.IsNullOrEmpty(userLogin.displayname) ? userLogin.username : userLogin.displayname;
            var tokenKey = Common.TokenManager.GenerateToken(userLogin.userid, displayName, userLogin.emailaddress, model.auth_password, model.auth_langid);
            //userLocalServiceUtil.Instance.UpdateAccessToken(_userInfo.vidagis_emailaddress, tokenKey);

            var obj = new auth_user
            {
                access_token = tokenKey,
                user = userLogin
            };
            return Ok(obj);
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    public class LoginViewModel : Repositories.Models.basemodel
    {
        public string auth_email { get; set; }
        
        public string auth_password { get; set; }

        public string auth_langid { get; set; }
    }
}