﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Text;
using Newtonsoft.Json;

using Apom_API_Province.Common;
using Apom_API_Province.Models;
using Repositories.Models;
using Repositories.Models.Asset;
using Repositories.Services.Asset;
using System.Data.SqlClient;

namespace Apom_API_Province.Controllers.APIs
{
    /// <summary>
    /// Lấy dữ liệu từ các mô hình wrf-chem || mem
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class componentgeotiffdailyController : ApiController
    {
        /// <summary>
        /// lấy thông tin dữ liệu (pm25, nhiệt độ, độ ẩm) từ mô hình wrf-chem hoặc mem với mã groupcomponent_id, tọa độ {x, y} và thời gian yêu cầu lấy dữ liệu (ngày || giờ) cho trước
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/componentgeotiffdaily/identify_geotiff")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> getByServiceInfo([FromBody]GeoTiffIdentifyParameters param)
        {
            try
            {
                //Log.writeLog("param.x: " + param.x);
                //Log.writeLog("param.y: " + param.y);
                double coorX = param.x; // Math.Ceiling(param.x);  //Convert.ToDouble(param.x.ToString());
                double coorY = param.y; // Math.Ceiling(param.y);  //Convert.ToDouble(param.y.ToString());
                string id = param.groupcomponent_id;
                string date_request = param.date_request;
                string lang_id = param.lang_id;
                
                //component_geotiff_daily_identify obj = componentgeotiffdailyLocalServiceUtil.Instance.getValByIdentify(coorX, coorY, id, date_request, lang_id);
                //string sql = String.Concat("select * from apom_identify_geotiff(" + coorX.ToString() + ",2451798.7616057424,'", id, "',", "'", date_request, "',", "'", lang_id, "')");

                //Log.writeLog("coorX: " + coorX);
                //Log.writeLog("coorY: " + coorY);
                //string sql = $"select * from apom_identify_geotiff({coorX.ToString().Replace(",", ".")}, {coorY.ToString().Replace(",", ".")}, '{id}', '{date_request}', '{lang_id}')";
                //string sql1 = $"select * from apom_identify_geotiff({coorX.ToString()}, {coorY.ToString()}, '{id}', '{date_request}', '{lang_id}')";
                string sql1 = "select * from apom_identify_geotiff(@coorX, @coorY, @id, @date_request, @lang_id)";
                var parameters1 = new List<SqlParameter>();
                parameters1.Add(new SqlParameter("@coorX", coorX));
                parameters1.Add(new SqlParameter("@coorY", coorY));
                parameters1.Add(new SqlParameter("@id", id));
                parameters1.Add(new SqlParameter("@date_request", date_request));
                parameters1.Add(new SqlParameter("@lang_id", lang_id));

                //string sql2 = $"select * from apom_identify_geotiff({coorX.ToString()}, {coorY.ToString()}, '{Common.Constant.TEMP_GROUPCOMPONENT_ID}', '{date_request}', '{lang_id}')";
                string sql2 = "select * from apom_identify_geotiff(@coorX, @coorY, @id, @date_request, @lang_id)";
                var parameters2 = new List<SqlParameter>();
                parameters2.Add(new SqlParameter("@coorX", coorX));
                parameters2.Add(new SqlParameter("@coorY", coorY));
                parameters2.Add(new SqlParameter("@id", Common.Constant.TEMP_GROUPCOMPONENT_ID));
                parameters2.Add(new SqlParameter("@date_request", date_request));
                parameters2.Add(new SqlParameter("@lang_id", lang_id));

                //string sql3 = $"select * from apom_identify_geotiff({coorX.ToString()}, {coorY.ToString()}, '{Common.Constant.RH_GROUPCOMPONENT_ID}', '{date_request}', '{lang_id}')";
                string sql3 = "select * from apom_identify_geotiff(@coorX, @coorY, @id, @date_request, @lang_id)";
                var parameters3 = new List<SqlParameter>();
                parameters3.Add(new SqlParameter("@coorX", coorX));
                parameters3.Add(new SqlParameter("@coorY", coorY));
                parameters3.Add(new SqlParameter("@id", Common.Constant.RH_GROUPCOMPONENT_ID));
                parameters3.Add(new SqlParameter("@date_request", date_request));
                parameters3.Add(new SqlParameter("@lang_id", lang_id));

                //Log.writeLog("identify_geotiff sql: " + sql1);
                //List<component_geotiff_daily_identify> lst = new List<component_geotiff_daily_identify>();

                List<component_geotiff_daily_identify> lstGroupComponent = await componentgeotiffdailyLocalServiceUtil.Instance.executeQueryAsync<component_geotiff_daily_identify>(sql1, parameters1.ToArray());

                #region temp - rh
                // temp
                List<component_geotiff_daily_identify> lstTemp = await componentgeotiffdailyLocalServiceUtil.Instance.executeQueryAsync<component_geotiff_daily_identify>(sql2, parameters2.ToArray());

                // rh
                List<component_geotiff_daily_identify> lstRH = await componentgeotiffdailyLocalServiceUtil.Instance.executeQueryAsync<component_geotiff_daily_identify>(sql3, parameters3.ToArray());
                #endregion

                component_geotiff_daily_identify objGroupComponent = lstGroupComponent.Count > 0 ? lstGroupComponent[0] : null;
                component_geotiff_daily_identify objTemp = lstTemp.Count > 0 ? lstTemp[0] : null;
                component_geotiff_daily_identify objRH = lstRH.Count > 0 ? lstRH[0] : null;

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(objGroupComponent));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new {
                        comps = objGroupComponent,
                        comps_temp = objTemp,
                        comps_rh = objRH
                    }
                });
            }
            catch (Exception ex)
            {
                Log.writeLog("ex: " + ex.ToString());
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// lấy thông tin danh sách dữ liệu pm25 từ mô hình wrf-chem hoặc mem với tọa độ {x, y}, thời gian yêu cầu lấy dữ liệu (ngày || giờ) cho trước, số ngày trước và sau thời gian yêu cầu
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/componentgeotiffdaily/identify_list_geotiff")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> getListByServiceInfo([FromBody]GeoTiffIdentifyParameters param)
        {
            try
            {
                //Log.writeLog("param.x: " + param.x);
                //Log.writeLog("param.y: " + param.y);
                double coorX = param.x; // Math.Ceiling(param.x);  //Convert.ToDouble(param.x.ToString());
                double coorY = param.y; // Math.Ceiling(param.y);  //Convert.ToDouble(param.y.ToString());
                //double coorX = Convert.ToDouble(param.x.ToString().Replace(",", "."));
                //double coorY = Convert.ToDouble(param.y.ToString().Replace(",", "."));
                string id = param.groupcomponent_id;
                string date_request = param.date_request;
                //int numdays = param.numdays;
                int prevDays = param.predays;
                int nextDays = param.nextdays;
                string lang_id = param.lang_id;

                List<component_geotiff_daily_identify> lst = await componentgeotiffdailyLocalServiceUtil.Instance.getListValByIdentifyAsync(coorX, coorY, id, date_request, prevDays, nextDays, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.writeLog("ex: " + ex.ToString());
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// lấy thông tin danh sách dữ liệu pm25 từ mô hình wrf-chem hoặc mem với tọa độ {x, y}, thời gian yêu cầu lấy dữ liệu (ngày || giờ) cho trước, số ngày trước và sau thời gian yêu cầu
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/componentgeotiffdaily/identify_province_list_geotiff")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> getListByServiceInfoProvince([FromBody] GeoTiffIdentifyParameters param)
        {
            try
            {
                double coorX = param.x;
                double coorY = param.y;
                string id = param.groupcomponent_id;
                string date_request = param.date_request;
                int prevDays = param.predays;
                int nextDays = param.nextdays;

                List<component_geotiff_daily_identify> lst = await componentgeotiffdailyLocalServiceUtil.Instance.getListValByIdentifyProvinceAsync(coorX, coorY, id, date_request, prevDays, nextDays);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.writeLog("ex: " + ex.ToString());
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }



        [HttpPost]
        [Route("api/componentgeotiffdaily/identify_province_id_list_geotiff")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> getListByServiceInfoProvinceId([FromBody] GeoTiffIdentifyProvinceParameters param)
        {
            try
            {
                string provinceId = param.province_id;
                string id = param.groupcomponent_id;
                string date_request = param.date_request;
                int prevDays = param.predays;
                int nextDays = param.nextdays;

                List<component_geotiff_daily_identify> lst = await componentgeotiffdailyLocalServiceUtil.Instance.getListValByIdentifyProvinceAsync(provinceId, id, date_request, prevDays, nextDays);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.writeLog("ex: " + ex.ToString());
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        [HttpPost]
        [Route("api/componentgeotiffdaily/identify_district_id_list_geotiff")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> getListByServiceInfoDistrictId([FromBody] GeoTiffIdentifyDistrictParameters param)
        {
            try
            {
                string districtId = param.district_id;
                string id = param.groupcomponent_id;
                string date_request = param.date_request;
                int prevDays = param.predays;
                int nextDays = param.nextdays;

                List<component_geotiff_daily_identify> lst = await componentgeotiffdailyLocalServiceUtil.Instance.getListValByIdentifyDistrictAsync(districtId, id, date_request, prevDays, nextDays);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.writeLog("ex: " + ex.ToString());
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// bảng xếp hạng aqi mới nhất từ mô hình mem cho tất cả các tỉnh/thành phố
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/componentgeotiffdaily/ranking")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> rankingData([FromBody]GeoTiffRankingParameters param)
        {
            try
            {
                string group_id = param.group_id;
                string component_id = param.component_id;
                string lang_id = param.lang_id;

                List<component_geotiff_daily_ranking> lst = await componentgeotiffdailyLocalServiceUtil.Instance.getRankingAdministrativeAsync(group_id, component_id, lang_id);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// bảng xếp hạng aqi theo ngày từ mô hình mem cho tất cả các tỉnh/thành phố
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/componentgeotiffdaily/rankingvn")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> rankingDataVN([FromBody] GeoTiffRankingParametersVN param)
        {
            try
            {
                string group_id = param.group_id;
                string component_id = param.component_id;
                string lang_id = param.lang_id;
                string date_shooting = param.date_shooting;
                string date_shooting_pre = param.date_shooting_pre;

                List<component_geotiff_daily_ranking> lst = await componentgeotiffdailyLocalServiceUtil.Instance.getRankingAdministrativeAsync(group_id, component_id, lang_id, date_shooting, date_shooting_pre);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// bảng xếp hạng aqi theo ngày từ mô hình mem cho tất cả các tỉnh/thành phố
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/componentgeotiffdaily/rankingprovince")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> rankingDataProvince([FromBody] GeoTiffRankingParametersProvince param)
        {
            try
            {
                string group_id = param.group_id;
                string component_id = param.component_id;
                string lang_id = param.lang_id;
                string province_id = param.province_id;
                string date_shooting = param.date_shooting;
                string date_shooting_pre = param.date_shooting_pre;

                List<component_geotiff_daily_ranking> lst = await componentgeotiffdailyLocalServiceUtil.Instance.getRankingAdministrativeAsync(group_id, component_id, lang_id, province_id, date_shooting, date_shooting_pre);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }
    }
}