﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Text;
using Newtonsoft.Json;

using Apom_API_Province.Common;
using Apom_API_Province.Models;
using Repositories.Models;
using Repositories.Models.Dashboard;
using Repositories.Services.Dashboard;

namespace Apom_API_Province.Controllers.APIs
{
    /// <summary>
    /// Phân tích dữ liệu
    /// </summary>
    public class analysisController : ApiController
    {
        /// <summary>
        /// số lượng quận/huyện trong 1 tỉnh/thành phố bị ô nhiễm và không ô nhiễm
        /// số ngày ô nhiễm trong 1 tỉnh/thành: pm25 > 50 
        /// quận/huyện có chỉ số aqi || pm25 thấp nhất
        /// quận/huyện có chỉ số aqi || pm25 cao nhất
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/analysis/district_statistic")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> districtStatisticData([FromBody]AnalysisParameters param)
        {
            try
            {
                string province_id = param.id;
                string from_date = param.from_date;
                string to_date = param.to_date;
                string component_id = param.component_id;
                string lang_id = param.lang_id;

                statistic_region_count_aqi obj = await statistic_region_aqiLocalServiceUtil.Instance.getDistrictCountAsync(province_id, from_date, to_date, component_id, lang_id);

                List<statistic_region_aqi> objDistrict = await statistic_region_aqiLocalServiceUtil.Instance.getDistrictMinMaxByAQIAsync(province_id, from_date, to_date, component_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new {
                        comps = obj,
                        minmax = objDistrict
                    }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// lấy danh sách dữ liệu aqi || pm25 trong 1 tỉnh/thành phố với khoảng thời gian cho trước
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/analysis/province_daily_statistic")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> dailyProvinceByAQI([FromBody]AnalysisParameters param)
        {
            try
            {
                string province_id = param.id;
                string from_date = param.from_date;
                string to_date = param.to_date;
                string component_id = param.component_id;
                string lang_id = param.lang_id;

                List<statistic_region_aqi_chart> lst = await statistic_region_aqiLocalServiceUtil.Instance.getDailyProvinceByAQIAsync(province_id, from_date, to_date, component_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new
                    {
                        comps = lst
                    }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// lấy danh sách dữ liệu aqi || pm25 trong 1 quận/huyện với khoảng thời gian cho trước
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/analysis/district_daily_statistic")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> dailyDistrictByAQI([FromBody]AnalysisParameters param)
        {
            try
            {
                string district_id = param.id;
                string from_date = param.from_date;
                string to_date = param.to_date;
                string component_id = param.component_id;
                string lang_id = param.lang_id;

                List<statistic_region_aqi_chart> lst = await statistic_region_aqiLocalServiceUtil.Instance.getDailyDistrictByAQIAsync(district_id, from_date, to_date, component_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new
                    {
                        comps = lst
                    }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// lấy danh sách dữ liệu trung bình aqi || pm25 của tất cả các quận/huyện trong 1 tỉnh/thành phố với khoảng thời gian cho trước
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/analysis/district_avg_statistic")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> districtAVGByProvinceAQI([FromBody]AnalysisParameters param)
        {
            try
            {
                string province_id = param.id;
                string from_date = param.from_date;
                string to_date = param.to_date;
                string component_id = param.component_id;
                string lang_id = param.lang_id;

                List<statistic_region_aqi> lst = await statistic_region_aqiLocalServiceUtil.Instance.getDistrictAvgByProvinceAQIAsync(province_id, from_date, to_date, component_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new
                    {
                        comps = lst
                    }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }
    }
}