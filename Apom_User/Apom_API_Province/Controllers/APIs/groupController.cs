﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Apom_API_Province.Common;
using Repositories.Models;
using Repositories.Models.Asset;
using Repositories.Services.Asset;

namespace Apom_API_Province.Controllers.APIs
{
    /// <summary>
    /// Thông tin trạm quan trắc
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class groupController : ApiController
    {
        /// <summary>
        /// lấy thông tin của 1 trạm đo
        /// </summary>
        /// <param name="group_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/group/info")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getDailyShort(string group_id, string lang_id)
        {
            try
            {
                group_short obj = await groupLocalServiceUtil.Instance.infoGroupAsync(group_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                if (obj != null)
                {
                    response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                }
                return response;
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// lấy thông tin của 1 trạm đo
        /// </summary>
        /// <param name="group_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/group/info_detail")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getDailyÌnoDetail(string group_id, string lang_id)
        {
            try
            {
                group_short obj = await groupLocalServiceUtil.Instance.infoGroupAsync(group_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                if (obj != null)
                {
                    response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                }
                return response;
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}