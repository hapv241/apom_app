﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Apom_API_Province.Common;
using Apom_API_Province.Models;
using Repositories.Models;
using Repositories.Models.Asset;
using Repositories.Services.Asset;

namespace Apom_API_Province.Controllers.APIs
{
    /// <summary>
    /// Làm việc với các trạm đo, trong các trạm đo sẽ có các chất (component), như: pm25, no, aqi...
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class groupcomponentController : ApiController
    {
        /// <summary>
        /// tìm tất cả các chất (component) mà trong 1 trạm đo (group_id) cung cấp
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/groupcomp/compbygroup")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> getComponentByGroupShort([FromBody]Parameters param)
        //public List<component_station_daily_aqi> getDailyShort(string group_id, string comp_id, int numdays)
        {
            try
            {
                string group_id = param.group_id;
                List<group_component_short> lst = await groupcomponentLocalServiceUtil.Instance.getComponentFromGroupAsync(group_id);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }
    }
}