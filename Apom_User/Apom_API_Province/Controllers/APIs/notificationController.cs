﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Apom_API_Province.Common;
using Repositories.Models.Notification;

namespace Apom_API_Province.Controllers.APIs
{
    public class notificationController : ApiController
    {
        [HttpPost]
        [Route("api/notification/sendnotification")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> SendNotification(notification obj)
        {
            try
            {
                //NotificationHub objNotifHub = new NotificationHub();
                //Notification objNotif = new Notification();
                //objNotif.SentTo = obj.UserID;

                //context.Configuration.ProxyCreationEnabled = false;
                //context.Notifications.Add(objNotif);
                //context.SaveChanges();

                //objNotifHub.SendNotification(objNotif.SentTo);

                //var query = (from t in context.Notifications  
                //             select t).ToList();  

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}