﻿using Apom_API.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Apom_API.Controllers.APIs
{
    /// <summary>
    /// base api class
    /// </summary>
    public class BaseRepositoryController : ApiController
    {
        protected int page_num = 1;
        protected int page_size = Constant.PAGE_SIZE == 0 ? 10 : Constant.PAGE_SIZE;
        protected string user_id { get; set; }
        protected string user_name { get; set; }
        protected string displayname { get; set; }
        protected string email_address { get; set; }

        public void getUserInfo()
        {
            string accessToken = HttpContext.Current.Request.Headers.GetValues(Constant.ACCESS_TOKEN).First();
            ClaimsIdentity tokenIdetity = TokenManager.GetTokenInfo(accessToken);
            string user_id = tokenIdetity.FindFirst(Constant.CLAIM_TYPES_UID).Value;
            string displayname = tokenIdetity.Name;
            string user_name = tokenIdetity.Claims.Where(c => c.Type.Contains("emailaddress")).FirstOrDefault().Value.ToString();
            string emailAddress = tokenIdetity.FindFirst(ClaimTypes.Email).Value;
            this.user_id = user_id;
            this.displayname = displayname;
            this.user_name = user_name;
            this.email_address = emailAddress;
        }
    }
}