﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Apom_API.Common;
using Repositories.Models;
using Repositories.Models.Asset;
using Repositories.Services.Asset;

namespace Apom_API.Controllers.APIs
{
    /// <summary>
    /// Làm việc với trạm đo
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class stationController : ApiController
    {
        /// <summary>
        /// lấy thông tin trạm đo và giá trị pm25 || aqi với thời gian moi nhat
        /// </summary>
        /// <param name="group_id">mã trạm</param>
        /// <param name="component_id">mã chất</param>
        /// <param name="lang_id">ngôn ngữ</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/station/daily_short_newest")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getDailyShortNewest(string group_id, string component_id, string lang_id)
        {
            try
            {
                component_station_daily_short obj = await componentLocalServiceUtil.Instance.getDailyShortNewestAsync(group_id, component_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// lấy thông tin trạm đo và giá trị pm25 || aqi với thời gian cho trước
        /// </summary>
        /// <param name="group_id">mã trạm</param>
        /// <param name="component_id">mã chất</param>
        /// <param name="datetime_shooting">thời gian yêu cầu lấy dữ liệu</param>
        /// <param name="lang_id">ngôn ngữ</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/station/daily_short")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getDailyShort(string group_id, string component_id, string datetime_shooting, string lang_id)
        {
            try
            {
                component_station_daily_short obj = await componentLocalServiceUtil.Instance.getDailyShortAsync(group_id, component_id, datetime_shooting, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}