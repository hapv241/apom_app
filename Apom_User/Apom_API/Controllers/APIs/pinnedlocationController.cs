﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Apom_API.Common;
using Apom_API.Models;
using Repositories.Models;
using Repositories.Models.Administrative;
using Repositories.Services.Administrative;
using Repositories.Models.Config;
using Repositories.Services.Config;

namespace Apom_API.Controllers.APIs
{
    /// <summary>
    /// Quản lý ghim tỉnh/thành phố lên trang chủ theo AQI_PM25
    /// </summary>
    public class pinnedlocationController : ApiController
    {
        /// <summary>
        /// ghim tỉnh/thành phố lên trang chủ theo AQI_PM25. Khi chưa đăng nhập: lấy mặc định từ hệ thống, nếu đã đăng nhập: lấy các tỉnh/thành phố mà người dùng chọn
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/pinnedlocation/default")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> defaultPinnedLocation([FromBody] PinnedLocationParameters param)
        {
            try
            {
                string groupId = param.group_id;
                string componentId = param.component_id;
                string langId = param.lang_id;
                string userId = param.user_id;
                bool isProvince = param.is_province;

                List<pinned_location> lstPinnedLocationDefault = await cf_pinned_locationLocalServiceUtil.Instance.getPinnedLocationDefaultAsync(groupId, componentId, langId, userId, isProvince);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lstPinnedLocationDefault));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lstPinnedLocationDefault }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// cập nhật / gỡ bỏ ghim tỉnh/thành phố lên trang chủ theo AQI_PM25
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>        
        [HttpPost]
        [Route("api/pinnedlocation/update")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> updatePinnedLocation([FromBody] PinnedLocationParameters param)
        {
            try
            {
                string provinceId = param.province_id;
                string userId = param.user_id;
                bool isAdd = param.is_add;
                bool isProvince = param.is_province;

                // check isExist user
                if (String.IsNullOrEmpty(userId))
                {
                    return Ok(new jsonResponse
                    {
                        Code = HttpStatusCode.Unauthorized,
                        Data = null
                    });
                }

                user u = await userLocalServiceUtil.Instance.getUserAsync(userId);
                if (u == null)
                {
                    return Ok(new jsonResponse
                    {
                        Code = HttpStatusCode.Unauthorized,
                        Data = null
                    });
                }

                // check over limit no. change pinned location in a day of user
                Constant.UserEvent ue = Constant.USER_EVENT.Find(x => x.user_id.Equals(userId));
                bool isAllowUpdate = false;
                if (ue == null)
                {
                    ue = new Constant.UserEvent(userId);
                    ue.num_pinnedlocation_in_a_day = 1;
                    ue.date_change_pinnedlocation_in_a_day = DateTime.Now;
                    Constant.USER_EVENT.Add(ue);
                    isAllowUpdate = true;
                }
                else
                {
                    if (!ue.checkOverLimit())
                    {
                        if (!ue.checkOverDate())
                            ue.num_pinnedlocation_in_a_day++;
                        else
                        {
                            ue.num_pinnedlocation_in_a_day = 0;
                            ue.date_change_pinnedlocation_in_a_day = DateTime.Now;
                        }
                        isAllowUpdate = true;
                    }
                }

                if (!isAllowUpdate)
                {
                    return Ok(new jsonResponse
                    {
                        Code = HttpStatusCode.Forbidden,
                        Data = null
                    });
                }
                // update to database
                bool result = await cf_pinned_locationLocalServiceUtil.Instance.updatePinnedLocationAsync(provinceId, userId, isAdd, isProvince);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent("ok");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = HttpStatusCode.OK
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }
    }
}