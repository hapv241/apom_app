﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Text;
using Newtonsoft.Json;

using Apom_API.Common;
using Apom_API.Models;
using Repositories.Models;
using Repositories.Models.Asset;
using Repositories.Services.Asset;

namespace Apom_API.Controllers.APIs
{
    /// <summary>
    /// Làm việc với các dữ liệu lấy từ dịch vụ bản đồ
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class componentgeotiffserviceController : ApiController
    {
        /// <summary>
        /// lấy thông tin từ dịch vụ bản đồ để xác định loại dịch vụ này chạy theo giờ (wrf-chem) hay theo ngày (mem) và mã groupcomponent_id từ CSDL để thực hiện lấy dữ liệu ở các bước tiếp theo
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/componentgeotiffservice/getbyserviceinfo")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> getByServiceInfo([FromBody]GeoTiffParameters param)
        {
            try
            {
                string url = param.url;
                string featurebase = param.featurebase;
                string featuretype = param.featuretype;

                component_geotiff_service obj = await componentgeotiffserviceLocalServiceUtil.Instance.getByServiceInfoAsync(url, featurebase, featuretype);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = obj }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }
    }
}