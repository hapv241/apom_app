﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Apom_API.Common;
using Apom_API.Models;
using Repositories.Models;
using Repositories.Models.Asset;
using Repositories.Services.Asset;

namespace Apom_API.Controllers.APIs
{
    /// <summary>
    /// Làm việc với dữ liệu trạm
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class componentstationController : ApiController
    {
        /// <summary>
        /// lấy dữ liệu trạm của aqi || pm25 với thời gian yêu cầu  cho trước, số ngày trước và sau thời gian yêu cầu
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/compstation/daily")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> getDailyShort([FromBody]Parameters param)
        {
            try
            {
                string group_id = param.group_id;
                string comp_id = param.comp_id;
                string date_request = param.date_request;
                int prevDays = param.predays;
                int nextDays = param.nextdays;
                string lang_id = param.lang_id;
                List<component_station_daily_aqi> lst = await componentstationdailyLocalServiceUtil.Instance.getDailyAQIByCompAsync(group_id, comp_id, date_request, prevDays, nextDays, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { daily_aqi = lst }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }
    }
}