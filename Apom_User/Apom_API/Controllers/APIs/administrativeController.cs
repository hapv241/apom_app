﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using System.Text;
//using System.Web.Http.Cors;
using Newtonsoft.Json;

using Common;
using Apom_API.Common;
using Apom_API.Models;
using Repositories.Models;
using Repositories.Models.Administrative;
using Repositories.Models.Category;
using Repositories.Models.Notification;
using Repositories.Services.Administrative;
using Repositories.Services.Category;
using Repositories.Services.Notification;

namespace Apom_API.Controllers.APIs
{
    //[EnableCors(origins: "*",headers: "*", methods: "*")]
    /// <summary>
    /// Làm việc với đối tượng là: quản trị & địa phận hành chính: tỉnh/thành phố || quận/huyện
    /// </summary>
    public class administrativeController : ApiController
    {
        /// <summary>
        /// xác định tỉnh/thành phố và các quận/huyện nằm trong 1 tỉnh/thành phố
        /// </summary>
        /// <param name="province_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/administrative/administrative_province_district")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getAdministrativeByProvinceId(string province_id, string lang_id)
        {
            try
            {
                List<administrative_base> lst = await provinceLocalServiceUtil.Instance.getAdministrativeByProvinceIdAsync(province_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// xác định các quận/huyện nằm trong 1 tỉnh/thành phố
        /// </summary>
        /// <param name="province_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/administrative/districts")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getDistricts(string province_id, string lang_id)
        {
            try
            {
                List<district_short> lst = await districtLocalServiceUtil.Instance.getDistrictsByProvinceAsync(province_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// xác định boundary của 1 tỉnh/thành phố (min_x, min_y, max_x, max_y)
        /// </summary>
        /// <param name="province_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/administrative/province_extent")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getExtentProvince(string province_id, string lang_id)
        {
            try
            {
                extent obj = await provinceLocalServiceUtil.Instance.getExtentByProvinceIdAsync(province_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }


        /// <summary>
        /// xác định boundary của 1 quận/huyện (min_x, min_y, max_x, max_y)
        /// </summary>
        /// <param name="district_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/administrative/district_extent")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getExtentDistrict(string district_id, string lang_id)
        {
            try
            {
                extent obj = await districtLocalServiceUtil.Instance.getExtentByDistrictIdAsync(district_id, lang_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// lấy thông tin đơn vị hành chính từ (lat, long)
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/administrative/info_by_coor")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> getinfoByCoor(double latitude, double longitude, string lang_id)
        {
            try
            {
                administrative_info obj = await statisticDataLocalServiceUtil.Instance.getInfoAdministrativeByCoor(latitude, longitude, lang_id);
                Common.Log.writeLog("obj: " + obj.province_id);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(obj));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return response;
            }
            catch (Exception ex)
            {
                Common.Log.writeLog("getinfoByCoor error: " + ex.ToString());
                Log.LOG4NET.Error(ex.ToString(), ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// thống kê dữ liệu aqi || pm25 của 1 quận/huyện trong khoảng thời gian
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/administrative/statistic")]
        [ResponseType(typeof(IEnumerable<object>))]
        public async Task<IHttpActionResult> statisticData([FromBody]GeoTiffStatisticParameters param)
        {
            try
            {
                string province_id = param.province_id;
                string district_id = param.district_id;
                string from_day = param.fromday;
                string to_day = param.today;
                string type_product = param.type_product;
                string lang_id = param.lang_id;

                List<statistic_data> lst = await statisticDataLocalServiceUtil.Instance.analysisDataAsync(province_id, district_id, from_day, to_day, type_product, lang_id);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = new { comps = lst }
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// lấy thông tin tham số hệ thống chung của hệ thống APOM
        /// </summary>
        /// <returns></returns>
        [TokenAdminValidation]
        [HttpPost]
        public async Task<IHttpActionResult> getGeneralSetting()
        {
            try
            {
                List<general_setting> lst = await generalSettingLocalServiceUtil.Instance.getAllAsync();
                general_setting _general_setting = lst.Count > 0 ? lst[0] : new general_setting();

                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = _general_setting
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// Gửi email tới người dùng có những tỉnh/thành phố mà họ quan tâm bị vượt ngưỡng trong ngày yêu cầu
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// 
        [TokenAdminValidation]
        [HttpPost]
        [Route("api/administrative/send_email_over_threshold_mem_aqi")]
        public async Task<IHttpActionResult> sendEmailOverThresholdAQI([FromBody] BaseParameters param)
        {
            try
            {
                List<notif_over_threshold_province_user_result> lstResult = new List<notif_over_threshold_province_user_result>();
                List<notif_over_threshold_province_user_result> lstResult_Error = new List<notif_over_threshold_province_user_result>();
                string dateRequest = param.date_request;
                string langId = String.IsNullOrEmpty(param.lang_id) ? Repositories.Constant.LANGUAGE_DEFAULT : param.lang_id;
                int valOverThreshold = 50;
                List<notif_over_threshold_province_user> lst = await notificationuserLocalServiceUtil.Instance.getUserProvince_OverThresholdAsync(dateRequest, langId, valOverThreshold);

                List<general_setting> lstGenealSetting = await generalSettingLocalServiceUtil.Instance.getAllAsync();
                general_setting _general_setting = lstGenealSetting.Count > 0 ? lstGenealSetting[0] : new general_setting();
                string email_server_pop = _general_setting.email_server_pop;
                string email_address = _general_setting.email_address;
                string email_password = _general_setting.email_password;
                int port = _general_setting.email_gate == null ? 587 : _general_setting.email_gate.Value;
                bool enableSsl = _general_setting.ssl == null ? true : _general_setting.ssl.Value;

                string userId = "";
                string user_name = "";
                string user_email = "";
                string provinceId = "", provinceName = "";
                decimal valOverThreshold_Province = 0;
                string contentInit = "<style>";
                contentInit += "table, th, td {";
                contentInit += "border: 1px solid black;";
                contentInit += "border-collapse: collapse;";
                contentInit += "}";
                contentInit += "</style><table>";
                contentInit += "<thead>";
                contentInit += "<tr>";
                contentInit += $"<th>{Resources.Administrator.notif_over_threshold_region}</th>";
                contentInit += $"<th>{Resources.Administrator.notif_over_threshold_val}</th>";
                contentInit += "</tr>";
                contentInit += "</thead>";
                contentInit += "<tbody>";
                string content = "";
                for (int i = 0; i < lst.Count; i++)
                {
                    notif_over_threshold_province_user item = lst[i];
                    string userIdCurrent = item.user_id;
                    if(userId != userIdCurrent)
                    {
                        if(!String.IsNullOrEmpty(userId))
                        {
                            content = contentInit + content;
                            content += "</tbody>";
                            content += "</table>";

                            bool isSendOK = false;
                            #region send email
                            string subject = String.Concat("[", Resources.Administrator.sys_name, "]: ", Resources.Administrator.notif_over_threshold_title);
                            subject = subject.Replace("{0}", dateRequest);

                            string body = Resources.Administrator.notif_over_threshold;
                            body = body.Replace("{0}", user_name);
                            body = body.Replace("{1}", Resources.Administrator.sys_name);
                            body = body.Replace("{2}", valOverThreshold.ToString());
                            body = body.Replace("{3}", dateRequest);
                            body = body.Replace("{4}", content);

                            Email.EmailModel emailModel = new Email.EmailModel
                            {
                                smtp = email_server_pop,
                                port = port,
                                mail_from = email_address,
                                mail_to = user_email,
                                credentials_user = email_address,
                                credentials_password = Libs.Utils.Encrypts.Decrypt(String.Concat(email_address, "_", Constant.KEYENDE), email_password),
                                subject = subject,
                                body = body,
                                enableSsl = enableSsl,
                                bodyIsHTML = true
                            };
                            #endregion
                            try
                            {
                                isSendOK = await Email.SendMail(emailModel);
                                if (isSendOK)
                                {
                                    lstResult.Add(new notif_over_threshold_province_user_result
                                    {
                                        user_id = userId,
                                        user_name = user_name,
                                        email_address = user_email,
                                        is_ok = isSendOK
                                    });
                                }
                                else
                                {
                                    lstResult_Error.Add(new notif_over_threshold_province_user_result
                                    {
                                        user_id = userId,
                                        user_name = user_name,
                                        email_address = user_email,
                                        is_ok = false
                                    });
                                }
                            }
                            catch(Exception ex)
                            {
                                Log.LOG4NET.Error(ex.ToString(), ex);
                                lstResult_Error.Add(new notif_over_threshold_province_user_result
                                {
                                    user_id = userId,
                                    user_name = user_name,
                                    email_address = user_email,
                                    is_ok = false
                                });
                            }

                            content = "";
                        }
                        userId = userIdCurrent;
                        user_name = item.user_name;
                        user_email = item.email_address;
                    }
                    provinceId = item.province_id;
                    provinceName = item.province_name;
                    valOverThreshold_Province = Math.Round(item.aqi, 2);
                    content += "<tr>";
                    content += $"<td>{provinceName}</td>";
                    content += $"<td>{valOverThreshold_Province}</td>";
                    content += "</tr>";
                }
                
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.OK,
                    Message = "",
                    Data = lstResult,
                    Error = lstResult_Error
                });
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error(ex.ToString(), ex);
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null,
                    Error = null
                });
            }
        }
    }
}