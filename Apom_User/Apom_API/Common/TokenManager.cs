﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace Apom_API.Common
{
    public class TokenManager
    {
        private static string Secret = Common.Constant.SECRET_TOKEN;

        public static string GenerateToken(string user_id, string displayname, string emailaddress, string pwd, string language)
        {
            byte[] key = Convert.FromBase64String(Secret);
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);
            int expire_token = Common.Constant.EXPIRE_TOKEN;
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Sid, Convert.ToString(user_id)),
                new Claim(ClaimTypes.Name, displayname),
                new Claim(ClaimTypes.Email, emailaddress),
                new Claim(Constant.CLAIM_TYPES_UID, user_id),
                new Claim(Constant.CLAIM_TYPES_PWD, pwd),
                new Claim(Constant.CLAIM_TYPES_LANG, language==null?"vi":language)
            };

            ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims);

            SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
            {
                Subject = oAuthIdentity,
                Expires = DateTime.UtcNow.AddDays(expire_token),
                //Expires = DateTime.UtcNow.AddSeconds(30),
                SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);

            string tokenKey = handler.WriteToken(token);
            return tokenKey;
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                JwtSecurityToken jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
                if (jwtToken == null) return null;
                if (jwtToken.ValidTo < DateTime.UtcNow) return null;
                byte[] key = Convert.FromBase64String(Secret);
                TokenValidationParameters parameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(key)
                };
                SecurityToken securityToken;
                ClaimsPrincipal principal = tokenHandler.ValidateToken(token, parameters, out securityToken);
                return principal;
            }
            catch
            {
                return null;
            }
        }

        public static ClaimsIdentity GetTokenInfo(string token)
        {
            string username = null;
            ClaimsPrincipal principal = GetPrincipal(token);
            if (principal == null)
                return null;
            ClaimsIdentity identity = null;
            try
            {
                identity = (ClaimsIdentity)principal.Identity;
            }
            catch (NullReferenceException)
            {
                return null;
            }
            Claim usernameClaim = identity.FindFirst(ClaimTypes.Name);
            username = usernameClaim.Value;
            return identity;
        }
    }
}