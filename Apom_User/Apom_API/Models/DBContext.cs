﻿using System;
using System.Data.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

using Repositories.Models.Asset;
using Repositories.Models.Config;

namespace Apom_API.Models
{
    public class DataApomAppConnection : DbContext
    {
        public DataApomAppConnection() : base("DataApomAppConnection")
        {
        }
        public DbSet<component> Component { get; set; }
        public DbSet<component_station_daily> ComponentStationDaily { get; set; }
        public DbSet<group> Group { get; set; }
        public DbSet<group_component> GroupComponent { get; set; }
        public DbSet<cf_pinned_location> PinnedLocation { get; set; }
    }
}