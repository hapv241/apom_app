﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Libs.Utils;

using Repositories.Models.Administrative;

namespace Apom_User.Common
{
    public class SessionContext
    {
        #region clear user when logout
        public static void ClearLogout()
        {
            Type t = typeof(Constant.SessionKey);
            FieldInfo[] fields = t.GetFields(BindingFlags.Static | BindingFlags.Public);

            foreach (FieldInfo fi in fields)
            {
                SessionHelper.RemoveSession(fi.GetValue(null).ToString());
            }

            //SessionContext.Language = "vi";

        }
        #endregion

        #region admin
        public static string Language
        {
            get { return SessionHelper.GetValue<string>(Constant.SessionKey.SESSION_LANGUAGE); }
            set { SessionHelper.SetValue(Constant.SessionKey.SESSION_LANGUAGE, value); }
        }
        public static general_setting GeneralSetting
        {
            get { return SessionHelper.GetValue<general_setting>(Constant.SessionKey.SESSION_GENERAL_SETTING); }
            set { SessionHelper.SetValue(Constant.SessionKey.SESSION_GENERAL_SETTING, value); }
        }

        // Current User
        public static user UserProfile
        {
            get { return SessionHelper.GetValue<user>(Constant.SessionKey.SESSION_USER); }
            set { SessionHelper.SetValue(Constant.SessionKey.SESSION_USER, value); }
        }

        // Current User
        public static string Theme
        {
            get { return SessionHelper.GetValue<string>(Constant.SessionKey.THEME_APOM); }
            set { SessionHelper.SetValue(Constant.SessionKey.THEME_APOM, value); }
        }

        // Clear session
        public static void ClearAllSession()
        {
            SessionHelper.ResetSession();
        }
        #endregion
    }
}