﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using System.Threading;
using System.Globalization;
using System.Web;
using System.Security.Claims;
using System.Linq;

namespace Apom_User.Common.ActionFilter
{
    public class CustomAuthenticationFilter : ActionFilterAttribute, IAuthenticationFilter
    {
        public ActionResult HttpUnAuthenResult()
        {
            return null;
        }

        public void OnAuthentication(AuthenticationContext filterContext)
        {
            string controller = filterContext.RouteData.Values["controller"].ToString();
            string action = filterContext.RouteData.Values["action"].ToString();
            if (controller.Equals("account", StringComparison.OrdinalIgnoreCase) && action.Equals("login", StringComparison.OrdinalIgnoreCase))
            {
            }
            else
            {
                var cookies = HttpContext.Current.Response.Cookies.AllKeys.Contains(Constant.WebConfig.COOKIE_LOGIN) ? HttpContext.Current.Response.Cookies[Constant.WebConfig.COOKIE_LOGIN] : null;
                var _language = HttpContext.Current.Response.Cookies[Common.Constant.WebConfig.LANGUAGE] != null ? HttpContext.Current.Response.Cookies[Common.Constant.WebConfig.LANGUAGE].Value : "";
                if (SessionContext.UserProfile == null && cookies != null)
                {
                    var email = Libs.Utils.Encrypts.Decrypt(Constant.WebConfig.KEYENDE, cookies.Values["paraI"]);
                    var is_check = cookies.Values["paraII"];
                    if (is_check == "1")
                    {
                        SessionContext.UserProfile = Repositories.Services.Administrative.userLocalServiceUtil.Instance.getUserbyEmailPwd(email, "", true);
                        SessionContext.Language = string.IsNullOrEmpty(_language) ? "vi" : _language;
                    }
                }

                //if (filterContext.HttpContext.Session[Constant.SessionKey.SESSION_USER] == null)
                if (SessionContext.UserProfile == null)
                {
                    filterContext.Result = new HttpUnauthorizedResult();
                }
                else
                {
                }
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                //check ajax request
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    string message = Resources.Administrator.ResourceManager.GetString("session_expired", CultureInfo.GetCultureInfo("vi"));
                    ActionResult result = new JsonResult
                    {
                        Data = new { Code = Repositories.HttpStatusCode.BAD_REQUEST, Message = message },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    filterContext.Result = result;
                }
                else
                {
                    string controller = filterContext.RouteData.Values["controller"].ToString();
                    string action = filterContext.RouteData.Values["action"].ToString();
                    string url = String.Concat(@"/", controller, @"/", action);
                    //Redirecting the user to the Login View of Account Controller  
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            { "controller", "Account" },
                            { "action", "Login" },
                            { "area", "" },
                            {"returnUrl", url }
                        }
                    );
                }
            }
        }
    }
}