﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;

using Libs.Utils;

namespace Apom_User.Common.ActionFilter
{
    public class CustomIdentityActionFilter: CustomActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            HttpContextBase httpContext = filterContext.Controller.ControllerContext.RequestContext.HttpContext;

            string paraI = httpContext.Request["paraI"] == null ? httpContext.Request.Form["paraI"] : httpContext.Request["paraI"];
            string time_request = httpContext.Request["t"] == null ? httpContext.Request.Form["t"] : httpContext.Request["t"];
            string email = "";
            try
            {
                email = Encrypts.Decrypt(String.Concat(time_request, "_", Constant.WebConfig.KEYENDE), paraI);
                if(!email.Equals(SessionContext.UserProfile.emailaddress))
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                    { "controller", "Bug" },
                    { "action", "Error403" },
                    { "area", "" }
                        }
                    );
                }
            }
            catch(Exception ex)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                    { "controller", "Bug" },
                    { "action", "Error403" },
                    { "area", "" }
                    }
                );
            }
        }
    }
}