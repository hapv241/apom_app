﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;

using Apom_User.Common;

namespace Apom_User.Common.ActionFilter
{
    public class CustomActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                //check ajax request
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    string message = Resources.Common.ResourceManager.GetString("session_expired", CultureInfo.GetCultureInfo("vi"));
                    ActionResult result = new JsonResult
                    {
                        Data = new { Code = EnumSystem.JsonResponseCode.SessionTimeout, Message = message },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    filterContext.Result = result;
                }
                else
                {
                    //Redirecting the user to the Login View of Account Controller  
                    filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                     { "controller", "Account" },
                     { "action", "Login" },
                     { "area", "" }
                    });
                }
            }
        }
    }
}