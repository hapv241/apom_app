﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apom_User.Common
{
    public class Log
    {
        public static readonly log4net.ILog LOG4NET = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }

    //public class Logger
    //{
    //    private readonly string _logFilePath;
    //    private static readonly object _lock = new object();

    //    public Logger(string logFilePath)
    //    {
    //        //_logFilePath = String.Concat(@"C:\", logFilePath);
    //        string logDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Logs");
    //        if (!Directory.Exists(logDirectory))
    //        {
    //            Directory.CreateDirectory(logDirectory);
    //        }

    //        _logFilePath = Path.Combine(logDirectory, "application.log");
    //    }

    //    public void Log(string message, string logLevel = "INFO")
    //    {
    //        string logMessage = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss} [{logLevel}] {message}";
    //        WriteToFile(logMessage);
    //    }

    //    public void Info(string message)
    //    {
    //        Log(message, "INFO");
    //    }

    //    public void Warning(string message)
    //    {
    //        Log(message, "WARNING");
    //    }

    //    public void Error(string message)
    //    {
    //        Log(message, "ERROR");
    //    }

    //    private void WriteToFile(string message)
    //    {
    //        lock (_lock)
    //        {
    //            try
    //            {
    //                File.AppendAllText(_logFilePath, message + Environment.NewLine);
    //            }
    //            catch (Exception ex)
    //            {
    //                Console.WriteLine($"Failed to write to log file: {ex.Message}");
    //            }
    //        }
    //    }
    //}
}