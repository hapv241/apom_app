﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.ComponentModel;

namespace Apom_User.Common
{
    public class EnumSystem
    {
        public enum JsonResponseCode
        {
            [Description("Thành công")]
            OK = 0,
            [Description("Không thành công")]
            NotGood = 1,
            [Description("Vui lòng kiểm tra lại thông tin bạn đã nhập")]
            Validation = 2,
            [Description("Nhập thiếu thông tin")]
            Miss = 3,
            [Description("Mất phiên làm việc")]
            LostSession = 4,
            [Description("Không tìm thấy bản ghi bạn yêu cầu")]
            NotFound = 5,
            [Description("Bạn không có quyền thực hiện thao tác này")]
            NotPermission = 6,
            [Description("Bản ghi này đã tồn tại trong hệ thống")]
            ExistingRecord = 7,
            [Description("Dữ liệu không chính xác")]
            DataIncorrect = 8,
            [Description("Undefined error. Please check with administrator")]
            GeneralError = -1,
            [Description("Session timeout")]
            SessionTimeout = -2,
        }
    }
}