﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Globalization;

using Newtonsoft.Json;

using Apom_User.Common;

namespace Apom_User.Controllers
{
    public class BaseController : Controller
    {
        public void Language()
        {
            string culture = Common.Constant.LANGUAGE_DEFAULT;
            var _language = Request.Cookies[Common.Constant.WebConfig.LANGUAGE] != null ? Request.Cookies[Common.Constant.WebConfig.LANGUAGE].Value : "";
            if (!string.IsNullOrEmpty(_language))
            {
                culture = _language;
            }
            SessionContext.Language = culture;
            // create cookies language
            var languageCookie = new HttpCookie(Common.Constant.WebConfig.LANGUAGE, culture);
            languageCookie.Expires = DateTime.Now.AddDays(Constant.WebConfig.COOKIE_TIMEOUT);
            HttpContext.Response.Cookies.Add(languageCookie);

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }

        public BaseController()
        {
            var container = "v" + Guid.NewGuid().ToString();
            ViewBag.container = container;

            ViewBag.ProvinceDefault = Constant.WebConfig.PROVINCE_DEFAULT;
            ViewBag.HostAPI = Constant.WebConfig.HOST_API;
            ViewBag.HOSTGSV = Constant.WebConfig.HOST_GEOSERVER;
            ViewBag.PageNum = 1;
            ViewBag.PageSize = Constant.WebConfig.NUMBER_IN_PAGE;
            ViewBag.LayoutLogin = "~/Views/Shared/stack-admin/_Layout_stack_login.cshtml";
            // layout map
            ViewBag.Layout = "~/Views/Shared/stack-admin/_Layout_stack.cshtml";
            // layout map
            ViewBag.LayoutNew = "~/Views/Shared/stack-admin/_Layout_stack_new.cshtml";
            // layout other page
            ViewBag.LayoutNewOther = "~/Views/Shared/stack-admin/_Layout_stack_new_other.cshtml";
            // layout other page
            ViewBag.LayoutOtherPage = "~/Views/Shared/stack-admin/_Layout_stack_other.cshtml";
            // layout map mobile
            ViewBag.LayoutNewMobile = "~/Views/Shared/stack-admin/_Layout_stack_new_mobile.cshtml";
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Language();

            #region get account from cookie

            var _language = Request.Cookies[Common.Constant.WebConfig.LANGUAGE] != null ? Request.Cookies[Common.Constant.WebConfig.LANGUAGE].Value : "";
            var cookies = Request.Cookies[Constant.WebConfig.COOKIE_LOGIN] != null ? Request.Cookies[Constant.WebConfig.COOKIE_LOGIN] : null;

            if (SessionContext.UserProfile == null && cookies != null)
            {
                var _email = Libs.Utils.Encrypts.Decrypt(Constant.WebConfig.KEYENDE, cookies.Values["paraI"]);
                var is_check_remember = cookies.Values["paraII"];
                var is_call_logout = cookies.Values["paraIII"];
                if (is_check_remember == "1" && is_call_logout == "0")
                {
                    SessionContext.UserProfile = Repositories.Services.Administrative.userLocalServiceUtil.Instance.getUserbyEmailPwd(_email, "", true);
                    SessionContext.Language = string.IsNullOrEmpty(_language) ? "vi" : _language;
                }
            }
            #endregion

            base.OnResultExecuting(filterContext);
        }

        protected ActionResult ToJson(object model)
        {
            var json = JsonConvert.SerializeObject(model);
            return Content(json, "application/json");
        }
    }
}