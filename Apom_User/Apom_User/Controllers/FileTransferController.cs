﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Repositories.Models.Archive;
using Repositories.Services.Archive;

namespace Apom_User.Controllers
{
    public class FileTransferController : Controller
    {
        #region preview file
        public ActionResult GetFile(string file_id)
        {
            try
            {
                dlfileentry fileEntry = dlfileentryLocalServiceUtil.Instance.getFileEntry(file_id);
                if (fileEntry == null) return null;
                
                byte[] bytes = dlfileentryLocalServiceUtil.Instance.getByte(file_id);
                

                return File(bytes, fileEntry.mimetype, fileEntry.title);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}