﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apom_User.Controllers
{
    public class BugController : BaseController
    {
        // GET: Bug
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Error400()
        {
            return View();
        }
        public ActionResult Error401()
        {
            return View();
        }
        public ActionResult Error403()
        {
            return View();
        }
        public ActionResult Error404()
        {
            return View();
        }
        public ActionResult Error500()
        {
            return View();
        }

        public ActionResult Maintenance()
        {
            return View();
        }
    }
}