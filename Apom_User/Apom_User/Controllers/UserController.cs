﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;

using Repositories.Models;
using Repositories.Models.Administrative;
using Repositories.Models.Archive;
using Repositories.Services.Administrative;
using Repositories.Services.Archive;

using Apom_User.Common;
using Common;
using System.Threading.Tasks;

namespace Apom_User.Controllers
{
    public class UserController : BaseAuthenController
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> UserInfo(string paraI, string time_request = null)
        {
            try
            {
                ViewBag.page = Common.Constant.Page.user_profile;
                long timeRegister = time_request == null ? SessionContext.UserProfile.time_request.Value : Convert.ToInt64(time_request);
                // convert parameter
                string emailAddress = "";
                try
                {
                    emailAddress = Libs.Utils.Encrypts.Decrypt(String.Concat(timeRegister, "_", Constant.WebConfig.KEYENDE), paraI);
                    if(time_request == null) SessionContext.UserProfile.time_request_old = timeRegister;
                }
                catch(Exception ex)
                {
                    timeRegister = SessionContext.UserProfile.time_request_old.Value;
                    emailAddress = Libs.Utils.Encrypts.Decrypt(String.Concat(timeRegister, "_", Constant.WebConfig.KEYENDE), paraI);
                }

                // header: start 
                var model = new user_info_view
                {
                    header = new Repositories.Models.header
                    {
                        root = new Repositories.Models.link_model
                        {
                            Title = Resources.Administrator.user_information,
                            link = "UserInfoUser"
                        },
                        lCate = new List<Repositories.Models.link_model>()
                    }
                };

                model.header.lCate.Add(new Repositories.Models.link_model
                {
                    Title = Resources.Administrator.user,
                    level = 1
                });

                var temModel = TempData["userProfile"];

                user _user = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(emailAddress, null);
                if(_user == null) return RedirectToAction("Error400", "Bug");
                model.user = new user_info_model();
                object objTo = model.user;
                ReflectionObject.setValue(_user, ref objTo);
                List<user_role_view> lstUserRoleView = await userroleLocalServiceUtil.Instance.getAllUserRoleViewAsync(model.user.userid);
                model.user.role = lstUserRoleView;
                                
                //model.userGroups = new GroupUserLocalServiceUtil().getByUserId(model.User.vidagis_userid);
                //model.userRoles = new UserRoleLocalServiceUtil().getByUserId(model.User.vidagis_userid, Common.SessionContext.Organization.vidagis_organizationid);
                //model.User.vidagis_image = dlFileEntryLocalServiceUtil.getPath(model.User.vidagis_image);
                model.paraI = paraI;
                string avatar = "";
                try
                {
                    //avatar = DLFileEntryLocalServiceUtil.Instance.getPath(model.User.vidagis_image);
                }
                catch (Exception ex)
                {
                    String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                    Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                }

                //ViewBag.isUpdate = true;
                //ViewBag.isDelete = isAdd;
                ViewBag.use_datatable = true;
                //ViewBag.avatar = avatar;
                ViewBag.page = Common.Constant.Page.user_profile;
                Log.LOG4NET.Info("Info: " + model.user.displayname);
                ViewData["lError"] = TempData["lError"];

                if (String.IsNullOrEmpty(SessionContext.Theme))
                {
                    SessionContext.Theme = "";
                    return View("UserInfo1", model);
                }
                else if (SessionContext.Theme == Constant.Theme.STACK)
                {
                    SessionContext.Theme = Constant.Theme.STACK;
                    return View(model);
                }
                else return View("Index_Theme_No_Support");

            }
            catch (Exception ex)
            {
                String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                return RedirectToAction("Error400", "Bug");
            }
        }

        [Apom_User.Common.ActionFilter.CustomIdentityActionFilter]
        public async Task<ActionResult> Account_Setting(string paraI, string t, string redirect = "")
        {
            try
            {
                long timeRegister = Convert.ToInt64(t);
                // convert parameter
                string emailAddress = Libs.Utils.Encrypts.Decrypt(String.Concat(timeRegister, "_", Constant.WebConfig.KEYENDE), paraI);
                
                // header: start 
                var model = new user_info_view
                {
                    header = new Repositories.Models.header
                    {
                        root = new Repositories.Models.link_model
                        {
                            Title = Resources.Administrator.user_information,
                            link = "UserInfoUser"
                        },
                        lCate = new List<Repositories.Models.link_model>()
                    }
                };

                model.header.lCate.Add(new Repositories.Models.link_model
                {
                    Title = Resources.Administrator.user,
                    level = 1
                });

                var temModel = TempData["userProfile"];

                user _user = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(emailAddress, null);
                if(_user.userid == userLocalServiceUtil.Instance.SupperAdminDefault.userid)
                {
                    return RedirectToAction("Error403", "Bug");
                }
                model.user = new user_info_model();
                object objTo = model.user;
                ReflectionObject.setValue(_user, ref objTo);
                List<user_role_view> lstUserRoleView = await userroleLocalServiceUtil.Instance.getAllUserRoleViewAsync(model.user.userid);
                model.user.role = lstUserRoleView;

                //model.userGroups = new GroupUserLocalServiceUtil().getByUserId(model.User.vidagis_userid);
                //model.userRoles = new UserRoleLocalServiceUtil().getByUserId(model.User.vidagis_userid, Common.SessionContext.Organization.vidagis_organizationid);
                //model.User.vidagis_image = dlFileEntryLocalServiceUtil.getPath(model.User.vidagis_image);
                model.paraI = paraI;
                string avatar = "";
                try
                {
                    //avatar = DLFileEntryLocalServiceUtil.Instance.getPath(model.User.vidagis_image);
                }
                catch (Exception ex)
                {
                    String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                    Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                }

                //ViewBag.isUpdate = true;
                //ViewBag.isDelete = isAdd;
                //ViewBag.use_datatable = true;
                //ViewBag.avatar = avatar;
                Log.LOG4NET.Info("Info: " + model.user.displayname);
                ViewData["lError"] = TempData["lError"];
                return View(model);

            }
            catch (Exception ex)
            {
                String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                return RedirectToAction("Error400", "Bug");
            }
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Apom_User.Common.ActionFilter.CustomIdentityActionFilter]
        public async Task<ActionResult> Account_Setting_Info()
        {
            this.Language();
            string userId = Request.Form["userid"];
            user _user = await userLocalServiceUtil.Instance.getUserAsync(userId);
            if (ModelState.IsValid)
            {
                string username = Request.Form["account-username"];
                //string email = Request.Form["account-email"];
                string firstname = Request.Form["account-firstname"];
                string middlename = Request.Form["account-middlename"];
                string lastname = Request.Form["account-lastname"];
                string bỉrthday = Request.Form["account-bỉthday"];
                string phonenumber = Request.Form["account-phonenumber"];
                string company = Request.Form["account-company"];
                string sex = Request.Form["account-sex"];
                string website = Request.Form["account-website"];

                bool isChangeAvatar = Request.Form["account-change-avatar"] == "1" ? true : false;

                _user.username = username;
                //_user.emailaddress = email;
                _user.firstname = firstname;
                _user.middlename = middlename;
                _user.lastname = lastname;
                _user.phonenumber = phonenumber;
                _user.companyname = company;
                _user.sex = sex;
                _user.website = website;
                if (!String.IsNullOrEmpty(bỉrthday))
                {
                    int year = Convert.ToInt16(bỉrthday.Substring(0,4));
                    int month = Convert.ToInt16(bỉrthday.Substring(5, 2));
                    int day = Convert.ToInt16(bỉrthday.Substring(8, 2));
                    _user.birthday = new DateTime(year, month, day);
                }

                var lError = new Repositories.Models.ErrorModel();
                lError.lMessage = new List<Repositories.Models.ErrorShowModel>();

                Apom_User.Models.DataApomAppConnection db = new Models.DataApomAppConnection();

                #region avatar
                if (Request.Files.Count > 0 && isChangeAvatar)
                {
                    try
                    {
                        dlfolder dl = dlfolderLocalServiceUtil.Instance.createFolder(db, Constant.WebConfig.REPOSITORY_ASSET, _user.userid);
                        var file = Request.Files["account-upload"];

                        if (file != null && file.ContentLength > 0)
                        {
                            int maxSize = Constant.WebConfig.SIZE_IMG_AVATAR;
                            if (file.ContentLength > maxSize * 1024)
                            {
                                string msgToolargeSizeAvatar = Resources.User.account_upload_new_photo_invalid_size.Replace(@"@num", Constant.WebConfig.SIZE_IMG_AVATAR.ToString());

                                lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                                {
                                    errorMessage = msgToolargeSizeAvatar,
                                    errorPath = "avatar-too-large"
                                });
                            }
                            else
                            {
                                string fileEntryId = String.IsNullOrEmpty(_user.image) ? "" : _user.image;

                                //dlFileEntry dlFileEntry = dlfileentryLocalServiceUtil.Instance.update(db, url_image, dl, file, service.getClassName(), model.User.oid);
                                dlfileentry fileEntry = dlfileentryLocalServiceUtil.Instance.update(db, fileEntryId, dl, file, Repositories.Services.Administrative.userLocalServiceUtil.Instance.getClassName(), _user.oid, _user.userid);
                                //// save logo
                                _user.image = fileEntry != null ? fileEntry.fileentryid : "";
                            }
                        }
                        else
                        {
                            //model.User.vidagis_image = url_image;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                #endregion
                if (lError.lMessage.Count > 0)
                {
                    lError.title = Resources.User.error_account_setting_info;
                    TempData["lError_ChangeInfo"] = lError;
                }
                else
                {
                    await userLocalServiceUtil.Instance.changeInfo(db, _user);
                    TempData["success_ChangeInfo"] = Resources.User.success_account_setting_info;
                }

                var time_request = DateTime.Now.Ticks.ToString();
                string paraI = Libs.Utils.Encrypts.Encrypt(String.Concat(time_request, "_", Constant.WebConfig.KEYENDE), _user.emailaddress);
                string redirect = Request.Url.PathAndQuery;
                return RedirectToAction("Account_Setting", "User", new { paraI = paraI, t = time_request, cmd = 0 });
            }
            else
            {
                //if (model.user_password.Length < Common.Constant.WebConfig.PWD_LENGTH)
                //{
                //    model.msg_error = Resources.Administrator.require_pwd_length;
                //    model.msg_error = model.msg_error.Replace("{0}", Common.Constant.WebConfig.PWD_LENGTH.ToString());
                //}
                //else if (!model.user_password.Equals(model.user_confirmpassword, StringComparison.OrdinalIgnoreCase))
                //{
                //    model.msg_error = Resources.Administrator.require_pwd_match;
                //}
                return RedirectToAction("Account_Setting", "User", new { msgError = Resources.Administrator.error_register_account });
            }
        }


        [Apom_User.Common.ActionFilter.CustomAdminActionFilter]
        public ActionResult Create_User(user_info_model model)
        {
            try
            {
                model = model == null ? new user_info_model() : model;
                return View(model);
            }
            catch (Exception ex)
            {
                String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                return RedirectToAction("Error400", "Bug");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Apom_User.Common.ActionFilter.CustomAdminActionFilter]
        public async Task<ActionResult> Create_User_Info()
        {
            this.Language();
            user _user = new user
            {
                userid = Guid.NewGuid().ToString()
            };

            if (ModelState.IsValid)
            {
                #region account info
                string email = Request.Form["account-email"].Trim();
                string password = Request.Form["account-password"].Trim();
                string re_password = Request.Form["account-re-password"].Trim();
                #endregion

                #region personmal info
                string username = Request.Form["account-username"].Trim();
                string firstname = Request.Form["account-firstname"].Trim();
                string middlename = Request.Form["account-middlename"].Trim();
                string lastname = Request.Form["account-lastname"].Trim();
                string bỉrthday = Request.Form["account-bỉthday"].Trim();
                string sex = Request.Form["account-sex"].Trim();
                #endregion

                #region contact info
                string phonenumber = Request.Form["account-phonenumber"].Trim();
                string company = Request.Form["account-company"].Trim();
                string website = Request.Form["account-website"].Trim();
                #endregion

                #region role
                string role = Request.Form["account-role"].Trim();
                bool isRoot = role == PermissionBase.Security.RoleSystem.SUPPER_ADMINISTRATOR ? true : false;
                role = (role == PermissionBase.Security.RoleSystem.SUPPER_ADMINISTRATOR || role == PermissionBase.Security.RoleSystem.ADMINISTRATOR) ? PermissionBase.Security.RoleSystem.ADMINISTRATOR : PermissionBase.Security.RoleSystem.MEMBER;
                #endregion

                var lError = new Repositories.Models.ErrorModel();
                lError.lMessage = new List<ErrorShowModel>();

                #region validate
                #region email
                var emailAddressAttribute = new EmailAddressAttribute();
                bool isEmail = emailAddressAttribute.IsValid(email);
                if (!isEmail)
                {
                    ErrorShowModel errModel = new ErrorShowModel
                    {
                        errorMessage = Resources.User.validate_is_not_email,
                        errorPath = "email-invalid"
                    };
                    lError.lMessage.Add(errModel);
                }
                // check is exist email
                user userCheck = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(email);
                if(userCheck != null)
                {
                    ErrorShowModel errModel = new ErrorShowModel
                    {
                        errorMessage = Resources.User.email_exist,
                        errorPath = "email-invalid"
                    };
                    lError.lMessage.Add(errModel);
                }
                #endregion

                #region password
                int minLengthPwd = Constant.WebConfig.PWD_LENGTH_MIN;
                int maxLengthPwd = Constant.WebConfig.PWD_LENGTH_MAX;
                var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
                if (string.IsNullOrEmpty(password))
                {
                    lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                    {
                        errorMessage = Resources.User.error_required,
                        errorPath = "password"
                    });
                }
                else
                {
                    if (password.Trim().Length < minLengthPwd)
                    {
                        lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                        {
                            errorMessage = Resources.User.password_require_num_char,
                            errorPath = "password"
                        });
                    }
                    if (password.Trim().Length > 50)
                    {
                        lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                        {
                            errorMessage = Resources.User.password_require_num_char_max.Replace(@"@num", maxLengthPwd.ToString()),
                            errorPath = "password"
                        });
                    }
                    if (hasSymbols.IsMatch(password[0].ToString()))
                    {
                        lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                        {
                            errorMessage = Resources.User.firstchart_not_special,
                            errorPath = "password"
                        });
                    }
                    if (!password.Any(char.IsUpper) || !password.Any(char.IsDigit) || !hasSymbols.IsMatch(password))
                    {
                        lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                        {
                            errorMessage = Resources.User.password_1_upper_1_digit,
                            errorPath = "password"
                        });
                    }
                }

                if (string.IsNullOrEmpty(re_password))
                {
                    lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                    {
                        errorMessage = Resources.User.error_required,
                        errorPath = "password-confirm"
                    });
                }

                if (!string.IsNullOrEmpty(password) &&
                    !string.IsNullOrEmpty(re_password) &&
                    (!password.Equals(re_password)))
                {
                    lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                    {
                        errorMessage = Resources.User.validate_pass_equal_confirm_pass,
                        errorPath = "password-equal-retype-password"
                    });
                }
                #endregion

                #region user name
                if (string.IsNullOrEmpty(username))
                {
                    lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                    {
                        errorMessage = Resources.User.error_required,
                        errorPath = "username-required"
                    });
                }
                #endregion
                #endregion

                bool isChangeAvatar = Request.Form["account-change-avatar"] == "1" ? true : false;

                _user.emailaddress = email;
                password = Libs.Utils.Encrypts.Encrypt(String.Concat(email, "_", Common.Constant.WebConfig.KEYENDE), password);
                _user.password = password;

                _user.username = username;
                _user.firstname = firstname;
                _user.middlename = middlename;
                _user.lastname = lastname;
                _user.sex = sex;
                if (!String.IsNullOrEmpty(bỉrthday))
                {
                    int year = Convert.ToInt16(bỉrthday.Substring(0, 4));
                    int month = Convert.ToInt16(bỉrthday.Substring(5, 2));
                    int day = Convert.ToInt16(bỉrthday.Substring(8, 2));
                    _user.birthday = new DateTime(year, month, day);
                }

                _user.phonenumber = phonenumber;
                _user.companyname = company;
                _user.website = website;

                Apom_User.Models.DataApomAppConnection db = new Models.DataApomAppConnection();

                #region avatar
                if (Request.Files.Count > 0 && isChangeAvatar)
                {
                    try
                    {
                        dlfolder dl = dlfolderLocalServiceUtil.Instance.createFolder(db, Constant.WebConfig.REPOSITORY_ASSET, _user.userid);
                        var file = Request.Files["account-upload"];

                        if (file != null && file.ContentLength > 0)
                        {
                            int maxSize = Constant.WebConfig.SIZE_IMG_AVATAR;
                            if (file.ContentLength > maxSize * 1024)
                            {
                                string msgToolargeSizeAvatar = Resources.User.account_upload_new_photo_invalid_size.Replace(@"@num", Constant.WebConfig.SIZE_IMG_AVATAR.ToString());

                                lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                                {
                                    errorMessage = msgToolargeSizeAvatar,
                                    errorPath = "avatar-too-large"
                                });
                            }
                            else
                            {
                                string fileEntryId = String.IsNullOrEmpty(_user.image) ? "" : _user.image;

                                dlfileentry fileEntry = dlfileentryLocalServiceUtil.Instance.update(db, fileEntryId, dl, file, userLocalServiceUtil.Instance.getClassName(), _user.oid, _user.userid);
                                //// save logo
                                _user.image = fileEntry != null ? fileEntry.fileentryid : "";
                            }
                        }
                        else
                        {
                            //model.User.vidagis_image = url_image;
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                #endregion
                if (lError.lMessage.Count > 0)
                {
                    lError.title = Resources.User.error_account_setting_info;
                    TempData["lError_ChangeInfo"] = lError;

                    var time_request = DateTime.Now.Ticks.ToString();
                    string paraI = Libs.Utils.Encrypts.Encrypt(String.Concat(time_request, "_", Apom_User.Common.Constant.WebConfig.KEYENDE), _user.emailaddress);
                    string redirect = Request.Url.PathAndQuery;
                    object userInfo = new user_info_model();
                    ReflectionObject.setValue(_user, ref userInfo);
                    //return View((user_info_model)userInfo);
                    //return RedirectToAction("Create_User", "User", );
                    return View("Create_User", userInfo);
                }
                else
                {
                    _user.isactive = true;
                    _user.is_get_notify = true;
                    _user.createdate = DateTime.Now;
                    _user = userLocalServiceUtil.Instance.add(db, _user);

                    user_role _userRole = new user_role
                    {
                        userid = _user.userid,
                        roleid = role,
                        isroot = isRoot,
                        dateupdate = DateTime.Now
                    };
                    userroleLocalServiceUtil.Instance.add(db, _userRole);

                    TempData["success_ChangeInfo"] = Resources.User.success_account_setting_info;

                    var time_request = DateTime.Now.Ticks.ToString();
                    string paraI = Libs.Utils.Encrypts.Encrypt(String.Concat(time_request, "_", Apom_User.Common.Constant.WebConfig.KEYENDE), _user.emailaddress);
                    string redirect = Request.Url.PathAndQuery;
                    //return RedirectToAction("Account_Setting", "User", new { paraI = paraI, t = time_request, cmd = 0 });
                    return RedirectToAction("UserList", "User");                    
                }
            }
            else
            {
                //if (model.user_password.Length < Common.Constant.WebConfig.PWD_LENGTH)
                //{
                //    model.msg_error = Resources.Administrator.require_pwd_length;
                //    model.msg_error = model.msg_error.Replace("{0}", Common.Constant.WebConfig.PWD_LENGTH.ToString());
                //}
                //else if (!model.user_password.Equals(model.user_confirmpassword, StringComparison.OrdinalIgnoreCase))
                //{
                //    model.msg_error = Resources.Administrator.require_pwd_match;
                //}
                return RedirectToAction("Account_Setting", "User", new { msgError = Resources.Administrator.error_register_account });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Apom_User.Common.ActionFilter.CustomIdentityActionFilter]
        public async Task<ActionResult> Account_Setting_ChangePassword()
        {
            this.Language();
            string userId = Request.Form["userid"];
            user _user = await userLocalServiceUtil.Instance.getUserAsync(userId);
            if (ModelState.IsValid)
            {
                int minLengthPwd = Constant.WebConfig.PWD_LENGTH_MIN;
                int maxLengthPwd = Constant.WebConfig.PWD_LENGTH_MAX;
                string oldPwdId = "account-old-password";
                string newPwdId = "account-new-password";
                string newRetypePwdId = "account-retype-new-password";

                var lError = new Repositories.Models.ErrorModel();
                lError.lMessage = new List<Repositories.Models.ErrorShowModel>();
                var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

                string oldPwdValue = Request.Form[oldPwdId];
                oldPwdValue = Libs.Utils.Encrypts.Encrypt(String.Concat(_user.emailaddress, "_", Common.Constant.WebConfig.KEYENDE), oldPwdValue);
                if (!oldPwdValue.Equals(_user.password))
                {
                    lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                    {
                        errorMessage = Resources.User.old_pass_incorrect,
                        errorPath = "password-old-incorrect"
                    });
                }
                else
                {
                    if (string.IsNullOrEmpty(Request.Form[newPwdId]))
                    {
                        lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                        {
                            errorMessage = Resources.User.error_required,
                            errorPath = "password"
                        });
                    }
                    else
                    {
                        if (Request.Form[newPwdId].Trim().Length < minLengthPwd)
                        {
                            lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                            {
                                errorMessage = Resources.User.password_require_num_char,
                                errorPath = "password"
                            });
                        }
                        if (Request.Form[newPwdId].Trim().Length > 50)
                        {
                            lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                            {
                                errorMessage = Resources.User.password_require_num_char_max.Replace(@"@num", maxLengthPwd.ToString()),
                                errorPath = "password"
                            });
                        }
                        if (hasSymbols.IsMatch(Request.Form[newPwdId].Trim()[0].ToString()))
                        {
                            lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                            {
                                errorMessage = Resources.User.firstchart_not_special,
                                errorPath = "password"
                            });
                        }
                        if (!Request.Form[newPwdId].Trim().Any(char.IsUpper) || !Request.Form[newPwdId].Trim().Any(char.IsDigit) || !hasSymbols.IsMatch(Request.Form[newPwdId].Trim()))
                        {
                            lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                            {
                                errorMessage = Resources.User.password_1_upper_1_digit,
                                errorPath = "password"
                            });
                        }
                    }

                    if (string.IsNullOrEmpty(Request.Form[newRetypePwdId]))
                    {
                        lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                        {
                            errorMessage = Resources.User.error_required,
                            errorPath = "password-confirm"
                        });
                    }

                    if (string.IsNullOrEmpty(Request.Form[oldPwdId]))
                    {
                        lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                        {
                            errorMessage = Resources.User.error_required,
                            errorPath = "password-old"
                        });
                    }

                    if (!string.IsNullOrEmpty(Request.Form[newPwdId]) &&
                        !string.IsNullOrEmpty(Request.Form[newRetypePwdId]) &&
                        (Request.Form[newPwdId] != Request.Form[newRetypePwdId]))
                    {
                        lError.lMessage.Add(new Repositories.Models.ErrorShowModel
                        {
                            errorMessage = Resources.User.validate_pass_equal_confirm_pass,
                            errorPath = "password-equal-retype-password"
                        });
                    }
                }

                if (lError.lMessage.Count > 0)
                {
                    lError.title = Resources.User.error_account_setting_change_pwd;
                    TempData["lError_ChangePwd"] = lError;
                }
                else
                {
                    Apom_User.Models.DataApomAppConnection db = new Models.DataApomAppConnection();
                    await userLocalServiceUtil.Instance.changePassAsync(db, userId, Request.Form[newPwdId], Constant.WebConfig.KEYENDE);
                    TempData["success_ChangePwd"] = Resources.User.success_account_setting_change_pwd;
                }
                var time_request = DateTime.Now.Ticks.ToString();
                string paraI = Libs.Utils.Encrypts.Encrypt(String.Concat(time_request, "_", Apom_User.Common.Constant.WebConfig.KEYENDE), _user.emailaddress);
                string redirect = Request.Url.PathAndQuery;
                return RedirectToAction("Account_Setting", "User", new { paraI=paraI, t=time_request, cmd=1});
            }
            else
            {
                //if (model.user_password.Length < Common.Constant.WebConfig.PWD_LENGTH)
                //{
                //    model.msg_error = Resources.Administrator.require_pwd_length;
                //    model.msg_error = model.msg_error.Replace("{0}", Common.Constant.WebConfig.PWD_LENGTH.ToString());
                //}
                //else if (!model.user_password.Equals(model.user_confirmpassword, StringComparison.OrdinalIgnoreCase))
                //{
                //    model.msg_error = Resources.Administrator.require_pwd_match;
                //}
                return RedirectToAction("Account_Setting", "User", new { msgError = Resources.User.error_account_setting_change_pwd });
            }
        }

        [Apom_User.Common.ActionFilter.CustomAdminActionFilter]
        public async Task<ActionResult> UserList()
        {
            try
            {
                ViewBag.page = Common.Constant.Page.user_list;
                List<user_info_model> lstUser = await userLocalServiceUtil.Instance.getAllUserAsync();
                List<user_role_view> lstUserRoleView = await userroleLocalServiceUtil.Instance.getAllUserRoleViewAsync();
                for(int i=0; i<lstUser.Count; i++)
                {
                    string userId = lstUser[i].userid;
                    lstUser[i].role = lstUserRoleView.Where(x => x.userid == userId).ToList();
                }

                user_info_model userInfoModel = new user_info_model();
                object objTo = userInfoModel;
                var userMain = lstUser.FindLast(x => x.userid == SessionContext.UserProfile.userid);
                if(userMain != null)
                {
                    ReflectionObject.setValue(userMain, ref objTo);
                }
                user_list_view model = new user_list_view
                {
                    userProfile = userMain,
                    lstUser = lstUser
                };

                ViewBag.use_datatable = true;
                ViewBag.page = Common.Constant.Page.user_list;
                return View(model);
            }
            catch (Exception ex)
            {
                String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                return RedirectToAction("Error400", "Bug");
            }
        }

        [Apom_User.Common.ActionFilter.CustomAdminActionFilter]
        public async Task<ActionResult> lock_account()
        {
            try
            {
                this.Language();
                string requestQueryString = Request.Form["data"];
                requestQueryString = requestQueryString.Substring(1);
                requestQueryString = requestQueryString.Remove(requestQueryString.Length - 1);
                JsonRequest deserializedRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<JsonRequest>(requestQueryString);
                string paraI = deserializedRequest.paraI;
                string time_request = deserializedRequest.time_request;

                long timeRegister = Convert.ToInt64(time_request);
                // convert parameter
                string emailAddress = Libs.Utils.Encrypts.Decrypt(String.Concat(timeRegister, "_", Constant.WebConfig.KEYENDE), paraI);

                user _user = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(emailAddress, null);
                if (_user == null) return RedirectToAction("Error400", "Bug");

                _user.isactive = false;
                Apom_User.Models.DataApomAppConnection db = new Models.DataApomAppConnection();
                userLocalServiceUtil.Instance.update(db, _user);

                return ToJson(new jsonResponse
                {
                    Code = System.Net.HttpStatusCode.OK,
                    Message = Resources.User.account_lock_account_success,
                    Data = null
                });
            }
            catch (Exception ex)
            {
                return ToJson(new jsonResponse
                {
                    Code = System.Net.HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        [Apom_User.Common.ActionFilter.CustomAdminActionFilter]
        public async Task<ActionResult> unlock_account()
        {
            try
            {
                this.Language();
                string requestQueryString = Request.Form["data"];
                requestQueryString = requestQueryString.Substring(1);
                requestQueryString = requestQueryString.Remove(requestQueryString.Length - 1);
                JsonRequest deserializedRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<JsonRequest>(requestQueryString);
                string paraI = deserializedRequest.paraI;
                string time_request = deserializedRequest.time_request;

                long timeRegister = Convert.ToInt64(time_request);
                // convert parameter
                string emailAddress = Libs.Utils.Encrypts.Decrypt(String.Concat(timeRegister, "_", Constant.WebConfig.KEYENDE), paraI);

                user _user = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(emailAddress, null);
                if (_user == null) return RedirectToAction("Error400", "Bug");

                _user.isactive = true;
                Apom_User.Models.DataApomAppConnection db = new Models.DataApomAppConnection();
                userLocalServiceUtil.Instance.update(db, _user);
                //int result = await userLocalServiceUtil.Instance.activeUser(_user.userid);
                return ToJson(new jsonResponse
                {
                    Code = System.Net.HttpStatusCode.OK,
                    Message = Resources.User.account_unlock_account_success,
                    Data = null
                });
            }
            catch (Exception ex)
            {
                return ToJson(new jsonResponse
                {
                    Code = System.Net.HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }


        [Apom_User.Common.ActionFilter.CustomAdminActionFilter]
        public async Task<ActionResult> del_account()
        {
            try
            {
                this.Language();
                string requestQueryString = Request.Form["data"];
                requestQueryString = requestQueryString.Substring(1);
                requestQueryString = requestQueryString.Remove(requestQueryString.Length - 1);
                JsonRequest deserializedRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<JsonRequest>(requestQueryString);
                string paraI = deserializedRequest.paraI;
                string time_request = deserializedRequest.time_request;

                long timeRegister = Convert.ToInt64(time_request);
                // convert parameter
                string emailAddress = Libs.Utils.Encrypts.Decrypt(String.Concat(timeRegister, "_", Constant.WebConfig.KEYENDE), paraI);

                user _user = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(emailAddress, null);
                if (_user == null) return RedirectToAction("Error400", "Bug");
                string userId = _user.userid;
                string userIdExcute = SessionContext.UserProfile.userid;

                #region check permission
                bool isHavePermissionSupperAdminDefault_Excute = userroleLocalServiceUtil.Instance.isSupperAdminDefault(userIdExcute);
                bool isHavePermissionSupperAdmin_Excute = userroleLocalServiceUtil.Instance.isSupperAdmin(userIdExcute);
                bool isHavePermissionAdmin_Excute = userroleLocalServiceUtil.Instance.isAdmin(userIdExcute);

                bool isHavePermissionSupperAdminDefault_Deleted = userroleLocalServiceUtil.Instance.isSupperAdminDefault(userIdExcute);
                bool isHavePermissionSupperAdmin_Deleted = userroleLocalServiceUtil.Instance.isSupperAdmin(userId);
                bool isHavePermissionAdmin_Deleted = userroleLocalServiceUtil.Instance.isAdmin(userId);

                bool isAllowDeleted = true;
                // supper admin couldn't deleted, only with supper_admin_default
                if(isHavePermissionSupperAdminDefault_Deleted)
                {
                    isAllowDeleted = false;
                }
                else if(isHavePermissionSupperAdmin_Deleted)
                {
                    // only delete: userIdExcute is supper admin default
                    isAllowDeleted = isHavePermissionSupperAdminDefault_Excute;
                }
                else if(isHavePermissionAdmin_Deleted)
                {
                    // only delete: userIdExcute is supper admin
                    isAllowDeleted = isHavePermissionSupperAdmin_Excute;
                }
                else if(!isHavePermissionSupperAdmin_Excute || !isHavePermissionAdmin_Excute)
                {
                    // only delete: userIdExcute = userId
                    isAllowDeleted = userIdExcute.Equals(userId);
                }

                if (!isAllowDeleted)
                {
                    return ToJson(new jsonResponse
                    {
                        Code = System.Net.HttpStatusCode.Forbidden,
                        Message = Resources.User.account_lock_account_failed,
                        Data = null
                    });
                }
                #endregion

                #region del account
                // apom_del_account
                string sql = $"select apom_del_account('{userId}');";
                int result = await userLocalServiceUtil.Instance.excuteNonQueryAsync(sql, null);
                #endregion

                string urlRedirect = "/list_users";
                if (userIdExcute.Equals(userId)) urlRedirect = "/account/login";
                return ToJson(new jsonResponse
                {
                    Code = System.Net.HttpStatusCode.OK,
                    Message = Resources.User.account_del_account_success,
                    Data = urlRedirect
                });
            }
            catch (Exception ex)
            {
                return ToJson(new jsonResponse
                {
                    Code = System.Net.HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        public async Task<ActionResult> chane_role_account()
        {
            try
            {
                this.Language();
                string requestQueryString = Request.Form["data"];
                requestQueryString = requestQueryString.Substring(1);
                requestQueryString = requestQueryString.Remove(requestQueryString.Length - 1);
                JsonRequest deserializedRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<JsonRequest>(requestQueryString);
                string paraI = deserializedRequest.paraI;
                string time_request = deserializedRequest.time_request;
                string roleId = deserializedRequest.data;

                long timeRegister = Convert.ToInt64(time_request);
                // convert parameter
                string emailAddress = Libs.Utils.Encrypts.Decrypt(String.Concat(timeRegister, "_", Constant.WebConfig.KEYENDE), paraI);

                user _user = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(emailAddress, null);
                if (_user == null) return RedirectToAction("Error400", "Bug");
                string userId = _user.userid;
                string userIdExcute = SessionContext.UserProfile.userid;

                #region check permission
                bool isHavePermissionSupperAdminDefault_Excute = userroleLocalServiceUtil.Instance.isSupperAdminDefault(userIdExcute);
                bool isHavePermissionSupperAdmin_Excute = userroleLocalServiceUtil.Instance.isSupperAdmin(userIdExcute);
                bool isHavePermissionAdmin_Excute = userroleLocalServiceUtil.Instance.isAdmin(userIdExcute);

                bool isHavePermissionSupperAdminDefault_ChangeRole = userroleLocalServiceUtil.Instance.isSupperAdminDefault(userId);
                bool isHavePermissionSupperAdmin_ChangeRole = userroleLocalServiceUtil.Instance.isSupperAdmin(userId);
                bool isHavePermissionAdmin_ChangeRole = userroleLocalServiceUtil.Instance.isAdmin(userId);

                bool isAllowChangeRole = true;
                // supper admin couldn't change, only with supper_admin_default
                if (isHavePermissionSupperAdminDefault_ChangeRole)
                {
                    isAllowChangeRole = false;
                }
                else if (isHavePermissionSupperAdmin_ChangeRole)
                {
                    // only change: userIdExcute is supper admin default
                    isAllowChangeRole = isHavePermissionSupperAdminDefault_Excute;
                }
                else if (isHavePermissionAdmin_ChangeRole)
                {
                    // only change: userIdExcute is supper admin
                    isAllowChangeRole = isHavePermissionSupperAdmin_Excute;
                }
                else if (!isHavePermissionSupperAdmin_Excute || !isHavePermissionAdmin_Excute)
                {
                    isAllowChangeRole = false;
                }

                if (!isAllowChangeRole)
                {
                    return ToJson(new jsonResponse
                    {
                        Code = System.Net.HttpStatusCode.Forbidden,
                        Message = Resources.User.account_change_role_fail,
                        Data = null
                    });
                }
                #endregion

                #region change role account
                bool isRoot = false;
                isRoot = roleId == PermissionBase.Security.RoleSystem.SUPPER_ADMINISTRATOR ? true : false;
                roleId = roleId == PermissionBase.Security.RoleSystem.SUPPER_ADMINISTRATOR ? PermissionBase.Security.RoleSystem.ADMINISTRATOR : roleId;
                bool result = await userroleLocalServiceUtil.Instance.updateRoleAsync(userId, roleId, isRoot);
                #endregion

                string urlRedirect = "/list_users";
                if (userIdExcute.Equals(userId)) urlRedirect = "/account/login";
                return ToJson(new jsonResponse
                {
                    Code = System.Net.HttpStatusCode.OK,
                    Message = Resources.User.account_del_account_success,
                    Data = urlRedirect
                });
            }
            catch (Exception ex)
            {
                return ToJson(new jsonResponse
                {
                    Code = System.Net.HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }
    }
}