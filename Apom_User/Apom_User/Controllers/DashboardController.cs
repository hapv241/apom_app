﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Apom_User.Common;
using Repositories.Models;
using Repositories.Models.Category;
using Repositories.Services.Category;

namespace Apom_User.Controllers
{
    public class DashboardController : BaseController
    {
        // GET: data_analysis
        public async Task<ActionResult> Index(string provinceId = "VNM.27_1")
        {
            Language();
            ViewBag.use_datatable = true;
            ViewBag.use_map = true;
            ViewBag.use_chart = true;
            ViewBag.page = Common.Constant.Page.data_analysis;

            List<map_scale> lstMapScale = await mapscaleLocalServiceUtil.Instance.getAllAsync();
            List<vnaqi_index_short> lstAQIIndexShort = await vnaqiindexLocalServiceUtil.Instance.getInfoShortAsync(Common.SessionContext.Language);
            List<province> lstProvince = await provinceLocalServiceUtil.Instance.getAllAsync(Common.SessionContext.Language);
            var cookies = Request.Cookies[Constant.WebConfig.COOKIE_URL] != null ? Request.Cookies[Constant.WebConfig.COOKIE_URL] : null;
            bool isAuthenticate = false, isVietNam = false;
            if (cookies != null)
            {
                isAuthenticate = cookies.Values["is_auth"] == "1" ? true : false;
                isVietNam = cookies.Values["is_vn"] == "1" ? true : false;
            }
            if (!isVietNam)
            {
                lstProvince = lstProvince.Where(x => x.province_id == provinceId).ToList();
            }

            analysis_model_view modelView = new analysis_model_view
            {
                Provinces = lstProvince,
                VNAQIIndex = lstAQIIndexShort,
                isAuthenticate= isAuthenticate,
                isVietNam= isVietNam
            };
            return View(modelView);
        }
    }
}