﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Http;
using System.Web.Mvc;
using System.Text;
using System.Threading.Tasks;

using Repositories.Models.Asset;
using Repositories.Models.Category;
using Repositories.Models.Config;
using Repositories.Services.Asset;
using Repositories.Services.Category;
using Repositories.Services.Config;
using Apom_User.Common;

using Common;
using System.Net.Http;
using System.Net.Http.Headers;
using Apom_User.Models;
using System.Security.Policy;
using System.Web.Services.Description;

namespace Apom_User.Controllers
{
    public class HomeController : BaseController
    {
        private string htmlVerticalMenuFromGroup(List<group_short> lstGroup, string parentId)
        {
            StringBuilder htmlBuilder = new StringBuilder();
            List<group_short> lstGroupParent = null;
            if (String.IsNullOrEmpty(parentId)) lstGroupParent = lstGroup.Where(x => x.parent_id == null).ToList();
            else lstGroupParent = lstGroup.Where(x => x.parent_id == parentId).ToList();
            foreach (group_short g in lstGroupParent)
            {
                htmlBuilder.Append("<li class='nav-item'>");
                string groupId = g.id;
                int typeStation = (int)g.type_station;
                string icon = String.IsNullOrEmpty(g.icon) ? "fa fa-minus" : g.icon;
                List<group_short> lstGroupChilds = lstGroup.Where(x => x.parent_id == groupId).ToList();

                if ((groupId == "wrf_aqi") || (groupId == "satellite_aqi") || (groupId == "station_airpolution"))
                {
                    htmlBuilder.Append($"<a class='nav-link' href='#'>");
                    htmlBuilder.Append($"<input data-id='{groupId}' data-typestation='{typeStation}' type='checkbox' id='switchery_{groupId}' class='switchery switchery-layer-menu' data-size='xs' checked />");
                    htmlBuilder.Append($"<span class='d-none d-sm-inline' style='margin-left: 5px;'>{g.name}</span>");
                    htmlBuilder.Append("</a>");
                }
                if (lstGroupChilds.Count != 0)
                //{
                //    /*
                //     <a class="nav-link" href="#">
                //        <i class="fa fa-minus"></i>
                //        <span>Hà Nội EPA</span>
                //    </a>
                //    */
                //    htmlBuilder.Append($"<a class='nav-link' href='#'>");
                //    //htmlBuilder.Append($"<i class='{icon}'></i>");
                //    htmlBuilder.Append($"<input data-id='{groupId}' data-typestation='{typeStation}' type='checkbox' id='switchery_{groupId}' class='switchery switchery-layer-menu' data-size='xs' checked />");
                //    htmlBuilder.Append($"<span class='d-none d-sm-inline' style='margin-left: 5px;'>{g.name}</span>");
                //    htmlBuilder.Append("</a>");
                //}
                //else
                {
                    /*
                     <a class="nav-link collapsed text-truncate" href="#submenuLayerSelector" data-toggle="collapse" data-target="#submenuLayerSelector">
                        <i class="fa fa-bars"></i>
                        <span class="d-none d-sm-inline">Chọn lớp</span>
                    </a>
                    <div class="collapse" id="submenuStationToolbar" aria-expanded="false">
                        <ul class="flex-column pl-1 nav">
                        </ul>
                    </div>
                    */
                    //string subMenuId = $"submenu{groupId.ToUpper()}";
                    //htmlBuilder.Append($"<a class='nav-link collapsed text-truncate' href='#{subMenuId}' data-toggle='collapse' data-target='#{subMenuId}'>");
                    //htmlBuilder.Append($"<i class='{icon}'></i>");
                    //htmlBuilder.Append($"<span class='d-none d-sm-inline'>{g.name}</span>");
                    //htmlBuilder.Append("</a>");
                    //htmlBuilder.Append($"<div class='collapse' id='{subMenuId}' aria-expanded='false'>");
                    //htmlBuilder.Append("<ul class='flex-column pl-1 nav'>");
                    htmlBuilder.Append(htmlVerticalMenuFromGroup(lstGroup, groupId));
                    //htmlBuilder.Append("</ul>");
                    //htmlBuilder.Append("</div>");
                }
                htmlBuilder.Append("</li>");
            }
            return htmlBuilder.ToString();
        }

        private bool addColumnComponent()
        {
            bool isSuccess = true;
            try
            {
                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("alter table public.a_component_station_daily ");
                List<component> compAll = componentLocalServiceUtil.Instance.getAll();
                for (int i = 0; i < compAll.Count; i++)
                {
                    component comp = compAll[i];
                    string id = comp.id;
                    sqlBuilder.Append($"add column {id} double precision,");
                    sqlBuilder.Append($"add column aqi_{id} double precision,");
                }
                sqlBuilder.Append($"add column aqi double precision");
                string sqlExecute = sqlBuilder.ToString();
                int result = componentLocalServiceUtil.Instance.excuteNonQuery(sqlExecute, null);
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        private void setURLCookie(bool is_auth, bool is_vn, string province_id)
        {
            var urlCookie = HttpContext.Response.Cookies[Constant.WebConfig.COOKIE_URL] == null ? new System.Web.HttpCookie(Constant.WebConfig.COOKIE_LOGIN) : HttpContext.Response.Cookies[Constant.WebConfig.COOKIE_LOGIN];
            urlCookie.Values.Add("is_auth", is_auth ? "1" : "0");
            urlCookie.Values.Add("is_vn", is_vn ? "1" : "0");
            urlCookie.Values.Add("province_id", province_id);
            urlCookie.Expires = DateTime.Now.AddDays(Constant.WebConfig.COOKIE_TIMEOUT);
            HttpContext.Response.Cookies.Add(urlCookie);
        }

        [Route("/home")]
        public async Task<ActionResult> Index(string themeid)
        {
            if (Request.Url.Port == Constant.WebConfig.PORT_ADMIN)
            {
                return RedirectToAction("UserList", "User");
            }
            return RedirectToAction("HaNoi");

            //SessionContext.Theme = String.IsNullOrEmpty(themeid) ? "" : (themeid == "apom" ? "" : themeid);

            //Language();
            //ViewBag.use_datatable = true;
            //ViewBag.use_map = true;
            //ViewBag.use_chart = true;
            ////ViewBag.use_switch = true;
            //ViewBag.use_baseslider = true;
            //ViewBag.use_notification = SessionContext.UserProfile != null ? SessionContext.UserProfile.is_get_notify : true;
            //ViewBag.page = Common.Constant.Page.home;

            //try
            //{
            //    setURLCookie(false, false, "");

            //    //addColumnComponent();
            //    //string htmlVerticalMenu = htmlVerticalMenuFromGroup(lstGroup, null);
            //    //ViewBag.htmlVerticalMenu = htmlVerticalMenu;

            //    List<group_short> lstGroup = await groupLocalServiceUtil.Instance.buildVerticalMenuFromGroupsAsync(Common.SessionContext.Language);
            //    List<map_scale> lstMapScale = await mapscaleLocalServiceUtil.Instance.getAllAsync();
            //    List<vnaqi_index_short> lstAQIIndexShort = await vnaqiindexLocalServiceUtil.Instance.getInfoShortAsync(Common.SessionContext.Language);
            //    List<component_geotiff_service_short> lstServices = await componentgeotiffserviceLocalServiceUtil.Instance.getAllShortAsync();

            //    lstGroup = lstGroup.Where(x => x.parent_id != null).ToList();
            //    lstServices.Where(x => x.type == "station").ToList().ForEach(x => x.title = Resources.Map.timeslider_station);

            //    #region pinned location
            //    //string userId = Common.SessionContext.UserProfile == null ? "" : Common.SessionContext.UserProfile.userid;
            //    //List<pinned_location> lstPinnedLocationDefault = cf_pinned_locationLocalServiceUtil.Instance.getPinnedLocationDefault("satellite_aqi_pm25", "pm25", Common.SessionContext.Language, userId);
            //    #endregion

            //    Repositories.Models.map_model_view mapModelView = new Repositories.Models.map_model_view
            //    {
            //        MapScale = lstMapScale,
            //        VNAQIIndex = lstAQIIndexShort,
            //        Layers = lstGroup,
            //        Services = lstServices
            //        //PinnedLocation = lstPinnedLocationDefault
            //    };

            //    //Models.DataApomAppConnection db = new Models.DataApomAppConnection();
            //    //Repositories.Services.Archive.dlfolderLocalServiceUtil.Instance.createFolder(db, Common.Constant.WebConfig.REPOSITORY_ASSET, "");
            //    //Repositories.Services.Archive.dlfolderLocalServiceUtil.Instance.createFolder(db, Common.Constant.WebConfig.REPOSITORY_DOCUMENT, "");
            //    //Repositories.Services.Archive.dlfolderLocalServiceUtil.Instance.createFolder(db, Common.Constant.WebConfig.REPOSITORY_REPORT, "");

            //    //List<general_setting> lstGS = generalSettingLocalServiceUtil.Instance.getAll();
            //    if (String.IsNullOrEmpty(SessionContext.Theme))
            //    {
            //        SessionContext.Theme = "";
            //        return View("Index1", mapModelView);
            //    }
            //    else if (SessionContext.Theme == Constant.Theme.STACK)
            //    {
            //        SessionContext.Theme = Constant.Theme.STACK;
            //        return View(mapModelView);
            //    }
            //    else return View("Index_Theme_No_Support");

            //}
            //catch (Exception ex)
            //{
            //    String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
            //    Apom_User.Common.Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
            //    return RedirectToAction("Error400", "Bug");
            //}
        }

        public async Task<ActionResult> VietNam(string themeid)
        {
            if (Request.Url.Port == Constant.WebConfig.PORT_ADMIN)
            {
                return RedirectToAction("UserList", "User");
            }

            SessionContext.Theme = String.IsNullOrEmpty(themeid) ? "" : (themeid == "apom" ? "" : themeid);

            Language();
            ViewBag.use_datatable = true;
            ViewBag.use_map = true;
            ViewBag.use_chart = true;
            ViewBag.use_baseslider = true;
            ViewBag.use_notification = SessionContext.UserProfile != null ? SessionContext.UserProfile.is_get_notify : true;
            ViewBag.page = Common.Constant.Page.home;

            try
            {
                setURLCookie(true, true, "");
                List<group_short> lstGroup = await groupLocalServiceUtil.Instance.buildVerticalMenuFromGroupsAsync(Common.SessionContext.Language);
                List<map_scale> lstMapScale = await mapscaleLocalServiceUtil.Instance.getAllAsync();
                List<vnaqi_index_short> lstAQIIndexShort = await vnaqiindexLocalServiceUtil.Instance.getInfoShortAsync(Common.SessionContext.Language);
                List<component_geotiff_service_short> lstServices = await componentgeotiffserviceLocalServiceUtil.Instance.getAllShortAsync();

                lstGroup = lstGroup.Where(x => x.parent_id != null).ToList();
                lstServices.Where(x => x.type == "station").ToList().ForEach(x => x.title = Resources.Map.timeslider_station);

                Repositories.Models.map_model_view mapModelView = new Repositories.Models.map_model_view
                {
                    MapScale = lstMapScale,
                    VNAQIIndex = lstAQIIndexShort,
                    Layers = lstGroup,
                    Services = lstServices,
                    isVietNam = true,
                    isAuthenticate = true
                    //PinnedLocation = lstPinnedLocationDefault
                };

                return View(mapModelView);
            }
            catch (Exception ex)
            {
                String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Apom_User.Common.Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                return RedirectToAction("Error400", "Bug");
            }
        }


        [Apom_User.Common.ActionFilter.CustomAuthenticationFilter]
        public async Task<ActionResult> HaNoi(string themeid)
        {
            if (Request.Url.Port == Constant.WebConfig.PORT_ADMIN)
            {
                return RedirectToAction("UserList", "User");
            }

            SessionContext.Theme = String.IsNullOrEmpty(themeid) ? "" : (themeid == "apom" ? "" : themeid);

            Language();
            ViewBag.use_datatable = true;
            ViewBag.use_map = true;
            ViewBag.use_chart = true;
            ViewBag.use_baseslider = true;
            ViewBag.use_notification = SessionContext.UserProfile != null ? SessionContext.UserProfile.is_get_notify : true;
            ViewBag.page = Common.Constant.Page.home;

            try
            {
                setURLCookie(true, false, Constant.WebConfig.PROVINCE_DEFAULT);
                List<group_short> lstGroup = await groupLocalServiceUtil.Instance.buildVerticalMenuFromGroupsAsync(Common.SessionContext.Language);
                List<map_scale> lstMapScale = await mapscaleLocalServiceUtil.Instance.getAllAsync();
                List<vnaqi_index_short> lstAQIIndexShort = await vnaqiindexLocalServiceUtil.Instance.getInfoShortAsync(Common.SessionContext.Language);
                List<component_geotiff_service_short> lstServices = await componentgeotiffserviceLocalServiceUtil.Instance.getAllShortAsync();

                lstGroup = lstGroup.Where(x => x.parent_id != null).ToList();
                lstServices.Where(x => x.type == "station").ToList().ForEach(x => x.title = Resources.Map.timeslider_station);

                Repositories.Models.map_model_view mapModelView = new Repositories.Models.map_model_view
                {
                    MapScale = lstMapScale,
                    VNAQIIndex = lstAQIIndexShort,
                    Layers = lstGroup,
                    Services = lstServices,
                    isAuthenticate = true
                    //PinnedLocation = lstPinnedLocationDefault
                };

                return View(mapModelView);
            }
            catch (Exception ex)
            {
                String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Apom_User.Common.Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                return RedirectToAction("Error400", "Bug");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        #region mobile
        private async Task truncateLayer_Extent(string geoserverURL, string endpoint, string authUID, string authPwd, string layerName, string srsName, Boundary boundary)
        {
            try
            
            {
                Console.WriteLine("truncate layer by extent...");
                HttpClient client = new HttpClient();

                string requestURL = String.Concat(geoserverURL, endpoint, @"/", layerName, ".json");
                Console.WriteLine("requestURL: " + requestURL);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, requestURL);

                string auth = String.Concat(authUID, ":", authPwd);
                Console.WriteLine("auth: " + auth);
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(auth)));

                Random rnd = new Random();
                long threadCount = rnd.Next(1, 128);
                double xMin = boundary.XAxis.min;
                double xMax = boundary.XAxis.max;
                double yMin = boundary.YXis.min;
                double yMax = boundary.YXis.max;
                string contentRequest = "{'seedRequest':{'name':'" + layerName + "','bounds':{'coords':{ 'double':['" + xMin + "','" + yMin + "','" + xMax + "','" + yMax + "']}},'srs':{'number':" + srsName + "},'zoomStart':0,'zoomStop':30,'format':'application\\/vnd.mapbox-vector-tile','type':'truncate','threadCount':" + threadCount + "}}";
                request.Content = new StringContent(contentRequest);
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                Console.WriteLine("request.Content: " + contentRequest);

                HttpResponseMessage response = await client.SendAsync(request);
                var httpResponse = response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                Console.WriteLine("truncate layer by extent successfull!");
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync(ex.ToString());
            }
        }

        public async Task<ActionResult> Mobile()
        {
            ViewBag.use_map = true;
            ViewBag.use_chart = true;
            try
            {
                //curl - v - u admin: geoserver - XPOST - H "Content-type: application/json" - d "{'seedRequest':{'name':'hydroalp:gis_a_station_days_aqi_pm25','bounds':{'coords':{ 'double':['90','8','130','30']}},'srs':{'number':4326},'zoomStart':0,'zoomStop':30,'format':'application\\/vnd.mapbox-vector-tile','type':'truncate','threadCount':81}}" http://160.250.65.166:8060/geoserver/gwc/rest/seed/hydroalp:gis_a_station_days_aqi_pm25.json
                //string geoserverURL = "http://160.250.65.166:8060/geoserver";
                //string endpoint = "/gwc/rest/seed";
                //string authUID = "admin";
                //string authPwd= "geoserver";
                //string layerName = "hydroalp:gis_a_station_days_aqi_pm25";
                //var boundary = new Boundary
                //{
                //    XAxis = new Boundary.MinMax
                //    {
                //        min = 90,
                //        max = 130
                //    },
                //    YXis = new Boundary.MinMax
                //    {
                //        min = 8,
                //        max = 30
                //    }
                //};
                //await this.truncateLayer_Extent(geoserverURL, endpoint, authUID, authPwd, layerName, "4326", boundary);
                setURLCookie(true, false, Constant.WebConfig.PROVINCE_DEFAULT);
                //List<group_short> lstGroup = await groupLocalServiceUtil.Instance.buildVerticalMenuFromGroupsAsync(Common.SessionContext.Language);
                //List<map_scale> lstMapScale = await mapscaleLocalServiceUtil.Instance.getAllAsync();
                List<vnaqi_index_short> lstAQIIndexShort = await vnaqiindexLocalServiceUtil.Instance.getInfoShortAsync(Common.SessionContext.Language);
                //List<component_geotiff_service_short> lstServices = await componentgeotiffserviceLocalServiceUtil.Instance.getAllShortAsync();

                //lstGroup = lstGroup.Where(x => x.parent_id != null).ToList();
                //lstServices = lstServices.Where(x => x.type != "station").ToList();

                Repositories.Models.map_model_view mapModelView = new Repositories.Models.map_model_view
                {
                    //MapScale = lstMapScale,
                    VNAQIIndex = lstAQIIndexShort,
                    //Layers = lstGroup,
                    //Services = lstServices,
                    isAuthenticate = true
                };

                return View(mapModelView);
            }
            catch (Exception ex)
            {
                String functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Apom_User.Common.Log.LOG4NET.Error("ERROR " + functionName + ": " + ex.Message);
                return RedirectToAction("Error400", "Bug");
            }
        }
        #endregion


        //
        // POST: /Account/Login
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [Apom_User.Common.ActionFilter.CustomAuthenticationFilter]
        public async Task<ActionResult> changeNotification(bool notify)
        {
            try
            {
                if (SessionContext.UserProfile == null)
                {
                    return ToJson(new Repositories.Models.jsonResponse
                    {
                        Code = System.Net.HttpStatusCode.BadRequest,
                        Message = ""
                    });
                }
                #region change setting notify of current user
                Apom_User.Models.DataApomAppConnection db = new Models.DataApomAppConnection();
                string userId = SessionContext.UserProfile.userid;
                Repositories.Models.Administrative.user user = await Repositories.Services.Administrative.userLocalServiceUtil.Instance.getUserAsync(userId);
                user.is_get_notify = notify;
                await Repositories.Services.Administrative.userLocalServiceUtil.Instance.updateAsync(db, user);
                Apom_User.Common.SessionContext.UserProfile.is_get_notify = notify;
                #endregion

                return ToJson(new Repositories.Models.jsonResponse
                {
                    Code = System.Net.HttpStatusCode.OK
                });
            }
            catch (Exception ex)
            {
                return ToJson(new Repositories.Models.jsonResponse
                {
                    Code = System.Net.HttpStatusCode.BadRequest,
                    Message = ex.Message
                });
            }
        }
    }
}