﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Apom_User
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;

            //routes.MapMvcAttributeRoutes();

            //routes.MapRoute(
            //    name: "Account",
            //    url: "admin/{action}/{id}",
            //    defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "account",
                url: "account/{action}/{id}",
                defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "about",
                url: "about_us/{id}",
                defaults: new { controller = "About", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "news",
                url: "news/{action}/{id}",
                defaults: new { controller = "News", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "dashboard",
            //    url: "dashboard/{id}",
            //    defaults: new { controller = "Analysis", action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "user_profile",
                url: "profile/{id}",
                defaults: new { controller = "User", action = "UserInfo", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "user_list",
                url: "list_users/{id}",
                defaults: new { controller = "User", action = "UserList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "user",
                url: "admin/{action}/{id}",
                defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                //defaults: new { controller = "User", action = "UserList", id = UrlParameter.Optional }
            );
        }
    }
}
