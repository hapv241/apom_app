﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

using Repositories.Models.Administrative;
using Repositories.Models.Notification;
using Repositories.Services.Administrative;
using Repositories.Services.Notification;

using Apom_User.Common;

namespace Apom_User
{
    [HubName("NotificationApomHub")]
    public class NotificationHub : Hub
    {
        //private static readonly ConcurrentDictionary<string, UserHubModels> Users = new ConcurrentDictionary<string, UserHubModels>(StringComparer.InvariantCultureIgnoreCase);

        #region private 
        private string LoadNotifData(string userId, bool is_vietnam)
        {
            int total = notificationuserLocalServiceUtil.Instance.totalNotification(userId, is_vietnam);
            return total == 0 ? "" : total.ToString();
        }
        private async Task<string> LoadNotifDataAsync(string userId, bool is_vietnam)
        {
            int total = await notificationuserLocalServiceUtil.Instance.totalNotificationAsync(userId, is_vietnam);
            return total == 0 ? "" : total.ToString();
        }

        private bool updateReminder(string userId, bool isReminder, bool is_vietnam)
        {
            return notificationuserLocalServiceUtil.Instance.updateReminder(userId, isReminder, is_vietnam);
        }

        private async Task<List<notif_info>> LoadNotifDetailLatestAsync(string userId, bool is_vietnam, string langId)
        {
            List<notif_info> lst = await notificationuserLocalServiceUtil.Instance.getNotificationsIsReadAsync(userId, is_vietnam, langId, 1, Constant.WebConfig.NUMBER_IN_PAGE);
            return lst;
        }
        #endregion

        public static void Send()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            context.Clients.All.displayStatus();
        }

        //Logged Use Call
        public async Task getNotificationDetailLatest(bool is_vietnam)
        {
            try
            {
                if (HttpContext.Current != null)
                {
                    var hubCookie = HttpContext.Current.Request.Cookies[Common.Constant.WebConfig.COOKIE_HUB];
                    if (!String.IsNullOrEmpty(hubCookie.Value))
                    {
                        IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

                        string loggedUser = hubCookie.Values[Common.Constant.WebConfig.COOKIE_HUB];
                        var langId = HttpContext.Current.Request.Cookies[Constant.WebConfig.LANGUAGE] != null ? HttpContext.Current.Request.Cookies[Constant.WebConfig.LANGUAGE].Value : "";

                        List<notif_info> lstNotifInfoLatest = await this.LoadNotifDetailLatestAsync(loggedUser, is_vietnam, langId);

                        // set isReminder = false
                        if (lstNotifInfoLatest.Count > 0) this.updateReminder(loggedUser, false, is_vietnam);

                        //context.Clients.All.broadcaastNotif(totalNotif);
                        //user u = userLocalServiceUtil.Instance.getUser(loggedUser);

                        Common.Constant.user_hub uh = Common.Constant.USER_HUB.Find(x => x.user_id == loggedUser);
                        if (uh != null)
                        {
                            var cid = uh.ConnectionIds.FirstOrDefault();
                            context.Clients.Client(cid).broadcaastNotifDetailLatest(lstNotifInfoLatest);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void getNotification(bool is_vietnam)
        {
            try
            {
                if (HttpContext.Current != null)
                {
                    var hubCookie = HttpContext.Current.Request.Cookies[Common.Constant.WebConfig.COOKIE_HUB];
                    if (!String.IsNullOrEmpty(hubCookie.Value))
                    {
                        IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

                        string totalNotif = "";
                        string loggedUser = hubCookie.Values[Common.Constant.WebConfig.COOKIE_HUB];

                        //Get TotalNotification
                        //Random rnd = new Random();
                        //totalNotif = rnd.Next(1, 100).ToString();
                        totalNotif = this.LoadNotifData(loggedUser, is_vietnam);

                        //context.Clients.All.broadcaastNotif(totalNotif);
                        //user u = userLocalServiceUtil.Instance.getUser(loggedUser);

                        Common.Constant.user_hub uh = Common.Constant.USER_HUB.Find(x => x.user_id == loggedUser);
                        if (uh != null)
                        {
                            var cid = uh.ConnectionIds.FirstOrDefault();
                            context.Clients.Client(cid).broadcaastNotif(totalNotif);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void SendNotification(string sendTo, bool is_vietnam)
        {
            try
            {
                IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

                //Get TotalNotification
                string totalNotif = LoadNotifData(sendTo, is_vietnam);

                //Get TotalNotification
                Random rnd = new Random();
                totalNotif = this.LoadNotifData(sendTo, is_vietnam);

                //context.Clients.All.broadcaastNotif(totalNotif);
                //user u = userLocalServiceUtil.Instance.getUser(loggedUser);

                Common.Constant.user_hub uh = Common.Constant.USER_HUB.Find(x => x.user_id == sendTo);
                if (uh != null)
                {
                    var cid = uh.ConnectionIds.FirstOrDefault();
                    context.Clients.Client(cid).broadcaastNotif(totalNotif);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public async Task SendNotificationAsync(string sendTo, bool is_vietnam)
        {
            try
            {
                IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

                //Get TotalNotification
                Random rnd = new Random();
                string totalNotif = await this.LoadNotifDataAsync(sendTo, is_vietnam);

                //context.Clients.All.broadcaastNotif(totalNotif);
                //user u = userLocalServiceUtil.Instance.getUser(loggedUser);

                Common.Constant.user_hub uh = Common.Constant.USER_HUB.Find(x => x.user_id == sendTo);
                if (uh != null)
                {
                    var cid = uh.ConnectionIds.FirstOrDefault();
                    context.Clients.Client(cid).broadcaastNotif(totalNotif);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void Hello()
        {
            Clients.All.hello();
        }

        public override Task OnConnected()
        {
            if (HttpContext.Current != null)
            {
                var hubCookie = HttpContext.Current.Request.Cookies[Common.Constant.WebConfig.COOKIE_HUB];
                if (!String.IsNullOrEmpty(hubCookie.Value))
                {
                    string userId = hubCookie.Values[Common.Constant.WebConfig.COOKIE_HUB];
                    string connectionId = Context.ConnectionId;

                    //user u = userLocalServiceUtil.Instance.getUser(userId);
                    //if (u == null)
                    //{
                    //    u = new user
                    //    {
                    //        userid = userId
                    //    };
                    //}

                    Common.Constant.user_hub uh = Common.Constant.USER_HUB.Find(x => x.user_id == userId);
                    if (uh == null)
                    {
                        uh = new Common.Constant.user_hub
                        {
                            user_id = userId,
                            device_tokens = connectionId
                        };
                        Common.Constant.USER_HUB.Add(uh);
                    }
                    else
                    {
                        uh.device_tokens = String.IsNullOrEmpty(uh.device_tokens) ? connectionId : ";" + connectionId;
                    }

                    lock (uh.ConnectionIds)
                    {
                        //if (!u.ConnectionIds.Contains(connectionId))
                        //{
                        //    u.ConnectionIds.Add(connectionId);
                        //    u.device_tokens = String.IsNullOrEmpty(u.device_tokens) ? connectionId : connectionId;
                        //    Apom_User.Models.DataApomAppConnection db = new Models.DataApomAppConnection();
                        //    userLocalServiceUtil.Instance.update(db, u);
                        //}
                        if (uh.ConnectionIds.Count == 1)
                        {
                            Clients.Others.userConnected(uh.user_id);
                        }
                    }
                }
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var hubCookie = Context.Request.Cookies[Common.Constant.WebConfig.COOKIE_HUB];
            if (!String.IsNullOrEmpty(hubCookie.Value))
            {
                string[] arr = hubCookie.Value.Split('=');
                string userId = arr[1];
                string connectionId = Context.ConnectionId;
                //user u = userLocalServiceUtil.Instance.getUser(userId);

                Common.Constant.user_hub uh = Common.Constant.USER_HUB.Find(x => x.user_id == userId);
                if (uh != null)
                {
                    lock (uh.ConnectionIds)
                    {
                        HashSet<string> h = uh.ConnectionIds;
                        h.RemoveWhere(cid => cid.Equals(connectionId));
                        uh.ConnectionIds = h;
                        if (!uh.ConnectionIds.Any())
                        {
                            Common.Constant.USER_HUB.Remove(uh);
                            Clients.Others.userDisconnected(uh.user_id);
                        }
                    }
                }
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}