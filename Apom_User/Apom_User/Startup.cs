﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Apom_User.Startup))]
namespace Apom_User
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
