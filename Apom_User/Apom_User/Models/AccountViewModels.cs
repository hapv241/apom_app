﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Apom_User.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel : Repositories.Models.basemodel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string user_email { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string user_password { get; set; }

        [Display(Name = "Remember me?")]
        public string user_rememberme { get; set; }
        public string redirect_url { get; set; } = "";
    }

    public class RegisterViewModel:Repositories.Models.basemodel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "User name")]
        public string user_name {get;set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First name")]
        public string user_firstname { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Middle name")]
        public string user_middlename { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Last name")]
        public string user_lastname { get; set; }

        [Display(Name = "Birthday")]
        [DataType(DataType.DateTime)]
        public DateTime? user_birthday { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Sex")]
        public string user_sex { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Phone number")]
        public string user_phonenumber { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Company/Organization name")]
        public string user_companyname { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string user_email { get; set; }

        [Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string user_password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("user_password", ErrorMessage = "The password and confirmation password do not match.")]
        public string user_confirmpassword { get; set; }
        
        public bool expired_register { get; set; }
        public string redirect_url { get; set; } = "";
    }

    public class ResetPasswordViewModel : Repositories.Models.basemodel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string user_email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string user_password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("user_password", ErrorMessage = "The password and confirmation password do not match.")]
        public string user_confirmpassword { get; set; }

        public string Code { get; set; }

        public bool expired_reset_password { get; set; }

        public bool request_reset_password { get; set; }
    }

    public class ForgotPasswordViewModel:Repositories.Models.basemodel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string user_email { get; set; }
    }
    
}
