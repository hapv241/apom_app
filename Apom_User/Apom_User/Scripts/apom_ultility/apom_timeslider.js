﻿//class apomTimeSlider extends apomUltility {
//    constructor(options) {
//        super(options);

//        this.id = options.id ? options.id : 'time_slider';
//        this.callbackOnChange = options.callbackOnChange ? options.callbackOnChange : null;
//        this.showUTC = options.showUTC === undefined ? true : options.showUTC;
//        this.showHourFormat = options.showHourFormat === undefined ? true : options.showHourFormat;
//        this.myDateFormat = options.myDateFormat ? options.myDateFormat : 'MM-DD-YYYY';
//        this.playBtn = options.playBtn ? options.playBtn : '';
//        this.previousBtn = options.previousBtn ? options.previousBtn : '';
//        this.nextBtn = options.nextBtn ? options.nextBtn : '';
//        this.minTime = options.minTime ? options.minTime : 0;
//        this.maxTime = options.maxTime ? options.maxTime : 0;
//        this.from = options.from ? options.from : 0;
//        this.to = options.to ? options.to : 24;
//        this.step = options.step ? options.step : 1;
//        this.grid = options.grid === undefined ? true : options.grid;
//        this.showMinorTicks = options.showMinorTicks === undefined ? true : options.showMinorTicks;
//        this.mousewheel = options.mousewheel === undefined ? true : options.mousewheel;
//        this.currentTime = null;
//        this.intervalPlay = null;
//        this.timerInterval = options.timerInterval ? options.timerInterval : 1000;
//        this.typeSlider = options.typeSlider ? options.typeSlider : '';

//        this.classNamePlayBtnRun = 'fa-play-circle-o';
//        this.classNamePlayBtnPause = 'fa-pause-circle-o';

//        var _this = this;
//        $("#" + this.id).timeSlider({
//            onChange: function (result) {
//                //console.log('1:onChange',result.valueMoment.toISOString());
//                // start time
//                _this.currentTime = result.valueMoment._d.getTime();
//                if (_this.callbackOnChange) _this.callbackOnChange(_this.currentTime);
//            },
//            format: {
//                showUTC: this.showUTC,
//                showHourFormat: this.showHourFormat,
//                myDateFormat: this.myDateFormat
//            },
//            buttons: {
//                from: { previousBtn: this.previousBtn, nextBtn: this.nextBtn }
//            },
//            min: this.minTime, // numDaysDaily pre-days 
//            max: this.maxTime,
//            from: this.from,
//            to: this.to,
//            step: this.step,
//            grid: this.grid,
//            showMinorTicks: this.showMinorTicks,
//            mousewheel: this.mousewheel
//        }).data('timeSlider');

//        if (this.playBtn != '' && this.nextBtn != '') {
//            $('#' + this.playBtn).click(function () {
//                var $spanObj = $(this).find('span');
//                if ($spanObj.hasClass(_this.classNamePlayBtnRun)) {
//                    $spanObj.removeClass(_this.classNamePlayBtnRun).addClass(_this.classNamePlayBtnPause);
//                    _this.intervalPlay = setInterval(function () {
//                        $('#' + _this.nextBtn).click();
//                    }, _this.timerInterval);
//                }
//                else {
//                    $spanObj.removeClass(_this.classNamePlayBtnPause).addClass(_this.classNamePlayBtnRun);
//                    clearInterval(_this.intervalPlay);
//                    _this.intervalPlay = null;
//                }
//            });
//        }
//    }
//}

const TICK_HEIGHT = 6;
const TICK_WIDTH = 2;
const AXIS_COLOR = 'transparent';
const TICK_COLOR = "white";
const TIME_STEP = 24 * 60 * 60 * 1000; // 1d
function localeTimeString(locale, d) {
    if (locale === 'vi-VN') {
        return `${('0' + d.getDate()).slice(-2)}-${('0' + (d.getMonth() + 1)).slice(-2)}-${d.getFullYear()}`;
    }
    return d.toLocaleString(locale, { dateStyle: "medium" });
}
class apomTimeSlider extends apomUltility {
    constructor(options) {
        super(options);

        this.elm = null;
        this.callbacks = {};
        this.timer = null;
        this.current = new Date().getTime();
        this.from = 0;
        this.to = 0;
        this.timeInterval = 2000;
        this.noTicks = 15;
        this.transformFn = null;
        this.noTick_A_Day = 24 * 60 * 60 * 1000; // 86400000
        this.runPlay = false;
        this.ticks = null;
    }

    _localeTimeString = function (locale, d) {
        if (locale === 'vi-VN') {
            return `${('0' + d.getDate()).slice(-2)}-${('0' + (d.getMonth() + 1)).slice(-2)}-${d.getFullYear()}`;
        }
        return d.toLocaleString(locale, { dateStyle: "medium" });
    }

    // must be contain now date 
    _generateTicks = function (noTicks) {
        var ticks = [];
        var dateFrom = new Date(this.from);
        var dateTo = new Date(this.to);
        var dateTimeSlider = new Date(this.current); // time current on time-slider
        var dateNow = new Date();
        var nowTime = dateNow.getTime();
        var isTime_Now_Slider = dateNow - dateTimeSlider;
        var isSameDate = Math.abs(dateNow - dateTimeSlider) < this.noTick_A_Day ? true : false;

        var prevDays = apomGis.Config.timeslider.prev_current;
        var nextDays = apomGis.Config.timeslider.next_currrent;
        var totalDate = prevDays + nextDays + 1;
        var noTick_Left = 0, noTick_Middle = 0, noTick_Right = 0;
        var step_Left = 0, step_Middle = 0, step_Right = 0;
        var tick_Left = 0, tick_Middle = 0, tick_Right = 0;
        if (isSameDate) {
            noTick_Left = parseInt(prevDays * noTicks / totalDate);
            noTick_Right = Math.ceil(nextDays * noTicks / totalDate);
            while (noTick_Left + noTick_Right + 1 > noTicks) { // isSameDate -> +1 
                noTick_Left = noTick_Left - 1;
            }
            step_Left = prevDays / noTick_Left;
            step_Right = nextDays / noTick_Right;
            tick_Left = step_Left * this.noTick_A_Day;
            tick_Right = step_Right * this.noTick_A_Day;

            var index = 0;
            while (index < noTick_Left) {
                var _tick = this.from + tick_Left * index;
                var _dateTick = new Date(_tick);
                ticks.push(_dateTick);
                index++;
            }
            index = 0;
            noTick_Right = noTick_Right + 1; // +1 for current_date on time_slider
            while (index < noTick_Right) {
                var _tick = this.current + tick_Right * index;
                var _dateTick = new Date(_tick);
                ticks.push(_dateTick);
                index++;
            }
        }
        else {
            var daysLeft = parseInt((dateTimeSlider - dateFrom) / this.noTick_A_Day);
            var daysMiddle = isTime_Now_Slider > 0 ? parseInt((dateNow - dateTimeSlider) / this.noTick_A_Day) : parseInt((dateTimeSlider - dateNow) / this.noTick_A_Day);
            var daysRight = Math.ceil((dateTo - dateNow) / this.noTick_A_Day);
            while (daysLeft + daysMiddle + daysRight < totalDate) {
                daysLeft = daysLeft + 1;
            }
            noTick_Left = parseInt(daysLeft * noTicks / totalDate);
            noTick_Middle = Math.ceil(daysMiddle * noTicks / totalDate);
            noTick_Right = Math.ceil(daysRight * noTicks / totalDate);

            while (noTick_Left + noTick_Middle + noTick_Right > noTicks) {
                noTick_Left = noTick_Left - 1;
            }
            step_Left = daysLeft / noTick_Left;
            step_Middle = daysMiddle / noTick_Middle;
            step_Right = daysRight / noTick_Right;
            tick_Left = step_Left * this.noTick_A_Day;
            tick_Middle = step_Middle * this.noTick_A_Day;
            tick_Right = step_Right * this.noTick_A_Day;

            var index = 0;
            while (index < noTick_Left) {
                var _tick = this.from + tick_Left * index;
                var _dateTick = new Date(_tick);
                ticks.push(_dateTick);
                index++;
            }
            index = 0;
            while (index < noTick_Middle) {
                var _tick = isTime_Now_Slider > 0 ? this.current + tick_Middle * index : nowTime + tick_Middle * index;
                var _dateTick = new Date(_tick);
                ticks.push(_dateTick);
                index++;
            }
            index = 0;
            while (index < noTick_Right) {
                var _tick = isTime_Now_Slider > 0 ? nowTime + tick_Middle * index : this.current + tick_Middle * index;
                var _dateTick = new Date(_tick);
                ticks.push(_dateTick);
                index++;
            }

        }


        //var step = nextDays <= 3 ? 3 : parseInt(3 + (nextDays - 3) / 2);
        //var _dateTo = dateTimeSlider.addDays(step);
        //var subTick = _dateTo - dateTo;
        //while (subTick < this.noTick_A_Day) {
        //    _dateTo = _dateTo.addDays(step);
        //    subTick = _dateTo - dateTo;
        //}
        //dateTo = _dateTo.addDays(step * -1);
        //ticks.push(dateTo);

        //var _dateFrom = dateTo.addDays(step * -1);
        //var subTick = _dateFrom - dateFrom;
        //while (subTick >= this.noTick_A_Day) {
        //    ticks.push(_dateFrom);
        //    _dateFrom = _dateFrom.addDays(step * -1);
        //    subTick = _dateFrom - dateFrom;
        //}
        //dateFrom = _dateFrom;
        //ticks = ticks.reverse();
        return ticks;
    }

    _markCurrentTime = function () {
        var currentDate = localeTimeString(this._locale(), new Date()); // new moment().format('DD-MM-YYYY');
        for (var i = 0; i < $('g text').length; i++) {
            var _textDate = $($('g text')[i]).text();
            if (currentDate === _textDate) {
                $($('g text')[i]).attr('fill', '#f00');
                $($('g text')[i]).css('fontWeight', 'bolder');
                $($('g line')[i]).attr('stroke-width', 20);
                $($('g line')[i]).attr('stroke', '#ff0');
                break;
            }
        }
    }

    init(elm, from, to, current, noTicks) {
        this.elm = elm;
        this.current = +current;
        this.noTicks = noTicks
        this.from = +from;
        this.to = +to;

        let p = d3.select(this.elm);
        let button = p.append("div");
        let timeInterval = this.timeInterval;
        button.attr("class", "play-btn").on('click', () => {
            if (this.timer) this.pause();
            else this.play(timeInterval, TIME_STEP);
        });
        button.html(`<div class='icon'>
                    <span class='font-icon-play'></span>
                </div>`);
        let slider = p.append('div');
        slider.attr("class", 'slider');
        slider.append('div').attr('class', 'slider-bg');
        let steps = slider.append('div').attr('class', 'slider-step');
        steps.on('click', (evt) => {
            this.current = +this.transformFn.invert(evt.offsetX);
            // round hour = 0, second = 0
            let current = new Date(this.current);
            current = new Date(current.getFullYear(), current.getMonth(), current.getDate(), current.getHours(), 0, 0);
            this.current = current.getTime();

            this.update();
        });
        let cuePoint = slider.append('div').attr('class', 'slider-run');
        let timeLabel = cuePoint.append('div').attr('class', 'point-drag').append('div').attr('class', 'time-label');
        let dragger = d3.drag().on("start", (evt) => { console.log('started', evt) })
            .on("drag", (evt) => cuePoint.style('width', evt.x))
            .on("end", evt => console.log("end", evt));
        cuePoint.select('.point-drag').call(dragger);
        console.log(steps.node().clientWidth)
        let svg = steps.append('svg').attr('width', steps.node().clientWidth)
            .attr('height', '20px')
            .style('overflow', 'visible');
        new ResizeObserver(() => { this.update() }).observe(elm)
        this.update();
    }

    currentDate() {
        let currentDate = moment(new Date(this.current)).format('YYYY-MM-DD HH:mm:ss');
        return currentDate;
    }

    _language() {
        if (this.language) return this.language;
        let name = "language=";
        let ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            c = c.trim()
            if (c.indexOf(name) == 0) {
                this.language = c.substring(name.length, c.length);
                return this.language;
            }
        }
        console.log("Dont know lang");
    }
    _locale() {
        switch (this._language()) {
            case "vi":
                return "vi-VN";
            default:
                return "en-US";
        }
    }

    update() {
        console.log("Update slider");
        let slider = d3.select('.slider');
        let steps = d3.select('.slider-step');
        let button = d3.select('.play-btn span');
        if (this.timer === null) {
            button.attr('class', 'font-icon-play');
        }
        else {
            button.attr('class', 'font-icon-pause');
        }
        steps.selectAll('g').remove();
        let cuePoint = slider.select('.slider-run');
        let timeLabel = slider.select('.time-label');

        let sF = d3.scaleTime().domain([this.from, this.to]).range([0, steps.node().clientWidth]);
        this.transformFn = sF;
        //this.ticks = sF.ticks(this.noTicks);
        this.ticks = this.ticks === null ? this._generateTicks(this.noTicks) : this.ticks;
        //this.runPlay ? this.ticks : this._generateTicks(this.noTicks);
        let locale = this._locale();

        let _this = this;
        let axis = d3.axisBottom().scale(sF).tickValues(this.ticks).tickSize(TICK_HEIGHT).tickFormat(function (d) {
            return localeTimeString(locale, d)
            //return d.toLocaleString(locale, { dateStyle: "medium" });
        });
        let svg = steps.select('svg');
        svg.append('g').call(axis);
        svg.selectAll('.tick text').attr('fill', function (d) {
            let date = new Date(d);
            if (date.getHours() === 0) return '#000'
            return '#00000080';
        });
        svg.selectAll('.tick text').attr('dy', 12);
        svg.selectAll('.domain').attr('stroke', AXIS_COLOR);
        svg.selectAll('.tick line').attr('stroke', TICK_COLOR);
        svg.selectAll('.tick line').attr('stroke-width', TICK_WIDTH);

        // label current date
        timeLabel.text(new Date(this.current).toLocaleString(locale));
        cuePoint.style('width', sF(this.current) + 'px');
        for (let cb of (this.callbacks['update'] || [])) {
            cb(this.current);
        }

        this._markCurrentTime();
    }

    play(delay, timeStep) {
        console.log("play");
        this.runPlay = true;
        let doIt = () => {
            if (this.current + timeStep > this.to) {
                this.current = this.to;
                clearTimeout(this.timer);
                this.timer = null;
            }
            else {
                this.current += timeStep;
            }
            this.update();
        }
        this.timer = setInterval(doIt, delay);
        doIt();
    }

    toggle() {
        if (this.timer) {
            this.pause();
        }
        else this.play();
    }

    pause() {
        console.log("pause");
        clearTimeout(this.timer);
        this.timer = null;
        this.update();
        this.runPlay = false;
    }

    on(eventName, fn) {
        this.callbacks[eventName] = this.callbacks[eventName] || [];
        this.callbacks[eventName].push(fn);
    }
};
