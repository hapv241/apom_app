﻿class apomChart extends apomUltility {
    constructor(options) {
        super(options);

        const langId = this.options.lang_id ?? 'vi';

        const timezone = new Date().getTimezoneOffset()

        Highcharts.setOptions({
            lang: {
                downloadJPEG: langId === 'vi' ? 'Tải ảnh JPEG' : 'Download JPEG image',
                downloadPNG: langId === 'vi' ? 'Tải ảnh PNG' : 'Download PNG image',
            },
            global: {
                timezoneOffset: timezone
            }
        });
        this.chart = null;
    }

    generateChart(opt_options) {
        var options = opt_options ? opt_options : [];
        var id = options.id;
        var typeChart = options.typeChart ? options.typeChart : 'spline';
        var enableCredits = options.enableCredits === undefined ? true : options.enableCredits;
        var tooltip = options.tooltip ? options.tooltip : {};
        var xAsis = options.xAsis ? options.xAsis : [];
        var yAxis = options.yAxis ? options.yAxis : [];
        var series = options.series ? options.series : [];
        series[0].marker = {
            symbol: 'square',
            //symbol: 'url(https://www.highcharts.com/samples/graphics/sun.png)',
            //radius: 6
        };

        // seting icon for now
        var indexNow = -1;
        var _data = series[0].data;
        for (var i = 0; i < _data.length; i++) {
            var timeInt = _data[i][0];
            var timeDate = new Date(timeInt);
            var date = timeDate.getDate();
            var month = timeDate.getMonth() + 1;
            var year = timeDate.getFullYear();

            var nowDate = new Date();
            var dateNow = nowDate.getDate();
            var monthNow = nowDate.getMonth() + 1;
            var yearNow = nowDate.getFullYear();
            if (date === dateNow && month === monthNow && year === yearNow) {
                indexNow = i;
                break;
            }
        }
        if (indexNow != -1) {
            var symbolIcon = 'url(https://www.highcharts.com/samples/graphics/sun.png)';
            symbolIcon = 'circle';
            var _dataNow = series[0].data[indexNow];
            series[0].data[indexNow] = {
                x: _dataNow[0],
                y: _dataNow[1],
                marker: {
                    symbol: symbolIcon,
                    radius: 8,
                    fillColor: 'red',
                }
            };
        }

        var enableExport = options.enableExport === undefined ? true : options.enableExport;
        var title = options.title ? options.title : '';
        var subTitle = options.subTitle ? options.subTitle : '';
        var legend = options.legend ? options.legend : {
            align: 'center',
            verticalAlign: 'bottom',
            y: -25
        };
        var menuITemsDefault = ["printChart",
            "separator",
            "downloadPNG",
            "downloadJPEG",
            "downloadPDF",
            "downloadSVG",
            "separator",
            "downloadCSV",
            "downloadXLS",
            "viewData",
            "openInCloud"];
        var menuItems = !enableExport ? [] : (options.menuItemsExport ? options.menuItemsExport : ["downloadPNG",
            "downloadJPEG",
            //"separator",
            //"downloadCSV",
            //"downloadXLS",
            //"viewData",
            "openInCloud"]);

        $('#' + id).highcharts({
            chart: {
                style: {
                    fontFamily: 'Poppins'
                },
                type: typeChart,
                zoomType: 'x',
                events: {
                    /*load: function () {
                        var label = this.renderer.label("Graph dates and times are in Indochina Time(ICT)")
                        .css({
                            width: '400px',
                            fontSize: '12px'
                        })
                        .attr({
                            'stroke': 'silver',
                            'stroke-width': 1,
                            'r': 2,
                            'padding': 5
                        })
                        .add();

                        label.align(Highcharts.extend(label.getBBox(), {
                            align: 'center',
                            x: 20, // offset
                            verticalAlign: 'bottom',
                            y: 0 // offset
                        }), null, 'spacingBox');

                    }*/
                },
                //paddingBottom: 50
            },
            credits: {
                enabled: enableCredits
            },
            tooltip: tooltip,
            title: {
                text: title,
                style: {
                    fontSize: '14px'
                }
            },
            subtitle: {
                text: subTitle,
                style: {
                    fontSize: '12px'
                }
            },
            xAxis: {
                type: xAsis.type ? xAsis.type : 'linear', // linear, logarithmic, datetime, category
                labels: {
                    /*
                    millisecond: '%H:%M:%S.%L',
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%e. %b',
                    week: '%e. %b',
                    month: '%b \'%y',
                    year: '%Y'
                    */
                    format: xAsis.format ? xAsis.format : '',
                    rotation: xAsis.rotation ? xAsis.rotation : 0,
                    // align: 'left'
                },
                title: {
                    text: xAsis.text ? xAsis.text : ''
                },
                minRange: xAsis.minRange || 5 * xAsis.tickInterval,
                tickInterval: xAsis.tickInterval ? xAsis.tickInterval : 1,
                //formatter: function () {
                //    //return Highcharts.dateFormat('%H:%M %p<br>%m-%d %a', this.value);
                //    return Highcharts.dateFormat('%H:%M<br>%b-%d-%y', this.value);
                //}
            },
            legend: legend,
            yAxis: {
                title: {
                    useHTML: true,
                    text: yAxis.text ? yAxis.text : ''
                },
                plotBands: yAxis.plotBands ? yAxis.plotBands : [],

            },
            plotOptions: {
                series: {
                    //color: "#2b5154",
                    //borderWidth: 0,
                    //dataLabels: {
                    //    enabled: true,
                    //    format: '{y}' //'{point.y:.0f}'
                    //},
                    //point: {
                    //    events: {
                    //        click: function (event) {
                    //            if (callbackClick != undefined) callbackClick(this.options.name, this.options.y);
                    //        }
                    //    }
                    //}
                },
            },
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: menuItems
                    }
                },
                //enabled: false
                enabled: enableExport
            },
            series: series

        });
    }



    generateChartDrilldown(opt_options) {
        var options = opt_options ? opt_options : [];
        var id = options.id;
        var title = options.title ? options.title : '';
        var subtitle = options.subtitle ? options.subtitle : '';
        var typeChart = options.typeChart ? options.typeChart : 'column';
        var enableCredits = options.enableCredits === undefined ? true : options.enableCredits;
        var tooltip = options.tooltip ? options.tooltip : {};
        var xAsis = options.xAsis ? options.xAsis : [];
        var yAxis = options.yAxis ? options.yAxis : [];
        var dataLabels = options.dataLabels ? options.dataLabels : {
            enabled: true,
            format: '{point.y:.1f}'
        };
        var series = options.series ? options.series : [];
        var drilldown = options.drilldown ? options.drilldown : {};
        var enableExport = options.enableExport === undefined ? true : options.enableExport;
        var callbackClick = options.callbackClick ? options.callbackClick : undefined;

        this.chart = $('#' + id).highcharts({
            chart: {
                type: typeChart
            },
            credits: {
                enabled: enableCredits
            },
            title: {
                align: 'left',
                text: title
            },
            subtitle: {
                align: 'left',
                text: subtitle
                //'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    useHTML: true,
                    text: yAxis.text ? yAxis.text : ''
                },
                plotBands: yAxis.plotBands ? yAxis.plotBands : [],

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: dataLabels,
                    point: {
                        events: {
                            click: function (event) {
                                if (callbackClick != undefined) callbackClick(this.options.name, this.options.y);
                            }
                        }
                    }
                },
            },
            exporting: { enabled: false },
            /*tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
            },*/
            tooltip: tooltip,

            series: series,

            drilldown: drilldown
        });
    }
};