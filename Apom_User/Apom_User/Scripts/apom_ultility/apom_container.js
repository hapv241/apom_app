﻿/*
container contain rows, no include GUI
each row: contain objects
each object: contain items
each item: obj - event - callback
*/
class apomContainer extends apomUltility {
    constructor(options) {
        super(options);

        this.container = [];
    }

    addRow(row) {
        this.container.push(row);
    }

    // using for switchery which can checked/unchecked
    createRow(objects) {
        var newRow = [];
        // add object
        for (var index in objects) {
            var obj = objects[index];
            newRow.push(obj);

            // reference: https://api.jquery.com/category/events/
            // register event for item
            if (obj.event) {
                if (obj.event === 'change') {
                    if (obj.callback) obj.obj[0].callback = obj.callback;
                    obj.obj.change(function () {
                        if ($(this).is(":checked")) {
                        }
                        if (this.callback) this.callback(this);
                    });
                }
                else {
                    // more action...
                }
            }
        }

        return newRow;
    };


    // using for div which toogle class 'active'
    createRowWithToogleClassActive(objects) {
        var newRow = [];
        // add object
        for (var index in objects) {
            var obj = objects[index];
            newRow.push(obj);

            // reference: https://api.jquery.com/category/events/
            // register event for item
            if (obj.event) {
                if (obj.event === 'change') {
                    if (obj.callback) obj.obj[0].callback = obj.callback;
                    obj.obj.on('classChanged', function() {
                        if ($(this).hasClass("active")) {
                        }
                        if (this.callback) this.callback(this);
                    });
                }
                else {
                    // more action...
                }
            }
        }

        return newRow;
    }

}