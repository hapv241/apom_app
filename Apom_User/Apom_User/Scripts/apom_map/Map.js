﻿apomGis.MapClass = function (opt_options) {
    this.options = opt_options || {};
    this.target = this.options.mapid ? this.options.mapid : 'map';
    this.hostAPI = this.options.hostapi ? this.options.hostapi : '';
    this.langId = this.options.langid ? this.options.langid : 'vi';
    this.control = null;
    this.selectionFeatureVT = {};
    this.selectionLayerVT = null;
    this.graphicLayer = null;
    this.fitExtent = this.options.fitExtent ? this.options.fitExtent : apomGis.Config.initExtent.full;

    this.positionImpl = null;

    // toolbar
    this.toolbarImpl = null;
    this.styleImpl = null; // style map for layers

    // load lần đầu tiên sẽ lấy từ cookie
    this.loadExtentFromCookie = true;
    this.nameExtentCookie = this.options.cookie ? this.options.cookie : 'apom_map_current';
    this.nameIdentifyCookie = this.options.cookie ? this.options.cookie : 'apom_map_identify_coor';
    this.version = {
        cookie: '1.1',
        selectionset: '1.0',
        identify: '1.0',
    };

    /*
        history extent map
    */
    this.NavigationHistory = [];
    this.historyNow = -1; // do khi load ban do da thiet lap extent
    this.isSaveHistory = false; // khi button backExtent/nextExtent click thif se ko luu history

    this.coreImpl = null;
    this.destroy = function () {
        /*** destroy all control ***/

        // destroy map object
        this.control.setTarget(null);
        this.control = null;
    };

    // hủy sự kiện trên map
    this.destroyInteraction = function (interaction) {
        if (interaction == apomGis.Config.action.measure && apomGis.Params.map.Core.measureTool != null) {
            apomGis.Params.map.Core.measureTool.destroy();
        }
    };

    this.setProjectionMap = function (codeProjection, unit, axisOrientation) {
        var map = this.control;
        var code = codeProjection == undefined ? map.getView().getProjection().getCode() : codeProjection;
        var unitDefault = unit == undefined ? map.getView().getProjection().getUnits() : unit;
        //unitDefault = 'degrees';
        var projectionMap = new ol.proj.Projection({
            code: code,
            units: unitDefault,
            axisOrientation: axisOrientation, // default enu
            global: true
        });
        return projectionMap;
    }

    this.create = function (mapid, projection) {
        if (mapid != undefined) this.target = mapid;

        // apomGis.Config.projection: can duoc khai bao trong trang View
        this.projectionMap = this.setProjectionMap(apomGis.Config.projection.code ? apomGis.Config.projection.code : 'EPSG:3857', apomGis.Config.projection.unit ? apomGis.Config.projection.unit : 'm', apomGis.Config.projection.axisOrientation ? apomGis.Config.projection.axisOrientation : 'enu');

        var controls = ol.control.defaults({ rotate: false });
        //var interactions = ol.interaction.defaults({altShiftDragRotate:false, pinchRotate:false});

        this.control = new ol.Map({
            /*
            // only openlayer 6
            interactions: ol.interaction.defaults({ dragPan: false, mouseWheelZoom: false }).extend([
                new ol.interaction.DragPan({
                    condition: function (event) {
                        return this.getPointerCount() === 2 || ol.events.condition.platformModifierKeyOnly(event);
                    },
                }),
                new ol.interaction.MouseWheelZoom({
                    condition: ol.events.condition.platformModifierKeyOnly,
                })
            ]),
            */
            controls: ol.control.defaults({
                attribution: false,
                zoom: false,
                rotate: false
            }).extend([
                new ol.control.Attribution()
            ]),
            //interactions: interactions,
            target: this.target,
            view: new ol.View({
                projection: this.projectionMap.getCode(),
                //minZoom: 5,
                //extent: [8754233.226789042, 1625350.552260324, 14637184.375961807, 2058536.978921501]
            }),
            //renderer: 'webgl'
        });
        this.control.parent = this;

        // Improve user experience by loading tiles while animating. Will make animations stutter on mobile or slow devices.
        this.control.loadTilesWhileAnimating = true;
        this.control.loadTilesWhileInteracting = true;

        /**** register map event ****/
        // moveend event
        this.control.on('moveend', this.onMoveEnd);
        // single click
        //this.control.on('singleclick', this.onClick ? this.onClick : this.onSingleClick);
        // triggered when a pointer is moved. Note that on touch devices this is triggered when the map is panned, so is not the same as mousemove.
        this.control.on('pointermove', this.onDisplayTooltip);
        // triggered after a map frame is rendered - call once
        this.control.once('postrender', this.oncePostrender);

        // style map
        this.styleImpl = new apomGis.StyleClass(this.control);

        var _this = this;
        $(document).bind('keydown', function (evt) {
            let action = _this.coreImpl.action;
            if (action === apomGis.Config.action.measure || action == apomGis.Config.action.identify) {
                // esc then destroy
                if (evt.keyCode === 27) {
                    _this.selectionFeatureVT = {};
                    if (_this.selectionLayerVT) _this.selectionLayerVT.changed();
                    if (_this.graphicLayer) _this.graphicLayer.removeAllFeature();
                    return false;
                }
            }
        });

    };

    /* controls */
    this.setRotation = function () {
        if (apomGis.Config.rotation.show) {
            this.Rotation = new apomGis.Controls.RotationClass({
                'map': this.control,
                'target': apomGis.Config.rotation.target
            });
            this.Rotation.create();
        }

    };

    this.setScale = function () {
        if (apomGis.Config.scale.show) {
            // scale line
            if (this.Scale != null) {
                if (this.Scale.control) {
                    this.control.removeControl(this.Scale.control);
                    this.Scale.destroy();
                };
            };
            //this.Scale = new apomGis.Controls.ScaleClass({
            //    'target': apomGis.Config.scale.target_line
            //});
            //this.Scale.create();
            //this.control.addControl(this.Scale.control);

            // scale bar
            this.ScaleBar = new apomGis.Controls.ScaleBarClass({
                'map': this.control,
                'target': apomGis.Config.scale.target_bar
            });

        }
    };

    this.setPosition = function () {
        if (apomGis.Config.position.show) {
            if (this.positionImpl != null) {
                if (this.positionImpl.control) {
                    this.control.removeControl(this.positionImpl.control);
                    this.positionImpl.destroy();
                };
            };
            this.positionImpl = new apomGis.Controls.PositionClass({
                'target': apomGis.Config.position.target,
                'projection': this.projectionMap
            });
            this.positionImpl.create();
            this.control.addControl(this.positionImpl.control);
        }
    };

    this.setSearchAddressOsm = function (options) {
        var _options = options ? options : [];
        var id = _options.id ? _options.id : 'search';
        var callbackAction = _options.callbackAction ? _options.callbackAction : undefined;
        this.SearchOSM = new apomGis.Controls.SearchAddressClass({
            'map': this.control,
            'id': id,
            'placehoder': CONFIG_SYSTEMT.PLACEHODER_SEARCH ? CONFIG_SYSTEMT.PLACEHODER_SEARCH : 'search',
            'callback': callbackAction
        });
        this.SearchOSM.create();
    };
    /* end controls */

    //show popup feature
    this.showPopupFeature = function (opt_options) {
        var options = opt_options || {};
        var map = options.map;
        var callbackTitle = options.callbackTitle;
        var callbackAttr = options.callbackAttr;
        var hostAPI = this.hostAPI;
        var langId = this.langId;
        //$('.ol-overlay-container').hide();
        this.popup = new ol.Overlay.PopupFeature({
            popupClass: 'default anim',
            map: map,
            canFix: false, //tao nut de ghim popup sang top right
            maxChar: 10000,// lenght content max đe hien thi, qua max thi show....
            idenitfyDataGeoTiff: opt_options.idenitfyDataGeoTiff ? opt_options.idenitfyDataGeoTiff : null,
            template: {
                title:
                  function (f) {
                      let titleInfo = [];
                      if (callbackTitle) {
                          titleInfo = callbackTitle(f, hostAPI, langId);
                          title = titleInfo.title;
                          des = titleInfo.des;
                      }
                      var html = '';
                      if (des && des != '') {
                          html = '<div style="margin-bottom: 3px;"><b class="ol-popuptitle-title">' + title + '</b></div>';
                          html += '<div><span class="ol-popuptitle-des">' + des + '</span></div>';
                      }
                      else html = '<b class="ol-popuptitle-title">' + title + '</b>';
                      return html;
                  },
                attributes:
                {
                    'popp':
                    {
                        title: '',  // attribute's title
                        value: function (f) {
                            let html = "";
                            let geostr = '';
                            var properties = f.getProperties();
                            // call api

                            if (callbackAttr) {
                                var properties = callbackAttr(f, hostAPI, langId);
                                if (properties.callbackShow) {
                                    html = properties.callbackShow(properties, langId);
                                }
                                else {
                                    for (var property in properties) {
                                        if (typeof (properties[property]) != "object") {
                                            html += '<tr>';
                                            html += '<td>' + property + '</td>';
                                            html += '<td>';
                                            html += properties[property] === null ? '' : properties[property];
                                            html += '</td>';
                                            html += '</tr>';
                                        }
                                    }
                                }
                            }
                            else {
                                for (var property in properties) {
                                    if (typeof (properties[property]) != "object") {
                                        html += '<tr>';
                                        html += '<td>' + property + '</td>';
                                        html += '<td>';
                                        html += properties[property] === null ? '' : properties[property];
                                        html += '</td>';
                                        html += '</tr>';
                                    }
                                }
                            }
                            return { 'html': html, 'geostr': geostr };
                        }
                    }
                }
            }
        });
        this.control.addOverlay(this.popup);
    };

    /* event map */
    this.saveCurrentExten = function () {
        var version = this.version.cookie;
        var lvZoom = this.control.getView().getZoom();
        var center = this.control.getView().values_ ? this.control.getView().values_.center : this.control.getView().getCenter();
        var projectionCode = this.control.getView().getProjection().getCode();
        var unitCode = this.control.getView().getProjection().getUnits();

        var name = this.nameExtentCookie;
        var value = '\'';
        center = '[' + center.toString() + ']';
        value += center + '|' + lvZoom.toString() + '|' + projectionCode + '|' + unitCode + '|' + version;
        value += '\'';
        // createCookie: quan ly cookie from utility
        createCookie(name, value, 30);
    };

    /****
    triggered after a map frame is rendered.
    ****/
    this.oncePostrender = function (evt) {
        if (this.parent.loadExtentFromLS) {

            var minZoom = apomGis.Config.zoom.view.minZoom;
            var maxZoom = apomGis.Config.zoom.view.maxZoom;

            /**** set view from cookies - run once when load map ***/
            //var viewExtent = [11975027, 1857780, 11977898, 1858341];
            if (this.parent.zoomt_to_extent) {
                var view = new ol.View({
                    maxZoom: maxZoom,
                    minZoom: minZoom,
                    projection: this.parent.projectionMap.getCode(),
                });
                this.setView(view);
                this.getView().fit(this.parent.zoomt_to_extent, this.getSize());
            }
            else {
                // get_cookies_array in utility.js
                var cookies = get_cookies_array();
                var lvZoom = apomGis.Config.initExtent.level;
                // default center from basemap
                var center = apomGis.Config.initExtent.center;
                var mycenter = [center.x, center.y]; // default value from config
                var projectionCode = '';
                var unitCode = '';
                var version = '';
                var versionCurrent = this.parent.version.cookie;

                // if not setting -> calculator xcenter - y center from extent
                if (mycenter[0] === 0 && mycenter[1] === 0) {
                    if (apomGis.Config.initExtent.isCenter) {
                        // xcenter
                        mycenter[0] = apomGis.Config.initExtent.center.x;
                        // ycenter
                        mycenter[1] = apomGis.Config.initExtent.center.y;
                    }
                    else {
                        // xcenter
                        mycenter[0] = (apomGis.Config.initExtent.full[0] + apomGis.Config.initExtent.full[2]) / 2;
                        // ycenter
                        mycenter[1] = (apomGis.Config.initExtent.full[1] + apomGis.Config.initExtent.full[3]) / 2;
                    }
                }
                if (this.parent.projectionMap == null) {
                    this.parent.projectionMap = this.parent.setProjectionMap(apomGis.Config.projection.code, apomGis.Config.projection.unit); // default
                }
                var codeProjection = this.parent.projectionMap.getCode();
                var codeUnit = this.parent.projectionMap.getUnits();

                // if exists in coookies
                if (cookies[this.parent.nameExtentCookie] != undefined && cookies[this.parent.nameExtentCookie].split(',').length > 1) {
                    var stringCookies = cookies[this.parent.nameExtentCookie];
                    var arrayCookiesData = stringCookies.split('|');
                    var _mycenter, _lvZoom;
                    // center | lever zoom | prj code | unit | version
                    if (arrayCookiesData.length > 0) _mycenter = JSON.parse(arrayCookiesData[0].substring(1));
                    if (arrayCookiesData.length > 1) _lvZoom = parseInt(arrayCookiesData[1]);
                    if (arrayCookiesData.length > 2) projectionCode = arrayCookiesData[2];
                    if (arrayCookiesData.length > 3) unitCode = arrayCookiesData[3].replace("'", '');
                    if (arrayCookiesData.length > 4) version = arrayCookiesData[4].replace("'", '');
                    if (projectionCode === codeProjection && unitCode === codeUnit && versionCurrent === version) {
                        mycenter = _mycenter;
                        lvZoom = _lvZoom;
                    }
                }

                var view = new ol.View({
                    maxZoom: maxZoom,
                    minZoom: minZoom,
                    zoom: lvZoom,
                    center: mycenter,
                    projection: codeProjection,
                    extent: this.parent.fitExtent
                });
                this.setView(view);
            }
        }

        var target = this.parent.target;
        var delta = -1;
        var parentMap = $('#' + target).parent();
        //$('#' + target).width(parentMap.width() - delta);
        //$('#' + target).height(parentMap.height() - delta);
        this.updateSize();
        var _this = this;
        $(window).resize(function () {
            //$('#' + target).width(parentMap.width() - delta);
            //$('#' + target).height(parentMap.height() - delta);
            _this.updateSize();
        });
    };

    this.onDisplayTooltip = function (evt) {
        //return;
        if (this.parent.positionImpl) {
            var fromEPSG = this.getView().getProjection().getCode();
            var toEPSG = this.parent.projectionMap.getCode();
            if (fromEPSG != toEPSG) {
                var coors = ol.proj.transform(evt.coordinate, fromEPSG, toEPSG);
                this.parent.positionImpl.updateCoordinateCurrent(coors);
            } else
                this.parent.positionImpl.updateCoordinateCurrent(evt.coordinate);
        }
    };

    this.onMoveEnd = function (evt) {
        var start = new Date().getTime();
        var map = evt.map;
        var mapClass = map.parent;
        map.updateSize();

        // load lan dau lay tu cookie
        if (mapClass.loadExtentFromCookie) {
            mapClass.loadExtentFromCookie = false;
        }
        else {
            // luu extent cua map
            mapClass.saveCurrentExten();
        }

        // truong hop an nut back extend/next extend
        if (mapClass.isSaveHistory) return;
        // xoa extent phan tu thu mapClass.historyNow + 1
        var iFrom = mapClass.historyNow + 1; // bat dau xoa tu phan tu thu mapClass.historyNow + 1
        var iCount = mapClass.NavigationHistory.length - (mapClass.historyNow + 1); // so luong phan tu bi xoa
        mapClass.NavigationHistory.splice(iFrom, iCount);
        mapClass.NavigationHistory.push({
            center: map.getView().getCenter(),
            resolution: map.getView().getResolution(),
            rotation: map.getView().getRotation()
        });
        mapClass.historyNow++;

    };

    this.onSingleClick = function (evt) {
        var action = this.parent.coreImpl.action;
        var coordinate = evt.coordinate;
        var features = [];
        if (action === apomGis.Config.action.identify) {
            // only check layer that is visible
            for (let i = 0; i < this.wmslayers.length; i++) {
                let wmsLayer = this.wmslayers[i];
                if (wmsLayer.getVisible()) {
                    let params = wmsLayer.get('params');
                    console.log(params);
                }
            }
        }
    };
    /* end event map */

    this.setMultyBasemap = function (basemapLayer, isSelected) {
        var map = this.control;
        basemapLayer.basemap = true;
        this.addLayer(basemapLayer);
        if (!isSelected) return;
        //this.extent['extent'] = apomGis.Config.initExtent.extent;
        //this.extent['level'] = apomGis.Config.initExtent.level;
        //var fromLonlat = apomGis.Config.initExtent.fromLonlat;
        var view = null;

        if (apomGis.Config.initExtent.isCenter) {
            view = new ol.View({
                // the view's initial state
                center: apomGis.Config.initExtent.center,
                zoom: apomGis.Config.initExtent.level,
                projection: this.projectionMap.getCode()
            });
        }
        else {
            view = new ol.View({
                projection: this.projectionMap.getCode(),
            });
        }
        if (view != null) map.setView(view);

        //this.setZoomControl();
        //this.setOV(basemapLayer);
        //this.setTocControl();
        //this.setLegendControl();
        this.setScale();
        this.setPosition();
        //this.setSearch();
        //this.setSearchAddress();
        //this.setProjectionCode();
        this.setRotation();
        //this.setBookmarkControl();
        //this.setVisualClass();
    };


    /**** manipulate map layers ****/
    this.getLayers = function () {
        return this.control.getLayers();
    };

    this.addLayer = function (layer) {
        this.control.addLayer(layer);
    };

    this.removeLayer = function (layer) {
        this.control.removeLayer(layer);
    };

    this.getView = function () {
        if (this.control == null) return null;
        return this.control.getView();
    };

    this.getProjection = function () {
        if (this.control == null) return null;
        return this.getView().getProjection();
    };

    this.getResolutionMap = function () {
        var extent = this.getExtent();
        var deltaW = extent[2] - extent[0]; // xmax - xmin
        var withMap = this.control.getSize()[0];
        return withMap === 0 ? 0 : deltaW / withMap;
    };

    this.getExtent = function () {
        var view = this.getView();
        var sizeMap = this.control.getSize();
        return view.calculateExtent(sizeMap);
    };

    // check extent1 contain extent2?
    this.checkExtentContainExtent = function (extent1, extent2) {
        var xmin1 = extent1[0];
        var ymin1 = extent1[1];
        var xmax1 = extent1[2];
        var ymax1 = extent1[3];

        var xmin2 = extent2[0];
        var ymin2 = extent2[1];
        var xmax2 = extent2[2];
        var ymax2 = extent2[3];

        if (xmin1 <= xmin2 && xmax1 >= xmax2 && ymin1 <= ymin2 && ymax1 >= ymax2) return true;
        return false;
    };

    // tra ve extent tu centerCoorPoint va Resolution map
    this.getExtentFromResolution = function (centerCoorPoint, resolution) {
        var xC = centerCoorPoint[0];
        var yC = centerCoorPoint[1];

        var withMap = this.control.getSize()[0];
        var deltaWith = resolution * withMap;
        var xMin = xC - deltaWith / 2;
        var xMax = xC + deltaWith / 2;

        var heightMap = this.control.getSize()[1];
        var deltaHeight = resolution * heightMap;
        var yMin = yC - deltaHeight / 2;
        var yMax = yC + deltaHeight / 2;
        return [xMin, yMin, xMax, yMax];
    };

    this.getScale = function () {
        var resolution = this.control.getView().getResolution();
        var units = this.control.getView().getProjection().getUnits();
        var dpi = 25.4 / 0.28;
        /*
        degrees: 111194.87428468118
        ft: 0.3048
        m: 1
        us-ft: 0.3048006096012192
        */
        //var mpu = 1;
        var mpu = ol.proj.METERS_PER_UNIT === undefined ? ol.proj.Units.METERS_PER_UNIT[units] : ol.proj.METERS_PER_UNIT[units];
        var scale = resolution * mpu * 39.37 * dpi;
        return scale;
    };

    this.getZoom = function () {
        return this.control.getView().getZoom();
    };

    this.setScaleForMap = function (scale) {
        var units = this.control.getView().getProjection().getUnits();
        var dpi = 25.4 / 0.28;
        var mpu = ol.proj.METERS_PER_UNIT === undefined ? ol.proj.Units.METERS_PER_UNIT[units] : ol.proj.METERS_PER_UNIT[units];
        var resolution = scale / (mpu * 39.37 * dpi);
        this.control.getView().setResolution(resolution);
    };

    this.zoomToExtent = function (extent) {
        this.control.getView().fit(extent, this.control.getSize());
    }
    /**** end manipulate map layers ****/
};