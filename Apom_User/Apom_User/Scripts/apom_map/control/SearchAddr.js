﻿apomGis.Controls.SearchAddressClass = function (options) {
    if (!(options.map instanceof ol.Map)) {
        throw new Error('Please provide a valid OpenLayers 3 map object.');
    }
    this.options = options ? options : [];
    this.map = this.options.map;
    this.id = this.options.id ? this.options.id : 'search';
    this.placehoder = this.options.placehoder ? this.options.placehoder : 'search';
    this.langId = this.map ? this.map.parent.langId : 'vi';
    this.hostAPI = this.map ? this.map.parent.hostAPI : '';
    this.callbackShowInfo = this.options.callback ? this.options.callback : this.showInfo;
    this.geoCoder = null;
    this.create = function () {
        this.geoCoder = new Geocoder('nominatim', {
            provider: 'osm',
            url: 'https://nominatim.openstreetmap.org/search',
            key: '__some_key__',
            lang: 'vi-VN', //en-US, fr-FR
            placeholder: this.placehoder + '...',
            targetType: 'text-input',
            //limit: 10,
            autoComplete: true,
            keepOpen: false,
            preventDefault: true,
            countrycodes: 'VN',
            hideWhenClickMap: false
        });
        // this.geoCoder.element = document.getElementById(this.id);
        this.geoCoder.setTarget(this.id);
        this.map.addControl(this.geoCoder);
        $('.gcd-txt-glass').hide();
        $('.gcd-txt-reset').hide();
        var _this = this;

        this.geoCoder.on('addresschosen', function (evt) {
            var _map = this.getMap();
            $('#gcd-input-query').val(evt.address.details.name);
            $('#gcd-input-query').attr('title', evt.address.details.name)
            //var feature = evt.feature,
            //    coord = evt.coordinate,
            //    address = evt.address;


            // add feature graphic
            //var geomT = "POINT(" + evt.coordinate[0] + " " + evt.coordinate[1] + ")";
            //var format = new ol.format.WKT();
            //var geomF = format.readGeometryFromText(geomT);
            //createGraphicFromGeom(geomF);
            if (_map.parent.graphicLayer) {
                if (_map.parent.graphicLayer) _map.parent.graphicLayer.removeAllFeature();
                _map.parent.graphicLayer.createGraphic({
                    geom: _map.parent.graphicLayer.geometry.createPoint(evt.coordinate[0], evt.coordinate[1])
                });
            }
            _map.parent.coreImpl.panToCoordinate(evt.coordinate);
            window.setTimeout(function () {
                // callback Info for station & geotiff
                if (_this.callbackShowInfo) {
                    _this.callbackShowInfo({
                        map: _map,
                        hostAPI: _this.hostAPI,
                        langId: _this.langId,
                        coordinate: evt.coordinate,
                        isStation: false,
                        isGeoTiff: true
                    });
                }
                else _this.showInfo(evt.address.details.name);
            }, 1000);
            //console.log(evt);
        });
    };

    this.showInfo = function (name) {
        //alert('name: ' + name);
    };

    this.showResult = function () {
        $('.gcd-txt-result').show();
    };

    //this.hideResult = function () {
    //    $('.gcd-txt-result').hide();
    //};

    //this.destroy = function () {
    //    $('.gcd-container').remove();
    //    if (this.geoCoder != null) this.map.removeControl(this.geoCoder);
    //};
}