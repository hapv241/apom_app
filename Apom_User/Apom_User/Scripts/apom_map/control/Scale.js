﻿apomGis.Controls.ScaleBarClass = function (options) {
    if (!(options.map instanceof ol.Map)) {
        throw new Error('Please provide a valid OpenLayers 3 map object.');
    }
    this.map = options.map;
    if (options.target) {
        this.target = options.target;

        this.onMoveEnd = function (evt) {
            var map = evt.map;
            var mapClass = map.parent;
            var scaleMap = Math.round(mapClass.getScale());
            //$('#' + options.target).text('1: ' + scaleMap);
            $('#' + options.target).val(scaleMap);
        };

        // moveend event
        this.map.on('moveend', this.onMoveEnd);

    }
    else {
        throw new Error('Invalid parameter(s) provided.');
    }
}