﻿//function LayerClass(map, layer, type) {
//    this.map = map;
//    this.layer = layer;
//    this.type = type; // apomGis.Config.layerType
//    this.ftCollection = new ol.Collection();//contain all feature in Graphic layer
//};

class LayerClass {
    constructor(options) {
        options = options === undefined ? {} : options;

        this._map = options.map === undefined ? null : options.map;
        Object.defineProperty(this, "map", {
    get() {
        //console.log('get!');
        return this._map;
    },
    set(value) {
        //console.log('set!');
        this._map = value;
    }
        });

        this._layer = options.layer === undefined ? null : options.layer;
        Object.defineProperty(this, "layer", {
    get() {
        //console.log('get!');
        return this._layer;
    },
    set(value) {
        //console.log('set!');
        this._layer = value;
    }
        });

        this._type = options.type === undefined ? null : options.type; // apomGis.Config.layerType
        Object.defineProperty(this, "type", {
    get() {
        //console.log('get!');
        return this._type;
    },
    set(value) {
        //console.log('set!');
        this._type = value;
    }
        });

        this._ftCollection = new ol.Collection();//contain all feature in Graphic layer
        Object.defineProperty(this, "ftCollection", {
    get() {
        //console.log('get!');
        return this._ftCollection;
    },
    set(value) {
        //console.log('set!');
        this._ftCollection = value;
    }
        });
    }
};

LayerClass.prototype.getMap = function () {
    return this.map;
};

LayerClass.prototype.getLayer = function () {
    return this.layer;
};

LayerClass.prototype.setMap = function (map) {
    this.map = map;
};

LayerClass.prototype.setLayer = function (layer) {
    this.layer = layer;
};

LayerClass.prototype.toogleVisible = function () {
    if (this.layer == undefined) return;
    var visible = this.layer.getVisible();
    this.layer.setVisible = !visible;
};

LayerClass.prototype.getVisible = function () {
    //if (this.layer == undefined) return -1;
    return this.layer.getVisible();
};

LayerClass.prototype.setVisible = function (visible) {
    //if (this.layer == undefined) return -1;
    return this.layer.setVisible(visible);
};

LayerClass.prototype.getOpacity = function () {
    if (this.layer == undefined) return -1;
    return this.layer.getOpacity();
};

LayerClass.prototype.setOpacity = function (opacity) {
    if (this.layer == undefined) return;
    this.layer.setOpacity(opacity);
};

LayerClass.prototype.setZIndex = function (value) {
    if (this.layer == undefined) return;
    this.layer.setZIndex(parseInt(value) || 0);
};

LayerClass.prototype.removeAllFeatures = function () {
    if (this.layer == undefined) return;
    if (this.layer.getSource) this.layer.getSource().clear();
};

LayerClass.prototype.createGoogleMap = function (opt_options) {
    var options = opt_options ? opt_options : [];
    var visible = options.visible === undefined ? true : options.visible;
    var ggSource = new ol.source.OSM(
			{
			    url: 'http://mt{0-3}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
			    attributions: [
						new ol.Attribution({
						    html: '© Google'
						}),
						new ol.Attribution(
								{
								    html: '<a href="https://developers.google.com/maps/terms">Terms of Use.</a>'
								})]
			});
    var ggLayer = new ol.layer.Tile({
        preload: 10,
        source: ggSource,
        visible: visible
    });
    return ggLayer;
};

// apikey=Your API key from http://www.thunderforest.com/docs/apikeys/ here
LayerClass.prototype.createOSM_Raster = function (opt_options) {
    var options = opt_options ? opt_options : [];
    var visible = options.visible === undefined ? true : options.visible;

    var osmSource = new ol.source.XYZ({
        url: 'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=e62bb07a649c4319a7330378ad8474ff'
    });
    var osmLayer = new ol.layer.Tile({
        source: osmSource,
        visible: visible
    });

    var params = [];
    params.featureBase = '';
    params.featureType = 'osm';
    params.geometryName = '';
    params.geometryType = '';
    params.projection = null;
    params.mapService = '';

    var formatGML = null;
    osmLayer.params = params;
    osmLayer.formatGML = formatGML;

    return osmLayer;
};

LayerClass.prototype.createOSM = function (opt_options) {
    var options = opt_options ? opt_options : [];
    var visible = options.visible === undefined ? true : options.visible;
    var wrapDateLine = options.wrapX ? options.wrapDateLine : false;
    var wrapX = options.wrapX ? options.wrapX : false;
    var noWrap = options.noWrap ? options.noWrap : true;
    var visible = options.visible === undefined ? true : options.visible;
    // 4326
    //var initExtent = options.initExtent ? options.initExtent : { 'extent': [-180.0, -90.0, 180.0, 90.0] };
    // 3857
    var initExtent = options.initExtent ? options.initExtent : { 'extent': [-20037508.34, -20048966.1, 20037508.34, 20048966.1] };
    // case opt_options.initExtent.extent = undefined
    // 4326
    //if (initExtent.extent === undefined) initExtent.extent = [-180.0, -90.0, 180.0, 90.0];
    // 3857
    if (initExtent.extent === undefined) initExtent.extent = [-20037508.34, -20048966.1, 20037508.34, 20048966.1];

    //var osmSource = new ol.source.OSM();
    var osmSource = new ol.source.OSM({
        wrapDateLine: wrapDateLine,
        wrapX: wrapX,
        noWrap: noWrap,
        attributions: [
            //ol.Attribution ? new ol.Attribution({
            //    html: 'All maps © ' +
            //        '<a href="http://www.opencyclemap.org/">OpenCycleMap</a>'
            //}) : new ol.control.Attribution({
            //    html: 'All maps © ' +
            //        '<a href="http://www.opencyclemap.org/">OpenCycleMap</a>'
            //}),
            ol.source.OSM.ATTRIBUTION
        ],
    });
    var osmLayer = new ol.layer.Tile({
        preload: 4,
        source: osmSource,
        extent: initExtent.extent,
        //extent: initExtent.full,
        //minZoom: 5,
        visible: visible
    });

    var params = [];
    params.featureBase = '';
    params.featureType = 'osm';
    params.geometryName = '';
    params.geometryType = '';
    params.projection = null;
    params.mapService = '';

    var formatGML = null;
    osmLayer.params = params;
    osmLayer.formatGML = formatGML;

    return osmLayer;
};

LayerClass.prototype.createSatellite = function (options) {
    var opt_options = options ? options : [];
    var title = opt_options.title ? opt_options.title : "";
    var mapservice = opt_options.mapservice ? opt_options.mapservice : "";
    var style = opt_options.style ? opt_options.style : "";
    var featurebase = opt_options.featurebase ? opt_options.featurebase : "";
    var featuretype = opt_options.featuretype ? opt_options.featuretype : "";
    var visible = opt_options.visible === undefined ? true : opt_options.visible;

    var type = featurebase == '' ? featuretype : featurebase + ':' + featuretype;

    var layerSatellite = new ol.layer.Tile({
        'title': title,
        'type': type, //'base',
        visible: visible,
        'opacity': 1.000000,
        source: new ol.source.XYZ({
            //attributions: ol.Attribution ? [new ol.Attribution({ html: '<a href=""></a>' })] : [new ol.control.Attribution({ html: '<a href=""></a>' })],
            //url: 'http://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}&s=Ga',
            url: mapservice,
            crossOrigin: "Anonymous"
        })
    });

    var params = [];
    layerSatellite.params = params;

    return layerSatellite;
};

LayerClass.prototype.createImageWMS = function (opt_options) {
    var options = opt_options ? opt_options : [];
    var title = options.title ? options.title : "";
    var mapservice = options.mapservice ? options.mapservice : "";
    var accesstoken = options.access_token ? options.access_token : "";
    var style = options.style ? options.style : "";
    var featurebase = options.featurebase ? options.featurebase : "";
    var featuretype = options.featuretype ? options.featuretype : "";
    var projection = options.projection ? options.projection : "";
    var geometryname = options.geometryname ? options.geometryname : "";
    var geometrytype = options.geometrytype ? options.geometrytype : "";
    var tableid = options.tableid ? options.tableid : "";
    var timeFilter = options.time ? options.time : "";
    var zIndex = options.zIndex ? options.zIndex : 1;
    var layer = featurebase == '' ? featuretype : featurebase + ':' + featuretype;
    var opacity = options.opacity === undefined ? 1 : options.opacity;
    var visible = options.visible === undefined ? true : options.visible;
    var wmsSource = new ol.source.ImageWMS({
        ratio: 1,
        url: mapservice,
        params: {
            'access_token': accesstoken,
            'FORMAT': apomGis.Config.formatImg,
            'VERSION': '1.1.1',
            "STYLES": style,
            "LAYERS": layer,
            "exceptions": 'application/vnd.ogc.se_inimage',
        }
    });

    var wmsLayer = new ol.layer.Image({
        source: wmsSource,
        zIndex: zIndex,
        opacity: opacity,
        visible: visible
    });

    var params = {
        'url': mapservice,
        'featurebase': featurebase,
        'featuretype': featuretype
    };

    wmsLayer.set('params', params);
    return wmsLayer;
};

LayerClass.prototype.createTileWMS = function (opt_options) {
    var options = opt_options ? opt_options : [];
    var title = options.title ? options.title : "";
    var mapservice = options.mapservice ? options.mapservice : "";
    var accesstoken = options.access_token ? options.access_token : "";
    var style = options.style ? options.style : "";
    var featurebase = options.featurebase ? options.featurebase : "";
    var featuretype = options.featuretype ? options.featuretype : "";
    var projection = options.projection ? options.projection : "";
    var geometryname = options.geometryname ? options.geometryname : "";
    var geometrytype = options.geometrytype ? options.geometrytype : "";
    var tableid = options.tableid ? options.tableid : "";
    var timeFilter = options.time ? options.time : "";
    var zIndex = options.zIndex ? options.zIndex : 1;
    var layer = featurebase == '' ? featuretype : featurebase + ':' + featuretype;    
    var opacity = options.opacity === undefined ? 1 : options.opacity;
    var visible = options.visible === undefined ? true : options.visible;
    var wmsSource = new ol.source.TileWMS({
        url: mapservice,
        params: {
            'access_token': accesstoken,
            'FORMAT': apomGis.Config.formatImg,
            'VERSION': '1.1.1',
            'tiled': true,
            "STYLES": style,
            "TIME": timeFilter,
            "LAYERS": layer,
            "exceptions": 'application/vnd.ogc.se_inimage',
            //tilesOrigin: 100.1 + "," + 24
            tilesOrigin: 12377632 + "," + 773895.9375
            //6.399999999999999
        },
        serverType: 'geoserver'
    });

    var wmsLayer = new ol.layer.Tile({
        source: wmsSource,
        zIndex: zIndex,
        opacity: opacity,
        visible: visible
    });

    var params = {
        'url': mapservice,
        'featurebase': featurebase,
        'featuretype': featuretype
    };

    wmsLayer.set('params', params);
    return wmsLayer;
};

LayerClass.prototype.createVectorTile_TMS = function (opt_options) {
    var options = opt_options ? opt_options : [];
    var url = options.url ? options.url : "";
    var projCode = options.projCode ? options.projCode : "EPSG:3857";
    var unit = options.unit ? options.unit : "m";
    var declutter = options.declutter ? options.declutter : false;
    var visible = options.visible === undefined ? true : options.visible;
    var minZoom = options.minZoom ? options.minZoom : '';
    var maxZoom = options.maxZoom ? options.maxZoom : '';
    var zIndex = opt_options.zIndex ? opt_options.zIndex : 1;

    if (url != '') {
        function tileLoadFunction(tile, url) {
            var format = tile.getFormat();
            var tileCoord = tile.getTileCoord();
            tile.setLoader(function (extent, resolution, projection) {
                fetch(url).then(function (response) {
                    response.arrayBuffer().then(function (data) {
                        const format = tile.getFormat() // ol/format/MVT configured as source format
                        const projection1 = format.readProjection(data);
                        if (tile.setProjection) tile.setProjection(projection1);
                        var features = [];
                        try {
                            //features = format.readFeatures(data, {});
                            features = format.readFeatures(data, {
                                extent: tile.extent,
                                featureProjection: tile.projection
                            });
                        }
                        catch (ex) {
                            //console.log('error read feature: ' + ex);
                            features = [];
                        }
                        tile.setFeatures(features);
                    });
                });
            });
        };

        var projection = new ol.proj.Projection({
            code: projCode,
            units: unit,
            axisOrientation: 'neu'
        });
        var tileLoadFunc = options.tileLoadFunc ? options.tileLoadFunc : tileLoadFunction;
        var layerVT_TMS = null;
        if (minZoom === '') {
            if (maxZoom === '') {
                layerVT_TMS = new ol.layer.VectorTile({
                    declutter: declutter,
                    source: new ol.source.VectorTile({
                        tilePixelRatio: 1, // oversampling when > 1
                        tileGrid: ol.tilegrid.createXYZ({ maxZoom: 22 }),
                        format: new ol.format.MVT({}),
                        projection: projection,
                        url: url,
                        tileLoadFunction: tileLoadFunc
                    }),

                    zIndex: zIndex,
                    visible: visible
                });
            }
            else {
                layerVT_TMS = new ol.layer.VectorTile({
                    declutter: declutter,
                    source: new ol.source.VectorTile({
                        tilePixelRatio: 1, // oversampling when > 1
                        //tileGrid: ol.tilegrid.createXYZ({
                        //    maxZoom: parseInt(maxZoom),
                        //}),
                        format: new ol.format.MVT(),
                        projection: projection,
                        url: url,
                        maxZoom: parseInt(maxZoom),
                        tileLoadFunction: tileLoadFunc
                    }),
                    zIndex: zIndex,
                    maxZoom: parseInt(maxZoom),
                    visible: visible
                });
            }
        }
        else {
            if (maxZoom === '') {
                layerVT_TMS = new ol.layer.VectorTile({
                    declutter: declutter,
                    source: new ol.source.VectorTile({
                        tilePixelRatio: 1, // oversampling when > 1
                        //tileGrid: ol.tilegrid.createXYZ({
                        //    minZoom: minZoom,
                        //    maxZoom: 22,
                        //}),
                        format: new ol.format.MVT(),
                        projection: projection,
                        url: url,
                        minZoom: minZoom,
                        tileLoadFunction: tileLoadFunc
                    }),
                    zIndex: zIndex,
                    minZoom: minZoom,
                    visible: visible
                });
            }
            else {
                layerVT_TMS = new ol.layer.VectorTile({
                    declutter: declutter,
                    source: new ol.source.VectorTile({
                        tilePixelRatio: 1, // oversampling when > 1
                        //tileGrid: ol.tilegrid.createXYZ({
                        //    minZoom: minZoom,
                        //    maxZoom: maxZoom,
                        //}),
                        format: new ol.format.MVT(),
                        projection: projection,
                        url: url,
                        minZoom: minZoom,
                        maxZoom: maxZoom,
                        tileLoadFunction: tileLoadFunc
                    }),
                    zIndex: zIndex,
                    minZoom: minZoom,
                    maxZoom: maxZoom,
                    visible: visible
                });
            }
        }

        var params = {
            'url': url
        };

        layerVT_TMS.set('params', params);
        return layerVT_TMS;
    }
    return null;
};

LayerClass.prototype.createVectorTile_WMTS = function (opt_options) {
    var options = opt_options ? opt_options : [];
    var url = options.url ? options.url : "";
    var layerName = options.layerName ? options.layerName : "";
    var projCode = options.projCode ? options.projCode : "EPSG:3857";
    var unit = options.unit ? options.unit : "m";
    var declutter = options.declutter ? options.declutter : false;
    var visible = options.visible === undefined ? true : options.visible;
    var minZoom = options.minZoom ? options.minZoom : '';
    var maxZoom = options.maxZoom ? options.maxZoom : '';
    var zIndex = opt_options.zIndex ? opt_options.zIndex : 1;

    function tileLoadFunction(tile, url) {
        var format = tile.getFormat();
        var tileCoord = tile.getTileCoord();
        tile.setLoader(function (extent, resolution, projection) {
            fetch(url).then(function (response) {
                response.arrayBuffer().then(function (data) {
                    const format = tile.getFormat() // ol/format/MVT configured as source format
                    const projection1 = format.readProjection(data);
                    if (tile.setProjection) tile.setProjection(projection1);
                    var features = [];
                    try {
                        //features = format.readFeatures(data, {});
                        features = format.readFeatures(data, {
                            extent: tile.extent,
                            featureProjection: tile.projection
                        });
                    }
                    catch (ex) {
                        //console.log('error read feature: ' + ex);
                        features = [];
                    }
                    tile.setFeatures(features);
                });
            });
        });
    };
    var tileLoadFunc = options.tileLoadFunc ? options.tileLoadFunc : tileLoadFunction;

    var gridsetName = projCode;
    var gridNames = apomGis.Config.projection.gridNames[projCode];
    var resolutions = apomGis.Config.projection.resolutions[projCode];
    var tileSize = apomGis.Config.projection.tileSize[projCode];
    var origin = apomGis.Config.projection.origin[projCode];
    var baseUrl = url;
    var style = '';
    var format = 'application/vnd.mapbox-vector-tile';
    var infoFormat = 'text/html';
    var projection = new ol.proj.Projection({
        code: projCode,
        units: unit,
        axisOrientation: 'neu'
    });
    params = {
        'REQUEST': 'GetTile',
        'SERVICE': 'WMTS',
        'VERSION': '1.0.0',
        'LAYER': layerName,
        'STYLE': style,
        'TILEMATRIX': gridsetName + ':{z}',
        'TILEMATRIXSET': gridsetName,
        'FORMAT': format,
        'TILECOL': '{x}',
        'TILEROW': '{y}'
    };

    function constructSource() {
        var url = baseUrl + '?'
        for (var param in params) {
            url = url + param + '=' + params[param] + '&';
        }
        url = url.slice(0, -1);

        var source = new ol.source.VectorTile({
            url: url,
            format: new ol.format.MVT({}),
            projection: projection,
            tileGrid: new ol.tilegrid.WMTS({
                tileSize: [256, 256],
                origin: [-180.0, 90.0],
                resolutions: resolutions,
                matrixIds: gridNames
            }),
            tileLoadFunction: tileLoadFunc,
            wrapX: true
        });
        return source;
    }

    var layerVT_WMTS = null;
    if (minZoom === '') {
        if (maxZoom === '') {
            layerVT_WMTS = new ol.layer.VectorTile({
                declutter: declutter,
                source: constructSource(),
                zIndex: zIndex,
                visible: visible
            });
        }
        else {
            layerVT_WMTS = new ol.layer.VectorTile({
                declutter: declutter,
                source: constructSource(),
                zIndex: zIndex,
                maxZoom: parseInt(maxZoom),
                visible: visible
            });
        }
    }
    else {
        if (maxZoom === '') {
            layerVT_WMTS = new ol.layer.VectorTile({
                declutter: declutter,
                source: constructSource(),
                zIndex: zIndex,
                minZoom: minZoom,
                visible: visible
            });
        }
        else {
            layerVT_WMTS = new ol.layer.VectorTile({
                declutter: declutter,
                source: constructSource(),
                zIndex: zIndex,
                minZoom: minZoom,
                maxZoom: maxZoom,
                visible: visible
            });
        }
    }
    //var layerVT_WMTS = new ol.layer.VectorTile({
    //    source: constructSource(),
    //    zIndex: zIndex
    //});
    return layerVT_WMTS;
};

LayerClass.prototype.createGraphicLayer = function (options) {
    var opt_options = options ? options : [];
    var title = opt_options.title ? opt_options.title : "";
    var zIndex = opt_options.zIndex ? opt_options.zIndex : 1;

    var createStyleFunction = function () {
        return function (feature) {
            var styleDefault = apomGis.style.draw.all ? apomGis.style.draw.all : new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 6,
                    stroke: new ol.style.Stroke({
                        color: 'black',
                        width: 1
                    }),
                    fill: new ol.style.Fill({
                        color: [107, 243, 234, 0.1],
                        opacity: 0.7
                    })
                }),
                fill: new ol.style.Fill({
                    color: [107, 243, 234, 0.7],
                    opacity: 0.7
                }),
                stroke: new ol.style.Stroke({
                    color: [220, 220, 220, 1],
                    width: 1
                })
            });
            return [styleDefault];
        };
    };

    var source = null;
    if (options != undefined) source = new ol.source.Vector(options);
    else source = new ol.source.Vector({
    });

    var graphicLayer = new ol.layer.Vector({
        source: source,
        style: opt_options.default_style ? opt_options.default_style : createStyleFunction(),
        zIndex: zIndex,
        // tru0ong hop wrapX: false when setting in source
        updateWhileAnimating: true,
        updateWhileInteracting: true
    });
    graphicLayer.setZIndex(99);
    var params = [];
    params.title = title;

    var formatGML = null;
    graphicLayer.params = params;
    graphicLayer.formatGML = formatGML;
    graphicLayer.isGraphicLayer = true;

    return graphicLayer;
};