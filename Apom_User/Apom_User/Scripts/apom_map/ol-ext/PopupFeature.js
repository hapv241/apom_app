﻿/**
 * ol-ext - A set of cool extensions for OpenLayers (ol) in node modules structure
 * @description ol3,openlayers,popup,menu,symbol,renderer,filter,canvas,interaction,split,statistic,charts,pie,LayerSwitcher,toolbar,animation
 * @version v3.1.3
 * @author Jean-Marc Viglino
 * @see https://github.com/Viglino/ol-ext#,
 * @license BSD-3-Clause
 */
/** @namespace  ol.ext
 */
/*global ol*/
if (window.ol && !ol.ext) {
    ol.ext = {};
}
/** Inherit the prototype methods from one constructor into another.
 * replace deprecated ol method
 *
 * @param {!Function} childCtor Child constructor.
 * @param {!Function} parentCtor Parent constructor.
 * @function module:ol.inherits
 * @api
 */
ol.ext.inherits = function (child, parent) {
    if (parent === undefined) return false;
    child.prototype = Object.create(parent.prototype);
    child.prototype.constructor = child;
};
// Compatibilty with ol > 5 to be removed when v6 is out
if (window.ol) {
    if (!ol.inherits) ol.inherits = ol.ext.inherits;
}

/** Ajax request
 * @fires success
 * @fires error
 * @param {*} options
 *  @param {string} options.auth Authorisation as btoa("username:password");
 *  @param {string} options.dataType The type of data that you're expecting back from the server, default JSON
 */
ol.ext.Ajax = function (options) {
    options = options || {};
    ol.Object.call(this);
    this._auth = options.auth;
    this.set('dataType', options.dataType || 'JSON');
};
ol.ext.inherits(ol.ext.Ajax, ol.Object);
/** Helper for get
 * @param {*} options
 *  @param {string} options.url
 *  @param {string} options.auth Authorisation as btoa("username:password");
 *  @param {string} options.dataType The type of data that you're expecting back from the server, default JSON
 *  @param {string} options.success
 *  @param {string} options.error
 */
ol.ext.Ajax.get = function (options) {
    var ajax = new ol.ext.Ajax(options);
    if (options.success) ajax.on('success', function (e) { options.success(e.response, e); });
    if (options.error) ajax.on('error', function (e) { options.error(e); });
    ajax.send(options.url, options.data, options.options);
};
/** Send an ajax request (GET)
 * @fires success
 * @fires error
 * @param {string} url
 * @param {*} data Data to send to the server as key / value
 * @param {*} options a set of options that are returned in the 
 *  @param {boolean} options.abort false to prevent aborting the current request, default true
 */
ol.ext.Ajax.prototype.send = function (url, data, options) {
    options = options || {};
    var self = this;
    // Url
    var encode = (options.encode !== false)
    if (encode) url = encodeURI(url);
    // Parameters
    var parameters = '';
    for (var index in data) {
        if (data.hasOwnProperty(index) && data[index] !== undefined) {
            parameters += (parameters ? '&' : '?') + index + '=' + (encode ? encodeURIComponent(data[index]) : data[index]);
        }
    }
    // Abort previous request
    if (this._request && options.abort !== false) {
        this._request.abort();
    }
    // New request
    var ajax = this._request = new XMLHttpRequest();
    ajax.open('GET', url + parameters, true);
    if (this._auth) {
        ajax.setRequestHeader("Authorization", "Basic " + this._auth);
    }
    // Load complete
    this.dispatchEvent({ type: 'loadstart' });
    ajax.onload = function () {
        self._request = null;
        self.dispatchEvent({ type: 'loadend' });
        if (this.status >= 200 && this.status < 400) {
            var response;
            // Decode response
            try {
                switch (self.get('dataType')) {
                    case 'JSON': {
                        response = JSON.parse(this.response);
                        break;
                    }
                    default: {
                        response = this.response;
                    }
                }
            } catch (e) {
                // Error
                self.dispatchEvent({
                    type: 'error',
                    status: 0,
                    statusText: 'parsererror',
                    error: e,
                    options: options,
                    jqXHR: this
                });
                return;
            }
            // Success
            //console.log('response',response)
            self.dispatchEvent({
                type: 'success',
                response: response,
                status: this.status,
                statusText: this.statusText,
                options: options,
                jqXHR: this
            });
        } else {
            self.dispatchEvent({
                type: 'error',
                status: this.status,
                statusText: this.statusText,
                options: options,
                jqXHR: this
            });
        }
    };
    // Oops
    ajax.onerror = function () {
        self._request = null;
        self.dispatchEvent({ type: 'loadend' });
        self.dispatchEvent({
            type: 'error',
            status: this.status,
            statusText: this.statusText,
            options: options,
            jqXHR: this
        });
    };
    // GO!
    ajax.send();
};

/** Vanilla JS helper to manipulate DOM without jQuery
 * @see https://github.com/nefe/You-Dont-Need-jQuery
 * @see https://plainjs.com/javascript/
 * @see http://youmightnotneedjquery.com/
 */
ol.ext.element = {};
/**
 * Create an element
 * @param {string} tagName The element tag, use 'TEXT' to create a text node
 * @param {*} options
 *  @param {string} options.className className The element class name 
 *  @param {Element} options.parent Parent to append the element as child
 *  @param {Element|string} options.html Content of the element
 *  @param {string} options.* Any other attribut to add to the element
 */
ol.ext.element.create = function (tagName, options) {
    options = options || {};
    var elt;
    // Create text node
    if (tagName === 'TEXT') {
        elt = document.createTextNode(options.html || '');
        if (options.parent) options.parent.appendChild(elt);
    } else {
        // Other element
        elt = document.createElement(tagName);
        if (/button/i.test(tagName)) elt.setAttribute('type', 'button');
        for (var attr in options) {
            switch (attr) {
                case 'className': {
                    if (options.className && options.className.trim) elt.setAttribute('class', options.className.trim());
                    break;
                }
                case 'html': {
                    if (options.html instanceof Element) elt.appendChild(options.html)
                    else if (options.html !== undefined) elt.innerHTML = options.html;
                    break;
                }
                case 'parent': {
                    options.parent.appendChild(elt);
                    break;
                }
                case 'style': {
                    this.setStyle(elt, options.style);
                    break;
                }
                case 'change':
                case 'click': {
                    ol.ext.element.addListener(elt, attr, options[attr]);
                    break;
                }
                case 'on': {
                    for (var e in options.on) {
                        ol.ext.element.addListener(elt, e, options.on[e]);
                    }
                    break;
                }
                case 'checked': {
                    elt.checked = !!options.checked;
                    break;
                }
                default: {
                    elt.setAttribute(attr, options[attr]);
                    break;
                }
            }
        }
    }
    return elt;
};
/** Set inner html or append a child element to an element
 * @param {Element} element
 * @param {Element|string} html Content of the element
 */
ol.ext.element.setHTML = function (element, html) {
    if (html instanceof Element) element.appendChild(html)
    else if (html !== undefined) element.innerHTML = html;
};
/** Append text into an elemnt
 * @param {Element} element
 * @param {string} text text content
 */
ol.ext.element.appendText = function (element, text) {
    element.appendChild(document.createTextNode(text || ''));
};
/**
 * Add a set of event listener to an element
 * @param {Element} element
 * @param {string|Array<string>} eventType
 * @param {function} fn
 */
ol.ext.element.addListener = function (element, eventType, fn) {
    if (typeof eventType === 'string') eventType = eventType.split(' ');
    eventType.forEach(function (e) {
        element.addEventListener(e, fn);
    });
};
/**
 * Add a set of event listener to an element
 * @param {Element} element
 * @param {string|Array<string>} eventType
 * @param {function} fn
 */
ol.ext.element.removeListener = function (element, eventType, fn) {
    if (typeof eventType === 'string') eventType = eventType.split(' ');
    eventType.forEach(function (e) {
        element.removeEventListener(e, fn);
    });
};
/**
 * Show an element
 * @param {Element} element
 */
ol.ext.element.show = function (element) {
    element.style.display = '';
};
/**
 * Hide an element
 * @param {Element} element
 */
ol.ext.element.hide = function (element) {
    element.style.display = 'none';
};
/**
 * Test if an element is hihdden
 * @param {Element} element
 * @return {boolean}
 */
ol.ext.element.hidden = function (element) {
    return ol.ext.element.getStyle(element, 'display') === 'none';
};
/**
 * Toggle an element
 * @param {Element} element
 */
ol.ext.element.toggle = function (element) {
    element.style.display = (element.style.display === 'none' ? '' : 'none');
};
/** Set style of an element
 * @param {DOMElement} el the element
 * @param {*} st list of style
 */
ol.ext.element.setStyle = function (el, st) {
    for (var s in st) {
        switch (s) {
            case 'top':
            case 'left':
            case 'bottom':
            case 'right':
            case 'minWidth':
            case 'maxWidth':
            case 'width':
            case 'height': {
                if (typeof (st[s]) === 'number') {
                    el.style[s] = st[s] + 'px';
                } else {
                    el.style[s] = st[s];
                }
                break;
            }
            default: {
                el.style[s] = st[s];
            }
        }
    }
};
/**
 * Get style propertie of an element
 * @param {DOMElement} el the element
 * @param {string} styleProp Propertie name
 * @return {*} style value
 */
ol.ext.element.getStyle = function (el, styleProp) {
    var value, defaultView = (el.ownerDocument || document).defaultView;
    // W3C standard way:
    if (defaultView && defaultView.getComputedStyle) {
        // sanitize property name to css notation
        // (hypen separated words eg. font-Size)
        styleProp = styleProp.replace(/([A-Z])/g, "-$1").toLowerCase();
        value = defaultView.getComputedStyle(el, null).getPropertyValue(styleProp);
    } else if (el.currentStyle) { // IE
        // sanitize property name to camelCase
        styleProp = styleProp.replace(/-(\w)/g, function (str, letter) {
            return letter.toUpperCase();
        });
        value = el.currentStyle[styleProp];
        // convert other units to pixels on IE
        if (/^\d+(em|pt|%|ex)?$/i.test(value)) {
            return (function (value) {
                var oldLeft = el.style.left, oldRsLeft = el.runtimeStyle.left;
                el.runtimeStyle.left = el.currentStyle.left;
                el.style.left = value || 0;
                value = el.style.pixelLeft + "px";
                el.style.left = oldLeft;
                el.runtimeStyle.left = oldRsLeft;
                return value;
            })(value);
        }
    }
    if (/px$/.test(value)) return parseInt(value);
    return value;
};
/** Get outerHeight of an elemen
 * @param {DOMElement} elt
 * @return {number}
 */
ol.ext.element.outerHeight = function (elt) {
    return elt.offsetHeight + ol.ext.element.getStyle(elt, 'marginBottom')
};
/** Get outerWidth of an elemen
 * @param {DOMElement} elt
 * @return {number}
 */
ol.ext.element.outerWidth = function (elt) {
    return elt.offsetWidth + ol.ext.element.getStyle(elt, 'marginLeft')
};
/** Get element offset rect
 * @param {DOMElement} elt
 * @return {*} 
 */
ol.ext.element.offsetRect = function (elt) {
    var rect = elt.getBoundingClientRect();
    return {
        top: rect.top + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0),
        left: rect.left + (window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0),
        height: rect.height || (rect.bottom - rect.top),
        width: rect.widtth || (rect.right - rect.left)
    }
};
/** Make a div scrollable without scrollbar.
 * On touch devices the default behavior is preserved
 * @param {DOMElement} elt
 * @param {function} onmove a function that takes a boolean indicating that the div is scrolling
 */
ol.ext.element.scrollDiv = function (elt, options) {
    var pos = false;
    var speed = 0;
    var d, dt = 0;
    var onmove = (typeof (options.onmove) === 'function' ? options.onmove : function () { });
    var page = options.vertical ? 'pageY' : 'pageX';
    var scroll = options.vertical ? 'scrollTop' : 'scrollLeft';
    // Prevent image dragging
    elt.querySelectorAll('img').forEach(function (i) {
        i.ondragstart = function () { return false; };
    });
    // Start scrolling
    ol.ext.element.addListener(elt, ['mousedown'], function (e) {
        pos = e[page];
        dt = new Date();
        elt.classList.add('ol-move');
    });
    // Register scroll
    ol.ext.element.addListener(window, ['mousemove'], function (e) {
        if (pos !== false) {
            var delta = pos - e[page];
            elt[scroll] += delta;
            d = new Date();
            if (d - dt) {
                speed = (speed + delta / (d - dt)) / 2;
            }
            pos = e[page];
            dt = d;
            // Tell we are moving
            if (delta) onmove(true);
        } else {
            // Not moving yet
            onmove(false);
        }
    });
    // Stop scrolling
    ol.ext.element.addListener(window, ['mouseup'], function (e) {
        elt.classList.remove('ol-move');
        dt = new Date() - dt;
        if (dt > 100) {
            // User stop: no speed
            speed = 0;
        } else if (dt > 0) {
            // Calculate new speed
            speed = ((speed || 0) + (pos - e[page]) / dt) / 2;
        }
        elt[scroll] += speed * 100;
        pos = false;
        speed = 0;
        dt = 0;
    });
    // Handle mousewheel
    if (options.mousewheel && !elt.classList.contains('ol-touch')) {
        ol.ext.element.addListener(elt,
          ['mousewheel', 'DOMMouseScroll', 'onmousewheel'],
          function (e) {
              var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
              elt.classList.add('ol-move');
              elt[scroll] -= delta * 30;
              return false;
          }
        );
    }
};


/*	Copyright (c) 2016 Jean-Marc VIGLINO, 
  released under the CeCILL-B license (French BSD license)
  (http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.txt).
*/
/**
 * @classdesc
 * A popup element to be displayed over the map and attached to a single map
 * location. The popup are customized using CSS.
 *
 * @example
var popup = new ol.Overlay.Popup();
map.addOverlay(popup);
popup.show(coordinate, "Hello!");
popup.hide();
*
* @constructor
* @extends {ol.Overlay}
* @param {} options Extend Overlay options 
*	@param {String} options.popupClass the a class of the overlay to style the popup.
*	@param {bool} options.closeBox popup has a close box, default false.
*	@param {function|undefined} options.onclose: callback function when popup is closed
*	@param {function|undefined} options.onshow callback function when popup is shown
*	@param {Number|Array<number>} options.offsetBox an offset box
*	@param {ol.OverlayPositioning | string | undefined} options.positionning 
*		the 'auto' positioning var the popup choose its positioning to stay on the map.
* @api stable
*/
ol.Overlay.Popup = function (options) {
    var self = this;
    options = options || {};
    if (typeof (options.offsetBox) === 'number') this.offsetBox = [options.offsetBox, options.offsetBox, options.offsetBox, options.offsetBox];
    else this.offsetBox = options.offsetBox;
    // Popup div
    var element = document.createElement("div");
    //element.classList.add('ol-overlaycontainer-stopevent');
    options.element = element;
    // Anchor div
    var anchorElement = document.createElement("div");
    anchorElement.classList.add("anchor");
    element.appendChild(anchorElement);
    // Content
    this.content = ol.ext.element.create("div", {
        html: options.html || '',
        className: "ol-popup-content-new",
        parent: element
    });
    // Closebox
    this.closeBox = options.closeBox;
    this.onclose = options.onclose;
    this.onshow = options.onshow;
    var button = document.createElement("button");
    button.classList.add("closeBox");
    button.classList.add("closeBox-new");
    if (options.closeBox) button.classList.add('hasclosebox');
    button.setAttribute('type', 'button');
    // element.insertBefore(button, anchorElement);
    button.addEventListener("click", function () {
        self.hide();
    });
    // Stop event
    if (options.stopEvent) {
        element.addEventListener("mousedown", function (e) { e.stopPropagation(); });
        element.addEventListener("touchstart", function (e) { e.stopPropagation(); });
    }
    ol.Overlay.call(this, options);
    this._elt = this.element_ ? this.element_ : this.element;
    // call setPositioning first in constructor so getClassPositioning is called only once
    this.setPositioning(options.positioning || 'auto');
    this.setPopupClass(options.popupClass || options.className || 'default');
    // Show popup on timeout (for animation purposes)
    if (options.position) {
        setTimeout(function () { this.show(options.position); }.bind(this));
    }
};
ol.ext.inherits(ol.Overlay.Popup, ol.Overlay);
/**
 * Get CSS class of the popup according to its positioning.
 * @private
 */
ol.Overlay.Popup.prototype.getClassPositioning = function () {
    var c = "";
    var pos = this.getPositioning();
    if (/bottom/.test(pos)) c += "ol-popup-bottom ";
    if (/top/.test(pos)) c += "ol-popup-top ";
    if (/left/.test(pos)) c += "ol-popup-left ";
    if (/right/.test(pos)) c += "ol-popup-right ";
    if (/^center/.test(pos)) c += "ol-popup-middle ";
    if (/center$/.test(pos)) c += "ol-popup-center ";
    return c;
};
/**
 * Set a close box to the popup.
 * @param {bool} b
 * @api stable
 */
ol.Overlay.Popup.prototype.setClosebox = function (b) {
    this.closeBox = b;
    if (b) this._elt.classList.add("hasclosebox");
    else this._elt.classList.remove("hasclosebox");
};
/**
 * Set the CSS class of the popup.
 * @param {string} c class name.
 * @api stable
 */
ol.Overlay.Popup.prototype.setPopupClass = function (c) {
    this._elt.className = "";
    var classesPositioning = this.getClassPositioning().split(' ')
      .filter(function (className) {
          return className.length > 0;
      });
    var classes = ["ol-popup"];
    if (c) {
        c.split(' ').filter(function (className) {
            return className.length > 0;
        })
        .forEach(function (className) {
            classes.push(className);
        });
    } else {
        classes.push("default");
    }
    classesPositioning.forEach(function (className) {
        classes.push(className);
    });
    if (this.closeBox) {
        classes.push("hasclosebox");
    }
    this._elt.classList.add.apply(this._elt.classList, classes);
};
/**
 * Add a CSS class to the popup.
 * @param {string} c class name.
 * @api stable
 */
ol.Overlay.Popup.prototype.addPopupClass = function (c) {
    this._elt.classList.add(c);
};
/**
 * Remove a CSS class to the popup.
 * @param {string} c class name.
 * @api stable
 */
ol.Overlay.Popup.prototype.removePopupClass = function (c) {
    this._elt.classList.remove(c);
};
/**
 * Set positionning of the popup
 * @param {ol.OverlayPositioning | string | undefined} pos an ol.OverlayPositioning 
 * 		or 'auto' to var the popup choose the best position
 * @api stable
 */
ol.Overlay.Popup.prototype.setPositioning = function (pos) {
    if (pos === undefined)
        return;
    if (/auto/.test(pos)) {
        this.autoPositioning = pos.split('-');
        if (this.autoPositioning.length == 1) this.autoPositioning[1] = "auto";
    }
    else this.autoPositioning = false;
    pos = pos.replace(/auto/g, "center");
    if (pos == "center") pos = "bottom-center";
    this.setPositioning_(pos);
};
/** @private
 * @param {ol.OverlayPositioning | string | undefined} pos
 */
ol.Overlay.Popup.prototype.setPositioning_ = function (pos) {
    if (this._elt) {
        ol.Overlay.prototype.setPositioning.call(this, pos);
        this._elt.classList.remove("ol-popup-top", "ol-popup-bottom", "ol-popup-left", "ol-popup-right", "ol-popup-center", "ol-popup-middle");
        var classes = this.getClassPositioning().split(' ')
          .filter(function (className) {
              return className.length > 0;
          });
        this._elt.classList.add.apply(this._elt.classList, classes);
    }
};
/** Check if popup is visible
* @return {boolean}
*/
ol.Overlay.Popup.prototype.getVisible = function () {
    return this._elt.classList.contains("visible");
};
/**
 * Set the position and the content of the popup.
 * @param {ol.Coordinate|string} coordinate the coordinate of the popup or the HTML content.
 * @param {string|undefined} html the HTML content (undefined = previous content).
 * @example
var popup = new ol.Overlay.Popup();
// Show popup
popup.show([166000, 5992000], "Hello world!");
// Move popup at coord with the same info
popup.show([167000, 5990000]);
// set new info
popup.show("New informations");
* @api stable
*/
ol.Overlay.Popup.prototype.show = function (coordinate, html) {
    if (!html && typeof (coordinate) == 'string') {
        html = coordinate;
        coordinate = null;
    }
    if (coordinate === true) {
        coordinate = this.getPosition();
    }
    var self = this;
    var map = this.getMap();
    if (!map) return;
    if (html && html !== this.prevHTML) {
        // Prevent flickering effect
        this.prevHTML = html;
        this.content.innerHTML = "";
        if (html instanceof Element) {
            this.content.appendChild(html);
        } else {
            this.content.insertAdjacentHTML('beforeend', html);
        }
        var button = document.createElement("button");
        button.classList.add("closeBox");
        button.classList.add("closeBox-new");
        button.classList.add('hasclosebox');
        button.setAttribute('type', 'button');
        button.addEventListener("click", function () {
            self.hide();
        });
        this.content.insertBefore(button, this.content.childNodes[0]);
        // Counter
        if (this._features.length > 1) {
            var div = ol.ext.element.create('DIV', { className: 'ol-count' });
            ol.ext.element.create('DIV', { className: 'ol-prev', parent: div })
              .addEventListener('click', function () {
                  this._count--;
                  if (this._count < 1) this._count = this._features.length;
                  html = this._getHtml(this._features[this._count - 1]).html;
                  ol.Overlay.Popup.prototype.show.call(this, this.getPosition(), html);
              }.bind(this));
            ol.ext.element.create('SPAN', { html: this._count + '/' + this._features.length, parent: div, className: 'ol-count-text' });
            ol.ext.element.create('DIV', { className: 'ol-next', parent: div })
              .addEventListener('click', function () {
                  this._count++;
                  if (this._count > this._features.length) this._count = 1;
                  html = this._getHtml(this._features[this._count - 1]).html;
                  ol.Overlay.Popup.prototype.show.call(this, this.getPosition(), html);
              }.bind(this));
            this.content.insertBefore(div, this.content.childNodes[0]);
        }

        // Refresh when loaded (img)
        Array.prototype.slice.call(this.content.querySelectorAll('img'))
          .forEach(function (image) {
              image.addEventListener("load", function () {
                  map.renderSync();
              });
          });
    }
    if (coordinate) {
        // Auto positionning
        var height = this.getMap().getSize()[1] / 2;
        if (this.autoPositioning) {
            var p = map.getPixelFromCoordinate(coordinate);
            var s = map.getSize();
            var pos = [];
            if (this.autoPositioning[0] == 'auto') {
                pos[0] = (p[1] < s[1] / 3) ? "top" : "bottom";
            }
            else pos[0] = this.autoPositioning[0];
            pos[1] = (p[0] < 2 * s[0] / 3) ? "left" : "right";
            this.setPositioning_(pos[0] + "-" + pos[1]);
            if (this.offsetBox) {
                this.setOffset([this.offsetBox[pos[1] == "left" ? 2 : 0], this.offsetBox[pos[0] == "top" ? 3 : 1]]);
            }
            if (pos[0] == 'top') {
                height = s[1] - p[1] - 60;
            } else {
                height = p[1] - 40;
            }
        } else {
            if (this.offsetBox) {
                this.setOffset(this.offsetBox);
            }
        }
        // Show
        this.setPosition(coordinate);
        //var height = this.getMap().getSize()[1] / 2;
        //$('.ol-popupfeature').css('max-height', height + 'px')
        $('.ol-popupfeature').css('overflow-y', 'auto').css('width', '100%').css('min-width', '250px');
        // Set visible class (wait to compute the size/position first)
        this._elt.parentElement.style.display = '';
        if (typeof (this.onshow) == 'function') this.onshow();
        this._tout = setTimeout(function () {
            self._elt.classList.add("visible");
        }, 0);
    }
};
/**
 * Hide the popup
 * @api stable
 */
ol.Overlay.Popup.prototype.hide = function () {
    if (this.getPosition() == undefined) return;
    if (typeof (this.onclose) == 'function') this.onclose();
    this.setPosition(undefined);
    if (this._tout) clearTimeout(this._tout);
    this._elt.classList.remove("visible");
    //if (typeof apomGis != 'undefined') apomGis.Params.map.graphicLayerIdentify.removeAllFeature();
};



/*	Copyright (c) 2018 Jean-Marc VIGLINO, 
  released under the CeCILL-B license (French BSD license)
  (http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.txt).
*/
/** Template attributes for popup
 * @typedef {Object} TemplateAttributes
 * @property {string} title
 * @property {function} format a function that takes an attribute and returns it formated
 * @property {string} before string to instert before the attribute (prefix)
 * @property {string} after string to instert after the attribute (sudfix)
 * @property {function} value a function that takes feature and a value and returns a value (calculated attributes)
 */
/** Template 
 * @typedef {Object} Template
 * @property {string|function} title title of the popup, attribute name or a function that takes a feature and returns the title
 * @property {Object.<TemplateAttributes>} attributes a list of template attributes 
 */
/**
 * A popup element to be displayed on a feature.
 *
 * @constructor
 * @extends {ol.Overlay.Popup}
 * @param {} options Extend Popup options 
 *  @param {String} options.popupClass the a class of the overlay to style the popup.
 *  @param {bool} options.closeBox popup has a close box, default false.
 *  @param {function|undefined} options.onclose: callback function when popup is closed
 *  @param {function|undefined} options.onshow callback function when popup is shown
 *  @param {Number|Array<number>} options.offsetBox an offset box
 *  @param {ol.OverlayPositioning | string | undefined} options.positionning 
 *    the 'auto' positioning var the popup choose its positioning to stay on the map.
 *  @param {Template} options.template A template with a list of properties to use in the popup
 *  @param {boolean} options.canFix Enable popup to be fixed, default false
 *  @param {boolean} options.showImage display image url as image, default false
 *  @param {boolean} options.maxChar max char to display in a cell, default 200
 *  @api stable
 */
ol.Overlay.PopupFeature = function (options) {
    options = options || {};
    options.closeBox = true;
    ol.Overlay.Popup.call(this, options);
    this.idenitfyDataGeoTiff = options.idenitfyDataGeoTiff ? options.idenitfyDataGeoTiff : null;
    this.isZoom = options.isZoom || null;
    this.setTemplate(options.template);
    this.set('canFix', options.canFix)
    this.set('showImage', options.showImage)
    this.set('maxChar', options.maxChar || 200)
    this.features = [];
    this.geometry = typeof apomGis != 'undefined' ? new apomGis.Interactions.GeometryClass(options.map) : null;
    var _this = this;
    // Bind with a select interaction
    if (options.map && (typeof options.map.on === 'function') && typeof apomGis != 'undefined') {
        this._map = options.map;
        options.map.on('singleclick', function (e) {
            var start = new Date().getTime();
            var map = options.map;
            var action = options.map.parent.coreImpl.action;
            var coordinate = e.coordinate;
            var features = [];
            if (action === apomGis.Config.action.identify) {
                var isLayerVectorTile = false;
                var isLayerVector = false;
                // truong hop vectortile || vector
                var feature = map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
                    //
                    if ((layer != null && layer != undefined)) {
                        if (layer instanceof ol.layer.VectorTile) isLayerVectorTile = true;
                        else if (layer instanceof ol.layer.Vector) isLayerVector = true;
                        features.push(feature);
                    }
                });

                var view = map.getView();
                var is_showpopup = !isLayerVector && features.length > 0;

                // truong hop wms || geotiff/raster layer
                if (_this.idenitfyDataGeoTiff) {
                    _this.idenitfyDataGeoTiff(map, coordinate, function (properties) {
                        if (properties != null && properties.length > 0) {
                            for (let i = 0; i < properties.length; i++) {
                                var feature = new ol.Feature();
                                feature.setGeometry(new ol.geom.Point([coordinate[0], coordinate[1]]));
                                feature.setProperties(properties[i]);
                                features.push(feature);
                            }
                        }
                        var is_showpopup = !isLayerVector && features.length > 0;

                        if (isLayerVector) {
                            // dang cap nhat thi ko hien popup
                        }
                        if (is_showpopup) {
                            _this.show(coordinate, features);
                        }
                    });
                }
                else {
                    if (isLayerVector) {
                        // dang cap nhat thi ko hien popup
                    }
                    if (is_showpopup) {
                        _this.show(coordinate, features);
                    }
                }

            }
            else {
                this.hide();
            }
            var time = new Date().getTime() - start;
            console.log('THOI GIAN: ', time);
        }.bind(this));
    }
};
ol.ext.inherits(ol.Overlay.PopupFeature, ol.Overlay.Popup);
/** Set the template
 * @param {Template} template A template with a list of properties to use in the popup
 */
ol.Overlay.PopupFeature.prototype.setTemplate = function (template) {
    this._template = template;
    if (this._template && this._template.attributes instanceof Array) {
        var att = {};
        this._template.attributes.forEach(function (a) {
            att[a] = true;
        });
        this._template.attributes = att;
    }
};
/** Show the popup on the map
 * @param {ol.coordinate|undefined} coordinate Position of the popup
 * @param {ol.Feature|Array<ol.Feature>} features The features on the popup
 */
ol.Overlay.PopupFeature.prototype.show = function (coordinate, features) {
    if (coordinate instanceof ol.Feature
      || (coordinate instanceof Array && coordinate[0] instanceof ol.Feature)) {
        features = coordinate;
        coordinate = null;
    }
    if (!(features instanceof Array)) features = [features];
    this._features = features.slice();
    if (!this._count) this._count = 1;

    // Calculate html upon feaures attributes
    this._count = 1;
    var f = this.get('keepSelection') ? current || features[0] : features[0];
    var html = this._getHtml(f).html;
    if (html) {
        if (!this.element.classList.contains('ol-fixed')) this.hide();
        if (!coordinate || features[0].getGeometry().getType() === 'Point') {
            coordinate = features[0].getGeometry().getFirstCoordinate ? features[0].getGeometry().getFirstCoordinate() : features[0].getGeometry().getFlatCoordinates();
        }
        ol.Overlay.Popup.prototype.show.call(this, coordinate, html);
        
        var height = this.getMap().getSize()[1] / 2;
        var p = this.getMap().getPixelFromCoordinate(coordinate);
        var s = this.getMap().getSize();
        var pos = [];
        if (this.autoPositioning[0] == 'auto') {
            pos[0] = (p[1] < s[1] / 3) ? "top" : "bottom";
        }
        else pos[0] = this.autoPositioning[0];
        if (pos[0] == 'top') {
            height = s[1] - p[1] - 60;
        } else {
            height = p[1] - 40;
        }
        //$('.ol-popupfeature').css('max-height', height + 'px')
        $('.ol-popupfeature').css('overflow-y', 'auto').css('width', '100%').css('min-width', '250px');
        //console.log('Thoi gian EVENT: ', new Date().getTime() - dateEvent);
    } else {
        this.hide();
    }
};
/**
 * @private
 */
ol.Overlay.PopupFeature.prototype._getHtml = function (feature) {
    if (!feature) return '';
    var html = ol.ext.element.create('DIV', { className: 'ol-popupfeature' });
    if (this.get('canFix')) {
        ol.ext.element.create('I', { className: 'ol-fix', parent: html })
          .addEventListener('click', function () {
              this.element_.classList.toggle('ol-fixed');
          }.bind(this));
    }
    var template = this._template;
    // calculate template
    if (!template || !template.attributes) {
        template = template || {};
        template.attributes = {};
        for (var i in feature.getProperties()) if (i != 'geometry') {
            template.attributes[i] = i;
        }
    }
    // Display title
    if (template.title) {
        var title;
        if (typeof template.title === 'function') {
            title = template.title(feature);
        } else {
            title = feature.get(template.title);
        }
        ol.ext.element.create('div', { html: title, parent: html, className: 'ol-popuptitle' });
    }
    // Display properties in a table
    if (template.attributes) {
        var atts = template.attributes;
        var content, val, geostr, objectHtml;
        for (var att in atts) {
            var a = atts[att];
            //tr = ol.ext.element.create('TR', { parent: table, style: {'background-color' : '#fff'} });
            //ol.ext.element.create('TD', { html: a.title, parent: tr });
            val = feature.get(att);
            // Get calculated value
            if (typeof (a.value) === 'function') {
                objectHtml = a.value(feature, val);
                val = objectHtml.html;
                geostr = objectHtml.geostr;
            }
            // Show image or content
            if (this.get('showImage') && /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/.test(val)) {
                content = ol.ext.element.create('IMG', {
                    src: val
                });
            } else {
                content = (a.before || '') + (a.format ? a.format(val) : val) + (a.after || '');
                var maxc = this.get('maxChar') || 200;
                //if (typeof (content) === 'string' && content.length > maxc) content = content.substr(0, maxc) + '[...]';
            }
            // Add value
            //ol.ext.element.create('TD', {
            //    html: content,
            //    parent: tr
            //});
            // lấy diện tích hoặc chiều dài
            //var table = ol.ext.element.create('TABLE', { html: content, parent: html, style: { 'margin-top': '10px' }, className: 'table table-striped table-bordered' });
            // tuannh
            var table = ol.ext.element.create('TABLE', { html: content, parent: html, style: { 'margin-top': '0px;' }, className: 'table table-striped table-no-bordered' });

        }

    }
    // Zoom button
    if (this.isZoom)
        ol.ext.element.create('BUTTON', { className: 'ol-zoombt', parent: html })
          .addEventListener('click', function () {
              if (feature.getGeometry().getType() === 'Point') {
                  //this.getMap().getView().animate({
                  //  center: feature.getGeometry().getFirstCoordinate(),
                  //  zoom:  Math.max(this.getMap().getView().getZoom(), 18)
                  //});
                  var ext = feature.getGeometry().getExtent();
                  this.getMap().getView().fit(ext, { duration: 1000 });
              } else {
                  var ext = feature.getGeometry().getExtent();
                  this.getMap().getView().fit(ext, { duration: 1000 });
              }
          }.bind(this));
    // Counter chuyen leen treen nut close
    //if (this._features.length > 1) {
    //    var div = ol.ext.element.create('DIV', { className: 'ol-count', parent: html });
    //    ol.ext.element.create('DIV', { className: 'ol-prev', parent: div })
    //      .addEventListener('click', function () {
    //          this._count--;
    //          if (this._count < 1) this._count = this._features.length;
    //          html = this._getHtml(this._features[this._count - 1]);
    //          ol.Overlay.Popup.prototype.show.call(this, this.getPosition(), html);
    //      }.bind(this));
    //    ol.ext.element.create('TEXT', { html: this._count + '/' + this._features.length, parent: div });
    //    ol.ext.element.create('DIV', { className: 'ol-next', parent: div })
    //      .addEventListener('click', function () {
    //          this._count++;
    //          if (this._count > this._features.length) this._count = 1;
    //          html = this._getHtml(this._features[this._count - 1]);
    //          ol.Overlay.Popup.prototype.show.call(this, this.getPosition(), html);
    //      }.bind(this));
    //}
    // Use select interaction
    if (this._map) {
        this._nomap = true;
        // this._map.getFeatures().clear();
        this.features = [];
        //this._map.getFeatures().push(feature);
        this.features.push(feature);
        this._nomap = false;
    }
    //if (typeof apomGis != 'undefined') apomGis.Params.map.graphicLayerIdentify.removeAllFeature();
    var format = new ol.format.WKT();
    var geomF = geostr ? format.readGeometryFromText(geostr) : null;
    if (geomF) {
        //let geoTyle = geomF.getType().toLocaleLowerCase(), geom = geomF, attrs = [], isIdentify = true;
        //apomGis.Params.map.graphicLayerIdentify.addFeature(geoTyle, geom, attrs, apomGis.style.highlight, isIdentify);
    }
    return { 'html': html, 'geostr': geostr };
};
/** Fix the popup
 * @param {boolean} fix
 */
ol.Overlay.PopupFeature.prototype.setFix = function (fix) {
    if (fix) this.element.classList.add('ol-fixed');
    else this.element.classList.remove('ol-fixed');
};
/** Is a popup fixed
 * @return {boolean} 
 */
ol.Overlay.PopupFeature.prototype.getFix = function () {
    return this.element.classList.contains('ol-fixed');
};
/** Get a function to use as format to get local string for an attribute
 * if the attribute is a number: Number.toLocaleString()
 * if the attribute is a date: Date.toLocaleString()
 * otherwise the attibute itself
 * @param {string} locales string with a BCP 47 language tag, or an array of such strings
 * @param {*} options Number or Date toLocaleString options
 * @return {function} a function that takes an attribute and return the formated attribute
 */
ol.Overlay.PopupFeature.localString = function (locales, options) {
    return function (a) {
        if (a && a.toLocaleString) {
            return a.toLocaleString(locales, options);
        } else {
            // Try to get a date from a string
            var date = new Date(a);
            if (isNaN(date)) return a;
            else return date.toLocaleString(locales, options);
        }
    };
};

var style_pop = document.createElement('style');
style_pop.innerHTML = `
  .ol-popup.ol-popupfeature tr: nth-child(2n+1) {
    background-color: #eee;
  }
.ol-popup .closeBox:after {
    content: "\\00d7";
  font-size:1.5em;
    top: 50%;
    left: 0;
    right: 0;
    width: 100%;
  text-align: center;
  line-height: 1em;
    margin: -0.5em 0;
    position: absolute;
    }
    .ol-popup .closeBox {
  background-color: rgba(0, 60, 136, 0.5);
    color: #fff;
    border: 0;
  border-radius: 2px;
    cursor: pointer;
    float: right;
  font-size: 0.9em;
  font-weight: 700;
    width: 1.4em;
    height: 1.4em;
    margin: 5px 5px 0 0;
    padding: 0;
    position: relative;
}
.ol-popup.hasclosebox.closeBox {
    display: block;
}

.ol-popup.closeBox: hover {
  background-color: rgba(0, 60, 136, 0.7);
}
.ol-popup {
  font-size: 0.9em;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.ol-popup.ol-popup-content {
    overflow: hidden;
    cursor: default;
    padding: 0.25em 0.5em;
}
.ol-popup.hasclosebox.ol-popup-content {
  margin-right: 1.7em;
}
.ol-popup.ol-popup-content: after {
    clear: both;
    content: "";
    display: block;
  font-size: 0;
    height: 0;
}
.ol-popup.default {
  background-color: #fff;
    border: 1px solid #69f;
  border-radius: 5px;
    margin: 11px 0;
}
.ol-popup .anchor {
    display: block;
    width: 0px;
    height: 0px;
    background: red;
    position: absolute;
    margin: -11px 21px;
    pointer-events: none;
}
.ol-popup .anchor:after,
.ol-popup .anchor:before {
    position: absolute;
}
.ol-popup-right .anchor:after,
.ol-popup-right.anchor: before {
    right: 0;
}
.ol-popup-top .anchor { top: 0; }
.ol-popup-bottom .anchor { bottom: 0; }
.ol-popup-right .anchor { right: 0; }
.ol-popup-left .anchor { left: 0; }
.ol-popup-center .anchor {
    left: 50%;
  margin-left: 0!important;
}
.ol-popup-middle .anchor {
    top: 50%;
  margin-top: 0!important;
}
.ol-popup-center .ol-popup-middle .anchor {
    display: none;
}
.ol-popup.default .anchor:after,
.ol-popup.default .anchor:before {
    content: "";
  border-color: #69f transparent;
  border-style: solid;
  border-width: 11px;
    margin: 0 -11px;
}
.ol-popup.default .anchor:after {
  border-color: #fff transparent;
  border-width: 9px;
    margin: 3px -9px;
}

.ol-popup-top.default .anchor:before,
.ol-popup-top.default .anchor:after {
  border-top: 0;
    top: 0;
}

.ol-popup-bottom.default .anchor:before,
.ol-popup-bottom.default .anchor:after {
  border-bottom: 0;
    bottom: 0;
}

.ol-popup-middle.default .anchor:before {
    margin: -11px -33px;
  border-color: transparent #69f;
}
.ol-popup-middle.default .anchor:after {
    margin: -9px -30px;
  border-color: transparent #fff;
}
.ol-popup-middle.ol-popup-left.default.anchor: before,
.ol-popup-middle.ol-popup-left.default.anchor: after
{	border-left: 0;
}
.ol-popup-middle.ol-popup-right.default .anchor:before,
.ol-popup-middle.ol-popup-right.default .anchor:after
{	border-right: 0;
}
.ol-popup-left.default {
    margin: 11px 10px 11px -22px;
}
.ol-popup-right.default {
    margin: 11px -22px 11px 10px;
}
.ol-popup-middle.default {
    margin: 0 10px;
}
@keyframes ol-popup_bounce{
    0%, 50%, 80%, 100%{
        transform: scale(0) !important;
    }
}
  `;
document.head.appendChild(style_pop);