﻿apomGis.CoreClass = function (map) {
    if (!(map instanceof ol.Map)) {
        throw new Error('Please provide a valid OpenLayers map object.');
    }
    this.map = map; // map_control
    this.zoomTool = null;
    this.extentTool = null;

    this.action = apomGis.Config.action.identify;

    this.setMap = function (map) {
        this.map = map;
    };

    // dang ky prototype cho class va layer
    this.registerPrototype = function () {
        // controls
        var controlClass = apomGis.Controls;
        for (var control in controlClass) {
            controlClass[control].prototype = new ControlClass(this.map);
        };

        // layers
        var layerClass = apomGis.Layers;
        for (var layer in layerClass) {
            layerClass[layer].prototype = new LayerClass(this.map);
        };
    };

    /* base gis */
    this.createZoomTool = function () {
        if (this.zoomTool == null) {
            this.zoomTool = new apomGis.Interactions.ZoomClass(this.map);
            this.zoomTool.create();
        }
    };

    this.createExtentTool = function () {
        if (this.extentTool == null) {
            this.extentTool = new apomGis.Interactions.ExtentClass(this.map);
            this.extentTool.create();
        }
    };

    this.fixZoomIn = function () {
        this.createZoomTool();
        this.zoomTool.fixZoomIn();
    };

    this.fixZoomOut = function () {
        this.createZoomTool();
        this.zoomTool.fixZoomOut();
    };

    // zoomIn - zoomOut
    this.Zoom = function (delta, duration) {
        var zoom = new apomGis.Interactions.ZoomClass(this.map);
        zoom.zoomAuto(delta, duration);
    };

    // zoomto
    this.zoomToPoint = function (x, y) {
        this.createZoomTool();
        this.zoomTool.zoomToPoint(x, y);
    };

    this.panToCoordinate = function (coordinates) {
        this.createZoomTool();
        this.zoomTool.panToCoordinate(coordinates);
    };

    this.zoomToPolyline = function (geometry) {
        this.createZoomTool();
        this.zoomTool.zoomToPolyline(geometry);
    };

    this.zoomToPolygon = function (geometry) {
        this.createZoomTool();
        this.zoomTool.zoomToPolygon(geometry);
    };

    this.zoomToExtent = function (extent, deltaX, deltaY) {
        this.createZoomTool();
        this.zoomTool.zoomToExtent(extent, deltaX, deltaY);
    };

    this.backExtent = function () {
        this.createExtentTool();
        this.extentTool.backExtent();
    };

    this.nextExtent = function () {
        this.createExtentTool();
        this.extentTool.nextExtent();
    };

    /* fullscreen */
    this.fullScrennMap = function () {
        let elem = this.map.parent.target;
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
        }
    };

    /* close fullscreen */
    this.closeFullscreenMap = function () {
        let elem = $('#' + this.map.parent.target).get(0);
        if (elem.exitFullscreen) {
            elem.exitFullscreen();
        } else if (elem.mozCancelFullScreen) { /* Firefox */
            elem.mozCancelFullScreen();
        } else if (elem.webkitExitFullscreen) { /* Chrome, Safari and Opera */
            elem.webkitExitFullscreen();
        } else if (elem.msExitFullscreen) { /* IE/Edge */
            elem.msExitFullscreen();
        }
    };

    this.panToCoordinate = function (coordinates) {
        var zoom = new apomGis.Interactions.ZoomClass(this.map);
        zoom.panToCoordinate(coordinates);
    };
    /* end base gis */
};