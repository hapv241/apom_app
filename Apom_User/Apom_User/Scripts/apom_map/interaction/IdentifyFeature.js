﻿apomGis.Interactions.IdentifyClass = function (options) {
    this.getInfo = function (feature, hostAPI, langId) {
        console.log('Identify feature...');
    };

    this._options = options ? options : [];
    this.map = this._options.map;
    this.callbackInfo = this._options.callback ? this._options.callback : this.getInfo;
    this.hostAPI = this.map ? this.map.parent.hostAPI : '';
    this.endpoinIdentify = this._options.endpoint_identify ? this._options.endpoint_identify : '/api/administrative/info_by_coor';
    this.langId = this.map ? this.map.parent.langId : 'vi';
    this.feature = null;
    this.draw = null;

    // custom feature
    this.infoFeatureStation = null;
    this.infoFeatureSatellite = null;

    this.create = function () {
        if (this.map == null) return;
        var hostAPI = this.hostAPI;
        var map = this.map;
        var _this = this;
        map.on('singleclick', function (e) {
            var start = new Date().getTime();
            var action = map.parent.coreImpl.action;
            var coordinate = e.coordinate;
            var features = [];
            if (action === apomGis.Config.action.identify) {
                var isLayerVectorTile = false;
                var isLayerVector = false;
                // case of: vectortile || vector
                // only get the nearest feature
                var nearestFeature = null;
                var feature = map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
                    //
                    if ((layer != null && layer != undefined)) {
                        if (layer instanceof ol.layer.VectorTile) isLayerVectorTile = true;
                        else if (layer instanceof ol.layer.Vector) isLayerVector = true;
                        features.push(feature);
                        if (nearestFeature == null) nearestFeature = feature;
                        else {
                            nearestFeature = _this.getNearestFeature(coordinate, nearestFeature, feature);
                        }
                    }
                });

                _this.feature = nearestFeature;

                if (map.parent.graphicLayer) map.parent.graphicLayer.removeAllFeature();
                let isStation = true;
                let isGeoTiff = true;
                // geotiff
                if (_this.feature === null) {
                    isStation = false;
                    _this.infoFeatureStation = null;
                    // clear feature vectortile
                    map.parent.selectionFeatureVT = {};
                    if (map.parent.selectionLayerVT) map.parent.selectionLayerVT.changed();
                }
                else {
                    // get coor of station
                    let flatCoor = _this.feature.getGeometry().getFlatCoordinates();
                    coordinate[0] = flatCoor[0];
                    coordinate[1] = flatCoor[1];
                    // set selected feature style
                    // only style pm25
                    if (_this.feature.getProperties().aqi_pm25) {
                        map.parent.selectionFeatureVT = {};
                        map.parent.selectionFeatureVT[_this.feature.getProperties().gid] = _this.feature;
                        if (map.parent.selectionLayerVT) map.parent.selectionLayerVT.changed();
                    }
                }

                // save cookie coor
                let name = this.parent.nameIdentifyCookie;
                let version = this.parent.version.identify;
                let value = coordinate[0] + '|' + coordinate[1] + '|' + version;
                // createCookie: quan ly cookie from utility
                createCookie(name, value, 30);

                // callback Info for station & geotiff
                _this.coordinate = coordinate;
                _this.callbackInfo({
                    map: this,
                    feature: _this.feature,
                    hostAPI: _this.hostAPI,
                    langId: _this.langId,
                    coordinate: coordinate,
                    isStation: isStation,
                    isGeoTiff: isGeoTiff,
                    e: e,
                    runChart: true,
                });

                // add feature graphic of geotiff layer
                if (map.parent.graphicLayer) {
                    map.parent.graphicLayer.createGraphic({
                        geom: map.parent.graphicLayer.geometry.createPoint(coordinate[0], coordinate[1])
                    });
                }
            }
        });
    };

    this.getNearestFeature = function (rootCoordinate, feature1, feature2) {
        if (feature1 == null) return feature2 ? feature2 : null;
        else if (feature2 == null) return feature1 ? feature1 : null;
        var nearestFeature = null;
        coor1 = feature1.getGeometry().getFlatCoordinates();
        var distance1 = Math.sqrt(Math.pow(coor1[0] - rootCoordinate[0], 2) + Math.pow(coor1[1] - rootCoordinate[1], 2));
        coor2 = feature2.getGeometry().getFlatCoordinates();
        var distance2 = Math.sqrt(Math.pow(coor2[0] - rootCoordinate[0], 2) + Math.pow(coor2[1] - rootCoordinate[1], 2));
        if (distance1 < distance2) return feature1;
        else return feature2;
    };

    this.getInfoAdministrative = function (latitude, longitude, isProvince, callbackInfo, callbackForcast) {
        let urlRequest = this.hostAPI + this.endpoinIdentify + "?latitude=" + latitude + "&longitude=" + longitude + "&lang_id=" + this.langId;
        $.ajax({
            type: "GET",
            url: urlRequest,
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            crossDomain: true,
            //async: false,
            beforeSend: function () {
            },
            success: function (response) {
                console.log(response);
                let provinceName = response.province_name === null ? '' : response.province_name;
                let districtName = response.district_name === null ? '' : response.district_name;
                let communeName = response.commune_name === null ? '' : response.commune_name;
                let administrativeInfo = communeName != '' ? communeName : '';
                administrativeInfo = districtName != '' ? (administrativeInfo != '' ? administrativeInfo + ', ' : '') + districtName : administrativeInfo;
                administrativeInfo = provinceName != '' ? (administrativeInfo != '' ? administrativeInfo + ', ' : '') + provinceName : administrativeInfo;
                let administrativeInfoHTML = '<span>' + administrativeInfo + '</span><br/>';
                administrativeInfoHTML = administrativeInfoHTML + '<span>' + CONFIG_SYSTEMT.LATITUDE + ': ' + parseFloat(latitude).toFixed(3) + ', ' + CONFIG_SYSTEMT.LONGITUDE + ': ' + parseFloat(longitude).toFixed(3) + '</span>';

                let administrativeForcast = `${provinceName}`;
                if (isProvince === true) administrativeForcast = districtName != '' ? `${districtName}, ${administrativeForcast}` : administrativeForcast;
                let administrativeForcastHTML = `<div><div class="title-medium map-forecast-feature-title">${administrativeForcast}</div><div class="title-small">(${CONFIG_SYSTEMT.LATITUDE.toLowerCase()}: ${parseFloat(latitude).toFixed(3)}, ${CONFIG_SYSTEMT.LONGITUDE.toLowerCase()}: ${parseFloat(longitude).toFixed(3)})</div></div>`;

                if (callbackInfo) callbackInfo(administrativeInfoHTML, administrativeInfo);
                if (callbackForcast) {
                    // title
                    let forecastTitle = $('.forecast-info-title').text();
                    forecastTitle = forecastTitle.replace('{y}', administrativeForcast);
                    $('.forecast-info-title').html(forecastTitle);
                    // callback
                    callbackForcast(administrativeForcastHTML, administrativeForcast);
                }
            },
            failure: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

    this.create();
};