﻿apomGis.Interactions.ZoomClass = function (map) {
    if (!(map instanceof ol.Map)) {
        throw new Error('Please provide a valid OpenLayers 3 map object.');
    }
    this.map = map;
    this.draw = null;
    this.create = function () {
        this.draw = new apomGis.Interactions.DrawClass(this.map);
        this.draw.create(apomGis.Config.geometryType.box);
    };

    this.zoomAuto = function (delta, duration) {
        var map = this.map;
        var view = map.getView();
        if (!view) {
            // the map does not have a view, so we can't act
            // upon it
            return;
        }
        var currentResolution = view.getResolution();
        if (currentResolution) {
            view.animate({ zoom: +view.getZoom() + delta, duration: duration, center: view.getCenter() })
        }
    };

    this.fixZoomIn = function () {
        var view = this.map.getView();
        var zoom = view.getZoom() + 1;
        var duration = 1000;
        //view.setZoom(zoom + 1);
        view.animate({
            zoom: zoom,
            //center: [centerX, centerY],
            duration: duration
        });
    };

    this.fixZoomOut = function () {
        var view = this.map.getView();
        var zoom = view.getZoom() - 1;
        var duration = 1000;
        //view.setZoom(zoom - 1);
        view.animate({
            zoom: zoom,
            //center: [centerX, centerY],
            duration: duration
        });
    };

    /*****
    zoomto
    *****/
    this.zoomToPoint = function (x, y) {
        var extentMap = map.getView().calculateExtent(map.getSize());
        console.log('extent: ', extentMap);
        // bound Map
        var xMinMap = extentMap[0];
        var yMinMap = extentMap[1];
        var xMaxMap = extentMap[2];
        var yMaxMap = extentMap[3];
        var widthMap = (xMaxMap - xMinMap);
        var heightMap = (yMaxMap - yMinMap);

        if (x < xMinMap || x > xMaxMap) {
            var xDelta = widthMap / 3;
            var yDelta = heightMap / 3;
            var xMin = x - xDelta / 2;
            var xMax = x + xDelta / 2;
            var yMin = y - yDelta / 2;
            var yMax = y + yDelta / 2;
            var newExtent = [xMin, yMin, xMax, yMax];
            this.zoomToExtent(newExtent);
        }
        else {
            var xDelta = ((x - xMinMap) < (xMaxMap - x)) ? (x - xMinMap) : (xMaxMap - x);
            var yDelta = ((y - yMinMap) < (yMaxMap - y)) ? (y - yMinMap) : (yMaxMap - y);

            var xMin = x - xDelta / 2;
            var xMax = x + xDelta / 2;
            var yMin = y - yDelta / 2;
            var yMax = y + yDelta / 2;
            var newExtent = [xMin, yMin, xMax, yMax];
            this.zoomToExtent(newExtent);
        }
    };

    this.panToCoordinate = function (pos) {
        this.map.getView().animate({
            center: pos,
            duration: 1000
        });
    };

    this.zoomToPolyline = function (geometry) {
        if (geometry.coordinates) {
            var lineString = new ol.geom.LineString(geometry.coordinates);
            this.zoomToExtent(lineString.getExtent());
        }
        else this.zoomToExtent(geometry.getExtent());
    }

    this.zoomToPolygon = function (geometry) {
        if (geometry.coordinates) {
            var polygon = new ol.geom.Polygon(geometry.coordinates);
            this.zoomToExtent(polygon.getExtent());
        }
        else this.zoomToExtent(geometry.getExtent());
    }

    this.zoomToMultyPolygon = function (geometry) {
        if (geometry.coordinates) {
            var polygon = new ol.geom.MultiPolygon(geometry.coordinates);
            this.zoomToExtent(polygon.getExtent());
        }
        else this.zoomToExtent(geometry.getExtent());
    }

    this.zoomToExtent = function (extent, deltaX, deltaY) {
        if (deltaX) {
            extent[0] -= deltaX; // xmin
            extent[2] += deltaX; // xmax
        }
        if (deltaY) {
            extent[1] -= deltaY; // ymin
            extent[3] += deltaY; // ymax
        }
        this.map.getView().fit(extent, this.map.getSize());
    }
};