﻿apomGis.Layers.VTClass = function () {
    this.create = function (options) {
        this.layer = options.layer;
    }
    this.clearTileCache = function () {
        var source = this.layer.getSource();
        source.clear();
        source.refresh({ force: true });
    }
};