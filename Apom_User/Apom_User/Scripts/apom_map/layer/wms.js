﻿apomGis.Layers.WMSClass = function () {
    this.map = null;
    this.layer = null;
    this.style = '';
    this.title = '';
    this.type = '';
    this.features = [];

    this.create = function (options) {
        this.layer = options.layer;
        this.style = options.style;
        this.type = options.type;
        this.title = options.title;
    };

    this.updateParams = function (filterParams) {
        this.layer.getSource().updateParams(filterParams);
    }
};