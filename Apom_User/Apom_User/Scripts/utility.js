﻿/******** cookies ********/
function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
    ListBookMark();
}

function get_cookies_array() {

    var cookies = {};

    if (document.cookie && document.cookie != '') {
        var split = document.cookie.split(';');
        for (var i = 0; i < split.length; i++) {
            var name_value = split[i].split("=");
            name_value[0] = name_value[0].replace(/^ /, '');
            cookies[decodeURIComponent(name_value[0])] = decodeURIComponent(name_value[1]);
        }
    }
    return cookies;
}
/******** end cookies ********/

/******** toast **********/
// https://kamranahmed.info/toast
function showErr(msg) {
    msg = msg || LANGUAGE.TOAST.delete_fail;
    $.toast({
        text: msg,
        position: 'top-right',
        loaderBg: '#e69a2a',
        icon: 'error',
        hideAfter: 3500,
        stack: 6
    });
}

function showWarning(msg) {
    msg = msg || LANGUAGE.TOAST.warning;
    $.toast({
        text: msg,
        position: 'top-right',
        loaderBg: '#e69a2a',
        icon: 'warning',
        hideAfter: 3500,
        stack: 6
    });
}

function showSuccess(msg) {
    msg = msg || LANGUAGE.TOAST.update_success;
    $.toast({
        text: msg,
        position: 'top-right',
        loaderBg: '#e69a2a',
        icon: 'success',
        hideAfter: 5500,
        stack: 6
    });
}
/******** end toast **********/

/***** extend function jquery ******/
(function (func) {
    $.fn.addClass = function () { // replace the existing function on $.fn
        func.apply(this, arguments); // invoke the original function
        this.trigger('classChanged'); // trigger the custom event
        return this; // retain jQuery chainability
    }
})($.fn.addClass); // pass the original function as an argument

(function (func) {
    $.fn.removeClass = function () {
        func.apply(this, arguments);
        this.trigger('classChanged');
        return this;
    }
})($.fn.removeClass);

//$.fn.extend({
jQuery.fn.extend({
    /***** validate datetime ******/
    datepickerValidate: function (options) {
        var options_ = options ? options : [];
        // minDate cua control
        var minDate = options_.minDate ? new Date(options_.minDate) : null;
        if (minDate != null) $(this).attr('min', minDate);
        // maxDate cua control
        var maxDate = options_.maxDate ? new Date(options_.maxDate) : null;
        if (maxDate != null) $(this).attr('max', maxDate);

        // doi tuong lien quan de thiet lap minDate || maxDate
        var leftObjs = options_.leftObjs ? options_.leftObjs : [];
        var rightObjs = options_.rightObjs ? options_.rightObjs : [];
        var i = 0;

        function setMinMaxDate(val, leftObjs, rightObjs, isMinDate) {
            if (val == "" || val == null) {
                var i = 0;
                // tim ngay lon nhat trong mang leftObjs
                var dateMax = null;
                i = leftObjs.length;
                while (i > 0) {
                    if (leftObjs[i - 1].val() != '') {
                        dateMax = leftObjs[i - 1].val();
                        break;
                    }
                    i--;
                }
                // tim ngay nho nhat trong mang rightObjs
                var dateMin = null;
                i = 0;
                while (i < rightObjs.length) {
                    if (rightObjs[i].val() != '') {
                        dateMin = rightObjs[i].val();
                        break;
                    }
                    i++;
                }

                /// thiet lap maxDate cho control
                i = leftObjs.length;
                while (i > 0) {
                    if (leftObjs[i - 1].val() === '' || leftObjs[i - 1].val() === null) {
                        if (typeof (dateMin) != "boolean") leftObjs[i - 1].attr('max', dateMin);
                        i--;
                    }
                    else {
                        // bo minDate cua phan tu dau tien trong mang rightObjs != ''
                        if (typeof (dateMin) != "boolean") leftObjs[i - 1].attr('max', dateMin);
                        break;
                    }
                }

                /// thiet lap minDate cho control
                i = 0;
                while (i < rightObjs.length) {
                    if (rightObjs[i].val() === '' || rightObjs[i].val() === null) {
                        if (typeof (dateMax) != "boolean") rightObjs[i].attr('min', dateMax);
                        i++;
                    }
                    else {
                        // bo minDate cua phan tu dau tien trong mang rightObjs != ''
                        if (typeof (dateMax) != "boolean") rightObjs[i].attr('min', dateMax);
                        break;
                    }
                }
                return;
            };

            if (isMinDate) {
                i = 0;
                while (i < rightObjs.length) {
                    if (rightObjs[i].val() === '' || rightObjs[i].val() === null) {
                        rightObjs[i].attr('min', val);
                        i++;
                    }
                    else {
                        // bo minDate cua phan tu dau tien trong mang rightObjs != ''
                        rightObjs[i].attr('min', val);
                        break;
                    }
                }
            }
            else {
                i = leftObjs.length;
                while (i > 0) {
                    if (leftObjs[i - 1].val() === '' || leftObjs[i - 1].val() === null) {
                        leftObjs[i - 1].attr('max', val);
                        i--;
                    }
                    else {
                        // bo minDate cua phan tu dau tien trong mang rightObjs != ''
                        leftObjs[i - 1].attr('max', val);
                        break;
                    }
                }
            }
        };

        var valDate = $(this).val();
        setMinMaxDate(valDate, leftObjs, rightObjs, true);
        setMinMaxDate(valDate, leftObjs, rightObjs, false);

        $(this).change(function (e) {
            var valDate = $(this).val();
            setMinMaxDate(valDate, leftObjs, rightObjs, true);
            setMinMaxDate(valDate, leftObjs, rightObjs, false);
        });/*.on('clearDate', function () {
            var valDate = $(this).datepicker("getDate");
            setMinMaxDate(valDate, leftObjs, rightObjs, true);
            setMinMaxDate(valDate, leftObjs, rightObjs, false);
        });*/
    }
    /***** end validate datetime ******/
});
/***** end extend function jquery ******/
Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

Date.prototype.show = function (lang_id = 'vi') {
    let dateNow = new Date(this.valueOf());

    const datesVI = ["Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy"];
    const datesEN = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const months = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    // Lấy thứ trong tuần (0: Chủ Nhật, 1: Thứ Hai, ..., 6: Thứ Bảy)
    let dates = lang_id === 'vi' ? datesVI : datesEN;
    let date = dates[dateNow.getDay()];
    // Lấy ngày, tháng và năm
    let day = dateNow.getDate();
    let month = lang_id === 'vi' ? dateNow.getMonth() + 1 : months[dateNow.getMonth()]; 
    let year = dateNow.getFullYear();

    // Hiển thị kết quả
    if (lang_id === 'vi') {
        return `${date}, ngày ${day} tháng ${month} năm ${year}`;
    }
    else return `${date}, ${month} ${day}, ${year}`; 
}

Date.prototype.showDate = function (lang_id = 'vi') {
    var dateNow = new Date(this.valueOf());

    const datesVI = ["Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy"];
    const datesEN = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    // Lấy thứ trong tuần (0: Chủ Nhật, 1: Thứ Hai, ..., 6: Thứ Bảy)
    const dates = lang_id === 'vi' ? datesVI : datesEN;
    const date = dates[dateNow.getDay()];

    // Hiển thị kết quả
    return `${date}`;
}

Date.prototype.compareDate = function (date) {
    var dateNow = new Date(this.valueOf());
    if (dateNow.getDate() === date.getDate()
        && dateNow.getMonth() === date.getMonth()
        && dateNow.getFullYear() === date.getFullYear()
    ) return true;
    return false;
}

//Date.prototype.getWeek = function () {
//    var target = new Date(this.valueOf());
//    var dayNr = (this.getDay() + 6) % 7;
//    target.setDate(target.getDate() - dayNr + 3);
//    var firstThursday = target.valueOf();
//    target.setMonth(0, 1);
//    if (target.getDay() != 4) {
//        target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
//    }
//    return 1 + Math.ceil((firstThursday - target) / 604800000);
//}

//function getDateRangeOfWeek(year, weekNo) {
//    var d1 = new Date(year + '-01-01');
//    numOfdaysPastSinceLastMonday = eval(d1.getDay() - 1);
//    d1.setDate(d1.getDate() - numOfdaysPastSinceLastMonday);
//    var weekNoToday = d1.getWeek();
//    var weeksInTheFuture = eval(weekNo - weekNoToday);
//    d1.setDate(d1.getDate() + eval(7 * weeksInTheFuture));
//    var rangeIsFrom = d1.getFullYear() + '-' + eval(d1.getMonth() + 1) + "-" + d1.getDate();
//    d1.setDate(d1.getDate() + 6);
//    var rangeIsTo = d1.getFullYear() + '-' + eval(d1.getMonth() + 1) + "-" + d1.getDate();
//    return {
//        from: rangeIsFrom,
//        to: rangeIsTo
//    }
//};

function getDateRangeOfWeek(year, weekNo) {
    var d1 = new Date(year + '-01-01');
    var firstOfYear = d1.getDay();
    var deltaDayOfFirstWeek = 0;
    // truong hop ngafy 01/01 > thursday -> sang tuan moi
    if (firstOfYear > 4) deltaDayOfFirstWeek = 8 - firstOfYear;
    else deltaDayOfFirstWeek = -1 * (firstOfYear - 1);
    d1 = d1.addDays(deltaDayOfFirstWeek);
    var weekIndex = 1;
    while (weekIndex < weekNo) {
        d1 = d1.addDays(7);
        weekIndex++;
    }
    var dateFrom = d1.getFullYear() + '-' + eval(d1.getMonth() + 1) + "-" + d1.getDate();
    d1.setDate(d1.getDate() + 6);
    var dateTo = d1.getFullYear() + '-' + eval(d1.getMonth() + 1) + "-" + d1.getDate();
    return {
        from: dateFrom,
        to: dateTo
    }
};


$.loading = function (isLoading, element, content) {
    let _element = element === undefined ? '' : element;
    let _content = content === undefined ? '' : content;
    if (isLoading) {
        if (_content != '') {
            if (_element != '') $(_element).block({ message: '<h1><img src="busy.gif" />' + _content + '</h1>' });
            else $.blockUI({ message: '<h1><img src="busy.gif" />' + _content + '</h1>' });
        }
        else {
            if (_element != '') {
                $(_element).block({
                    message: '<h3><img src="FileStore/img/busy.gif" /> Please wait ...</h3>',
                    css: {
                        border: '0px solid #a00',
                        width: '75%',
                        textAlign: 'center',
                        backgroundColor: 'transparent'
                    }
                });
            }
            else $.blockUI();
        }
        $('.blockOverlay').hide();
    }
    else {
        if (_element != '') $(_element).unblock();
        else $.unblockUI();
    }
};