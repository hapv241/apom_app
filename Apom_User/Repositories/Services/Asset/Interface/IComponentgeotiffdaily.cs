﻿using Repositories.Models.Asset;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Services.Asset
{
    public interface IComponentgeotiffdaily
    {
        List<component_geotiff_service_short> getAllShort();
        List<component_geotiff_daily_identify> getListValByIdentify(double coor_x, double coor_y, string groupcomponent_id, string date_request, int prevdays, int nextdays, string lang_id);
        List<component_geotiff_daily_identify> getListValByIdentify(double coor_x, double coor_y, string groupcomponent_id, string date_request, int numdays, string lang_id);
        Task<List<component_geotiff_daily_identify>> getListValByIdentifyAsync(double coor_x, double coor_y, string groupcomponent_id, string date_request, int prevdays, int nextdays, string lang_id);
        List<component_geotiff_daily_ranking> getRankingAdministrative(string group_id, string component_id, string lang_id);
        Task<List<component_geotiff_daily_ranking>> getRankingAdministrativeAsync(string group_id, string component_id, string lang_id);
        Task<List<component_geotiff_daily_ranking>> getRankingAdministrativeAsync(string group_id, string component_id, string lang_id, string date_shooting);
        component_geotiff_daily_identify getValByIdentify(double coor_x, double coor_y, string groupcomponent_id, string date_request, string lang_id);
    }
}