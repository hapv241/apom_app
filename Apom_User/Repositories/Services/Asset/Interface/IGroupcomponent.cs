﻿using Repositories.Models.Asset;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Services.Asset
{
    public interface IGroupcomponent
    {
        List<group_component_short> getComponentFromGroup(string group_id);
        Task<List<group_component_short>> getComponentFromGroupAsync(string group_id);
    }
}