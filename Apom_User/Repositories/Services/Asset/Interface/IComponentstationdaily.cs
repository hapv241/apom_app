﻿using Repositories.Models.Asset;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Services.Asset
{
    public interface IComponentstationdaily
    {
        List<component_station_daily_aqi> getDailyAQIByComp(string group_id, string comp_id, int numdays, string lang_id);
        List<component_station_daily_aqi> getDailyAQIByComp(string group_id, string comp_id, string date_request, int prevdays, int nextdays, string lang_id);
        Task<List<component_station_daily_aqi>> getDailyAQIByCompAsync(string group_id, string comp_id, string date_request, int prevdays, int nextdays, string lang_id);
    }
}