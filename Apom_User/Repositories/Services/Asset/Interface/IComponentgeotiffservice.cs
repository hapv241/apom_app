﻿using Repositories.Models.Asset;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Services.Asset
{
    public interface IComponentgeotiffservice
    {
        List<component_geotiff_service_short> getAllShort();
        Task<List<component_geotiff_service_short>> getAllShortAsync();
        component_geotiff_service getByServiceInfo(string url, string featureBase, string featureType);
        Task<component_geotiff_service> getByServiceInfoAsync(string url, string featureBase, string featureType);
    }
}