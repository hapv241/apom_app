﻿using Repositories.Models.Asset;
using System.Threading.Tasks;

namespace Repositories.Services.Asset
{
    public interface IComponentLocal
    {
        component_station_daily_short getDailyShort(string group_id, string component_id, string datetime_shooting, string lang_id);
        Task<component_station_daily_short> getDailyShortAsync(string group_id, string component_id, string datetime_shooting, string lang_id);
        component_station_daily_short getDailyShortNewest(string group_id, string component_id, string lang_id);
        Task<component_station_daily_short> getDailyShortNewestAsync(string group_id, string component_id, string lang_id);
    }
}