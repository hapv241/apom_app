﻿using Repositories.Models.Asset;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Services.Asset
{
    public interface IGroup
    {
        List<group_tree> buildGroupStationTree(string languageId);
        List<group_short> buildVerticalMenuFromGroups(string languageId);
        Task<List<group_short>> buildVerticalMenuFromGroupsAsync(string languageId);
        group_short infoGroup(string group_id, string lang_id);
        Task<group_short> infoGroupAsync(string group_id, string lang_id);
    }
}