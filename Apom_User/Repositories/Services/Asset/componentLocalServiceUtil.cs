﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Asset;

namespace Repositories.Services.Asset
{
    public class componentLocalServiceUtil : ObjectLocalServiceUtil<component>, IComponentLocal
    {
        public componentLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static componentLocalServiceUtil _Instance;
        public static componentLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new componentLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        public component_station_daily_short getDailyShortNewest(string group_id, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_station_daily_short_newest('{group_id}', '{component_id}', '{lang_id}');";
                List<component_station_daily_short> lst = this.executeQuery<component_station_daily_short>(sql);
                component_station_daily_short obj = lst.Count > 0 ? lst[0] : new component_station_daily_short();
                obj.group = groupLocalServiceUtil.Instance.infoGroup(group_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return new component_station_daily_short();
            }
        }

        public async Task<component_station_daily_short> getDailyShortNewestAsync(string group_id, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_station_daily_short_newest('{group_id}', '{component_id}', '{lang_id}');";
                List<component_station_daily_short> lst = await this.executeQueryAsync<component_station_daily_short>(sql);
                component_station_daily_short obj = lst.Count > 0 ? lst[0] : new component_station_daily_short();
                obj.group = await groupLocalServiceUtil.Instance.infoGroupAsync(group_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return new component_station_daily_short();
            }
        }

        public component_station_daily_short getDailyShort(string group_id, string component_id, string datetime_shooting, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_station_daily_short('{group_id}', '{component_id}', '{datetime_shooting}', '{lang_id}');";
                List<component_station_daily_short> lst = this.executeQuery<component_station_daily_short>(sql);
                component_station_daily_short obj = lst.Count > 0 ? lst[0] : new component_station_daily_short();
                obj.group = groupLocalServiceUtil.Instance.infoGroup(group_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getDailyShort: " + ex.ToString());
                return new component_station_daily_short();
            }
        }

        public async Task<component_station_daily_short> getDailyShortAsync(string group_id, string component_id, string datetime_shooting, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_station_daily_short('{group_id}', '{component_id}', '{datetime_shooting}', '{lang_id}');";
                List<component_station_daily_short> lst = await this.executeQueryAsync<component_station_daily_short>(sql);
                component_station_daily_short obj = lst.Count > 0 ? lst[0] : new component_station_daily_short();
                obj.group = await groupLocalServiceUtil.Instance.infoGroupAsync(group_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getDailyShortAsync: " + ex.ToString());
                return new component_station_daily_short();
            }
        }
    }
}
