﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Asset;
using System.Data.SqlClient;

namespace Repositories.Services.Asset
{
    public class componentgeotiffdailyLocalServiceUtil : ObjectLocalServiceUtil<component_geotiff_daily>
    {
        public componentgeotiffdailyLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static componentgeotiffdailyLocalServiceUtil _Instance;
        public static componentgeotiffdailyLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new componentgeotiffdailyLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        #region cache
        #region cache name
        private string getkeycacheRankingdata(string categoryDate, string lang_id)
        {
            return Constant.Cache.RANKING_TABLE_LIST.text + ".getRankingData." + categoryDate + "." + lang_id;
        }
        private string getkeycacheRankingdata(string categoryDate, string lang_id, string province_id)
        {
            return Constant.Cache.RANKING_TABLE_LIST.text + ".getRankingData." + categoryDate + "." + lang_id + "." + province_id;
        }

        private string getkeycacheListValByIdentifyProvince(string province_id, string groupcomponent_id, string date_request, int prevdays, int nextdays)
        {
            return Constant.Cache.LIST_VAL_IDENTIFY_ADMINISTRATIVE.text + ".province." + province_id + "." + groupcomponent_id + "." + date_request + "." + prevdays + "." + nextdays;
        }

        private string getkeycacheListValByIdentifyDistrict(string district_id, string groupcomponent_id, string date_request, int prevdays, int nextdays)
        {
            return Constant.Cache.LIST_VAL_IDENTIFY_ADMINISTRATIVE.text + ".district." + district_id + "." + groupcomponent_id + "." + date_request + "." + prevdays + "." + nextdays;
        }
        #endregion

        #region cache implement
        private List<component_geotiff_daily_ranking> updateCacheRankingData(string categoryDate, string group_id, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_ranking_daily_aqi('{group_id}', '{component_id}', '{lang_id}');";
                List<component_geotiff_daily_ranking> lst = this.executeQuery<component_geotiff_daily_ranking>(sql);
                if (lst.Count > 0)
                {
                    string keycache = this.getkeycacheRankingdata(categoryDate, lang_id);
                    this.UpdateCache(keycache, int.Parse(Constant.Cache.RANKING_TABLE_LIST.val), lst);
                }
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_daily_ranking>();
            }
        }
        private async Task<List<component_geotiff_daily_ranking>> updateCacheRankingDataAsync(string categoryDate, string group_id, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_ranking_daily_aqi('{group_id}', '{component_id}', '{lang_id}');";
                List<component_geotiff_daily_ranking> lst = await this.executeQueryAsync<component_geotiff_daily_ranking>(sql);
                if (lst.Count > 0)
                {
                    string keycache = this.getkeycacheRankingdata(categoryDate, lang_id);
                    this.UpdateCache(keycache, int.Parse(Constant.Cache.RANKING_TABLE_LIST.val), lst);
                }
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_daily_ranking>();
            }
        }
        private async Task<List<component_geotiff_daily_ranking>> updateCacheRankingDataAsync(string categoryDate, string group_id, string component_id, string lang_id, string date_shooting, string date_shooting_pre)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_ranking_daily_aqi('{group_id}', '{component_id}', '{lang_id}', '{date_shooting}', '{date_shooting_pre}');";
                List<component_geotiff_daily_ranking> lst = await this.executeQueryAsync<component_geotiff_daily_ranking>(sql);
                if (lst.Count > 0)
                {
                    string keycache = this.getkeycacheRankingdata(categoryDate, lang_id);
                    this.UpdateCache(keycache, int.Parse(Constant.Cache.RANKING_TABLE_LIST.val), lst);
                }
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_daily_ranking>();
            }
        }
        private async Task<List<component_geotiff_daily_ranking>> updateCacheRankingDataAsync(string categoryDate, string group_id, string component_id, string lang_id, string province_id, string date_shooting, string date_shooting_pre)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_ranking_daily_aqi('{group_id}', '{component_id}', '{lang_id}', '{province_id}', '{date_shooting}', '{date_shooting_pre}') order by avg desc, administrative_name;";
                List<component_geotiff_daily_ranking> lst = await this.executeQueryAsync<component_geotiff_daily_ranking>(sql);
                if (lst.Count > 0)
                {
                    string keycache = this.getkeycacheRankingdata(categoryDate, lang_id, province_id);
                    this.UpdateCache(keycache, int.Parse(Constant.Cache.RANKING_TABLE_LIST.val), lst);
                }
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_daily_ranking>();
            }
        }

        private async Task<List<component_geotiff_daily_identify>> updateCacheListValByIdentifyProvince(string province_id, string groupcomponent_id, string date_request, int prevdays, int nextdays)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_daily_aqi_province_prev_next(@groupcomponent_id, @province_id, @date_request, @prevdays, @nextdays); ";
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@groupcomponent_id", groupcomponent_id));
                parameters.Add(new SqlParameter("@province_id", province_id));
                parameters.Add(new SqlParameter("@date_request", date_request));
                parameters.Add(new SqlParameter("@prevdays", prevdays));
                parameters.Add(new SqlParameter("@nextdays", nextdays));
                List<component_geotiff_daily_identify> lst = await this.executeQueryAsync<component_geotiff_daily_identify>(sql, parameters.ToArray());
                if (lst.Count > 0)
                {
                    string keycache = this.getkeycacheListValByIdentifyProvince(province_id, groupcomponent_id, date_request, prevdays, nextdays);
                    this.UpdateCache(keycache, int.Parse(Constant.Cache.LIST_VAL_IDENTIFY_ADMINISTRATIVE.val), lst);
                }
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }

        private async Task<List<component_geotiff_daily_identify>> updateCacheListValByIdentifyDistrict(string district_id, string groupcomponent_id, string date_request, int prevdays, int nextdays)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_daily_aqi_district_prev_next(@groupcomponent_id, @province_id, @date_request, @prevdays, @nextdays); ";
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@groupcomponent_id", groupcomponent_id));
                parameters.Add(new SqlParameter("@province_id", district_id));
                parameters.Add(new SqlParameter("@date_request", date_request));
                parameters.Add(new SqlParameter("@prevdays", prevdays));
                parameters.Add(new SqlParameter("@nextdays", nextdays));
                List<component_geotiff_daily_identify> lst = await this.executeQueryAsync<component_geotiff_daily_identify>(sql, parameters.ToArray());
                if (lst.Count > 0)
                {
                    string keycache = this.getkeycacheListValByIdentifyDistrict(district_id, groupcomponent_id, date_request, prevdays, nextdays);
                    this.UpdateCache(keycache, int.Parse(Constant.Cache.LIST_VAL_IDENTIFY_ADMINISTRATIVE.val), lst);
                }
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="coor_x"></param>
        /// <param name="coor_y"></param>
        /// <param name="groupcomponent_id"></param>
        /// <param name="date_request">format: yyyy-MM-dd</param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public component_geotiff_daily_identify getValByIdentify(double coor_x, double coor_y, string groupcomponent_id, string date_request, string lang_id)
        {
            try
            {
                string sql = $"select* from {this.schema}.apom_identify_geotiff({coor_x}, {coor_y}, '{groupcomponent_id}', '{date_request}', '{lang_id}')";
                List<component_geotiff_daily_identify> lst = this.executeQuery<component_geotiff_daily_identify>(sql);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="coor_x"></param>
        /// <param name="coor_y"></param>
        /// <param name="groupcomponent_id"></param>
        /// <param name="date_request">format: yyyy-MM-dd</param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public List<component_geotiff_daily_identify> getListValByIdentify(double coor_x, double coor_y, string groupcomponent_id, string date_request, int numdays, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_layer_daily_aqi('{groupcomponent_id}',{coor_x}, {coor_y}, '{date_request}', {numdays}, '{lang_id}') order by requestdate::date; ";
                List<component_geotiff_daily_identify> lst = this.executeQuery<component_geotiff_daily_identify>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }

        public List<component_geotiff_daily_identify> getListValByIdentify(double coor_x, double coor_y, string groupcomponent_id, string date_request, int prevdays, int nextdays, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_layer_daily_aqi_prev_next('{groupcomponent_id}',{coor_x}, {coor_y}, '{date_request}', '{prevdays}', {nextdays}, '{lang_id}'); ";
                List<component_geotiff_daily_identify> lst = this.executeQuery<component_geotiff_daily_identify>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }


        public async Task<List<component_geotiff_daily_identify>> getListValByIdentifyAsync(double coor_x, double coor_y, string groupcomponent_id, string date_request, int prevdays, int nextdays, string lang_id)
        {
            try
            {
                //string sql = $"select * from {this.schema}.apom_get_component_layer_daily_aqi_prev_next('{groupcomponent_id}',{coor_x}, {coor_y}, '{date_request}', '{prevdays}', {nextdays}, '{lang_id}'); ";
                string sql = $"select * from {this.schema}.apom_get_component_layer_daily_aqi_prev_next(@groupcomponent_id, @coor_x, @coor_y, @date_request, @prevdays, @nextdays, @lang_id); ";
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@groupcomponent_id", groupcomponent_id));
                parameters.Add(new SqlParameter("@coor_x", coor_x));
                parameters.Add(new SqlParameter("@coor_y", coor_y));
                parameters.Add(new SqlParameter("@date_request", date_request));
                parameters.Add(new SqlParameter("@prevdays", prevdays));
                parameters.Add(new SqlParameter("@nextdays", nextdays));
                parameters.Add(new SqlParameter("@lang_id", lang_id));
                List<component_geotiff_daily_identify> lst = await this.executeQueryAsync<component_geotiff_daily_identify>(sql, parameters.ToArray());
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }
        public async Task<List<component_geotiff_daily_identify>> getListValByIdentifyProvinceAsync(double coor_x, double coor_y, string groupcomponent_id, string date_request, int prevdays, int nextdays)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_daily_aqi_province_prev_next(@groupcomponent_id, @coor_x, @coor_y, @date_request, @prevdays, @nextdays); ";
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@groupcomponent_id", groupcomponent_id));
                parameters.Add(new SqlParameter("@coor_x", coor_x));
                parameters.Add(new SqlParameter("@coor_y", coor_y));
                parameters.Add(new SqlParameter("@date_request", date_request));
                parameters.Add(new SqlParameter("@prevdays", prevdays));
                parameters.Add(new SqlParameter("@nextdays", nextdays));
                List<component_geotiff_daily_identify> lst = await this.executeQueryAsync<component_geotiff_daily_identify>(sql, parameters.ToArray());
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }
        public async Task<List<component_geotiff_daily_identify>> getListValByIdentifyProvinceAsync(string province_id, string groupcomponent_id, string date_request, int prevdays, int nextdays)
        {
            try
            {
                List<component_geotiff_daily_identify> lst = null;

                string keycache = this.getkeycacheListValByIdentifyProvince(province_id, groupcomponent_id, date_request, prevdays, nextdays);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<component_geotiff_daily_identify>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheListValByIdentifyProvince(province_id, groupcomponent_id, date_request, prevdays, nextdays);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }
        public async Task<List<component_geotiff_daily_identify>> getListValByIdentifyDistrictAsync(double coor_x, double coor_y, string groupcomponent_id, string date_request, int prevdays, int nextdays)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_daily_aqi_district_prev_next(@groupcomponent_id, @coor_x, @coor_y, @date_request, @prevdays, @nextdays); ";
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@groupcomponent_id", groupcomponent_id));
                parameters.Add(new SqlParameter("@coor_x", coor_x));
                parameters.Add(new SqlParameter("@coor_y", coor_y));
                parameters.Add(new SqlParameter("@date_request", date_request));
                parameters.Add(new SqlParameter("@prevdays", prevdays));
                parameters.Add(new SqlParameter("@nextdays", nextdays));
                List<component_geotiff_daily_identify> lst = await this.executeQueryAsync<component_geotiff_daily_identify>(sql, parameters.ToArray());
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }

        public async Task<List<component_geotiff_daily_identify>> getListValByIdentifyDistrictAsync(string district_id, string groupcomponent_id, string date_request, int prevdays, int nextdays)
        {
            try
            {
                List<component_geotiff_daily_identify> lst = null;

                string keycache = this.getkeycacheListValByIdentifyDistrict(district_id, groupcomponent_id, date_request, prevdays, nextdays);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<component_geotiff_daily_identify>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheListValByIdentifyDistrict(district_id, groupcomponent_id, date_request, prevdays, nextdays);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }

        public List<component_geotiff_service_short> getAllShort()
        {
            try
            {
                string sql = "select concat(ag.name, ' -> ', a.name) as title, " +
                        "a.oid, a.groupcomponent_id,  a.group_id, a.component_id, a.urlservice, a.featuretype, a.featurebase, " +
                        "a.timeslider, a.typeslider, a.visible, a.type, a.orderservice from ( " +
                        "select ag.parent_id, agc.group_id, ag.name, agc.component_id, " +
                        "acgs.oid, acgs.groupcomponent_id, acgs.urlservice, acgs.featuretype, acgs.featurebase, " +
                        "acgs.timeslider, acgs.typeslider, acgs.visible, acgs.type, acgs.orderservice " +
                        "from " + this.schema + ".a_component_geotiff_service as acgs " +
                        "left join " + this.schema + ".a_group_component as agc on acgs.groupcomponent_id = agc.id " +
                        "left join " + this.schema + ".a_group as ag on agc.group_id = ag.id " +
                        ") as a " +
                        "left join " + this.schema + ".a_group as ag on a.parent_id = ag.id " +
                        "order by orderservice;";
                List<component_geotiff_service_short> lst = this.executeQuery<component_geotiff_service_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_service_short>();
            }
        }

        public List<component_geotiff_daily_ranking> getRankingAdministrative(string group_id, string component_id, string lang_id)
        {
            try
            {
                // get latest dateshooting to get cache name
                string sql = $"select category_id from {this.schema}.a_component_geotiff_daily where groupcomponent_id in (select id from a_group_component where group_id = '{group_id}' and component_id = '{component_id}') order by date_shooting desc limit 1 offset 0";
                System.Data.DataTable dtRankingCategory = this.excuteDataTableQuery(sql, "ranking_category_latest");
                string categoryLatest = dtRankingCategory.Rows.Count > 0 ? dtRankingCategory.Rows[0][0].ToString() : "";
                if (String.IsNullOrEmpty(categoryLatest)) return new List<component_geotiff_daily_ranking>();

                List<component_geotiff_daily_ranking> lst = null;

                string keycache = this.getkeycacheRankingdata(categoryLatest, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<component_geotiff_daily_ranking>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCacheRankingData(categoryLatest, group_id, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_daily_ranking>();
            }
        }

        public async Task<List<component_geotiff_daily_ranking>> getRankingAdministrativeAsync(string group_id, string component_id, string lang_id)
        {
            try
            {
                // get latest dateshooting to get cache name
                string sql = $"select category_id from {this.schema}.a_component_geotiff_daily where groupcomponent_id in (select id from a_group_component where group_id = '{group_id}' and component_id = '{component_id}') order by date_shooting desc limit 1 offset 0";
                System.Data.DataTable dtRankingCategory = this.excuteDataTableQuery(sql, "ranking_category_latest");
                string categoryLatest = dtRankingCategory.Rows.Count > 0 ? dtRankingCategory.Rows[0][0].ToString() : "";
                if (String.IsNullOrEmpty(categoryLatest)) return new List<component_geotiff_daily_ranking>();

                List<component_geotiff_daily_ranking> lst = null;

                string keycache = this.getkeycacheRankingdata(categoryLatest, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<component_geotiff_daily_ranking>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheRankingDataAsync(categoryLatest, group_id, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_daily_ranking>();
            }
        }

        public async Task<List<component_geotiff_daily_ranking>> getRankingAdministrativeAsync(string group_id, string component_id, string lang_id, string date_shooting, string date_shooting_pre)
        {
            try
            {
                // get latest dateshooting to get cache name
                string sql = $"select category_id from {this.schema}.a_component_geotiff_daily " +
                            $"where groupcomponent_id in (" +
                                $"select id from a_group_component where group_id = '{group_id}' and component_id = '{component_id}') " +
                            $"and date_shooting = '{date_shooting}' " +
                            $"order by date_shooting desc limit 1 offset 0";
                System.Data.DataTable dtRankingCategory = this.excuteDataTableQuery(sql, "ranking_category_date");
                string categoryDate = dtRankingCategory.Rows.Count > 0 ? dtRankingCategory.Rows[0][0].ToString() : "";
                if (String.IsNullOrEmpty(categoryDate)) return new List<component_geotiff_daily_ranking>();

                List<component_geotiff_daily_ranking> lst = null;

                string keycache = this.getkeycacheRankingdata(categoryDate, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<component_geotiff_daily_ranking>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheRankingDataAsync(categoryDate, group_id, component_id, lang_id, date_shooting, date_shooting_pre);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_daily_ranking>();
            }
        }

        public async Task<List<component_geotiff_daily_ranking>> getRankingAdministrativeAsync(string group_id, string component_id, string lang_id, string province_id, string date_shooting, string date_shooting_pre)
        {
            try
            {
                // get latest dateshooting to get cache name
                string sql = $"select category_id from {this.schema}.a_component_geotiff_daily " +
                            $"where groupcomponent_id in (" +
                                $"select id from a_group_component where group_id = '{group_id}' and component_id = '{component_id}') " +
                            $"and date_shooting = '{date_shooting}' " +
                            $"order by date_shooting desc limit 1 offset 0";
                System.Data.DataTable dtRankingCategory = this.excuteDataTableQuery(sql, "ranking_category_date");
                string categoryDate = dtRankingCategory.Rows.Count > 0 ? dtRankingCategory.Rows[0][0].ToString() : "";
                if (String.IsNullOrEmpty(categoryDate)) return new List<component_geotiff_daily_ranking>();

                List<component_geotiff_daily_ranking> lst = null;

                string keycache = this.getkeycacheRankingdata(categoryDate, lang_id, province_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<component_geotiff_daily_ranking>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheRankingDataAsync(categoryDate, group_id, component_id, lang_id, province_id, date_shooting, date_shooting_pre);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_daily_ranking>();
            }
        }

    }
}
