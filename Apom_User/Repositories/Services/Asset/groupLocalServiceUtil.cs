﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Asset;

namespace Repositories.Services.Asset
{
    public class groupLocalServiceUtil : ObjectLocalServiceUtil<group>, IGroup
    {
        public groupLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static groupLocalServiceUtil _Instance;
        public static groupLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new groupLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        public group_short infoGroup(string group_id, string lang_id)
        {
            group_short obj = null;
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{lang_id}";
                string fDes = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "des" : $"des_{lang_id}";
                string sql = $"select id, {fName} as name, {fDes} as des, icon, isstation, isaqi from {this._tablename} where id = '{group_id}'";
                List<group_short> lst = this.executeQuery<group_short>(sql);
                obj = lst.Count > 0 ? lst[0] : null;
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("infoGroup: " + ex.ToString());
            }
            return obj;
        }

        public async Task<group_short> infoGroupAsync(string group_id, string lang_id)
        {
            group_short obj = null;
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{lang_id}";
                string fDes = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "des" : $"des_{lang_id}";
                string sql = $"select id, {fName} as name, {fDes} as des, icon, isstation, isaqi from {this._tablename} where id = '{group_id}'";
                List<group_short> lst = await this.executeQueryAsync<group_short>(sql);
                obj = lst.Count > 0 ? lst[0] : null;
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("infoGroupAsync: " + ex.ToString());
            }
            return obj;
        }

        public List<group_short> buildVerticalMenuFromGroups(string languageId)
        {
            List<group_short> lst = new List<group_short>();
            try
            {
                string fName = languageId.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{languageId}";
                string fDes = languageId.Equals(Constant.LANGUAGE_DEFAULT) ? "des" : $"des_{languageId}";
                string sql = $"select icon, parent_id, id, {fName} as name, {fDes} as des, CASE WHEN isstation = true THEN 1 WHEN isstation_weather = true THEN 2 WHEN islayer_model THEN 3 else 0 END type_station from {this._tablename} where (isactive = true) and (isgroup = true) order by parent_id desc";
                lst = this.executeQuery<group_short>(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("buildVerticalMenuFromGroups: " + ex.ToString());
            }
            return lst;
        }

        public async Task<List<group_short>> buildVerticalMenuFromGroupsAsync(string languageId)
        {
            List<group_short> lst = new List<group_short>();
            try
            {
                string fName = languageId.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{languageId}";
                string fDes = languageId.Equals(Constant.LANGUAGE_DEFAULT) ? "des" : $"des_{languageId}";
                string sql = $"select icon, parent_id, id, {fName} as name, {fDes} as des, CASE WHEN isstation = true THEN 1 WHEN isstation_weather = true THEN 2 WHEN islayer_model THEN 3 else 0 END type_station from {this._tablename} where (isactive = true) and (isgroup = true) order by parent_id desc";
                lst = await this.executeQueryAsync<group_short>(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("buildVerticalMenuFromGroupsAsync: " + ex.ToString());
            }
            return lst;
        }


        public List<group_tree> buildGroupStationTree(string languageId)
        {
            List<group_tree> lst = new List<group_tree>();
            try
            {
                string fName = languageId.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{languageId}";
                string sql = $"select ag1.id, ag1.{fName}, ag1.parent_id, ag2.{fName} as parent_name from a_group ag1 inner join a_group ag2 on ag1.parent_id = ag2.id and ag2.isactive = true where ag1.isstation = true and ag1.parent_id is not null and ag1.isaqi = true and ag1.isactive = true order by parent_name, name";
                lst = this.executeQuery<group_tree>(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("buildGroupStationTree: " + ex.ToString());
            }
            return lst;
        }
    }
}
