﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Asset;

namespace Repositories.Services.Asset
{
    public class groupcomponentLocalServiceUtil : ObjectLocalServiceUtil<group_component>, IGroupcomponent
    {
        public groupcomponentLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static groupcomponentLocalServiceUtil _Instance;
        public static groupcomponentLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new groupcomponentLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        public List<group_component_short> getComponentFromGroup(string group_id)
        {
            try
            {
                string sql = $"select group_id, component_id, ordercomponent from {this._tablename} where group_id in (select parent_id from a_group where id = '{group_id}' and isactive = true) and isactive = true order by ordercomponent;";
                List<group_component_short> lst = this.executeQuery<group_component_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getComponentFromGroup: " + ex.ToString());
                return new List<group_component_short>();
            }
        }

        public async Task<List<group_component_short>> getComponentFromGroupAsync(string group_id)
        {
            try
            {
                string sql = $"select group_id, component_id, ordercomponent from {this._tablename} where group_id in (select parent_id from a_group where id = '{group_id}' and isactive = true) and isactive = true order by ordercomponent;";
                List<group_component_short> lst = await this.executeQueryAsync<group_component_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getComponentFromGroupAsync: " + ex.ToString());
                return new List<group_component_short>();
            }
        }
    }
}
