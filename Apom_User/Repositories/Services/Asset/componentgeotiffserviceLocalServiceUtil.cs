﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Asset;

namespace Repositories.Services.Asset
{
    public class componentgeotiffserviceLocalServiceUtil : ObjectLocalServiceUtil<component_geotiff_service>, IComponentgeotiffservice
    {
        public componentgeotiffserviceLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static componentgeotiffserviceLocalServiceUtil _Instance;
        public static componentgeotiffserviceLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new componentgeotiffserviceLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        public component_geotiff_service getByServiceInfo(string url, string featureBase, string featureType)
        {
            try
            {
                string sql = $"select * from {this.schema}.a_component_geotiff_service where urlservice = '{url}' and featurebase = '{featureBase}' and featuretype = '{featureType}'";
                List<component_geotiff_service> lst = this.executeQuery<component_geotiff_service>(sql);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }

        public async Task<component_geotiff_service> getByServiceInfoAsync(string url, string featureBase, string featureType)
        {
            try
            {
                string sql = $"select * from {this.schema}.a_component_geotiff_service where urlservice = '{url}' and featurebase = '{featureBase}' and featuretype = '{featureType}'";
                List<component_geotiff_service> lst = await this.executeQueryAsync<component_geotiff_service>(sql);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return null;
            }
        }

        public List<component_geotiff_service_short> getAllShort()
        {
            try
            {
                string sql = "select concat(ag.name, ' -> ', a.name) as title, " +
                        "a.oid, a.groupcomponent_id,  a.group_id, a.parent_id, a.component_id, a.urlservice, a.featuretype, a.featurebase, " +
                        "a.timeslider, a.typeslider, a.visible, a.type, a.orderservice from ( " +
                        "select ag.parent_id, agc.group_id, ag.name, agc.component_id, " +
                        "acgs.oid, acgs.groupcomponent_id, acgs.urlservice, acgs.featuretype, acgs.featurebase, " +
                        "acgs.timeslider, acgs.typeslider, acgs.visible, acgs.type, acgs.orderservice " +
                        "from " + this.schema + ".a_component_geotiff_service as acgs " +
                        "left join " + this.schema + ".a_group_component as agc on acgs.groupcomponent_id = agc.id " +
                        "left join " + this.schema + ".a_group as ag on agc.group_id = ag.id " +
                        ") as a " +
                        "left join " + this.schema + ".a_group as ag on a.parent_id = ag.id " +
                        "order by orderservice;";
                List<component_geotiff_service_short> lst = this.executeQuery<component_geotiff_service_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_service_short>();
            }
        }

        public async Task<List<component_geotiff_service_short>> getAllShortAsync()
        {
            try
            {
                string sql = "select concat(ag.name, ' -> ', a.name) as title, " +
                        "a.oid, a.groupcomponent_id,  a.group_id, a.parent_id, a.component_id, a.urlservice, a.featuretype, a.featurebase, " +
                        "a.timeslider, a.typeslider, a.visible, a.type, a.orderservice from ( " +
                        "select ag.parent_id, agc.group_id, ag.name, agc.component_id, " +
                        "acgs.oid, acgs.groupcomponent_id, acgs.urlservice, acgs.featuretype, acgs.featurebase, " +
                        "acgs.timeslider, acgs.typeslider, acgs.visible, acgs.type, acgs.orderservice " +
                        "from " + this.schema + ".a_component_geotiff_service as acgs " +
                        "left join " + this.schema + ".a_group_component as agc on acgs.groupcomponent_id = agc.id " +
                        "left join " + this.schema + ".a_group as ag on agc.group_id = ag.id " +
                        " where(ag.isactive = true or acgs.groupcomponent_id is null)" +
                        ") as a " +
                        "left join " + this.schema + ".a_group as ag on a.parent_id = ag.id " +
                        "order by orderservice;";
                List<component_geotiff_service_short> lst = await this.executeQueryAsync<component_geotiff_service_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<component_geotiff_service_short>();
            }
        }
    }
}
