﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Asset;

namespace Repositories.Services.Asset
{
    public class componentstationdailyLocalServiceUtil : ObjectLocalServiceUtil<component_station_daily>, IComponentstationdaily
    {
        public componentstationdailyLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static componentstationdailyLocalServiceUtil _Instance;
        public static componentstationdailyLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new componentstationdailyLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        public List<component_station_daily_aqi> getDailyAQIByComp(string group_id, string comp_id, int numdays, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_station_daily_aqi('{group_id}', '{comp_id}', {numdays}) order by datetime_shooting desc;";
                List<component_station_daily_aqi> lst = this.executeQuery<component_station_daily_aqi>(sql);

                // get group name
                group_short infoGroup = groupLocalServiceUtil.Instance.infoGroup(group_id, lang_id);

                lst.ForEach(x => x.group_name = infoGroup.name);
                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getDailyAQIByComp: " + ex.ToString());
                return new List<component_station_daily_aqi>();
            }
        }

        public List<component_station_daily_aqi> getDailyAQIByComp(string group_id, string comp_id, string date_request, int prevdays, int nextdays, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_station_daily_aqi_prev_next('{group_id}', '{comp_id}', '{date_request}', {prevdays}, {nextdays}) order by datetime_shooting desc;";
                List<component_station_daily_aqi> lst = this.executeQuery<component_station_daily_aqi>(sql);

                // get group name
                group_short infoGroup = groupLocalServiceUtil.Instance.infoGroup(group_id, lang_id);

                lst.ForEach(x => x.group_name = infoGroup.name);
                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getDailyAQIByComp: " + ex.ToString());
                return new List<component_station_daily_aqi>();
            }
        }

        public async Task<List<component_station_daily_aqi>> getDailyAQIByCompAsync(string group_id, string comp_id, string date_request, int prevdays, int nextdays, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_get_component_station_daily_aqi_prev_next('{group_id}', '{comp_id}', '{date_request}', {prevdays}, {nextdays}) order by datetime_shooting desc;";
                List<component_station_daily_aqi> lst = await this.executeQueryAsync<component_station_daily_aqi>(sql);

                // get group name
                group_short infoGroup = await groupLocalServiceUtil.Instance.infoGroupAsync(group_id, lang_id);

                lst.ForEach(x => x.group_name = infoGroup.name);
                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getDailyAQIByCompAsync: " + ex.ToString());
                return new List<component_station_daily_aqi>();
            }
        }
    }
}
