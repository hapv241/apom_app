﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Administrative;

namespace Repositories.Services.Administrative
{
    public class roleLocalServiceUtil : ObjectLocalServiceUtil<user>
    {
        public roleLocalServiceUtil(string connString): base(connString)
        {

        }

        private static roleLocalServiceUtil _Instance;
        public static roleLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new roleLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }


    }
}
