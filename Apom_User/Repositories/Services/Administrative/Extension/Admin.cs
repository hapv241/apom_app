﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PermissionBase.Security;

using Repositories.Models.Administrative;
using Repositories.Services.Administrative;

namespace Repositories.Extension
{
    public static class Admin
    {
        public static bool isAdmin(this userLocalServiceUtil service, user obj)
        {
            string userId = obj.userid;
            string sql = $"select * from u_user_role where userid = '{userId}' and roleid = '{RoleSystem.ADMINISTRATOR}'";
            var _obj = service.getObject(sql);            
            return _obj != null;
        }

        public static bool isMember(this userLocalServiceUtil service, user obj)
        {
            string userId = obj.userid;
            string sql = $"select * from u_user_role where userid = '{userId}' and roleid = '{RoleSystem.MEMBER}'";
            var _obj = service.getObject(sql);
            return _obj != null;
        }
    }
}
