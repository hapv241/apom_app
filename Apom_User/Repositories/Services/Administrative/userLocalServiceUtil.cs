﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Administrative;

namespace Repositories.Services.Administrative
{
    public class userLocalServiceUtil : ObjectLocalServiceUtil<user>
    {
        public userLocalServiceUtil(string connString): base(connString)
        {

        }

        private static userLocalServiceUtil _Instance;
        public static userLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new userLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        private user _supperAdminDefault = new user
        {
            userid = "",
            emailaddress = "",
            password = "",
            isactive = false
        };

        public user SupperAdminDefault {
            get
            {
                return this._supperAdminDefault;
            }

            set
            {
                this._supperAdminDefault = value;
            }
        }

        #region cache
        private string getkeycacheUserInfoAlldata()
        {
            return Constant.Cache.USER_INFO.text + ".getall.";
        }
        #endregion


        #region implement cache
        public List<user_info_model> UpdateCacheUserInfoAllUser()
        {
            string sqlQuery = $@"select uu.userid, username, firstname, middlename, lastname, isactive, 
                                emailaddress, time_register, lasttime_access
                                from {this._tablename} as uu
                                order by lasttime_access desc";
            List<user_info_model> lst = this.executeQuery<user_info_model>(sqlQuery);
            return lst;
        }
        #endregion

        #region get data

        public user getUser(string userid)
        {
            try
            {
                if (userid == this.SupperAdminDefault.userid) return this.SupperAdminDefault;
                string sqlClause = $"select * from {this._tablename} where userid = '{userid}'";
                List<user> lst = this.executeQuery(sqlClause);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return null;
            }
        }

        public async Task<user> getUserAsync(string userid)
        {
            try
            {
                if (userid == this.SupperAdminDefault.userid) return this.SupperAdminDefault;
                string sqlClause = $"select * from {this._tablename} where userid = '{userid}'";
                List<user> lst = await this.executeQueryAsync(sqlClause);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return null;
            }
        }

        public user getUserbyEmailPwd(string email, string pwd = "")
        {
            try
            {
                if (email == this.SupperAdminDefault.emailaddress)
                {
                    if (!String.IsNullOrEmpty(pwd) && this.SupperAdminDefault.password == pwd)
                    {
                        return this.SupperAdminDefault;
                    }
                    else return this.SupperAdminDefault;
                }
                string sqlClause = "";
                sqlClause = $"select * from {this._tablename} where emailaddress = '{email}'";
                if (!String.IsNullOrEmpty(pwd)) sqlClause += $" and password = '{pwd}'";
                List<user> lst = this.executeQuery(sqlClause);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return null;
            }
        }

        public async Task<user> getUserbyEmailPwdAsync(string email, string pwd = "")
        {
            try
            {
                if (email == this.SupperAdminDefault.emailaddress)
                {
                    if (!String.IsNullOrEmpty(pwd) && this.SupperAdminDefault.password == pwd)
                    {
                        return this.SupperAdminDefault;
                    }
                    else return this.SupperAdminDefault;
                }
                string sqlClause = "";
                sqlClause = $"select * from {this._tablename} where emailaddress = '{email}'";
                if (!String.IsNullOrEmpty(pwd)) sqlClause += $" and password = '{pwd}'";
                List<user> lst = await this.executeQueryAsync(sqlClause);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return null;
            }
        }

        public user getUserbyEmailPwd(string email, string pwd, bool isActive)
        {
            try
            {
                if (email == this.SupperAdminDefault.emailaddress) return this.SupperAdminDefault;
                string sqlClause = "";
                sqlClause = $"select * from {this._tablename} where emailaddress = '{email}'";
                if (!String.IsNullOrEmpty(pwd)) sqlClause += $" and password = '{pwd}'";
                List<user> lst = this.executeQuery(sqlClause);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return null;
            }
        }

        public async Task<user> getUserbyEmailPwdAsync(string email, string pwd, bool isActive)
        {
            try
            {
                if (email == this.SupperAdminDefault.emailaddress) return this.SupperAdminDefault;
                string sqlClause = "";
                sqlClause = $"select * from {this._tablename} where emailaddress = '{email}'";
                if (!String.IsNullOrEmpty(pwd)) sqlClause += $" and password = '{pwd}'";
                List<user> lst = await this.executeQueryAsync(sqlClause);
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return null;
            }
        }

        public int isExistemail(string email)
        {
            try
            {
                if (email == this.SupperAdminDefault.emailaddress) return 1;
                string sqlQuery = $"select count(*) from {this._tablename} where emailaddress = '{email}'";
                DataTable dt = this.excuteDataTableQuery(sqlQuery);
                int result = Convert.ToInt32(dt.Rows[0][0]);
                return result;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return -1;
            }
        }

        public async Task<int> isExistemailAsync(string email)
        {
            try
            {
                if (email == this.SupperAdminDefault.emailaddress) return 1;
                string sqlQuery = $"select count(*) from {this._tablename} where emailaddress = '{email}'";
                DataTable dt = await this.excuteDataTableQueryAsync(sqlQuery);
                int result = Convert.ToInt32(dt.Rows[0][0]);
                return result;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return -1;
            }
        }

        public List<user_info_model> getAllUser()
        {
            try
            {
                //List<user_info_model> lst = null;
                //string keycache = this.getkeycacheUserInfoAlldata();
                //if (Common.CacheManager<user_info_model>.Instance.hasKey(keycache)) lst = Common.CacheManager<user_info_model>.Instance.GetCache(keycache).ToList();
                //else lst = this.UpdateCacheUserInfoAllUser();
                //return lst;

                string sqlQuery = $@"select uu.userid, username, firstname, middlename, lastname, isactive, 
                                emailaddress, time_register, lasttime_access
                                from {this._tablename} as uu
                                order by lasttime_access desc";
                List<user_info_model> lst = this.executeQuery<user_info_model>(sqlQuery);
                return lst;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return new List<user_info_model>();
            }
        }

        public async Task<List<user_info_model>> getAllUserAsync()
        {
            try
            {
                string sqlQuery = $@"select uu.userid, username, firstname, middlename, lastname, isactive, 
                                emailaddress, time_register, lasttime_access
                                from {this._tablename} as uu
                                order by lasttime_access desc NULLS LAST";
                List<user_info_model> lst = await this.executeQueryAsync<user_info_model>(sqlQuery);
                return lst;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return new List<user_info_model>();
            }
        }
        #endregion

        #region non-query
        public async Task<int> activeUser(string userid)
        {
            try
            {
                string sqlClause = $"update {this._tablename} set isactive = true where userid = '{userid}'";
                int result = await this.excuteNonQueryAsync(sqlClause, null);
                return result;
            }
            catch(Exception ex)
            {
                this.Log.Error(ex.ToString());
                return -1;
            }
        }

        public int updateLastLoginTime(string userid, DateTime loginTime)
        {
            string strLoginTime = String.Concat(loginTime.Year, "-", loginTime.Month, "-", loginTime.Day, " ", loginTime.Hour, ":", loginTime.Minute, ":", loginTime.Second);
            try
            {
                string sqlClause = $"update {this._tablename} set lasttime_access = '{strLoginTime}' where userid = '{userid}'";
                int result = this.excuteNonQuery(sqlClause, null);
                return result;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return -1;
            }
        }

        public int updateResetPasswordTime(string emailAddr, long resetPwdTime)
        {
            try
            {
                string sqlClause = $"update {this._tablename} set password_reset = true, time_reset_pwd = {resetPwdTime} where emailaddress = '{emailAddr}'";
                int result = this.excuteNonQuery(sqlClause, null);
                return result;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return -1;
            }
        }

        public int updatePassword(string emailAddr, string newPass)
        {
            try
            {
                string sqlClause = $"update {this._tablename} set password_reset = false, password = '{newPass}' where emailaddress = '{emailAddr}'";
                int result = this.excuteNonQuery(sqlClause, null);
                return result;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return -1;
            }
        }

        public int delUser(string userid)
        {
            try
            {
                string sqlClause = $"delete from {this._tablename} where userid = '{userid}'";
                int result = this.excuteNonQuery(sqlClause, null);
                return result;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return -1;
            }
        }

        public async Task changeInfo(DbContext db, user obj)
        {
            await this.updateAsync(db, obj);
        }

        public void changePass(DbContext db, string userId, string newPass, string key)
        {
            //var user = this._objSlaveInfo.FirstOrDefault(p => p.vidagis_username == username);

            string sql = $"select * from {this._tablename} where userid = @userid and isactive = true";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@userid", userId));
            List<user> list = this.executeQuery(sql, parameters.ToArray());
            user u = list.Count > 0 ? list[0] : null;
            if (u != null)
            {
                u.password = Libs.Utils.Encrypts.Encrypt(String.Concat(u.emailaddress, "_", key), newPass);
                //this._dbTennant.SaveChanges();
                this.update(db, u);
            }
        }

        public async Task changePassAsync(DbContext db, string userId, string newPass, string key)
        {
            //var user = this._objSlaveInfo.FirstOrDefault(p => p.vidagis_username == username);

            string sql = $"select * from {this._tablename} where userid = @userid and isactive = true";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@userid", userId));
            List<user> list = await this.executeQueryAsync(sql, parameters.ToArray());
            user u = list.Count > 0 ? list[0] : null;
            if (u != null)
            {
                u.password = Libs.Utils.Encrypts.Encrypt(String.Concat(u.emailaddress, "_", key), newPass);
                //this._dbTennant.SaveChanges();
                await this.updateAsync(db, u);
            }
        }
        #endregion
    }

}
