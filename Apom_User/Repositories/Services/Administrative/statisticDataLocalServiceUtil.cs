﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Administrative;
using System.Data.SqlClient;

namespace Repositories.Services.Administrative
{
    public class statisticDataLocalServiceUtil : ObjectLocalServiceUtil<statistic_data>
    {
        public statisticDataLocalServiceUtil(string connString): base(connString)
        {

        }

        private static statisticDataLocalServiceUtil _Instance;
        public static statisticDataLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new statisticDataLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        public List<statistic_data> analysisData(string provinceId, string districtId, string fromDay, string toDay, string typeProduct, string languageId)
        {
            List<statistic_data> lst = new List<statistic_data>();
            try
            {
                string sql = "";
                if (String.IsNullOrEmpty(provinceId))
                {
                    // toan quoc
                    sql = $"select ROW_NUMBER() OVER (ORDER BY id) AS no, * from {this.schema}.apom_analysis_data_day_geotiff('', '', '{fromDay}', '{toDay}', '{typeProduct}', '{languageId}') order by name, datetime_shooting;";
                }
                else
                {
                    if (String.IsNullOrEmpty(districtId))
                    {
                        sql = $"select ROW_NUMBER() OVER (ORDER BY id) AS no, * from {this.schema}.apom_analysis_data_day_geotiff('{provinceId}', '', '{fromDay}', '{toDay}', '{typeProduct}', '{languageId}') order by name, datetime_shooting;";
                    }
                    else
                    {
                        sql = $"select ROW_NUMBER() OVER (ORDER BY id) AS no, * from {this.schema}.apom_analysis_data_day_geotiff('{provinceId}', '{districtId}', '{fromDay}', '{toDay}', '{typeProduct}', '{languageId}') order by name, datetime_shooting;";
                    }
                }
                
                lst = this.executeQuery<statistic_data>(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("analysisData: " + ex.ToString());
            }
            return lst;
        }

        public async Task<List<statistic_data>> analysisDataAsync(string provinceId, string districtId, string fromDay, string toDay, string typeProduct, string languageId)
        {
            List<statistic_data> lst = new List<statistic_data>();
            try
            {
                string sql = "";
                if (String.IsNullOrEmpty(provinceId))
                {
                    // toan quoc
                    sql = $"select ROW_NUMBER() OVER (ORDER BY id) AS no, * from {this.schema}.apom_analysis_data_day_geotiff('', '', '{fromDay}', '{toDay}', '{typeProduct}', '{languageId}') order by name, datetime_shooting;";
                }
                else
                {
                    if (String.IsNullOrEmpty(districtId))
                    {
                        sql = $"select ROW_NUMBER() OVER (ORDER BY id) AS no, * from {this.schema}.apom_analysis_data_day_geotiff('{provinceId}', '', '{fromDay}', '{toDay}', '{typeProduct}', '{languageId}') order by name, datetime_shooting;";
                    }
                    else
                    {
                        sql = $"select ROW_NUMBER() OVER (ORDER BY id) AS no, * from {this.schema}.apom_analysis_data_day_geotiff('{provinceId}', '{districtId}', '{fromDay}', '{toDay}', '{typeProduct}', '{languageId}') order by name, datetime_shooting;";
                    }
                }

                lst = await this.executeQueryAsync<statistic_data>(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("analysisDataAsync: " + ex.ToString());
            }
            return lst;
        }

        public async Task<administrative_info> getInfoAdministrativeByCoor(double latitude, double longitude, string languageId)
        {
            administrative_info obj = new administrative_info();
            try
            {
                //string sql = $"select * from apom_identify_info_administrative({latitude}, {longitude}, '{languageId}');";
                string sql = $"select * from {this.schema}.apom_identify_info_administrative(@latitude, @longitude, @languageId)";
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@latitude", latitude));
                parameters.Add(new SqlParameter("@longitude", longitude));
                parameters.Add(new SqlParameter("@languageId", languageId));
                //Repositories.Log.writeLog("sql: " + sql);
                obj = await this.getObjectAsync<administrative_info>(sql, parameters.ToArray());
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getInfoAdministrativeByCoor: " + ex.ToString());
                //Repositories.Log.writeLog("getInfoAdministrativeByCoor error: " + ex.ToString());
            }
            return obj;
        }
    }
}
