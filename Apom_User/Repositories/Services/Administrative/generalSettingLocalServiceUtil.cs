﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Administrative;

namespace Repositories.Services.Administrative
{
    public class generalSettingLocalServiceUtil :ObjectLocalServiceUtil<general_setting>
    {
        public generalSettingLocalServiceUtil(string connString): base(connString)
        {

        }

        private static generalSettingLocalServiceUtil _Instance;
        public static generalSettingLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new generalSettingLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }


    }
}
