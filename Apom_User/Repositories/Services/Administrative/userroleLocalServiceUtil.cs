﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Administrative;

namespace Repositories.Services.Administrative
{
    public class userroleLocalServiceUtil : ObjectLocalServiceUtil<user_role>
    {
        public userroleLocalServiceUtil(string connString): base(connString)
        {

        }

        private static userroleLocalServiceUtil _Instance;
        public static userroleLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new userroleLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        public List<user_role_view> getAllUserRoleView(string userId = null)
        {
            try
            {
                // user nao chua co role -> asign role = member
                string sqlQuery = $@"select * from 
                                (
                                    select uur.roleid, ur.name as rolename, uur.userid, uur.isroot 
                                    from {this._tablename} uur
                                    left join {this.schema}.u_role as ur on ur.roleid = uur.roleid
                                    union
                                    select a.roleid, ur.name as rolename, a.userid, false as isroot from
                                    (
                                        select userid, 'member' as roleid from {this.schema}.u_user
                                        where userid not in (
	                                        select uur.userid from public.u_user_role uur
	                                        left join {this.schema}.u_role as ur on ur.roleid = uur.roleid
                                        )
                                    ) as a
                                    left join {this.schema}.u_role as ur on ur.roleid = a.roleid
                                ) as a";
                if (!String.IsNullOrEmpty(userId))
                {
                    sqlQuery += $" where userid = '{userId}'";
                }
                List<user_role_view> lst = this.executeQuery<user_role_view>(sqlQuery);
                // default role: member
                if(this.isSupperAdminDefault(userId))
                {
                    lst.Add(new user_role_view
                    {
                        roleid = PermissionBase.Security.RoleSystem.ADMINISTRATOR,
                        userid = userId,
                        isroot = true,
                        rolename = "Administrator"
                    });
                }
                if (lst.Count == 0) lst = new List<user_role_view>
                {
                    new user_role_view
                    {
                        
                    }
                };
                return lst;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return new List<user_role_view>();
            }
        }

        public async Task<List<user_role_view>> getAllUserRoleViewAsync(string userId = null)
        {
            try
            {
                // user nao chua co role -> asign role = member
                string sqlQuery = $@"select * from 
                                (
                                    select uur.roleid, ur.name as rolename, uur.userid, uur.isroot 
                                    from {this._tablename} uur
                                    left join {this.schema}.u_role as ur on ur.roleid = uur.roleid
                                    union
                                    select a.roleid, ur.name as rolename, a.userid, false as isroot from
                                    (
                                        select userid, 'member' as roleid from {this.schema}.u_user
                                        where userid not in (
	                                        select uur.userid from public.u_user_role uur
	                                        left join {this.schema}.u_role as ur on ur.roleid = uur.roleid
                                        )
                                    ) as a
                                    left join {this.schema}.u_role as ur on ur.roleid = a.roleid
                                ) as a";
                if (!String.IsNullOrEmpty(userId))
                {
                    sqlQuery += $" where userid = '{userId}'";
                }
                List<user_role_view> lst = await this.executeQueryAsync<user_role_view>(sqlQuery);
                // default role: member
                if (this.isSupperAdminDefault(userId))
                {
                    lst.Add(new user_role_view
                    {
                        roleid = PermissionBase.Security.RoleSystem.ADMINISTRATOR,
                        userid = userId,
                        isroot = true,
                        rolename = "Administrator"
                    });
                }
                if (lst.Count == 0) lst = new List<user_role_view>
                {
                    new user_role_view
                    {

                    }
                };
                return lst;
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                return new List<user_role_view>();
            }
        }

        private user_role getByUserIdRoleId(string userId)
        {
            user_role userRole = null;
            try
            {
                string sql = $"select * from {this._tablename} where userid='{userId}';";
                userRole = this.getObject(sql);
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
            }
            return userRole;
        }

        public async Task<user_role> getByUserIdRoleIdAsync(string userId)
        {
            user_role userRole = null;
            try
            {
                string sql = $"select * from {this._tablename} where userid='{userId}';";
                userRole = await this.getObjectAsync(sql);
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
            }
            return userRole;
        }

        public bool updateRole(string userId, string roleId, bool isRoot)
        {
            bool bExcute = true;
            try
            {
                user_role userRole = this.getByUserIdRoleId(userId);
                string sql = "";
                string sIsRoot = isRoot ? "true" : "false";
                if(userRole == null)
                {
                    sql = $"insert into {this._tablename} (roleid, userid, isroot) values('{roleId}', '{userId}', {isRoot});";
                }
                else
                {
                    sql = $"update {this._tablename} set roleid = '{roleId}', isroot={sIsRoot} where userid = '{userId}';";
                }
                int result = this.excuteNonQuery(sql, null);
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                bExcute = false;
            }
            return bExcute;
        }

        public async Task<bool> updateRoleAsync(string userId, string roleId, bool isRoot)
        {
            bool bExcute = true;
            try
            {
                user_role userRole = await this.getByUserIdRoleIdAsync(userId);
                string sql = "";
                string sIsRoot = isRoot ? "true" : "false";
                if (userRole == null)
                {
                    sql = $"insert into {this._tablename} (roleid, userid, isroot) values('{roleId}', '{userId}', {isRoot});";
                }
                else
                {
                    sql = $"update {this._tablename} set roleid = '{roleId}', isroot={sIsRoot} where userid = '{userId}';";
                }
                int result = await this.excuteNonQueryAsync(sql, null);
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.ToString());
                bExcute = false;
            }
            return bExcute;
        }


        public bool isAdmin(string userId)
        {
            user _user = userLocalServiceUtil.Instance.getUser(userId);
            if (_user == null) return false;
            bool isSupperAdmin = this.isSupperAdmin(userId);
            if (isSupperAdmin) return true;
            List<user_role_view> lstUserRoleView = this.getAllUserRoleView(userId);
            string roleAdmin = PermissionBase.Security.RoleSystem.ADMINISTRATOR;
            var isRoleAdmin = lstUserRoleView.Where(x => x.roleid == roleAdmin).FirstOrDefault();
            return isRoleAdmin == null ? false : true;
        }

        public bool isSupperAdmin(string userId)
        {
            user _user = userLocalServiceUtil.Instance.getUser(userId);
            if (_user == null) return false;
            bool isSupperAdminDefault = this.isSupperAdminDefault(userId);
            if (isSupperAdminDefault) return true;
            List<user_role_view> lstUserRoleView = this.getAllUserRoleView(userId);
            string roleAdmin = PermissionBase.Security.RoleSystem.ADMINISTRATOR;
            var isRoleAdmin = lstUserRoleView.Where(x => x.roleid == roleAdmin && x.isroot == true).FirstOrDefault();
            return isRoleAdmin == null ? false : true;
        }

        public bool isSupperAdminDefault(string userId)
        {
            user _user = userLocalServiceUtil.Instance.getUser(userId);
            if (_user == null) return false;
            user supperAdminDefault = userLocalServiceUtil.Instance.SupperAdminDefault;
            if (_user.userid == supperAdminDefault.userid && _user.password == supperAdminDefault.password) return true;
            return false;
        }
    }
}
