﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

using ServiceBase;

using Repositories.Models.Notification;
using System.Xml.Linq;
using System.IO;

namespace Repositories.Services.Notification
{
    public class notificationuserLocalServiceUtil : ObjectLocalServiceUtil<notification_user>
    {
        public notificationuserLocalServiceUtil(string connString): base(connString)
        {

        }

        private static notificationuserLocalServiceUtil _Instance;
        public static notificationuserLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new notificationuserLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        #region private
        #endregion

        public int totalNotification(string user_id, bool is_vietnam)
        {
            //string sql = $"select count(*) from {this._tablename} where from_user = '{user_id}'";
            string sql = $@"SELECT count(*)
	                        FROM {this._tablename} as rnu
                            inner join public.r_notification as rn on rnu.notification_id = rn.notification_id
	                        where rnu.user_logged = '{user_id}' and (rnu.is_reminder = true)";
            if (is_vietnam) sql += " and rn.district_id is null";
            else sql += " and rn.district_id is not null";
            System.Data.DataTable dt = this.excuteDataTableQuery(sql, "");
            int count = 0;
            try
            {
                if (dt.Rows.Count > 0) count = Int32.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("totalNotification: " + ex.ToString());
                count = 0;
            }
            return count;
        }

        public async Task<int> totalNotificationAsync(string user_id, bool is_vietnam)
        {
            //string sql = $"select count(*) from {this._tablename} where from_user = '{user_id}'";
            string sql = $@"SELECT count(*)
	                        FROM {this._tablename} as rnu
                            inner join public.r_notification as rn on rnu.notification_id = rn.notification_id
	                        where rnu.user_logged = '{user_id}' and (rnu.is_reminder = true)";
            if (is_vietnam) sql += " and rn.district_id is null";
            else sql += " and rn.district_id is not null";
            System.Data.DataTable dt = await this.excuteDataTableQueryAsync(sql, "");
            int count = 0;
            try
            {
                if (dt.Rows.Count > 0) count = Int32.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("totalNotificationAsync: " + ex.ToString());
                count = 0;
            }
            return count;
        }

        public bool updateReminder(string userId, bool isReminder, bool is_vietnam)
        {
            try
            {
                string sql = $@"update {this._tablename} set is_reminder = {isReminder} where user_logged = '{userId}' 
                                    and notification_id in ( 
                                        SELECT rnu.notification_id 
                                        FROM public.r_notification_user as rnu  
                                        inner join public.r_notification as rn on rnu.notification_id = rn.notification_id 
                                        where rnu.user_logged = '{userId}' and (rnu.is_reminder = true)";
                if (is_vietnam) sql += " and rn.district_id is null)";
                else sql += " and rn.district_id is not null)";

                this.excuteNonQuery(sql, null);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                this.Log.Error("updateReminder: " + ex.ToString());
                return false;
            }
        }

        public async Task<List<notif_info>> getNotificationsIsReadAsync(string user_id, bool is_vietnam, string lang_id, int page_num, int page_size)
        {
            try
            {
                int limit = page_size;
                int offset = (page_num - 1) * page_size;

                string whereClause = "";
                if (is_vietnam) whereClause = " and rn.district_id is null";
                else whereClause = " and rn.district_id is not null";
                string sql = $@"select rn.notification_id as id, rn.title_{lang_id} as title, 
                                rn.content_{lang_id} as content, rnu.is_read, rn.notification_date as date
                                from {this.schema}.r_notification as rn
                                inner join {this._tablename} as rnu on rn.notification_id = rnu.notification_id
                                where (rnu.is_delete = false or rnu.is_delete is null)
                                and rnu.user_logged = '{user_id}'
                                {whereClause}
                                order by date desc
                                limit {limit} offset {offset}";

                List<notif_info> lst = await this.executeQueryAsync<notif_info>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Debug("getNotificationsIsReadAsync: " + ex.ToString());
                return new List<notif_info>();
            }
        }

        public async Task<List<notif_over_threshold_province_user>> getUserProvince_OverThresholdAsync(string date_request, string lang_id, int val_over_threshold)
        {
            try
            {
                string provinceName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "province_name" : $"province_name_{lang_id}";
                string sql = $@"select uu.userid as user_id, uu.username as user_name, uu.emailaddress as email_address, 
	                                cpl.province_id, 
	                                vapa.{provinceName}, vapa.avg_aqi as aqi
                                from {this.schema}.cf_pinned_location as cpl
                                inner join {this.schema}.v_administrative_province_avg as vapa
	                                on cpl.province_id = vapa.province_id
                                inner join {this.schema}.u_user as uu
	                                on cpl.user_id = uu.userid
                                where cpl.user_id is not null and
	                                (vapa.date_shooting = '{date_request}' and vapa.avg_aqi > {val_over_threshold})
                                order by cpl.user_id, vapa.province_name";

                List<notif_over_threshold_province_user> lst = await this.executeQueryAsync<notif_over_threshold_province_user>(sql);

                // fake data
                //for(int i=0;i<lst.Count;i++)
                //{
                //    if(lst[i].email_address != "tuannh@vidagis.com")
                //    {
                //        lst[i].email_address = "tuannh37@gmail.com";
                //    }
                //}

                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getUserProvince_OverThresholdAsync: " + ex.ToString());
                return new List<notif_over_threshold_province_user>();
            }
        }
    }
}
