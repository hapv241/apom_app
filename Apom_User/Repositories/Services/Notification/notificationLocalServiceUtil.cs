﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Notification;

namespace Repositories.Services.Notification
{
    public class notificationLocalServiceUtil : ObjectLocalServiceUtil<notification>
    {
        public notificationLocalServiceUtil(string connString): base(connString)
        {

        }

        private static notificationLocalServiceUtil _Instance;
        public static notificationLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new notificationLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }


    }
}
