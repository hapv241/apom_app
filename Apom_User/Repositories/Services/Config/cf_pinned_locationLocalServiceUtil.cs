﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

using Common;
using Common.Repository;
using ServiceBase;
using Repositories.Models.Config;

namespace Repositories.Services.Config
{
    public class cf_pinned_locationLocalServiceUtil : ObjectLocalServiceUtil<cf_pinned_location>
    {
        public cf_pinned_locationLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static cf_pinned_locationLocalServiceUtil _Instance;
        public static cf_pinned_locationLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new cf_pinned_locationLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        #region cache
        #region cache name
        private string getkeycachePinnedLocationDefault(string user_id, string lang_id, bool is_province)
        {
            string cacheProvince = is_province ? "province" : "district";
            if (String.IsNullOrEmpty(user_id))
                return Constant.Cache.PINNED_LOCATION_DEFAULT_LIST.text + ".getpinnedlocation.default" + "." + lang_id + "." + cacheProvince;
            else
                return Constant.Cache.PINNED_LOCATION_DEFAULT_LIST.text + ".getpinnedlocation." + user_id + "." + lang_id + "." + cacheProvince;
        }
        private string getkeycachePinnedLocation(string categoryLatest, string lang_id)
        {
            return Constant.Cache.PINNED_LOCATION_LIST.text + ".getpinnedlocation." + categoryLatest + "." + lang_id;
        }
        #endregion

        #region cache implement
        private List<pinned_location> updateCachePinnedLocationDefault(string categoryLatest, string lang_id, string user_id = null, bool is_province = true)
        {
            try
            {
                string fName = $"name_{lang_id}";
                string sql = $"select cfpl.province_id, gap.{fName} as province_name, null as aqi from {this._tablename} as cfpl";
                sql += " inner join " + this.schema + ".gis_administrative_province as gap on cfpl.province_id = gap.province_id";
                if (String.IsNullOrEmpty(user_id)) sql += " where user_id is null";
                else sql += $" where user_id = '{user_id}'";
                sql += $" order by gap.{fName}";
                List<pinned_location> lst = this.executeQuery<pinned_location>(sql);

                string keycache = this.getkeycachePinnedLocationDefault(user_id, lang_id, is_province);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.PINNED_LOCATION_DEFAULT_LIST.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<pinned_location>();
            }
        }
        private async Task<List<pinned_location>> updateCachePinnedLocationDefaultAsync(string categoryLatest, string lang_id, string user_id = null, bool is_province = true)
        {
            try
            {
                string fName = $"name_{lang_id}";
                string sql = "";
                if (is_province)
                {
                    sql = $"select cfpl.province_id, gap.{fName} as province_name, null as aqi from {this._tablename} as cfpl";
                    sql += $" inner join {this.schema}.gis_administrative_province as gap on cfpl.province_id = gap.province_id";
                }
                else
                {
                    sql = $"select cfpl.district_id as province_id, gap.{fName} as province_name, null as aqi from {this.schema}.cf_pinned_location_district as cfpl";
                    sql += $" inner join {this.schema}.gis_administrative_district as gap on cfpl.district_id = gap.district_id";
                }
                if (String.IsNullOrEmpty(user_id)) sql += " where user_id is null";
                else sql += $" where user_id = '{user_id}'";
                sql += $" order by gap.{fName}";
                List<pinned_location> lst = await this.executeQueryAsync<pinned_location>(sql);

                string keycache = this.getkeycachePinnedLocationDefault(user_id, lang_id, is_province);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.PINNED_LOCATION_DEFAULT_LIST.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<pinned_location>();
            }
        }

        private List<pinned_location> updateCachePinnedLocation(string categoryLatest, string group_id, string component_id, string lang_id, string user_id = null, bool is_province = true)
        {
            try
            {
                List<pinned_location> lstPinnedLocationDefault = this.getPinnedLocationDefault(group_id, component_id, lang_id, user_id, is_province);
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "province_name" : $"province_name_{lang_id}";

                List<pinned_location> lst = new List<pinned_location>();
                foreach (pinned_location itemPLD in lstPinnedLocationDefault)
                {
                    string sql = $"select province_id, {fName}, avg_aqi from {this.schema}.v_administrative_province_avg where category_id = '{categoryLatest}' and province_id = '{itemPLD.province_id}'";
                    pinned_location itemPL = this.getObject<pinned_location>(sql);
                    lst.Add(itemPL);
                }

                string keycache = this.getkeycachePinnedLocation(categoryLatest, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.PINNED_LOCATION_LIST.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<pinned_location>();
            }
        }
        #endregion

        #endregion

        #region public functions
        public List<pinned_location> getPinnedLocationDefault(string group_id, string component_id, string lang_id, string user_id = null, bool is_province = true)
        {
            try
            {
                // get latest dateshooting to get cache name
                string sql = $"select category_id from {this.schema}.a_component_geotiff_daily where groupcomponent_id in (select id from {this.schema}.a_group_component where group_id = '{group_id}' and component_id = '{component_id}') order by date_shooting desc limit 1 offset 0";
                System.Data.DataTable dtRankingCategory = this.excuteDataTableQuery(sql, "ranking_category_latest");
                string categoryLatest = dtRankingCategory.Rows.Count > 0 ? dtRankingCategory.Rows[0][0].ToString() : "";
                if (String.IsNullOrEmpty(categoryLatest)) return new List<pinned_location>();

                List<pinned_location> lst = null;

                string keycache = this.getkeycachePinnedLocationDefault(user_id, lang_id, is_province);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<pinned_location>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCachePinnedLocationDefault(categoryLatest, lang_id, user_id, is_province);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<pinned_location>();
            }
        }
        public async Task<List<pinned_location>> getPinnedLocationDefaultAsync(string group_id, string component_id, string lang_id, string user_id = null, bool is_province = true)
        {
            try
            {
                // get latest dateshooting to get cache name
                string sql = $"select category_id from {this.schema}.a_component_geotiff_daily where groupcomponent_id in (select id from {this.schema}.a_group_component where group_id = '{group_id}' and component_id = '{component_id}') order by date_shooting desc limit 1 offset 0";
                System.Data.DataTable dtRankingCategory = await this.excuteDataTableQueryAsync(sql, "ranking_category_latest");
                string categoryLatest = dtRankingCategory.Rows.Count > 0 ? dtRankingCategory.Rows[0][0].ToString() : "";
                if (String.IsNullOrEmpty(categoryLatest)) return new List<pinned_location>();

                List<pinned_location> lst = null;

                string keycache = this.getkeycachePinnedLocationDefault(user_id, lang_id, is_province);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<pinned_location>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCachePinnedLocationDefaultAsync(categoryLatest, lang_id, user_id, is_province);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<pinned_location>();
            }
        }

        public List<pinned_location> getPinnedLocation(string group_id, string component_id, string lang_id, string user_id = null)
        {
            try
            {
                // get latest dateshooting to get cache name
                string sql = $"select category_id from {this.schema}.a_component_geotiff_daily where groupcomponent_id in (select id from {this.schema}.a_group_component where group_id = '{group_id}' and component_id = '{component_id}') order by date_shooting desc limit 1 offset 0";
                System.Data.DataTable dtRankingCategory = this.excuteDataTableQuery(sql, "ranking_category_latest");
                string categoryLatest = dtRankingCategory.Rows.Count > 0 ? dtRankingCategory.Rows[0][0].ToString() : "";
                if (String.IsNullOrEmpty(categoryLatest)) return new List<pinned_location>();

                List<pinned_location> lst = null;

                string keycache = this.getkeycachePinnedLocation(categoryLatest, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<pinned_location>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCachePinnedLocation(categoryLatest, group_id, component_id, lang_id, user_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<pinned_location>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="province_id"></param>
        /// <param name="user_id"></param>
        /// <param name="isAdd">true: add, false: remove</param>
        /// <returns></returns>
        public bool updatePinnedLocation(string province_id, string user_id, bool isAdd, bool is_province = true)
        {
            try
            {
                if (isAdd)
                {
                    string sql = $"select count(*) from {this._tablename} where province_id = '{province_id}' and user_id = '{user_id}'";
                    DataTable dt = this.excuteDataTableQuery(sql, "");
                    DataRow drw = dt.Rows[0];
                    int count = Int32.Parse(drw[0].ToString());
                    if (count == 0)
                    {
                        sql = $"insert into {this._tablename}(province_id, user_id) values('{province_id}', '{user_id}');";
                        int result = this.excuteNonQuery(sql, null);

                        // clear cache
                        string keycache = "";
                        keycache = this.getkeycachePinnedLocationDefault(user_id, "vi", is_province);
                        this.ClearCache(keycache);
                        keycache = this.getkeycachePinnedLocationDefault(user_id, "en", is_province);
                        this.ClearCache(keycache);
                    }
                }
                else
                {
                    string sql = $"delete from {this._tablename} where province_id = '{province_id}' and user_id = '{user_id}'";
                    int result = this.excuteNonQuery(sql, null);

                    // clear cache for language: vi, en
                    string keycache = "";
                    keycache = this.getkeycachePinnedLocationDefault(user_id, "vi", is_province);
                    this.ClearCache(keycache);
                    keycache = this.getkeycachePinnedLocationDefault(user_id, "en", is_province);
                    this.ClearCache(keycache);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="province_id"></param>
        /// <param name="user_id"></param>
        /// <param name="isAdd">true: add, false: remove</param>
        /// <returns></returns>
        public async Task<bool> updatePinnedLocationAsync(string province_id, string user_id, bool isAdd, bool is_province = true)
        {
            try
            {
                string tableName = is_province ? "public.cf_pinned_location" : "public.cf_pinned_location_district";
                string fieldName = is_province ? "province_id" : "district_id";
                if (isAdd)
                {
                    string sql = $"select count(*) from {tableName} where {fieldName} = '{province_id}' and user_id = '{user_id}'";
                    DataTable dt = await this.excuteDataTableQueryAsync(sql, "");
                    DataRow drw = dt.Rows[0];
                    int count = Int32.Parse(drw[0].ToString());
                    if (count == 0)
                    {
                        sql = $"insert into {tableName}({fieldName}, user_id) values('{province_id}', '{user_id}');";
                        int result = await this.excuteNonQueryAsync(sql, null);
                    }
                }
                else
                {
                    string sql = $"delete from {tableName} where {fieldName} = '{province_id}' and user_id = '{user_id}'";
                    int result = await this.excuteNonQueryAsync(sql, null);
                }
                // clear cache for language: vi, en
                string keycache = "";
                keycache = this.getkeycachePinnedLocationDefault(user_id, "vi", is_province);
                this.ClearCache(keycache);
                keycache = this.getkeycachePinnedLocationDefault(user_id, "en", is_province);
                this.ClearCache(keycache);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return false;
            }
            return true;
        }
        #endregion

        public List<cf_pinned_location> getByProvinceId(string province_id)
        {
            string sql = $"select * from {this._tablename} where province_id = '{province_id}'";
            List<cf_pinned_location> lst = this.excuteListQuery(sql);
            return lst;
        }

        public async Task<List<cf_pinned_location>> getByProvinceIdAsync(string province_id)
        {
            string sql = $"select * from {this._tablename} where province_id = '{province_id}'";
            List<cf_pinned_location> lst = await this.excuteListQueryAsync(sql);
            return lst;
        }

        public async Task<List<cf_pinned_location_district>> getByDistrictIdAsync(string district_id)
        {
            string sql = $"select * from public.cf_pinned_location_district where district_id = '{district_id}'";
            List<cf_pinned_location_district> lst = await this.excuteListQueryAsync<cf_pinned_location_district>(sql, null);
            return lst;
        }


    }
}
