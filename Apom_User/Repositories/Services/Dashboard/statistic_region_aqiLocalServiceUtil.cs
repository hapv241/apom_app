﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;
using Common.Repository;
using ServiceBase;
using Repositories.Models.Dashboard;

namespace Repositories.Services.Dashboard
{
    public class statistic_region_aqiLocalServiceUtil : ObjectLocalServiceUtil<statistic_region_count_aqi>
    {
        public statistic_region_aqiLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static statistic_region_aqiLocalServiceUtil _Instance;
        public static statistic_region_aqiLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new statistic_region_aqiLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        #region cache
        #region cache name
        private string getkeycacheDistrictCount(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            return Constant.Cache.ANALYSIS_STATISTIC_AQI.text + ".getdistrictcount." + province_id + "." + from_date + "." + to_date + "." + component_id + "." + lang_id;
        }
        private string getkeycacheDistrictMinMaxByAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            return Constant.Cache.ANALYSIS_STATISTIC_AQI.text + ".getdistrictminmax." + province_id + "." + from_date + "." + to_date + "." + component_id + "." + lang_id;
        }
        private string getkeycacheDailyProvinceByAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            return Constant.Cache.ANALYSIS_STATISTIC_AQI.text + ".getdailyprovincebyaqi." + province_id + "." + from_date + "." + to_date + "." + component_id + "." + lang_id;
        }
        private string getkeycacheDailyDistrictByAQI(string district_id, string from_date, string to_date, string component_id, string lang_id)
        {
            return Constant.Cache.ANALYSIS_STATISTIC_AQI.text + ".getdailydistrictbyaqi." + district_id + "." + from_date + "." + to_date + "." + component_id + "." + lang_id;
        }
        private string getkeycacheDistrictAvgByProvinceAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            return Constant.Cache.ANALYSIS_STATISTIC_AQI.text + ".getdistrictavgbyprovinceaqi." + province_id + "." + from_date + "." + to_date + "." + component_id + "." + lang_id;
        }
        #endregion

        #region cache implement
        private statistic_region_count_aqi updateCacheDistrictCount(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_analysis_aqi_statistic_count_district_by_province_aqi('{province_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}');";

                statistic_region_count_aqi obj = this.getObject<statistic_region_count_aqi>(sql);

                string keycache = this.getkeycacheDistrictCount(province_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), obj);
                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new statistic_region_count_aqi();
            }
        }
        private async Task<statistic_region_count_aqi> updateCacheDistrictCountAsync(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from {this.schema}.apom_analysis_aqi_statistic_count_district_by_province_aqi('{province_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}');";

                statistic_region_count_aqi obj = await this.getObjectAsync<statistic_region_count_aqi>(sql);

                string keycache = this.getkeycacheDistrictCount(province_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), obj);
                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new statistic_region_count_aqi();
            }
        }

        private List<statistic_region_aqi> updateCacheDistrictMinMaxByAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select* from apom_analysis_aqi_statistic_district_min_max_by_province_aqi('{province_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}'); ";
                List <statistic_region_aqi> lst = this.executeQuery<statistic_region_aqi>(sql);

                string keycache = this.getkeycacheDistrictMinMaxByAQI(province_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi>();
            }
        }

        private async Task<List<statistic_region_aqi>> updateCacheDistrictMinMaxByAQIAsync(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select* from apom_analysis_aqi_statistic_district_min_max_by_province_aqi('{province_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}'); ";
                List<statistic_region_aqi> lst = await this.executeQueryAsync<statistic_region_aqi>(sql);

                string keycache = this.getkeycacheDistrictMinMaxByAQI(province_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi>();
            }
        }

        private List<statistic_region_aqi_chart> updateCacheDailyProvinceByAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from apom_analysis_aqi_statistic_province_daily('{province_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}'); ";
                List<statistic_region_aqi_chart> lst = this.executeQuery<statistic_region_aqi_chart>(sql);

                string keycache = this.getkeycacheDailyProvinceByAQI(province_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi_chart>();
            }
        }

        private async Task<List<statistic_region_aqi_chart>> updateCacheDailyProvinceByAQIAsync(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from apom_analysis_aqi_statistic_province_daily('{province_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}'); ";
                List<statistic_region_aqi_chart> lst = await this.executeQueryAsync<statistic_region_aqi_chart>(sql);

                string keycache = this.getkeycacheDailyProvinceByAQI(province_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi_chart>();
            }
        }

        private List<statistic_region_aqi_chart> updateCacheDailyDistrictByAQI(string district_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from apom_analysis_aqi_statistic_district_daily('{district_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}'); ";
                List<statistic_region_aqi_chart> lst = this.executeQuery<statistic_region_aqi_chart>(sql);

                string keycache = this.getkeycacheDailyDistrictByAQI(district_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi_chart>();
            }
        }

        private async Task<List<statistic_region_aqi_chart>> updateCacheDailyDistrictByAQIAsync(string district_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select * from apom_analysis_aqi_statistic_district_daily('{district_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}'); ";
                List<statistic_region_aqi_chart> lst = await this.executeQueryAsync<statistic_region_aqi_chart>(sql);

                string keycache = this.getkeycacheDailyDistrictByAQI(district_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi_chart>();
            }
        }

        private List<statistic_region_aqi> updateCacheDistrictAvgByProvinceAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select* from apom_analysis_aqi_statistic_district_by_province('{province_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}'); ";
                List<statistic_region_aqi> lst = this.executeQuery<statistic_region_aqi>(sql);

                string keycache = this.getkeycacheDistrictAvgByProvinceAQI(province_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi>();
            }
        }

        private async Task<List<statistic_region_aqi>> updateCacheDistrictAvgByProvinceAQIAsync(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                string sql = $"select* from apom_analysis_aqi_statistic_district_by_province('{province_id}', '{from_date}', '{to_date}', '{component_id}', '{lang_id}'); ";
                List<statistic_region_aqi> lst = await this.executeQueryAsync<statistic_region_aqi>(sql);

                string keycache = this.getkeycacheDistrictAvgByProvinceAQI(province_id, from_date, to_date, component_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.ANALYSIS_STATISTIC_AQI.val), lst);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi>();
            }
        }
        #endregion

        #endregion

        #region public functions
        public statistic_region_count_aqi getDistrictCount(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                statistic_region_count_aqi obj = null;

                string keycache = this.getkeycacheDistrictCount(province_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) obj = Common.CacheManager<statistic_region_count_aqi>.Instance.GetCache1(keycache) as statistic_region_count_aqi;
                else obj = this.updateCacheDistrictCount(province_id, from_date, to_date, component_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new statistic_region_count_aqi();
            }
        }
        public async Task<statistic_region_count_aqi> getDistrictCountAsync(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                statistic_region_count_aqi obj = null;

                string keycache = this.getkeycacheDistrictCount(province_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) obj = Common.CacheManager<statistic_region_count_aqi>.Instance.GetCache1(keycache) as statistic_region_count_aqi;
                else obj = await this.updateCacheDistrictCountAsync(province_id, from_date, to_date, component_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new statistic_region_count_aqi();
            }
        }

        public List<statistic_region_aqi> getDistrictMinMaxByAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                List<statistic_region_aqi> lst = new List<statistic_region_aqi>();

                string keycache = this.getkeycacheDistrictMinMaxByAQI(province_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<statistic_region_aqi>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCacheDistrictMinMaxByAQI(province_id, from_date, to_date, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi>();
            }
        }

        public async Task<List<statistic_region_aqi>> getDistrictMinMaxByAQIAsync(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                List<statistic_region_aqi> lst = new List<statistic_region_aqi>();

                string keycache = this.getkeycacheDistrictMinMaxByAQI(province_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<statistic_region_aqi>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheDistrictMinMaxByAQIAsync(province_id, from_date, to_date, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi>();
            }
        }

        public List<statistic_region_aqi_chart> getDailyProvinceByAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                List<statistic_region_aqi_chart> lst = new List<statistic_region_aqi_chart>();

                string keycache = this.getkeycacheDailyProvinceByAQI(province_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<statistic_region_aqi_chart>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCacheDailyProvinceByAQI(province_id, from_date, to_date, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi_chart>();
            }
        }

        public async Task<List<statistic_region_aqi_chart>> getDailyProvinceByAQIAsync(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                List<statistic_region_aqi_chart> lst = new List<statistic_region_aqi_chart>();

                string keycache = this.getkeycacheDailyProvinceByAQI(province_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<statistic_region_aqi_chart>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheDailyProvinceByAQIAsync(province_id, from_date, to_date, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi_chart>();
            }
        }

        public List<statistic_region_aqi_chart> getDailyDistrictByAQI(string district_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                List<statistic_region_aqi_chart> lst = new List<statistic_region_aqi_chart>();

                string keycache = this.getkeycacheDailyDistrictByAQI(district_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<statistic_region_aqi_chart>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCacheDailyDistrictByAQI(district_id, from_date, to_date, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi_chart>();
            }
        }

        public async Task<List<statistic_region_aqi_chart>> getDailyDistrictByAQIAsync(string district_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                List<statistic_region_aqi_chart> lst = new List<statistic_region_aqi_chart>();

                string keycache = this.getkeycacheDailyDistrictByAQI(district_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<statistic_region_aqi_chart>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheDailyDistrictByAQIAsync(district_id, from_date, to_date, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi_chart>();
            }
        }

        public List<statistic_region_aqi> getDistrictAvgByProvinceAQI(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                List<statistic_region_aqi> lst = new List<statistic_region_aqi>();

                string keycache = this.getkeycacheDistrictAvgByProvinceAQI(province_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<statistic_region_aqi>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCacheDistrictAvgByProvinceAQI(province_id, from_date, to_date, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi>();
            }
        }

        public async Task<List<statistic_region_aqi>> getDistrictAvgByProvinceAQIAsync(string province_id, string from_date, string to_date, string component_id, string lang_id)
        {
            try
            {
                List<statistic_region_aqi> lst = new List<statistic_region_aqi>();

                string keycache = this.getkeycacheDistrictAvgByProvinceAQI(province_id, from_date, to_date, component_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<statistic_region_aqi>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheDistrictAvgByProvinceAQIAsync(province_id, from_date, to_date, component_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<statistic_region_aqi>();
            }
        }
        #endregion
    }
}
