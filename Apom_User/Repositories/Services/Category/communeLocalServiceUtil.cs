﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Category;

namespace Repositories.Services.Category
{
    public class communeLocalServiceUtil : ObjectLocalServiceUtil<commune>
    {
        public communeLocalServiceUtil(string connString): base(connString)
        {

        }

        private static communeLocalServiceUtil _Instance;
        public static communeLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new communeLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }
    }
}
