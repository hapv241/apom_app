﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Category;

namespace Repositories.Services.Category
{
    public class districtLocalServiceUtil : ObjectLocalServiceUtil<district>
    {
        public districtLocalServiceUtil(string connString): base(connString)
        {

        }

        private static districtLocalServiceUtil _Instance;
        public static districtLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new districtLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        #region cache
        #region cache name

        private string getkeycacheExtentByDistrictId(string district_id, string lang_id)
        {
            return Constant.Cache.DISTRICTS.text + ".getextentbydistrict." + district_id + "." + lang_id;
        }
        private string getkeycacheDistrictsByProvince(string province_id, string lang_id)
        {
            return Constant.Cache.DISTRICTS.text + ".getdistrictsbyprovince." + province_id + "." + lang_id;
        }
        #endregion

        private extent updateCacheExtentByDistrictId(string district_id, string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name_vi" : $"name_{lang_id}";
                string sql = $"select district_id as id, {fName} as name, extent_minx as minx, extent_miny as miny, extent_maxx as maxx, extent_maxy as maxy from {this.schema}.gis_administrative_district where district_id = '{district_id}'";
                extent obj = this.getObject<extent>(sql);

                string keycache = this.getkeycacheExtentByDistrictId(district_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.DISTRICTS.val), obj);

                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new extent();
            }
        }
        
        private async Task<extent> updateCacheExtentByDistrictIdAsync(string district_id, string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name_vi" : $"name_{lang_id}";
                string sql = $"select district_id as id, {fName} as name, extent_minx as minx, extent_miny as miny, extent_maxx as maxx, extent_maxy as maxy from {this.schema}.gis_administrative_district where district_id = '{district_id}'";
                extent obj = await this.getObjectAsync<extent>(sql);

                string keycache = this.getkeycacheExtentByDistrictId(district_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.DISTRICTS.val), obj);

                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new extent();
            }
        }

        private List<district_short> updateCacheDistrictsByProvince(string province_id, string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{lang_id}";
                string fType = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "type" : $"type_{lang_id}";
                string whereClause = "";
                if (!String.IsNullOrEmpty(province_id)) whereClause += $"and province_id = '{province_id}'";
                string sql = $"SELECT gid, province_id, district_id, {fName}, {fType} FROM {this._tablename} where (1=1) {whereClause} order by {fName}";
                List<district_short> lst = this.executeQuery<district_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<district_short>();
            }
        }

        private async Task<List<district_short>> updateCacheDistrictsByProvinceAsync(string province_id, string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{lang_id}";
                string fType = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "type" : $"type_{lang_id}";
                string whereClause = "";
                if (!String.IsNullOrEmpty(province_id)) whereClause += $"and province_id = '{province_id}'";
                string sql = $"SELECT gid, province_id, district_id, {fName}, {fType} FROM {this._tablename} where (1=1) {whereClause} order by {fName}";
                List<district_short> lst = await this.executeQueryAsync<district_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<district_short>();
            }
        }
        #endregion

        public List<district_short> getDistrictsByProvince(string province_id, string lang_id)
        {
            try
            {
                List<district_short> lst = new List<district_short>();

                string keycache = this.getkeycacheDistrictsByProvince(province_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<List<district_short>>.Instance.GetCache1(keycache) as List<district_short>;
                else lst = this.updateCacheDistrictsByProvince(province_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<district_short>();
            }
        }

        public async Task<List<district_short>> getDistrictsByProvinceAsync(string province_id, string lang_id)
        {
            try
            {
                List<district_short> lst = new List<district_short>();

                string keycache = this.getkeycacheDistrictsByProvince(province_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<List<district_short>>.Instance.GetCache1(keycache) as List<district_short>;
                else lst = await this.updateCacheDistrictsByProvinceAsync(province_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<district_short>();
            }
        }


        public extent getExtentByDistrictId(string district_id, string lang_id)
        {
            try
            {
                extent obj = null;

                string keycache = this.getkeycacheExtentByDistrictId(district_id, lang_id);
                if (this.hasKeyCache(keycache)) obj = Common.CacheManager<extent>.Instance.GetCache1(keycache) as extent;
                else obj = this.updateCacheExtentByDistrictId(district_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new extent();
            }
        }

        public async Task<extent> getExtentByDistrictIdAsync(string district_id, string lang_id)
        {
            try
            {
                extent obj = null;

                string keycache = this.getkeycacheExtentByDistrictId(district_id, lang_id);
                if (this.hasKeyCache(keycache)) obj = Common.CacheManager<extent>.Instance.GetCache1(keycache) as extent;
                else obj = await this.updateCacheExtentByDistrictIdAsync(district_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new extent();
            }
        }
    }
}
