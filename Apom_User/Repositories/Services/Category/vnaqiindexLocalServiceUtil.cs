﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Category;

namespace Repositories.Services.Category
{
    public class vnaqiindexLocalServiceUtil:ObjectLocalServiceUtil<vnaqi_index>
    {
        public vnaqiindexLocalServiceUtil(string connString): base(connString)
        {

        }

        private static vnaqiindexLocalServiceUtil _Instance;
        public static vnaqiindexLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new vnaqiindexLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        public List<vnaqi_index_short> getInfoShort(string languageId)
        {
            try
            {
                string fStatusName = languageId.Equals(Constant.LANGUAGE_DEFAULT) ? "aqi_statusname" : $"aqi_statusname_{languageId}";
                string fDes = languageId.Equals(Constant.LANGUAGE_DEFAULT) ? "aqi_des" : $"aqi_des_{languageId}";
                string sql = $"select oid, aqi_from as from, aqi_to as to, aqi_pm25_from as pm25_from, aqi_pm25_to as pm25_to, {fStatusName} as statusname, {fDes} as des, bg_color, font_color from {this._tablename} order by aqi_from;";
                List<vnaqi_index_short> lst = this.executeQuery<vnaqi_index_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getInfoShort: " + ex.ToString());
                return new List<vnaqi_index_short>();
            }
        }

        public async Task<List<vnaqi_index_short>> getInfoShortAsync(string languageId)
        {
            try
            {
                string langID = String.IsNullOrEmpty(languageId) ? "vi" : languageId;
                string fStatusName = langID.Equals(Constant.LANGUAGE_DEFAULT) ? "aqi_statusname" : $"aqi_statusname_{langID}";
                string fStatus = langID.Equals(Constant.LANGUAGE_DEFAULT) ? "aqi_status" : $"aqi_status_{langID}";
                string fDes = langID.Equals(Constant.LANGUAGE_DEFAULT) ? "aqi_des" : $"aqi_des_{langID}";
                string sql = $"select oid, aqi_from as from, aqi_to as to, aqi_pm25_from as pm25_from, aqi_pm25_to as pm25_to, {fStatusName} as statusname, {fStatus} as status, {fDes} as des, bg_color, font_color from {this._tablename} order by aqi_from;";
                List<vnaqi_index_short> lst = await this.executeQueryAsync<vnaqi_index_short>(sql);
                return lst;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.Log.Error("getInfoShortAsync: " + ex.ToString());
                return new List<vnaqi_index_short>();
            }
        }
    }
}
