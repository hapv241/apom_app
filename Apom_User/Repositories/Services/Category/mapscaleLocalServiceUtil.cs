﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Category;

namespace Repositories.Services.Category
{
    public class mapscaleLocalServiceUtil:ObjectLocalServiceUtil<map_scale>
    {
        public mapscaleLocalServiceUtil(string connString): base(connString)
        {

        }

        private static mapscaleLocalServiceUtil _Instance;
        public static mapscaleLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new mapscaleLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }


    }
}
