﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceBase;

using Repositories.Models.Category;

namespace Repositories.Services.Category
{
    public class provinceLocalServiceUtil : ObjectLocalServiceUtil<province>
    {
        public provinceLocalServiceUtil(string connString): base(connString)
        {

        }

        private static provinceLocalServiceUtil _Instance;
        public static provinceLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new provinceLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        #region cache
        #region cache name
        private string getkeycacheProvinceAll(string lang_id)
        {
            return Constant.Cache.PROVINCES.text + ".getprovinceall." + lang_id;
        }
        private string getkeycacheExtentByProvinceId(string province_id, string lang_id)
        {
            return Constant.Cache.PROVINCES.text + ".getextentbyprovince." + province_id + "." + lang_id;
        }
        private string getkeycacheAdministrativeByProvinceId(string province_id, string lang_id)
        {
            return Constant.Cache.PROVINCES.text + ".getadministrativebyprovince." + province_id + "." + lang_id;
        }

        
        #endregion

        #region cache implement
        private List<province> updateCacheProvinceAll(string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{lang_id}";
                string sql = $"select * from {this._tablename} order by {fName}";
                List<province> lst = this.executeQuery<province>(sql);

                string keycache = this.getkeycacheProvinceAll(lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.PROVINCES.val), lst);

                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<province>();
            }
        }
        private async Task<List<province>> updateCacheProvinceAllAsync(string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{lang_id}";
                string sql = $"select * from {this._tablename} order by {fName}";
                List<province> lst = await this.executeQueryAsync<province>(sql);

                string keycache = this.getkeycacheProvinceAll(lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.PROVINCES.val), lst);

                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<province>();
            }
        }

        private extent updateCacheExtentByProvinceId(string province_id, string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name_vi" : $"name_{lang_id}";
                string sql = $"select province_id as id, {fName} as name, extent_minx as minx, extent_miny as miny, extent_maxx as maxx, extent_maxy as maxy from {this.schema}.gis_administrative_province where province_id = '{province_id}'";
                extent obj = this.getObject<extent>(sql);

                string keycache = this.getkeycacheExtentByProvinceId(province_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.PROVINCES.val), obj);

                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new extent();
            }
        }

        private async Task<extent> updateCacheExtentByProvinceIdAsync(string province_id, string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name_vi" : $"name_{lang_id}";
                string sql = $"select province_id as id, {fName} as name, extent_minx as minx, extent_miny as miny, extent_maxx as maxx, extent_maxy as maxy from {this.schema}.gis_administrative_province where province_id = '{province_id}'";
                extent obj = await this.getObjectAsync<extent>(sql);

                string keycache = this.getkeycacheExtentByProvinceId(province_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.PROVINCES.val), obj);

                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new extent();
            }
        }

        private async Task<List<administrative_base>> updateCacheAdministrativeByProvinceIdAsync(string province_id, string lang_id)
        {
            try
            {
                string fName = lang_id.Equals(Constant.LANGUAGE_DEFAULT) ? "name" : $"name_{lang_id}";
                string sql = $"select * from(select province_id as id, {fName} as name, 'province' as type from public.v_administrative_province where province_id = '{province_id}' union select district_id as id, {fName} as name, 'district' as type from public.v_administrative_district where province_id = '{province_id}') a order by type desc, name";
                List<administrative_base> lst = await this.executeQueryAsync<administrative_base>(sql);

                string keycache = this.getkeycacheAdministrativeByProvinceId(province_id, lang_id);
                this.UpdateCache(keycache, int.Parse(Constant.Cache.PROVINCES.val), lst);

                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<administrative_base>();
            }
        }
        #endregion
        #endregion

        public List<province> getAll(string lang_id)
        {
            try
            {
                List<province> lst = new List<province>();

                string keycache = this.getkeycacheProvinceAll(lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<province>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCacheProvinceAll(lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<province>();
            }
        }

        public async Task<List<province>> getAllAsync(string lang_id)
        {
            try
            {
                List<province> lst = new List<province>();

                string keycache = this.getkeycacheProvinceAll(lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<province>.Instance.GetCache(keycache).ToList();
                else lst = await this.updateCacheProvinceAllAsync(lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<province>();
            }
        }

        public extent getExtentByProvinceId(string province_id, string lang_id)
        {
            try
            {
                extent obj = null;

                string keycache = this.getkeycacheExtentByProvinceId(province_id, lang_id);
                if (this.hasKeyCache(keycache)) obj = Common.CacheManager<extent>.Instance.GetCache1(keycache) as extent;
                else obj = this.updateCacheExtentByProvinceId(province_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new extent();
            }
        }

        public async Task<extent> getExtentByProvinceIdAsync(string province_id, string lang_id)
        {
            try
            {
                extent obj = null;

                string keycache = this.getkeycacheExtentByProvinceId(province_id, lang_id);
                if (this.hasKeyCache(keycache)) obj = Common.CacheManager<extent>.Instance.GetCache1(keycache) as extent;
                else obj = await this.updateCacheExtentByProvinceIdAsync(province_id, lang_id);
                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new extent();
            }
        }

        // get province, district from province_id
        public async Task<List<administrative_base>> getAdministrativeByProvinceIdAsync(string province_id, string lang_id)
        {
            try
            {
                List<administrative_base> lst = new List<administrative_base>();

                string keycache = this.getkeycacheAdministrativeByProvinceId(province_id, lang_id);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<List<administrative_base>>.Instance.GetCache1(keycache) as List<administrative_base>;
                else lst = await this.updateCacheAdministrativeByProvinceIdAsync(province_id, lang_id);
                return lst;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return new List<administrative_base>();
            }
        }
    }
}
