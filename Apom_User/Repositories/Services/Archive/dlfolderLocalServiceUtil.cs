﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.SqlClient;

using Common;
using Common.Repository;
using ServiceBase;
using Repositories.Models.Archive;

namespace Repositories.Services.Archive
{
    public class dlfolderLocalServiceUtil : ObjectLocalServiceUtil<dlfolder>
    {
        public dlfolderLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static dlfolderLocalServiceUtil _Instance;
        public static dlfolderLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new dlfolderLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        #region public functions
        private string rootFolder()
        {
            string rootFolder = Constant.REPOSITORY_ROOT;
            try
            {
                string root = HttpContext.Current.Server.MapPath(rootFolder);
                //string root = String.Concat(parentPath, "/", rootFolder);
                Folder.createDirectory(root);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return rootFolder;
        }

        public dlfolder createFolder(DbContext db, string folderName, string userId)
        {
            try
            {
                string folderPath = "";
                string treepath = this.rootFolder();
                dlfolder dl = null;
                Log.Info("create folder: " + folderName);
                bool bExist = this.isFolderExist("", folderName, true);

                if (bExist)
                {
                    dl = this.getFolder("", folderName, true);
                    // check exist folder
                    folderPath = HttpContext.Current.Server.MapPath(treepath + "/" + dl.folderid);
                    Folder.createDirectory(folderPath);
                    return dl;
                }

                long millis = Common.Utilities.getMilisecond1970();
                
                // create folder physical
                folderPath = HttpContext.Current.Server.MapPath(treepath + "/" + millis.ToString());
                Common.Repository.Folder.createDirectory(folderPath);

                // create folder
                dl = new dlfolder
                {
                    folderid = millis.ToString(),
                    name = folderName,
                    description = folderName,
                    parentfolderid = "",
                    treepath = treepath,
                    createdate = DateTime.Now,
                    userid = userId
                };
                this.add(db, dl);
                return dl;
            }
            catch (Exception ex)
            {
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Log.Error($"ERROR {functionName}: {ex.ToString()}");
                return null;
            }
        }

        /// <summary>
        /// folderId = "" -> query theo folderName
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public dlfolder getFolder(string folderId, string folderName, bool relatively)
        {
            dlfolder dl = null;
            try
            {
                if (!String.IsNullOrEmpty(folderId))
                {
                    string sql = $"select * from {this._tablename} where folderid = @folderid";
                    var parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter("@folderid", folderId));
                    List<dlfolder> list = this.executeQuery(sql, parameters.ToArray());
                    dl = list.Count > 0 ? list[0] : null;
                }

                if (dl == null && !String.IsNullOrEmpty(folderName))
                {
                    if (!relatively)
                    {
                        string sql = $"select * from {this._tablename} where name like '%@name%'";
                        var parameters = new List<SqlParameter>();
                        parameters.Add(new SqlParameter("@name", folderName));
                        List<dlfolder> list = this.executeQuery(sql, parameters.ToArray());
                        dl = list.Count > 0 ? list[0] : null;
                    }
                    else
                    {
                        string sql = $"select * from {this._tablename} where name = @name";
                        var parameters = new List<SqlParameter>();
                        parameters.Add(new SqlParameter("@name", folderName));
                        List<dlfolder> list = this.executeQuery(sql, parameters.ToArray());
                        dl = list.Count > 0 ? list[0] : null;
                    }
                }
            }
            catch (Exception ex)
            {
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Log.Error($"ERROR {functionName}: {ex.ToString()}");
            }
            return dl;
        }

        /// <summary>
        /// folderId = "" -> query theo folderName
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        public bool isFolderExist(string folderId, string folderName, bool relatively)
        {
            Log.Info("Check exist folder: " + folderId + "-----" + folderName);
            dlfolder dl = null;
            if (!String.IsNullOrEmpty(folderId)) dl = this.getFolder(folderId, "", relatively);
            else dl = this.getFolder("", folderName, relatively);
            return dl == null ? false : true;
        }

        public dlfolder getFolder(string treepath, string folderName)
        {
            string sql = $"select * from {this._tablename} where name = @name and treepath=@treepath";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@name", folderName));
            parameters.Add(new SqlParameter("@treepath", treepath));
            dlfolder folder = this.getObject(sql, parameters.ToArray());
            return folder;
        }

        #endregion
    }
}
