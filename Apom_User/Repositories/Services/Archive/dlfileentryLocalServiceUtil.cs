﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;

using ServiceBase;
using Repositories.Models.Archive;

namespace Repositories.Services.Archive
{
    public class dlfileentryLocalServiceUtil : ObjectLocalServiceUtil<dlfileentry>
    {
        private static Dictionary<string, string> dicExtension = new Dictionary<string, string>();

        private string resize_type_size = "size";
        private string resize_type_percent = "percent";

        private static string RESIZE_IMG_TYPE = "size";
        private static double PERCENTAGE_RESIZE_IMG = 0.8;
        private static int RESIZE_IMG_HEIGHT = 80;
        private static string REPOSITORY_ASSET = "/Asset";

        public dlfileentryLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static dlfileentryLocalServiceUtil _Instance;
        public static dlfileentryLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new dlfileentryLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        private FileStream exportfile(FileStreamResult fileStreamResult, string pathFileName)
        {
            try
            {
                FileStream fileStream = System.IO.File.Create(pathFileName);
                fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
                fileStreamResult.FileStream.CopyTo(fileStream);
                fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);
                return fileStream;
            }
            catch (Exception ex)
            {
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return null;
            }
        }

        #region get cachename
        private string getDataByEntryId(string fileEntryId)
        {
            return Constant.Cache.FILE_ENTRY.text + ".fileentry_id." + fileEntryId;
        }
        #endregion


        #region cache function
        public List<dlfileentry> updateCacheDataByEntryId(string fileEntryId)
        {
            string sql = $"select * from {this._tablename} where fileentryid = @fileentryid";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fileentryid", fileEntryId));
            List<dlfileentry> lst = this.executeQuery(sql, parameters.ToArray());

            string keycache = this.getDataByEntryId(fileEntryId);
            this.UpdateCache(keycache, int.Parse(Constant.Cache.FILE_ENTRY.val), lst);
            return lst;
        }
        #endregion        

        public dlfileentry getFileEntry(string fileEntryId)
        {
            try
            {
                dlfileentry f = null;
                List<dlfileentry> lst = null;
                string keycache = this.getDataByEntryId(fileEntryId);
                if (this.hasKeyCache(keycache)) lst = Common.CacheManager<dlfileentry>.Instance.GetCache(keycache).ToList();
                else lst = this.updateCacheDataByEntryId(fileEntryId);
                f = lst.Count > 0 ? lst[0] : null;
                return f;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return null;
            }

            //return this._objSlaveInfo.FirstOrDefault(a => a.fileentryid.Equals(fileEntryId) && a.organizationid.Equals(_organizationId));
        }

        public string getPath(string fileEntryId)
        {
            try
            {
                fileEntryInfo f = null;
                string keycache = dlfileentryInfoLocalServiceUtil.Instance.getDataByEntryId(fileEntryId);
                if (dlfileentryInfoLocalServiceUtil.Instance.hasKeyCache(keycache)) f = (fileEntryInfo)Common.CacheManager<fileEntryInfo>.Instance.GetCache1(keycache);
                else f = dlfileentryInfoLocalServiceUtil.Instance.updateCacheDataByEntryId(fileEntryId);

                return f.img_content;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return null;
            }
        }

        public byte[] getByte(string fileEntryId)
        {
            try
            {
                fileEntryInfo f = null;
                string keycache = dlfileentryInfoLocalServiceUtil.Instance.getDataByEntryId(fileEntryId);
                if (dlfileentryInfoLocalServiceUtil.Instance.hasKeyCache(keycache)) f = (fileEntryInfo)Common.CacheManager<fileEntryInfo>.Instance.GetCache1(keycache);
                else f = dlfileentryInfoLocalServiceUtil.Instance.updateCacheDataByEntryId(fileEntryId);

                return f.bytes;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return null;
            }
        }

        public System.Drawing.Bitmap getBMP(string fileEntryId)
        {
            try
            {
                fileEntryInfo f = null;
                string keycache = dlfileentryInfoLocalServiceUtil.Instance.getDataByEntryId(fileEntryId);
                if (dlfileentryInfoLocalServiceUtil.Instance.hasKeyCache(keycache)) f = (fileEntryInfo)Common.CacheManager<fileEntryInfo>.Instance.GetCache1(keycache);
                else f = dlfileentryInfoLocalServiceUtil.Instance.updateCacheDataByEntryId(fileEntryId);

                return f.bmp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return null;
            }
        }

        private dlfileentry add(DbContext db, dlfolder dl, string fileName, FileStreamResult fileStreamResult, string className, object classPK, string host, string userId, bool resizeImg, string imgType, double imgPercent, int imgHeight, bool isUpFTP, string ftpIP, string ftpUID, string ftpPWD)
        {
            try
            {
                long millis = Common.Utilities.getMilisecond1970();

                string title = fileName;
                string contentType = fileStreamResult.ContentType;
                string extension = dlfileentryLocalServiceUtil.getExtensionFromMimeType(contentType);
                string _fileName = millis.ToString() + "." + extension;
                string treePath = dl.treepath.Trim() + "/" + dl.folderid.Trim();
                var path = System.Web.HttpContext.Current.Server.MapPath(treePath) + "/" + _fileName;
                string version = "1.0";
                string domainName = host;

                long contentLength = fileStreamResult.FileStream.Length;

                if (isUpFTP && Common.FTPUtility.ISUPLOAD_FTP("", dl.folderid, ftpIP, ftpUID, ftpPWD))
                {
                    // upload on FTP
                    String ftpFilePath = String.Format("{0}/{1}/{2}", ftpIP, dl.folderid, _fileName);
                    Common.FTPUtility.UPLOAD_FILE_FTP(fileStreamResult, ftpFilePath, ftpUID, ftpPWD);
                    path = ftpFilePath;
                    treePath = @"/" + dl.folderid;
                    domainName = ftpIP;
                }
                else
                {
                    // upload on FileStores
                    path = System.Web.HttpContext.Current.Server.MapPath(treePath) + "/" + _fileName;
                    FileStream fs = this.exportfile(fileStreamResult, path);

                    #region resize anh
                    if (resizeImg)
                    {
                        int resizedW = 0;
                        int resizedH = 0;
                        System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(path);
                        //kich thuoc ban dau cua anh
                        int originalW = bitmap.Width;
                        int originalH = bitmap.Height;
                        if (imgType == resize_type_percent)
                        {
                            double percentage = imgPercent;
                            //kich thuoc cho anh moi theo ty le dua vao
                            resizedW = (int)(originalW * percentage);
                            resizedH = (int)(originalH * percentage);
                        }
                        else
                        {
                            //kich thuoc cho anh moi theo ty le dua vao - co duinh theo hieght
                            resizedH = (int)imgHeight;
                            resizedW = (int)(originalW * resizedH / originalH);
                        }

                        //tạo 1 ảnh Bitmap mới theo kích thước trên
                        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(resizedW, resizedH);
                        //tạo 1 graphic mới từ Bitmap
                        System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(bmp);
                        //vẽ lại ảnh ban đầu lên bmp theo kích thước mới
                        graphic.DrawImage(bitmap, 0, 0, resizedW, resizedH);
                        //giải phóng tài nguyên mà graphic đang giữ
                        graphic.Dispose();
                        System.IO.MemoryStream ms = new System.IO.MemoryStream();
                        bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                        _fileName = millis.ToString() + ".resize." + extension;
                        path = System.Web.HttpContext.Current.Server.MapPath(treePath) + "/" + _fileName;
                        bmp.Save(path);
                    }
                    #endregion

                    fs.Close();
                    fs.Dispose();
                }

                // save file entryID
                string fileEntryId = Guid.NewGuid().ToString();
                dlfileentry dlFile = new dlfileentry
                {
                    fileentryid = fileEntryId,
                    folderid = dl.folderid,
                    extension = extension,
                    mimetype = contentType,
                    name = _fileName,
                    repositoryid = REPOSITORY_ASSET,
                    size = contentLength,
                    title = title,
                    treepath = treePath,
                    userid = userId,
                    createdate = DateTime.Now,
                    version = version,
                    classname = className,
                    classpk = classPK.ToString(),
                    host = domainName
                };
                this.add(db, dlFile);

                string fileVersionId = Guid.NewGuid().ToString();
                dlfileversion dlFileVersion = new dlfileversion
                {
                    fileversionid = fileVersionId,
                    repositoryid = REPOSITORY_ASSET,
                    folderid = dl.folderid,
                    fileentryid = fileEntryId,
                    extension = extension,
                    mimetype = contentType,
                    size = contentLength,
                    title = title,
                    treepath = treePath + "/" + _fileName,
                    userid = userId,
                    createdate = DateTime.Now,
                    version = version,
                    host = domainName
                };
                dlfileversionLocalServiceUtil.Instance.add(db, dlFileVersion);

                return dlFile;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return null;
            }
        }

        private dlfileentry add(DbContext db, dlfolder dl, HttpPostedFileBase file, string classname, object classpk, string host, string userId, bool resizeImg, string imgType, double imgPercent, int imgHeight, bool isUpFTP, string ftpIP, string ftpUID, string ftpPWD)
        {
            try
            {
                long millis = Common.Utilities.getMilisecond1970();
                string title = System.IO.Path.GetFileName(file.FileName);
                string extension = String.IsNullOrEmpty(System.IO.Path.GetExtension(file.FileName)) ? "txt" : System.IO.Path.GetExtension(file.FileName).Substring(1);
                string fileName = millis.ToString() + "." + extension;
                string version = "1.0";
                string treePath = "", path = "";
                string folderid = "";
                string domainName = host;
                int contentLength = file.ContentLength;

                if (isUpFTP && Common.FTPUtility.ISUPLOAD_FTP("", dl.folderid, ftpIP, ftpUID, ftpPWD))
                {
                    // upload on FTP
                    String ftpFilePath = String.Format("{0}/{1}/{2}", ftpIP, dl.folderid, fileName);
                    Common.FTPUtility.UPLOAD_FILE_FTP(file, ftpFilePath, ftpUID, ftpPWD);
                    path = ftpFilePath;
                    treePath = @"/" + dl.folderid;
                    domainName = ftpIP;
                }
                else
                {
                    folderid = dl.folderid;
                    treePath = dl.treepath + "/" + dl.folderid;
                    var pathFolder = System.Web.HttpContext.Current.Server.MapPath(treePath);
                    Common.Repository.Folder.createDirectory(pathFolder);
                    path = pathFolder + "/" + fileName;
                    file.SaveAs(path);
                    // upload on FileStores
                    //path = System.Web.HttpContext.Current.Server.MapPath(treePath) + "/" + fileName;
                    //file.SaveAs(path);

                    #region resize anh
                    if (resizeImg)
                    {
                        int resizedW = 0;
                        int resizedH = 0;
                        System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(path);
                        //kich thuoc ban dau cua anh
                        int originalW = bitmap.Width;
                        int originalH = bitmap.Height;
                        if (imgType == resize_type_percent)
                        {
                            double percentage = imgPercent;
                            //kich thuoc cho anh moi theo ty le dua vao
                            resizedW = (int)(originalW * percentage);
                            resizedH = (int)(originalH * percentage);
                        }
                        else
                        {
                            //kich thuoc cho anh moi theo ty le dua vao - co duinh theo hieght
                            resizedH = (int)imgHeight;
                            resizedW = (int)(originalW * resizedH / originalH);
                        }

                        //tạo 1 ảnh Bitmap mới theo kích thước trên
                        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(resizedW, resizedH);
                        //tạo 1 graphic mới từ Bitmap
                        System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(bmp);
                        //vẽ lại ảnh ban đầu lên bmp theo kích thước mới
                        graphic.DrawImage(bitmap, 0, 0, resizedW, resizedH);
                        //giải phóng tài nguyên mà graphic đang giữ
                        graphic.Dispose();
                        System.IO.MemoryStream ms = new System.IO.MemoryStream();
                        bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                        fileName = millis.ToString() + ".resize." + extension;
                        path = System.Web.HttpContext.Current.Server.MapPath(treePath) + "/" + fileName;
                        bmp.Save(path);
                    }
                    #endregion
                }

                // save file entryID
                string fileEntryId = Guid.NewGuid().ToString();
                dlfileentry dlFile = new dlfileentry
                {
                    fileentryid = fileEntryId,
                    folderid = folderid,
                    extension = extension,
                    mimetype = file.ContentType,
                    name = fileName,
                    repositoryid = REPOSITORY_ASSET,
                    size = contentLength,
                    title = title,
                    treepath = treePath,
                    userid = userId,
                    createdate = DateTime.Now,
                    version = version,
                    classname = classname,
                    classpk = classpk.ToString(),
                    host = domainName
                };
                this.add(db, dlFile);

                string fileVersionId = Guid.NewGuid().ToString();
                dlfileversion dlFileVersion = new dlfileversion
                {
                    fileversionid = fileVersionId,
                    repositoryid = REPOSITORY_ASSET,
                    folderid = folderid,
                    fileentryid = fileEntryId,
                    extension = extension,
                    mimetype = file.ContentType,
                    size = contentLength,
                    title = title,
                    treepath = treePath + "/" + fileName,
                    userid = userId,
                    createdate = DateTime.Now,
                    version = version,
                    classname = classname,
                    classpk = classpk.ToString(),
                    host = domainName
                };

                dlfileversionLocalServiceUtil.Instance.add(db, dlFileVersion);
                return dlFile;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return null;
            }

        }

        public dlfileentry update(DbContext db, string fileEntryId, dlfolder dl, HttpPostedFileBase file, string classname, object classpk, string userId, bool resize_img=false, bool isUpFTP=false, string ftpIP="", string ftpUID="", string ftpPWD="")
        {
            try
            {
                string domainName = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority;

                if (String.IsNullOrEmpty(fileEntryId))
                    return this.add(db, dl, file, classname, classpk, domainName, userId, resize_img, RESIZE_IMG_TYPE, PERCENTAGE_RESIZE_IMG, RESIZE_IMG_HEIGHT, isUpFTP, ftpIP, ftpUID, ftpPWD);
                dlfileentry dlFile = this.getFileEntry(fileEntryId);
                if (dlFile == null) return this.add(db, dl, file, classname, classpk, domainName, userId, resize_img, RESIZE_IMG_TYPE, PERCENTAGE_RESIZE_IMG, RESIZE_IMG_HEIGHT, isUpFTP, ftpIP, ftpUID, ftpPWD);
                long millis = Common.Utilities.getMilisecond1970();
                string title = System.IO.Path.GetFileName(file.FileName);
                string extension = String.IsNullOrEmpty(System.IO.Path.GetExtension(file.FileName)) ? "txt" : System.IO.Path.GetExtension(file.FileName).Substring(1);
                string fileName = millis.ToString() + "." + extension;
                string treePath = dl.treepath + "/" + dl.folderid;
                string path = "";
                int contentLength = file.ContentLength;

                // upload on FileStores
                path = System.Web.HttpContext.Current.Server.MapPath(treePath) + "/" + fileName;
                file.SaveAs(path);

                #region resize anh
                if (resize_img)
                {
                    int resizedW = 0;
                    int resizedH = 0;
                    System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(path);
                    //kich thuoc ban dau cua anh
                    int originalW = bitmap.Width;
                    int originalH = bitmap.Height;
                    if (RESIZE_IMG_TYPE == resize_type_percent)
                    {
                        double percentage = PERCENTAGE_RESIZE_IMG;
                        //kich thuoc cho anh moi theo ty le dua vao
                        resizedW = (int)(originalW * percentage);
                        resizedH = (int)(originalH * percentage);
                    }
                    else
                    {
                        //kich thuoc cho anh moi theo ty le dua vao - co duinh theo hieght
                        resizedH = (int)RESIZE_IMG_HEIGHT;
                        resizedW = (int)(originalW * resizedH / originalH);
                    }

                    //tạo 1 ảnh Bitmap mới theo kích thước trên
                    System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(resizedW, resizedH);
                    //tạo 1 graphic mới từ Bitmap
                    System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(bmp);
                    //vẽ lại ảnh ban đầu lên bmp theo kích thước mới
                    graphic.DrawImage(bitmap, 0, 0, resizedW, resizedH);
                    //giải phóng tài nguyên mà graphic đang giữ
                    graphic.Dispose();
                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
                    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                    fileName = millis.ToString() + ".resize." + extension;
                    path = System.Web.HttpContext.Current.Server.MapPath(treePath) + "/" + fileName;
                    bmp.Save(path);
                }
                #endregion

                // save file entryID
                string version = Common.Utilities.increamentVersion(dlFile.version);
                dlFile.extension = extension;
                dlFile.mimetype = file.ContentType;
                dlFile.name = fileName;
                dlFile.size = contentLength;
                dlFile.title = title;
                dlFile.treepath = treePath;
                dlFile.userid = userId;
                dlFile.modifydate = DateTime.Now;
                dlFile.version = version;
                dlFile.host = domainName;
                dlFile.classpk = classpk == null ? "" : classpk.ToString();
                //this.update(dlFile);
                //this.UpdateFile(dlFile);
                this.update(db, dlFile);

                #region save file version
                string fileVersionId = Guid.NewGuid().ToString();
                dlfileversion dlFileVersion = new dlfileversion
                {
                    fileversionid = fileVersionId,
                    repositoryid = REPOSITORY_ASSET,
                    folderid = dl.folderid,
                    fileentryid = fileEntryId,
                    extension = extension,
                    mimetype = file.ContentType,
                    size = contentLength,
                    title = title,
                    treepath = treePath + "/" + fileName,
                    userid = userId,
                    createdate = DateTime.Now,
                    version = version,
                    classname = classname,
                    classpk = classpk.ToString(),
                    host = domainName
                };
                dlfileversionLocalServiceUtil.Instance.add(db, dlFileVersion);
                #endregion

                return dlFile;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToUpper();
                this.Log.Error("ERROR " + functionName + ": " + ex.ToString());
                return null;
            }

        }

        #region extension
        private static string getExtensionFromMimeType(string mimeType)
        {
            string extension = "";

            if (dicExtension.Count == 0)
            {
                dicExtension.Add("video/3gpp", "3gp");
                dicExtension.Add("video/3gpp2", "3g2");
                dicExtension.Add("application/vnd.mseq", "mseq");
                dicExtension.Add("application/vnd.3m.post-it-notes", "pwn");
                dicExtension.Add("application/vnd.3gpp.pic-bw-large", "plb");
                dicExtension.Add("application/vnd.3gpp.pic-bw-small", "psb");
                dicExtension.Add("application/vnd.3gpp.pic-bw-var", "pvb");
                dicExtension.Add("application/vnd.3gpp2.tcap", "tcap");
                dicExtension.Add("application/x-7z-compressed", "7z");
                dicExtension.Add("application/x-abiword", "abw");
                dicExtension.Add("application/x-ace-compressed", "ace");
                dicExtension.Add("application/vnd.americandynamics.acc", "acc");
                dicExtension.Add("application/vnd.acucobol", "acu");
                dicExtension.Add("application/vnd.acucorp", "atc");
                dicExtension.Add("audio/adpcm", "adp");
                dicExtension.Add("application/x-authorware-bin", "aab");
                dicExtension.Add("application/x-authorware-map", "aam");
                dicExtension.Add("application/x-authorware-seg", "aas");
                dicExtension.Add("application/vnd.adobe.air-application-installer-package+zip", "air");
                dicExtension.Add("application/x-shockwave-flash", "swf");
                dicExtension.Add("application/vnd.adobe.fxp", "fxp");
                dicExtension.Add("application/pdf", "pdf");
                dicExtension.Add("application/vnd.cups-ppd", "ppd");
                dicExtension.Add("application/x-director", "dir");
                dicExtension.Add("application/vnd.adobe.xdp+xml", "xdp");
                dicExtension.Add("application/vnd.adobe.xfdf", "xfdf");
                dicExtension.Add("audio/x-aac", "aac");
                dicExtension.Add("application/vnd.ahead.space", "ahead");
                dicExtension.Add("application/vnd.airzip.filesecure.azf", "azf");
                dicExtension.Add("application/vnd.airzip.filesecure.azs", "azs");
                dicExtension.Add("application/vnd.amazon.ebook", "azw");
                dicExtension.Add("application/vnd.amiga.ami", "ami");
                dicExtension.Add("application/andrew-inset", "N/A");
                dicExtension.Add("application/vnd.android.package-archive", "apk");
                dicExtension.Add("application/vnd.anser-web-certificate-issue-initiation", "cii");
                dicExtension.Add("application/vnd.anser-web-funds-transfer-initiation", "fti");
                dicExtension.Add("application/vnd.antix.game-component", "atx");
                dicExtension.Add("application/x-apple-diskimage", "dmg");
                dicExtension.Add("application/vnd.apple.installer+xml", "mpkg");
                dicExtension.Add("application/applixware", "aw");
                dicExtension.Add("application/vnd.hhe.lesson-player", "les");
                dicExtension.Add("application/vnd.aristanetworks.swi", "swi");
                dicExtension.Add("text/x-asm", "s");
                dicExtension.Add("application/atomcat+xml", "atomcat");
                dicExtension.Add("application/atomsvc+xml", "atomsvc");
                dicExtension.Add("application/atom+xml", "atom, xml");
                dicExtension.Add("application/pkix-attr-cert", "ac");
                dicExtension.Add("audio/x-aiff", "aif");
                dicExtension.Add("video/x-msvideo", "avi");
                dicExtension.Add("application/vnd.audiograph", "aep");
                dicExtension.Add("image/vnd.dxf", "dxf");
                dicExtension.Add("model/vnd.dwf", "dwf");
                dicExtension.Add("text/plain-bas", "par");
                dicExtension.Add("application/x-bcpio", "bcpio");
                dicExtension.Add("application/octet-stream", "bin");
                dicExtension.Add("image/bmp", "bmp");
                dicExtension.Add("application/x-bittorrent", "torrent");
                dicExtension.Add("application/vnd.rim.cod", "cod");
                dicExtension.Add("application/vnd.blueice.multipass", "mpm");
                dicExtension.Add("application/vnd.bmi", "bmi");
                dicExtension.Add("application/x-sh", "sh");
                dicExtension.Add("image/prs.btif", "btif");
                dicExtension.Add("application/vnd.businessobjects", "rep");
                dicExtension.Add("application/x-bzip", "bz");
                dicExtension.Add("application/x-bzip2", "bz2");
                dicExtension.Add("application/x-csh", "csh");
                dicExtension.Add("text/x-c", "c");
                dicExtension.Add("application/vnd.chemdraw+xml", "cdxml");
                dicExtension.Add("text/css", "css");
                dicExtension.Add("chemical/x-cdx", "cdx");
                dicExtension.Add("chemical/x-cml", "cml");
                dicExtension.Add("chemical/x-csml", "csml");
                dicExtension.Add("application/vnd.contact.cmsg", "cdbcmsg");
                dicExtension.Add("application/vnd.claymore", "cla");
                dicExtension.Add("application/vnd.clonk.c4group", "c4g");
                dicExtension.Add("image/vnd.dvb.subtitle", "sub");
                dicExtension.Add("application/cdmi-capability", "cdmia");
                dicExtension.Add("application/cdmi-container", "cdmic");
                dicExtension.Add("application/cdmi-domain", "cdmid");
                dicExtension.Add("application/cdmi-object", "cdmio");
                dicExtension.Add("application/cdmi-queue", "cdmiq");
                dicExtension.Add("application/vnd.cluetrust.cartomobile-config", "c11amc");
                dicExtension.Add("application/vnd.cluetrust.cartomobile-config-pkg", "c11amz");
                dicExtension.Add("image/x-cmu-raster", "ras");
                dicExtension.Add("model/vnd.collada+xml", "dae");
                dicExtension.Add("text/csv", "csv");
                dicExtension.Add("application/mac-compactpro", "cpt");
                dicExtension.Add("application/vnd.wap.wmlc", "wmlc");
                dicExtension.Add("image/cgm", "cgm");
                dicExtension.Add("x-conference/x-cooltalk", "ice");
                dicExtension.Add("image/x-cmx", "cmx");
                dicExtension.Add("application/vnd.xara", "xar");
                dicExtension.Add("application/vnd.cosmocaller", "cmc");
                dicExtension.Add("application/x-cpio", "cpio");
                dicExtension.Add("application/vnd.crick.clicker", "clkx");
                dicExtension.Add("application/vnd.crick.clicker.keyboard", "clkk");
                dicExtension.Add("application/vnd.crick.clicker.palette", "clkp");
                dicExtension.Add("application/vnd.crick.clicker.template", "clkt");
                dicExtension.Add("application/vnd.crick.clicker.wordbank", "clkw");
                dicExtension.Add("application/vnd.criticaltools.wbs+xml", "wbs");
                dicExtension.Add("application/vnd.rig.cryptonote", "cryptonote");
                dicExtension.Add("chemical/x-cif", "cif");
                dicExtension.Add("chemical/x-cmdf", "cmdf");
                dicExtension.Add("application/cu-seeme", "cu");
                dicExtension.Add("application/prs.cww", "cww");
                dicExtension.Add("text/vnd.curl", "curl");
                dicExtension.Add("text/vnd.curl.dcurl", "dcurl");
                dicExtension.Add("text/vnd.curl.mcurl", "mcurl");
                dicExtension.Add("text/vnd.curl.scurl", "scurl");
                dicExtension.Add("application/vnd.curl.car", "car");
                dicExtension.Add("application/vnd.curl.pcurl", "pcurl");
                dicExtension.Add("application/vnd.yellowriver-custom-menu", "cmp");
                dicExtension.Add("application/dssc+der", "dssc");
                dicExtension.Add("application/dssc+xml", "xdssc");
                dicExtension.Add("application/x-debian-package", "deb");
                dicExtension.Add("audio/vnd.dece.audio", "uva");
                dicExtension.Add("image/vnd.dece.graphic", "uvi");
                dicExtension.Add("video/vnd.dece.hd", "uvh");
                dicExtension.Add("video/vnd.dece.mobile", "uvm");
                dicExtension.Add("video/vnd.uvvu.mp4", "uvu");
                dicExtension.Add("video/vnd.dece.pd", "uvp");
                dicExtension.Add("video/vnd.dece.sd", "uvs");
                dicExtension.Add("video/vnd.dece.video", "uvv");
                dicExtension.Add("application/x-dvi", "dvi");
                dicExtension.Add("application/vnd.fdsn.seed", "seed");
                dicExtension.Add("application/x-dtbook+xml", "dtb");
                dicExtension.Add("application/x-dtbresource+xml", "res");
                dicExtension.Add("application/vnd.dvb.ait", "ait");
                dicExtension.Add("application/vnd.dvb.service", "svc");
                dicExtension.Add("audio/vnd.digital-winds", "eol");
                dicExtension.Add("image/vnd.djvu", "djvu");
                dicExtension.Add("application/xml-dtd", "dtd");
                dicExtension.Add("application/vnd.dolby.mlp", "mlp");
                dicExtension.Add("application/x-doom", "wad");
                dicExtension.Add("application/vnd.dpgraph", "dpg");
                dicExtension.Add("audio/vnd.dra", "dra");
                dicExtension.Add("application/vnd.dreamfactory", "dfac");
                dicExtension.Add("audio/vnd.dts", "dts");
                dicExtension.Add("audio/vnd.dts.hd", "dtshd");
                dicExtension.Add("image/vnd.dwg", "dwg");
                dicExtension.Add("application/vnd.dynageo", "geo");
                dicExtension.Add("application/ecmascript", "es");
                dicExtension.Add("application/vnd.ecowin.chart", "mag");
                dicExtension.Add("image/vnd.fujixerox.edmics-mmr", "mmr");
                dicExtension.Add("image/vnd.fujixerox.edmics-rlc", "rlc");
                dicExtension.Add("application/exi", "exi");
                dicExtension.Add("application/vnd.proteus.magazine", "mgz");
                dicExtension.Add("application/epub+zip", "epub");
                dicExtension.Add("message/rfc822", "eml");
                dicExtension.Add("application/vnd.enliven", "nml");
                dicExtension.Add("application/vnd.is-xpr", "xpr");
                dicExtension.Add("image/vnd.xiff", "xif");
                dicExtension.Add("application/vnd.xfdl", "xfdl");
                dicExtension.Add("application/emma+xml", "emma");
                dicExtension.Add("application/vnd.ezpix-album", "ez2");
                dicExtension.Add("application/vnd.ezpix-package", "ez3");
                dicExtension.Add("image/vnd.fst", "fst");
                dicExtension.Add("video/vnd.fvt", "fvt");
                dicExtension.Add("image/vnd.fastbidsheet", "fbs");
                dicExtension.Add("application/vnd.denovo.fcselayout-link", "fe_launch");
                dicExtension.Add("video/x-f4v", "f4v");
                dicExtension.Add("video/x-flv", "flv");
                dicExtension.Add("image/vnd.fpx", "fpx");
                dicExtension.Add("image/vnd.net-fpx", "npx");
                dicExtension.Add("text/vnd.fmi.flexstor", "flx");
                dicExtension.Add("video/x-fli", "fli");
                dicExtension.Add("application/vnd.fluxtime.clip", "ftc");
                dicExtension.Add("application/vnd.fdf", "fdf");
                dicExtension.Add("text/x-fortran", "f");
                dicExtension.Add("application/vnd.mif", "mif");
                dicExtension.Add("application/vnd.framemaker", "fm");
                dicExtension.Add("image/x-freehand", "fh");
                dicExtension.Add("application/vnd.fsc.weblaunch", "fsc");
                dicExtension.Add("application/vnd.frogans.fnc", "fnc");
                dicExtension.Add("application/vnd.frogans.ltf", "ltf");
                dicExtension.Add("application/vnd.fujixerox.ddd", "ddd");
                dicExtension.Add("application/vnd.fujixerox.docuworks", "xdw");
                dicExtension.Add("application/vnd.fujixerox.docuworks.binder", "xbd");
                dicExtension.Add("application/vnd.fujitsu.oasys", "oas");
                dicExtension.Add("application/vnd.fujitsu.oasys2", "oa2");
                dicExtension.Add("application/vnd.fujitsu.oasys3", "oa3");
                dicExtension.Add("application/vnd.fujitsu.oasysgp", "fg5");
                dicExtension.Add("application/vnd.fujitsu.oasysprs", "bh2");
                dicExtension.Add("application/x-futuresplash", "spl");
                dicExtension.Add("application/vnd.fuzzysheet", "fzs");
                dicExtension.Add("image/g3fax", "g3");
                dicExtension.Add("application/vnd.gmx", "gmx");
                dicExtension.Add("model/vnd.gtw", "gtw");
                dicExtension.Add("application/vnd.genomatix.tuxedo", "txd");
                dicExtension.Add("application/vnd.geogebra.file", "ggb");
                dicExtension.Add("application/vnd.geogebra.tool", "ggt");
                dicExtension.Add("model/vnd.gdl", "gdl");
                dicExtension.Add("application/vnd.geometry-explorer", "gex");
                dicExtension.Add("application/vnd.geonext", "gxt");
                dicExtension.Add("application/vnd.geoplan", "g2w");
                dicExtension.Add("application/vnd.geospace", "g3w");
                dicExtension.Add("application/x-font-ghostscript", "gsf");
                dicExtension.Add("application/x-font-bdf", "bdf");
                dicExtension.Add("application/x-gtar", "gtar");
                dicExtension.Add("application/x-texinfo", "texinfo");
                dicExtension.Add("application/x-gnumeric", "gnumeric");
                dicExtension.Add("application/vnd.google-earth.kml+xml", "kml");
                dicExtension.Add("application/vnd.google-earth.kmz", "kmz");
                dicExtension.Add("application/vnd.grafeq", "gqf");
                dicExtension.Add("image/gif", "gif");
                dicExtension.Add("text/vnd.graphviz", "gv");
                dicExtension.Add("application/vnd.groove-account", "gac");
                dicExtension.Add("application/vnd.groove-help", "ghf");
                dicExtension.Add("application/vnd.groove-identity-message", "gim");
                dicExtension.Add("application/vnd.groove-injector", "grv");
                dicExtension.Add("application/vnd.groove-tool-message", "gtm");
                dicExtension.Add("application/vnd.groove-tool-template", "tpl");
                dicExtension.Add("application/vnd.groove-vcard", "vcg");
                dicExtension.Add("video/h261", "h261");
                dicExtension.Add("video/h263", "h263");
                dicExtension.Add("video/h264", "h264");
                dicExtension.Add("application/vnd.hp-hpid", "hpid");
                dicExtension.Add("application/vnd.hp-hps", "hps");
                dicExtension.Add("application/x-hdf", "hdf");
                dicExtension.Add("audio/vnd.rip", "rip");
                dicExtension.Add("application/vnd.hbci", "hbci");
                dicExtension.Add("application/vnd.hp-jlyt", "jlt");
                dicExtension.Add("application/vnd.hp-pcl", "pcl");
                dicExtension.Add("application/vnd.hp-hpgl", "hpgl");
                dicExtension.Add("application/vnd.yamaha.hv-script", "hvs");
                dicExtension.Add("application/vnd.yamaha.hv-dic", "hvd");
                dicExtension.Add("application/vnd.yamaha.hv-voice", "hvp");
                dicExtension.Add("application/vnd.hydrostatix.sof-data", "sfd-hdstx");
                dicExtension.Add("application/hyperstudio", "stk");
                dicExtension.Add("application/vnd.hal+xml", "hal");
                dicExtension.Add("text/html", "html");
                dicExtension.Add("application/vnd.ibm.rights-management", "irm");
                dicExtension.Add("application/vnd.ibm.secure-container", "sc");
                dicExtension.Add("text/calendar", "ics");
                dicExtension.Add("application/vnd.iccprofile", "icc");
                dicExtension.Add("image/x-icon", "ico");
                dicExtension.Add("application/vnd.igloader", "igl");
                dicExtension.Add("image/ief", "ief");
                dicExtension.Add("application/vnd.immervision-ivp", "ivp");
                dicExtension.Add("application/vnd.immervision-ivu", "ivu");
                dicExtension.Add("application/reginfo+xml", "rif");
                dicExtension.Add("text/vnd.in3d.3dml", "3dml");
                dicExtension.Add("text/vnd.in3d.spot", "spot");
                dicExtension.Add("model/iges", "igs");
                dicExtension.Add("application/vnd.intergeo", "i2g");
                dicExtension.Add("application/vnd.cinderella", "cdy");
                dicExtension.Add("application/vnd.intercon.formnet", "xpw");
                dicExtension.Add("application/vnd.isac.fcs", "fcs");
                dicExtension.Add("application/ipfix", "ipfix");
                dicExtension.Add("application/pkix-cert", "cer");
                dicExtension.Add("application/pkixcmp", "pki");
                dicExtension.Add("application/pkix-crl", "crl");
                dicExtension.Add("application/pkix-pkipath", "pkipath");
                dicExtension.Add("application/vnd.insors.igm", "igm");
                dicExtension.Add("application/vnd.ipunplugged.rcprofile", "rcprofile");
                dicExtension.Add("application/vnd.irepository.package+xml", "irp");
                dicExtension.Add("text/vnd.sun.j2me.app-descriptor", "jad");
                dicExtension.Add("application/java-archive", "jar");
                dicExtension.Add("application/java-vm", "class");
                dicExtension.Add("application/x-java-jnlp-file", "jnlp");
                dicExtension.Add("application/java-serialized-object", "ser");
                dicExtension.Add("text/x-java-source,java", "java");
                dicExtension.Add("application/javascript", "js");
                dicExtension.Add("application/json", "json");
                dicExtension.Add("application/vnd.joost.joda-archive", "joda");
                dicExtension.Add("video/jpm", "jpm");
                dicExtension.Add("image/jpeg", "jpeg, jpg");
                dicExtension.Add("image/x-citrix-jpeg", "jpeg, jpg");
                dicExtension.Add("image/pjpeg", "pjpeg");
                dicExtension.Add("video/jpeg", "jpgv");
                dicExtension.Add("application/vnd.kahootz", "ktz");
                dicExtension.Add("application/vnd.chipnuts.karaoke-mmd", "mmd");
                dicExtension.Add("application/vnd.kde.karbon", "karbon");
                dicExtension.Add("application/vnd.kde.kchart", "chrt");
                dicExtension.Add("application/vnd.kde.kformula", "kfo");
                dicExtension.Add("application/vnd.kde.kivio", "flw");
                dicExtension.Add("application/vnd.kde.kontour", "kon");
                dicExtension.Add("application/vnd.kde.kpresenter", "kpr");
                dicExtension.Add("application/vnd.kde.kspread", "ksp");
                dicExtension.Add("application/vnd.kde.kword", "kwd");
                dicExtension.Add("application/vnd.kenameaapp", "htke");
                dicExtension.Add("application/vnd.kidspiration", "kia");
                dicExtension.Add("application/vnd.kinar", "kne");
                dicExtension.Add("application/vnd.kodak-descriptor", "sse");
                dicExtension.Add("application/vnd.las.las+xml", "lasxml");
                dicExtension.Add("application/x-latex", "latex");
                dicExtension.Add("application/vnd.llamagraphics.life-balance.desktop", "lbd");
                dicExtension.Add("application/vnd.llamagraphics.life-balance.exchange+xml", "lbe");
                dicExtension.Add("application/vnd.jam", "jam");
                dicExtension.Add("application/vnd.lotus-1-2-3", "123");
                dicExtension.Add("application/vnd.lotus-approach", "apr");
                dicExtension.Add("application/vnd.lotus-freelance", "pre");
                dicExtension.Add("application/vnd.lotus-notes", "nsf");
                dicExtension.Add("application/vnd.lotus-organizer", "org");
                dicExtension.Add("application/vnd.lotus-screencam", "scm");
                dicExtension.Add("application/vnd.lotus-wordpro", "lwp");
                dicExtension.Add("audio/vnd.lucent.voice", "lvp");
                dicExtension.Add("audio/x-mpegurl", "m3u");
                dicExtension.Add("video/x-m4v", "m4v");
                dicExtension.Add("application/mac-binhex40", "hqx");
                dicExtension.Add("application/vnd.macports.portpkg", "portpkg");
                dicExtension.Add("application/vnd.osgeo.mapguide.package", "mgp");
                dicExtension.Add("application/marc", "mrc");
                dicExtension.Add("application/marcxml+xml", "mrcx");
                dicExtension.Add("application/mxf", "mxf");
                dicExtension.Add("application/vnd.wolfram.player", "nbp");
                dicExtension.Add("application/mathematica", "ma");
                dicExtension.Add("application/mathml+xml", "mathml");
                dicExtension.Add("application/mbox", "mbox");
                dicExtension.Add("application/vnd.medcalcdata", "mc1");
                dicExtension.Add("application/mediaservercontrol+xml", "mscml");
                dicExtension.Add("application/vnd.mediastation.cdkey", "cdkey");
                dicExtension.Add("application/vnd.mfer", "mwf");
                dicExtension.Add("application/vnd.mfmp", "mfm");
                dicExtension.Add("model/mesh", "msh");
                dicExtension.Add("application/mads+xml", "mads");
                dicExtension.Add("application/mets+xml", "mets");
                dicExtension.Add("application/mods+xml", "mods");
                dicExtension.Add("application/metalink4+xml", "meta4");
                dicExtension.Add("application/vnd.mcd", "mcd");
                dicExtension.Add("application/vnd.micrografx.flo", "flo");
                dicExtension.Add("application/vnd.micrografx.igx", "igx");
                dicExtension.Add("application/vnd.eszigno3+xml", "es3");
                dicExtension.Add("application/x-msaccess", "mdb");
                dicExtension.Add("video/x-ms-asf", "asf");
                dicExtension.Add("application/x-msdownload", "exe");
                dicExtension.Add("application/vnd.ms-artgalry", "cil");
                dicExtension.Add("application/vnd.ms-cab-compressed", "cab");
                dicExtension.Add("application/vnd.ms-ims", "ims");
                dicExtension.Add("application/x-ms-application", "application");
                dicExtension.Add("application/x-msclip", "clp");
                dicExtension.Add("image/vnd.ms-modi", "mdi");
                dicExtension.Add("application/vnd.ms-fontobject", "eot");
                dicExtension.Add("application/vnd.ms-excel", "xls");
                dicExtension.Add("application/vnd.ms-excel.addin.macroenabled.12", "xlam");
                dicExtension.Add("application/vnd.ms-excel.sheet.binary.macroenabled.12", "xlsb");
                dicExtension.Add("application/vnd.ms-excel.template.macroenabled.12", "xltm");
                dicExtension.Add("application/vnd.ms-excel.sheet.macroenabled.12", "xlsm");
                dicExtension.Add("application/vnd.ms-htmlhelp", "chm");
                dicExtension.Add("application/x-mscardfile", "crd");
                dicExtension.Add("application/vnd.ms-lrm", "lrm");
                dicExtension.Add("application/x-msmediaview", "mvb");
                dicExtension.Add("application/x-msmoney", "mny");
                dicExtension.Add("application/vnd.openxmlformats-officedocument.presentationml.presentation", "pptx");
                dicExtension.Add("application/vnd.openxmlformats-officedocument.presentationml.slide", "sldx");
                dicExtension.Add("application/vnd.openxmlformats-officedocument.presentationml.slideshow", "ppsx");
                dicExtension.Add("application/vnd.openxmlformats-officedocument.presentationml.template", "potx");
                dicExtension.Add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx");
                dicExtension.Add("application/vnd.openxmlformats-officedocument.spreadsheetml.template", "xltx");
                dicExtension.Add("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx");
                dicExtension.Add("application/vnd.openxmlformats-officedocument.wordprocessingml.template", "dotx");
                dicExtension.Add("application/x-msbinder", "obd");
                dicExtension.Add("application/vnd.ms-officetheme", "thmx");
                dicExtension.Add("application/onenote", "onetoc");
                dicExtension.Add("audio/vnd.ms-playready.media.pya", "pya");
                dicExtension.Add("video/vnd.ms-playready.media.pyv", "pyv");
                dicExtension.Add("application/vnd.ms-powerpoint", "ppt");
                dicExtension.Add("application/vnd.ms-powerpoint.addin.macroenabled.12", "ppam");
                dicExtension.Add("application/vnd.ms-powerpoint.slide.macroenabled.12", "sldm");
                dicExtension.Add("application/vnd.ms-powerpoint.presentation.macroenabled.12", "pptm");
                dicExtension.Add("application/vnd.ms-powerpoint.slideshow.macroenabled.12", "ppsm");
                dicExtension.Add("application/vnd.ms-powerpoint.template.macroenabled.12", "potm");
                dicExtension.Add("application/vnd.ms-project", "mpp");
                dicExtension.Add("application/x-mspublisher", "pub");
                dicExtension.Add("application/x-msschedule", "scd");
                dicExtension.Add("application/x-silverlight-app", "xap");
                dicExtension.Add("application/vnd.ms-pki.stl", "stl");
                dicExtension.Add("application/vnd.ms-pki.seccat", "cat");
                dicExtension.Add("application/vnd.visio", "vsd");
                dicExtension.Add("application/vnd.visio2013", "vsdx");
                dicExtension.Add("video/x-ms-wm", "wm");
                dicExtension.Add("audio/x-ms-wma", "wma");
                dicExtension.Add("audio/x-ms-wax", "wax");
                dicExtension.Add("video/x-ms-wmx", "wmx");
                dicExtension.Add("application/x-ms-wmd", "wmd");
                dicExtension.Add("application/vnd.ms-wpl", "wpl");
                dicExtension.Add("application/x-ms-wmz", "wmz");
                dicExtension.Add("video/x-ms-wmv", "wmv");
                dicExtension.Add("video/x-ms-wvx", "wvx");
                dicExtension.Add("application/x-msmetafile", "wmf");
                dicExtension.Add("application/x-msterminal", "trm");
                dicExtension.Add("application/msword", "doc");
                dicExtension.Add("application/vnd.ms-word.document.macroenabled.12", "docm");
                dicExtension.Add("application/vnd.ms-word.template.macroenabled.12", "dotm");
                dicExtension.Add("application/x-mswrite", "wri");
                dicExtension.Add("application/vnd.ms-works", "wps");
                dicExtension.Add("application/x-ms-xbap", "xbap");
                dicExtension.Add("application/vnd.ms-xpsdocument", "xps");
                dicExtension.Add("audio/midi", "mid");
                dicExtension.Add("application/vnd.ibm.minipay", "mpy");
                dicExtension.Add("application/vnd.ibm.modcap", "afp");
                dicExtension.Add("application/vnd.jcp.javame.midlet-rms", "rms");
                dicExtension.Add("application/vnd.tmobile-livetv", "tmo");
                dicExtension.Add("application/x-mobipocket-ebook", "prc");
                dicExtension.Add("application/vnd.mobius.mbk", "mbk");
                dicExtension.Add("application/vnd.mobius.dis", "dis");
                dicExtension.Add("application/vnd.mobius.plc", "plc");
                dicExtension.Add("application/vnd.mobius.mqy", "mqy");
                dicExtension.Add("application/vnd.mobius.msl", "msl");
                dicExtension.Add("application/vnd.mobius.txf", "txf");
                dicExtension.Add("application/vnd.mobius.daf", "daf");
                dicExtension.Add("text/vnd.fly", "fly");
                dicExtension.Add("application/vnd.mophun.certificate", "mpc");
                dicExtension.Add("application/vnd.mophun.application", "mpn");
                dicExtension.Add("video/mj2", "mj2");
                dicExtension.Add("audio/mpeg", "mpga");
                dicExtension.Add("video/vnd.mpegurl", "mxu");
                dicExtension.Add("video/mpeg", "mpeg");
                dicExtension.Add("application/mp21", "m21");
                dicExtension.Add("audio/mp4", "mp4a");
                dicExtension.Add("video/mp4", "mp4");
                dicExtension.Add("application/mp4", "mp4");
                dicExtension.Add("application/vnd.apple.mpegurl", "m3u8");
                dicExtension.Add("application/vnd.musician", "mus");
                dicExtension.Add("application/vnd.muvee.style", "msty");
                dicExtension.Add("application/xv+xml", "mxml");
                dicExtension.Add("application/vnd.nokia.n-gage.data", "ngdat");
                dicExtension.Add("application/vnd.nokia.n-gage.symbian.install", "n-gage");
                dicExtension.Add("application/x-dtbncx+xml", "ncx");
                dicExtension.Add("application/x-netcdf", "nc");
                dicExtension.Add("application/vnd.neurolanguage.nlu", "nlu");
                dicExtension.Add("application/vnd.dna", "dna");
                dicExtension.Add("application/vnd.noblenet-directory", "nnd");
                dicExtension.Add("application/vnd.noblenet-sealer", "nns");
                dicExtension.Add("application/vnd.noblenet-web", "nnw");
                dicExtension.Add("application/vnd.nokia.radio-preset", "rpst");
                dicExtension.Add("application/vnd.nokia.radio-presets", "rpss");
                dicExtension.Add("text/n3", "n3");
                dicExtension.Add("application/vnd.novadigm.edm", "edm");
                dicExtension.Add("application/vnd.novadigm.edx", "edx");
                dicExtension.Add("application/vnd.novadigm.ext", "ext");
                dicExtension.Add("application/vnd.flographit", "gph");
                dicExtension.Add("audio/vnd.nuera.ecelp4800", "ecelp4800");
                dicExtension.Add("audio/vnd.nuera.ecelp7470", "ecelp7470");
                dicExtension.Add("audio/vnd.nuera.ecelp9600", "ecelp9600");
                dicExtension.Add("application/oda", "oda");
                dicExtension.Add("application/ogg", "ogx");
                dicExtension.Add("audio/ogg", "oga");
                dicExtension.Add("video/ogg", "ogv");
                dicExtension.Add("application/vnd.oma.dd2+xml", "dd2");
                dicExtension.Add("application/vnd.oasis.opendocument.text-web", "oth");
                dicExtension.Add("application/oebps-package+xml", "opf");
                dicExtension.Add("application/vnd.intu.qbo", "qbo");
                dicExtension.Add("application/vnd.openofficeorg.extension", "oxt");
                dicExtension.Add("application/vnd.yamaha.openscoreformat", "osf");
                dicExtension.Add("audio/webm", "weba");
                dicExtension.Add("video/webm", "webm");
                dicExtension.Add("application/vnd.oasis.opendocument.chart", "odc");
                dicExtension.Add("application/vnd.oasis.opendocument.chart-template", "otc");
                dicExtension.Add("application/vnd.oasis.opendocument.database", "odb");
                dicExtension.Add("application/vnd.oasis.opendocument.formula", "odf");
                dicExtension.Add("application/vnd.oasis.opendocument.formula-template", "odft");
                dicExtension.Add("application/vnd.oasis.opendocument.graphics", "odg");
                dicExtension.Add("application/vnd.oasis.opendocument.graphics-template", "otg");
                dicExtension.Add("application/vnd.oasis.opendocument.image", "odi");
                dicExtension.Add("application/vnd.oasis.opendocument.image-template", "oti");
                dicExtension.Add("application/vnd.oasis.opendocument.presentation", "odp");
                dicExtension.Add("application/vnd.oasis.opendocument.presentation-template", "otp");
                dicExtension.Add("application/vnd.oasis.opendocument.spreadsheet", "ods");
                dicExtension.Add("application/vnd.oasis.opendocument.spreadsheet-template", "ots");
                dicExtension.Add("application/vnd.oasis.opendocument.text", "odt");
                dicExtension.Add("application/vnd.oasis.opendocument.text-master", "odm");
                dicExtension.Add("application/vnd.oasis.opendocument.text-template", "ott");
                dicExtension.Add("image/ktx", "ktx");
                dicExtension.Add("application/vnd.sun.xml.calc", "sxc");
                dicExtension.Add("application/vnd.sun.xml.calc.template", "stc");
                dicExtension.Add("application/vnd.sun.xml.draw", "sxd");
                dicExtension.Add("application/vnd.sun.xml.draw.template", "std");
                dicExtension.Add("application/vnd.sun.xml.impress", "sxi");
                dicExtension.Add("application/vnd.sun.xml.impress.template", "sti");
                dicExtension.Add("application/vnd.sun.xml.math", "sxm");
                dicExtension.Add("application/vnd.sun.xml.writer", "sxw");
                dicExtension.Add("application/vnd.sun.xml.writer.global", "sxg");
                dicExtension.Add("application/vnd.sun.xml.writer.template", "stw");
                dicExtension.Add("application/x-font-otf", "otf");
                dicExtension.Add("application/vnd.yamaha.openscoreformat.osfpvg+xml", "osfpvg");
                dicExtension.Add("application/vnd.osgi.dp", "dp");
                dicExtension.Add("application/vnd.palm", "pdb");
                dicExtension.Add("text/x-pascal", "p");
                dicExtension.Add("application/vnd.pawaafile", "paw");
                dicExtension.Add("application/vnd.hp-pclxl", "pclxl");
                dicExtension.Add("application/vnd.picsel", "efif");
                dicExtension.Add("image/x-pcx", "pcx");
                dicExtension.Add("image/vnd.adobe.photoshop", "psd");
                dicExtension.Add("application/pics-rules", "prf");
                dicExtension.Add("image/x-pict", "pic");
                dicExtension.Add("application/x-chat", "chat");
                dicExtension.Add("application/pkcs10", "p10");
                dicExtension.Add("application/x-pkcs12", "p12");
                dicExtension.Add("application/pkcs7-mime", "p7m");
                dicExtension.Add("application/pkcs7-signature", "p7s");
                dicExtension.Add("application/x-pkcs7-certreqresp", "p7r");
                dicExtension.Add("application/x-pkcs7-certificates", "p7b");
                dicExtension.Add("application/pkcs8", "p8");
                dicExtension.Add("application/vnd.pocketlearn", "plf");
                dicExtension.Add("image/x-portable-anymap", "pnm");
                dicExtension.Add("image/x-portable-bitmap", "pbm");
                dicExtension.Add("application/x-font-pcf", "pcf");
                dicExtension.Add("application/font-tdpfr", "pfr");
                dicExtension.Add("application/x-chess-pgn", "pgn");
                dicExtension.Add("image/x-portable-graymap", "pgm");
                dicExtension.Add("image/png", "png");
                dicExtension.Add("image/x-citrix-png", "png");
                dicExtension.Add("image/x-png", "png");
                dicExtension.Add("image/x-portable-pixmap", "ppm");
                dicExtension.Add("application/pskc+xml", "pskcxml");
                dicExtension.Add("application/vnd.ctc-posml", "pml");
                dicExtension.Add("application/postscript", "ai");
                dicExtension.Add("application/x-font-type1", "pfa");
                dicExtension.Add("application/vnd.powerbuilder6", "pbd");
                dicExtension.Add("application/pgp-encrypted", "pgp");
                dicExtension.Add("application/pgp-signature", "pgp");
                dicExtension.Add("application/vnd.previewsystems.box", "box");
                dicExtension.Add("application/vnd.pvi.ptid1", "ptid");
                dicExtension.Add("application/pls+xml", "pls");
                dicExtension.Add("application/vnd.pg.format", "str");
                dicExtension.Add("application/vnd.pg.osasli", "ei6");
                dicExtension.Add("text/prs.lines.tag", "dsc");
                dicExtension.Add("application/x-font-linux-psf", "psf");
                dicExtension.Add("application/vnd.publishare-delta-tree", "qps");
                dicExtension.Add("application/vnd.pmi.widget", "wg");
                dicExtension.Add("application/vnd.quark.quarkxpress", "qxd");
                dicExtension.Add("application/vnd.epson.esf", "esf");
                dicExtension.Add("application/vnd.epson.msf", "msf");
                dicExtension.Add("application/vnd.epson.ssf", "ssf");
                dicExtension.Add("application/vnd.epson.quickanime", "qam");
                dicExtension.Add("application/vnd.intu.qfx", "qfx");
                dicExtension.Add("video/quicktime", "qt");
                dicExtension.Add("application/x-rar-compressed", "rar");
                dicExtension.Add("audio/x-pn-realaudio", "ram");
                dicExtension.Add("audio/x-pn-realaudio-plugin", "rmp");
                dicExtension.Add("application/rsd+xml", "rsd");
                dicExtension.Add("application/vnd.rn-realmedia", "rm");
                dicExtension.Add("application/vnd.realvnc.bed", "bed");
                dicExtension.Add("application/vnd.recordare.musicxml", "mxl");
                dicExtension.Add("application/vnd.recordare.musicxml+xml", "musicxml");
                dicExtension.Add("application/relax-ng-compact-syntax", "rnc");
                dicExtension.Add("application/vnd.data-vision.rdz", "rdz");
                dicExtension.Add("application/rdf+xml", "rdf");
                dicExtension.Add("application/vnd.cloanto.rp9", "rp9");
                dicExtension.Add("application/vnd.jisp", "jisp");
                dicExtension.Add("application/rtf", "rtf");
                dicExtension.Add("text/richtext", "rtx");
                dicExtension.Add("application/vnd.route66.link66+xml", "link66");
                dicExtension.Add("application/rss+xml", "rss, xml");
                dicExtension.Add("application/shf+xml", "shf");
                dicExtension.Add("application/vnd.sailingtracker.track", "st");
                dicExtension.Add("image/svg+xml", "svg");
                dicExtension.Add("application/vnd.sus-calendar", "sus");
                dicExtension.Add("application/sru+xml", "sru");
                dicExtension.Add("application/set-payment-initiation", "setpay");
                dicExtension.Add("application/set-registration-initiation", "setreg");
                dicExtension.Add("application/vnd.sema", "sema");
                dicExtension.Add("application/vnd.semd", "semd");
                dicExtension.Add("application/vnd.semf", "semf");
                dicExtension.Add("application/vnd.seemail", "see");
                dicExtension.Add("application/x-font-snf", "snf");
                dicExtension.Add("application/scvp-vp-request", "spq");
                dicExtension.Add("application/scvp-vp-response", "spp");
                dicExtension.Add("application/scvp-cv-request", "scq");
                dicExtension.Add("application/scvp-cv-response", "scs");
                dicExtension.Add("application/sdp", "sdp");
                dicExtension.Add("text/x-setext", "etx");
                dicExtension.Add("video/x-sgi-movie", "movie");
                dicExtension.Add("application/vnd.shana.informed.formdata", "ifm");
                dicExtension.Add("application/vnd.shana.informed.formtemplate", "itp");
                dicExtension.Add("application/vnd.shana.informed.interchange", "iif");
                dicExtension.Add("application/vnd.shana.informed.package", "ipk");
                dicExtension.Add("application/thraud+xml", "tfi");
                dicExtension.Add("application/x-shar", "shar");
                dicExtension.Add("image/x-rgb", "rgb");
                dicExtension.Add("application/vnd.epson.salt", "slt");
                dicExtension.Add("application/vnd.accpac.simply.aso", "aso");
                dicExtension.Add("application/vnd.accpac.simply.imp", "imp");
                dicExtension.Add("application/vnd.simtech-mindmapper", "twd");
                dicExtension.Add("application/vnd.commonspace", "csp");
                dicExtension.Add("application/vnd.yamaha.smaf-audio", "saf");
                dicExtension.Add("application/vnd.smaf", "mmf");
                dicExtension.Add("application/vnd.yamaha.smaf-phrase", "spf");
                dicExtension.Add("application/vnd.smart.teacher", "teacher");
                dicExtension.Add("application/vnd.svd", "svd");
                dicExtension.Add("application/sparql-query", "rq");
                dicExtension.Add("application/sparql-results+xml", "srx");
                dicExtension.Add("application/srgs", "gram");
                dicExtension.Add("application/srgs+xml", "grxml");
                dicExtension.Add("application/ssml+xml", "ssml");
                dicExtension.Add("application/vnd.koan", "skp");
                dicExtension.Add("text/sgml", "sgml");
                dicExtension.Add("application/vnd.stardivision.calc", "sdc");
                dicExtension.Add("application/vnd.stardivision.draw", "sda");
                dicExtension.Add("application/vnd.stardivision.impress", "sdd");
                dicExtension.Add("application/vnd.stardivision.math", "smf");
                dicExtension.Add("application/vnd.stardivision.writer", "sdw");
                dicExtension.Add("application/vnd.stardivision.writer-global", "sgl");
                dicExtension.Add("application/vnd.stepmania.stepchart", "sm");
                dicExtension.Add("application/x-stuffit", "sit");
                dicExtension.Add("application/x-stuffitx", "sitx");
                dicExtension.Add("application/vnd.solent.sdkm+xml", "sdkm");
                dicExtension.Add("application/vnd.olpc-sugar", "xo");
                dicExtension.Add("audio/basic", "au");
                dicExtension.Add("application/vnd.wqd", "wqd");
                dicExtension.Add("application/vnd.symbian.install", "sis");
                dicExtension.Add("application/smil+xml", "smi");
                dicExtension.Add("application/vnd.syncml+xml", "xsm");
                dicExtension.Add("application/vnd.syncml.dm+wbxml", "bdm");
                dicExtension.Add("application/vnd.syncml.dm+xml", "xdm");
                dicExtension.Add("application/x-sv4cpio", "sv4cpio");
                dicExtension.Add("application/x-sv4crc", "sv4crc");
                dicExtension.Add("application/sbml+xml", "sbml");
                dicExtension.Add("text/tab-separated-values", "tsv");
                dicExtension.Add("image/tiff", "tiff");
                dicExtension.Add("application/vnd.tao.intent-module-archive", "tao");
                dicExtension.Add("application/x-tar", "tar");
                dicExtension.Add("application/x-tcl", "tcl");
                dicExtension.Add("application/x-tex", "tex");
                dicExtension.Add("application/x-tex-tfm", "tfm");
                dicExtension.Add("application/tei+xml", "tei");
                dicExtension.Add("text/plain", "txt");
                dicExtension.Add("application/vnd.spotfire.dxp", "dxp");
                dicExtension.Add("application/vnd.spotfire.sfs", "sfs");
                dicExtension.Add("application/timestamped-data", "tsd");
                dicExtension.Add("application/vnd.trid.tpt", "tpt");
                dicExtension.Add("application/vnd.triscape.mxs", "mxs");
                dicExtension.Add("text/troff", "t");
                dicExtension.Add("application/vnd.trueapp", "tra");
                dicExtension.Add("application/x-font-ttf", "ttf");
                dicExtension.Add("text/turtle", "ttl");
                dicExtension.Add("application/vnd.umajin", "umj");
                dicExtension.Add("application/vnd.uoml+xml", "uoml");
                dicExtension.Add("application/vnd.unity", "unityweb");
                dicExtension.Add("application/vnd.ufdl", "ufd");
                dicExtension.Add("text/uri-list", "uri");
                dicExtension.Add("application/vnd.uiq.theme", "utz");
                dicExtension.Add("application/x-ustar", "ustar");
                dicExtension.Add("text/x-uuencode", "uu");
                dicExtension.Add("text/x-vcalendar", "vcs");
                dicExtension.Add("text/x-vcard", "vcf");
                dicExtension.Add("application/x-cdlink", "vcd");
                dicExtension.Add("application/vnd.vsf", "vsf");
                dicExtension.Add("model/vrml", "wrl");
                dicExtension.Add("application/vnd.vcx", "vcx");
                dicExtension.Add("model/vnd.mts", "mts");
                dicExtension.Add("model/vnd.vtu", "vtu");
                dicExtension.Add("application/vnd.visionary", "vis");
                dicExtension.Add("video/vnd.vivo", "viv");
                dicExtension.Add("application/ccxml+xml,", "ccxml");
                dicExtension.Add("application/voicexml+xml", "vxml");
                dicExtension.Add("application/x-wais-source", "src");
                dicExtension.Add("application/vnd.wap.wbxml", "wbxml");
                dicExtension.Add("image/vnd.wap.wbmp", "wbmp");
                dicExtension.Add("audio/x-wav", "wav");
                dicExtension.Add("application/davmount+xml", "davmount");
                dicExtension.Add("application/x-font-woff", "woff");
                dicExtension.Add("application/wspolicy+xml", "wspolicy");
                dicExtension.Add("image/webp", "webp");
                dicExtension.Add("application/vnd.webturbo", "wtb");
                dicExtension.Add("application/widget", "wgt");
                dicExtension.Add("application/winhlp", "hlp");
                dicExtension.Add("text/vnd.wap.wml", "wml");
                dicExtension.Add("text/vnd.wap.wmlscript", "wmls");
                dicExtension.Add("application/vnd.wap.wmlscriptc", "wmlsc");
                dicExtension.Add("application/vnd.wordperfect", "wpd");
                dicExtension.Add("application/vnd.wt.stf", "stf");
                dicExtension.Add("application/wsdl+xml", "wsdl");
                dicExtension.Add("image/x-xbitmap", "xbm");
                dicExtension.Add("image/x-xpixmap", "xpm");
                dicExtension.Add("image/x-xwindowdump", "xwd");
                dicExtension.Add("application/x-x509-ca-cert", "der");
                dicExtension.Add("application/x-xfig", "fig");
                dicExtension.Add("application/xhtml+xml", "xhtml");
                dicExtension.Add("application/xml", "xml");
                dicExtension.Add("application/xcap-diff+xml", "xdf");
                dicExtension.Add("application/xenc+xml", "xenc");
                dicExtension.Add("application/patch-ops-error+xml", "xer");
                dicExtension.Add("application/resource-lists+xml", "rl");
                dicExtension.Add("application/rls-services+xml", "rs");
                dicExtension.Add("application/resource-lists-diff+xml", "rld");
                dicExtension.Add("application/xslt+xml", "xslt");
                dicExtension.Add("application/xop+xml", "xop");
                dicExtension.Add("application/x-xpinstall", "xpi");
                dicExtension.Add("application/xspf+xml", "xspf");
                dicExtension.Add("application/vnd.mozilla.xul+xml", "xul");
                dicExtension.Add("chemical/x-xyz", "xyz");
                dicExtension.Add("text/yaml", "yaml");
                dicExtension.Add("application/yang", "yang");
                dicExtension.Add("application/yin+xml", "yin");
                dicExtension.Add("application/vnd.zul", "zir");
                dicExtension.Add("application/zip", "zip");
                dicExtension.Add("application/vnd.handheld-entertainment+xml", "zmm");
                dicExtension.Add("application/vnd.zzazz.deck+xml", "zaz");
            }
            if (dicExtension.ContainsKey(mimeType)) extension = dicExtension[mimeType];
            return extension;
        }
        #endregion
    }

    public class dlfileentryInfoLocalServiceUtil : ObjectLocalServiceUtil<fileEntryInfo>
    {
        public dlfileentryInfoLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static dlfileentryInfoLocalServiceUtil _Instance;
        public static dlfileentryInfoLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null) _Instance = new dlfileentryInfoLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        /// <summary>
        /// LOG FILE 
        /// </summary>
        static Type type = typeof(dlfileentryInfoLocalServiceUtil);

        #region get cachename
        public string getDataByEntryId(string fileEntryId)
        {
            return Constant.Cache.FILE_ENTRY_INFO.text + ".fileentry_id." + fileEntryId;
        }
        #endregion

        #region cache function
        public fileEntryInfo updateCacheDataByEntryId(string fileEntryId, string ftpUid = "", string ftpPwd = "")
        {
            dlfileentry fileEntry = dlfileentryLocalServiceUtil.Instance.getFileEntry(fileEntryId);

            if (fileEntry == null) return null;
            string img_content = "";
            System.Drawing.Bitmap bitmap = null;
            byte[] bytes = null;
            if (fileEntry.host.IndexOf("ftp") != -1)
            {
                string ftpPathFile = fileEntry.host + fileEntry.treepath + @"/" + fileEntry.name;
                string base64String = Common.FTPUtility.GETBASE64_IMG_FTP(ftpPathFile, ftpUid, ftpPwd);
                img_content = "data:" + fileEntry.mimetype + ";base64," + base64String;
                // 7: length("base64,")
                string base64 = img_content.Substring(img_content.IndexOf("base64,") + 7);
                bitmap = Common.Utilities.Base64StringToBitmap(base64String);
                bytes = Common.FTPUtility.GETBYTES_FTP(ftpPathFile, ftpUid, ftpPwd);
            }
            else
            {
                img_content = fileEntry.treepath + "/" + fileEntry.name;
                bitmap = new System.Drawing.Bitmap(System.Web.HttpContext.Current.Server.MapPath(img_content));
                bytes = Common.FTPUtility.GETBYTES_WEBSERVER(System.Web.HttpContext.Current.Server.MapPath(img_content));
            }

            fileEntryInfo f = new fileEntryInfo
            {
                fileentryid = fileEntryId,
                img_content = img_content,
                bmp = bitmap,
                bytes = bytes
            };

            string keycache = this.getDataByEntryId(fileEntryId);
            this.UpdateCache(keycache, int.Parse(Constant.Cache.FILE_ENTRY_INFO.val), f);

            return f;
        }
        #endregion
    }
}
