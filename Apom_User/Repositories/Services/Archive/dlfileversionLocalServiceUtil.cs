﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

using ServiceBase;
using Repositories.Models.Archive;

namespace Repositories.Services.Archive
{
    public class dlfileversionLocalServiceUtil : ObjectLocalServiceUtil<dlfileversion>
    {
        public dlfileversionLocalServiceUtil(string connString) : base(connString)
        {

        }

        private static dlfileversionLocalServiceUtil _Instance;
        public static dlfileversionLocalServiceUtil Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new dlfileversionLocalServiceUtil(ObjectLocalServiceUtil<object>.CONNSTR);
                return _Instance;
            }
        }

        #region public functions

        public dlfileversion getFileVersion(string fileVersionId)
        {
            string sql = $"select * from {this._tablename} where fileversionid = @vidagis_fileversionid";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fileversionid", fileVersionId));
            List<dlfileversion> list = this.executeQuery(sql, parameters.ToArray());
            dlfileversion obj = list.Count > 0 ? list[0] : null;
            return obj;
        }
        #endregion
    }
}
