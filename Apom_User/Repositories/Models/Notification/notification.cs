﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Notification
{
    [Table("r_notification", Schema = "public")]
    public class notification
    {
        [Key]
        [Required]
        public long notification_id { get; set; }

        [MaxLength(100)]
        public string title_vi { get; set; }

        [MaxLength(100)]
        public string title_en { get; set; }

        [MaxLength(5000)]
        public string content_vi { get; set; }

        [MaxLength(5000)]
        public string content_en { get; set; }

        [MaxLength(500)]
        public string content_url { get; set; }
        
        public int type { get; set; }

        [MaxLength(100)]
        public string notification_type { get; set; }

        [MaxLength(100)]
        public string notification_code { get; set; }

        public DateTime createdate { get; set; }

        public DateTime notification_date { get; set; }

        [MaxLength(50)]
        public string province_id { get; set; }

        [MaxLength(50)]
        public string district_id { get; set; }        
    }

    [Table("r_notification_user", Schema = "public")]
    public class notification_user
    {
        [Key]
        [Required]
        public long oid { get; set; }

        [Required]
        public long notification_id { get; set; }

        [MaxLength(100)]
        public string user_logged { get; set; }

        [MaxLength(100)]
        public string from_user { get; set; }

        public bool is_read { get; set; }

        public bool is_delete { get; set; }

        public bool is_reminder { get; set; }
    }

    public class notif_request
    {
        public string user_id { get; set; }
        public string msg { get; set; }
    }

    public class notif_info
    {
        public long id { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime date { get; set; }
        public bool is_read { get; set; }
    }

    public class notif_over_threshold_province_user
    {
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string email_address { get; set; }
        public string province_id { get; set; }
        public string province_name { get; set; }
        public decimal aqi { get; set; }
    }

    public class notif_over_threshold_province_user_result
    {
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string email_address { get; set; }
        public bool is_ok { get; set; }
    }
}
