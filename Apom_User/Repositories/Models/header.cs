﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models
{
    public class link_model
    {
        public string Title { get; set; }
        public string link { get; set; }

        public int level { get; set; }
    }

    public class header
    {
        public link_model root { get; set; }
        public List<link_model> lCate { get; set; }
        public string SubMenu { get; set; }

        public double meansure { get; set; }
        public string config { get; set; }
        public string tenantid { get; set; }
        public int? languageid { get; set; }

        /*
         * fields upload file 
        */
        public string fielduploadfile { get; set; }

    }
}
