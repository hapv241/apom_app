﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Administrative
{
    [Table("u_user", Schema = "public")]
    public class user
    {
        public user()
        {
        }

        [Key]
        [Required]
        public long oid { get; set; }

        [MaxLength(50)]
        [Required]
        public string userid { get; set; }
        [MaxLength(50)]
        public string username { get; set; }
        [MaxLength(50)]
        public string displayname { get; set; }
        [MaxLength(50)]
        [Required]
        public string emailaddress { get; set; }
        [MaxLength(50)]
        [Required]
        public string password { get; set; }
        [MaxLength(50)]
        public string firstname { get; set; }
        [MaxLength(50)]
        public string middlename { get; set; }
        [MaxLength(50)]
        public string lastname { get; set; }
        public DateTime? birthday { get; set; }
        public string sex { get; set; }
        [MaxLength(30)]
        public string phonenumber { get; set; }
        [MaxLength(50)]
        public string image { get; set; }
        public bool? lockaccount { get; set; }
        public DateTime? lasttime_access { get; set; }

        [NotMapped]
        public string lasttime_access_str
        {
            get
            {
                if (lasttime_access == null) return "";

                return lasttime_access.Value.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        public bool? password_reset { get; set; }
        public bool? passwordencrypted { get; set; }
        public bool? is_delete { get; set; }
        public DateTime? createdate { get; set; } = DateTime.Now;
        public DateTime? modifydate { get; set; }
        public bool? isactive { get; set; }

        // thoi gian bat dau dang ky account - kiem tra thoi gian hieu luc cua link
        public long? time_register { get; set; }        

        public long? time_reset_pwd { get; set; }

        [NotMapped]
        public long? time_request { get; set; }

        [NotMapped]
        public long? time_request_old { get; set; }

        [NotMapped]
        public string fullname
        {
            get
            {
                return (!string.IsNullOrEmpty(firstname) ? firstname + " " : "") +
                  (!string.IsNullOrEmpty(middlename) ? middlename + " " : "") +
                  (!string.IsNullOrEmpty(lastname) ? lastname + " " : "");
            }

        }

        public string companyname { get; set; }
        public string website { get; set; }

        public string device_tokens { get; set; }

        public bool is_get_notify { get; set; } = true;

        [NotMapped]
        public string access_token { get; set; }
    }

    // userid, username, firstname, middlename, lastname, isactive, time_register, lasttime_access, roleid, name
    [NotMapped]
    public class user_info_model : user
    {
        [NotMapped]
        public string re_password { get; set; }

        [NotMapped]
        public string activename
        {
            get
            {
                if (this.isactive == null) return "user_status_lock";
                return this.isactive.Value ? "user_status_active" : "user_status_lock";
            }
        }

        [NotMapped]
        public string classactive
        {
            get
            {
                if (this.isactive == null) return "badge badge-danger";
                return this.isactive.Value ? "badge badge-success" : "badge badge-danger";
            }
        }

        [NotMapped]
        public string time_register_str
        {
            get
            {
                if (this.time_register == null) return "";
                long tick = this.time_register.Value * 1000;
                DateTime dt = new DateTime(tick);
                return dt.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        [NotMapped]
        public string image_src
        {
            get
            {
                if(!String.IsNullOrEmpty(this.image))
                {
                    string path = Repositories.Services.Archive.dlfileentryLocalServiceUtil.Instance.getPath(this.image);
                    return path;
                }
                return "";
            }
        }

        //private List<user_role_view> _role = new List<user_role_view>();
        [NotMapped]
        public List<user_role_view> role
        {
            get; set;
        }

        [NotMapped]
        public bool isAdmin
        {
            get
            {
                string roleAdmin = PermissionBase.Security.RoleSystem.ADMINISTRATOR;
                if (this.role == null) return false;
                var isRoleAdmin = this.role.Where(x => x.roleid == roleAdmin).FirstOrDefault();
                return isRoleAdmin == null ? false : true;
            }
        }

        [NotMapped]
        public bool isSuperAdmin
        {
            get
            {
                string roleAdmin = PermissionBase.Security.RoleSystem.ADMINISTRATOR;
                if (this.role == null) return false;
                var isRoleSuperAdmin = this.role.Where(x => x.roleid == roleAdmin && x.isroot == true).FirstOrDefault();
                return isRoleSuperAdmin == null ? false : true;
            }
        }
    }

    public class user_info_view: basemodel
    {
        public Repositories.Models.header header { get; set; }
        public user_profile userProfile { get; set; }
        public user_info_model user { get; set; }
        public string paraI { get; set; }
        //public List<GroupUser> userGroups { get; set; }
        //public List<RoleUser> userRoles { get; set; }
    }

    public class user_list_view
    {
        public user_info_model userProfile { get; set; }
        public List<user_info_model> lstUser { get; set; }
    }

    public class user_profile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime? Birthday { get; set; }
        public bool? Gender { get; set; }
        public string ImageUrl { get; set; }
        public string FullName_Show
        {
            get
            {
                if (!string.IsNullOrEmpty(FullName))
                {
                    return FullName;
                }
                else if (string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
                    return "";
                else
                    return FirstName + " " + LastName;
            }
        }
        public string ImageThumb150
        {
            get
            {
                if (!string.IsNullOrEmpty(ImageUrl))
                {
                    var image = ImageUrl.Split('/');
                    return string.Format("/UserImage/{0}/{1}/{2}", 150, 150, image[image.Length - 1]);
                }
                else
                    return @"/Content/admin_style/avatars/profile-pic.png";
            }
        }
    }

    public class auth_user
    {
        public string access_token { get; set; }
        public user user { get; set; }
    }

}
