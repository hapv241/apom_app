﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Administrative
{
    [Table("u_general_setting", Schema = "public")]
    public class general_setting
    {
        public general_setting()
        {
        }

        [Key]
        [Required]
        [Display(Name = "oid", Description = "ObjectID")]
        public long oid { get; set; }

        [Display(Name = "themeid", Description = "themeid")]
        public string themeid { get; set; }

        [Display(Name = "Language default", Description = "languagedefaultid")]
        public string languagedefaultid { get; set; }

        [Display(Name = "Format date", Description = "formatdate")]
        public string formatdate { get; set; }

        [Display(Name = "Format time", Description = "formattime")]
        public string formattime { get; set; }

        [Display(Name = "Symbol currency", Description = "symbolcurrency")]
        public string symbolcurrency { get; set; }

        [Display(Name = "Symbol currency digit group", Description = "symbolcurrency_digitgroup")]
        public string symbolcurrency_digitgroup { get; set; }

        [Display(Name = "Symbol currency digit decimal", Description = "symbolcurrency_digitdecimal")]
        public string symbolcurrency_digitdecimal { get; set; }

        [Display(Name = "Number currency digit decimal", Description = "numbercurrency_digitdecimal")]
        public int numbercurrency_digitdecimal { get; set; }

        [Display(Name = "Symbol number digit group", Description = "symbolnumber_digitgroup")]
        public string symbolnumber_digitgroup { get; set; }

        [Display(Name = "Symbol number digit decimal", Description = "symbolnumber_digitdecimal")]
        public string symbolnumber_digitdecimal { get; set; }

        [Display(Name = "Number digit decimal", Description = "number_digitdecimal")]
        public int number_digitdecimal { get; set; }

        [Display(Name = "Projection", Description = "projection")]
        public string projection { get; set; }

        [Display(Name = "Unit scale", Description = "unit_scale")]
        public string unit_scale { get; set; }

        [Display(Name = "X1", Description = "x1")]
        public double? extent_map_x1 { get; set; }

        [Display(Name = "X2", Description = "x2")]
        public double? extent_map_x2 { get; set; }

        [Display(Name = "Y1", Description = "y1")]
        public double? extent_map_y1 { get; set; }

        [Display(Name = "Y2", Description = "y2")]
        public double? extent_map_y2 { get; set; }

        [Display(Name = "max_size_file", Description = "Max file size")]
        public long? max_size_file { get; set; }

        [Display(Name = "server_pop", Description = "Server Pop")]
        public string email_server_pop { get; set; }

        [Display(Name = "gate", Description = "Gate")]
        public int? email_gate { get; set; }

        [Display(Name = "email", Description = "Email")]
        public string email_address { get; set; }

        [Display(Name = "password", Description = "Password")]
        public string email_password { get; set; }

        [Display(Name = "email_passwordencrypted", Description = "Email Passwordencrypted")]
        public bool? email_passwordencrypted { get; set; }

        [Display(Name = "ssl", Description = "SSL")]
        public bool? ssl { get; set; }

        [Display(Name = "use_ldap", Description = "Use LDAP")]
        public bool? use_ldap { get; set; }

        [Display(Name = "domain_ldap", Description = "Domain LDAP")]
        public string domain_ldap { get; set; }

    }
}
