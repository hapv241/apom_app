﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Administrative
{
    [Table("u_role", Schema = "public")]
    public class role
    {
        public role()
        {
        }

        [Key]
        [Required]
        public long oid { get; set; }

        [MaxLength(50)]
        public string roleid { get; set; }

        [MaxLength(50)]
        public string name { get; set; }

        [MaxLength(255)]
        public string title { get; set; }

        [MaxLength(500)]
        public string description { get; set; }

        [MaxLength(50)]
        public string userid { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
        public bool? rolesystem { get; set; }
        public bool? active { get; set; }
    }
}
