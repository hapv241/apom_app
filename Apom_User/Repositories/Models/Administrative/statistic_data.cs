﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models.Administrative
{
    public class statistic_data
    {
        public long no { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public DateTime? datetime_shooting { get; set; }
        public double val { get; set; }
        public double avg_val { get; set; }
    }

    public class administrative_info
    {
        public string province_id { get; set; }
        public string province_name { get; set; }
        public string district_id { get; set; }
        public string district_name { get; set; }
        public string commune_id { get; set; }
        public string commune_name { get; set; }
    }
}
