﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Administrative
{
    [Table("u_user_role", Schema = "public")]
    public class user_role
    {
        public user_role()
        {
        }

        [Key]
        [Required]
        public long oid { get; set; }

        [MaxLength(50)]
        public string roleid { get; set; }

        [MaxLength(50)]
        public string userid { get; set; }
        public DateTime? dateupdate { get; set; }
        public bool? isroot { get; set; }
    }

    public class user_role_view
    {

        public string roleid { get; set; }

        public string rolename { get; set; }


        public string userid { get; set; }
        public bool? isroot { get; set; }
    }
}
