﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models
{
    public class basemodel
    {
        public string msg_success { get; set; }
        public string msg_error { get; set; }
        public string msg_info { get; set; }
    }
}
