﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models
{
    public class map_model_view
    {
        public List<Category.map_scale> MapScale { get; set; }
        public List<Category.vnaqi_index_short> VNAQIIndex { get; set; }
        public List<Asset.group_short> Layers { get; set; }
        public List<Asset.component_geotiff_service_short> Services { get; set; }

        public List<Config.pinned_location> PinnedLocation { get; set; }
        public bool isVietNam { get; set; } = false;
        public bool isAuthenticate { get; set; } = false;
    }

    public class analysis_model_view
    {
        public List<Asset.group_tree> TreeGroup { get; set; }
        public List<Category.province> Provinces { get; set; }
        public List<Category.vnaqi_index_short> VNAQIIndex { get; set; }
        public bool isVietNam { get; set; } = false;
        public bool isAuthenticate { get; set; } = false;

    }
}
