﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models.Asset
{
    [Table("a_component_geotiff_daily", Schema = "public")]
    public class component_geotiff_daily
    {
        [Key]
        [MaxLength(50)]
        public string category_id { get; set; }
        [MaxLength(50)]
        public string groupcomponent_id { get; set; }
        public DateTime? date_shooting { get; set; }
        [MaxLength(100)]
        public string tablename { get; set; }
        [MaxLength(100)]
        public string tableindex { get; set; }
        [MaxLength(100)]
        public string filename { get; set; }
        public double? min_x { get; set; }
        public double? min_y { get; set; }
        public double? max_x { get; set; }
        public double? max_y { get; set; }
        public double? resolution_x { get; set; }
        public double? resolution_y { get; set; }        
        [MaxLength(50)]
        public string projectioncode { get; set; }
        
        [MaxLength(50)]
        public string user_id { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
    }

    public class component_geotiff_daily_identify
    {
        public bool geotiff {
            get
            {
                return true;
            }
        }
        public string gcid { get; set; }
        public string requestdate { get; set; }
        public string titlegroup { set; get; }
        public string titlecomponent { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public double? val { get; set; }
        public double? val_aqi { get; set; }
    }

    public class component_geotiff_daily_ranking
    {
        // truong serial
        public long no { get; set; }
        public string category_id { get; set; }
        public string administrative_id { get; set; }
        public string administrative_name { set; get; }
        public double? avg { get; set; }
        public double? avg_pre { get; set; }
        public string date_shooting { get; set; }
    }
}
