﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models.Asset
{
    [Table("a_component_geotiff_service", Schema = "public")]
    public class component_geotiff_service
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long oid { get; set; }
        [MaxLength(50)]
        public string groupcomponent_id { get; set; }
        [MaxLength(255)]
        public string urlservice { get; set; }
        [MaxLength(100)]
        public string featuretype { get; set; }
        [MaxLength(100)]
        public string featurebase { get; set; }
        public bool? timeslider { get; set; }
        [MaxLength(10)]
        public string typeslider { get; set; }
        public bool? visible { get; set; }
        [MaxLength(10)]
        public string type { get; set; }        
        public int? orderservice { get; set; }
        
        [MaxLength(50)]
        public string user_id { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
    }

    public class component_geotiff_service_short
    {
        public long oid { get; set; }
        public string groupcomponent_id { get; set; }
        public string group_id { get; set; }
        public string parent_id { get; set; }
        public string component_id { get; set; }
        public string urlservice { get; set; }
        public string featuretype { get; set; }
        public string featurebase { get; set; }
        public bool? timeslider { get; set; }
        public string typeslider { get; set; }
        public bool? visible { get; set; }
        public string type { get; set; }
        public int? orderservice { get; set; }
        public string title { get; set; }
        public string classBtnTriggerSlider { get; set; }
        public string classDivTimeSlider { get; set; }
        public string inputTimeSlider { get; set; }
        public string btnRunTimeSlider { get; set; }
        public string btnBackwardTimeSlider { get; set; }
        public string btnForwardTimeSlider { get; set; }
        public string spanCollapseTimeSlider { get; set; }
    }
}
