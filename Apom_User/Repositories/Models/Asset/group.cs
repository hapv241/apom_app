﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Repositories.Models.Asset
{
    [Table("a_group", Schema = "public")]
    public class group
    {
        [Key]
        [MaxLength(50)]
        public string id { get; set; }
        [MaxLength(255)]
        public string name { get; set; }
        [MaxLength(255)]
        public string name_en { get; set; }
        [MaxLength(500)]
        public string des { get; set; }
        [MaxLength(500)]
        public string des_en { get; set; }
        [MaxLength(50)]
        public string parent_id { get; set; }
        [MaxLength(50)]
        public string icon { get; set; }        
        public bool? isgroup { get; set; }
        public bool? isstation { get; set; }
        public bool? isstation_weather { get; set; }
        public bool? islayer_model { get; set; }
        public bool? isaqi { get; set; }
        public bool? isactive { get; set; }

        [MaxLength(50)]
        public string user_id { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
    }

    public class group_short
    {
        public string id { get; set; }
        public string name { get; set; }
        public string des { get; set; }
        public string icon { get; set; }
        public string parent_id { get; set; }
        /// <summary>
        /// 1: station_airpolution
        /// 2: station_weather
        /// 3: layer_model
        /// </summary>
        public int? type_station { get; set; }
        public bool? isstation { get; set; }
        public bool? isaqi { get; set; }
    }

    public class group_detail_info
    {
        public string id { get; set; }
        public string name { get; set; }
        public string des { get; set; }
        public string icon { get; set; }
        public string parent_id { get; set; }
        /// <summary>
        /// 1: station_airpolution
        /// 2: station_weather
        /// 3: layer_model
        /// </summary>
        public int? type_station { get; set; }
        public bool? isstation { get; set; }
        public bool? isaqi { get; set; }
    }

    public class group_tree
    {
        public string id { get; set; }
        public string name { get; set; }
        public string parent_id { get; set; }
        public string parent_name { get; set; }
    }
}
