﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models.Asset
{
    [Table("a_component_station_daily", Schema = "public")]
    public class component_station_daily
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long oid { get; set; }
        [MaxLength(50)]
        public string group_id { get; set; }
        public DateTime? datetime_shooting { get; set; }
        public string data_shooting { get; set; }

        #region nong do
        public double? pm25 { get; set; }
        public double? pm10 { get; set; }
        public double? no2 { get; set; }
        public double? co { get; set; }
        public double? so2 { get; set; }
        public double? o3 { get; set; }
        public double? altm { get; set; }
        public double? temp { get; set; }
        public double? hud { get; set; }
        public double? wdir { get; set; }
        public double? wspd { get; set; }
        public double? vis { get; set; }
        #endregion

        #region aqi
        public double? aqi_pm25 { get; set; }
        public double? aqi_pm10 { get; set; }
        public double? aqi_no2 { get; set; }
        public double? aqi_co { get; set; }
        public double? aqi_so2 { get; set; }
        public double? aqi_o3 { get; set; }
        public double? aqi_altm { get; set; }
        public double? aqi_temp { get; set; }
        public double? aqi_hud { get; set; }
        public double? aqi_wdir { get; set; }
        public double? aqi_wspd { get; set; }
        public double? aqi_vis { get; set; }
        public double? aqi { get; set; }
        #endregion

        [MaxLength(50)]
        public string user_id { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
    }

    public class component_station_daily_short
    {
        public string group_id { get; set; }
        public string group_name { get; set; }
        public string group_des { get; set; }
        public string group_parent_id { get; set; }
        public string group_parent_name { get; set; }
        public string group_parent_des { get; set; }
        public double? comp_val { get; set; }
        public double? comp_aqi { get; set; }
        public DateTime? datetime_shooting { get; set; }
        public int aqi_status
        {
            get
            {
                if (comp_aqi != null)
                {
                    /*
                     0: [0,50)
                     1: [50,100)
                     2: [100,150)
                     3: [150,200)
                     4: [200,300)
                     5: [300, n)
                    */
                    return comp_aqi < 50 ? 0 : (comp_aqi < 100 ? 1 : (comp_aqi < 150 ? 2 : (comp_aqi < 200 ? 3 : (comp_aqi < 300 ? 4 : 5))));
                }
                return -1;
            }
        }

        public group_short group { get; set; }
    }

    public class component_station_daily_aqi
    {
        public string group_id { get; set; }
        public string group_name { get; set; }
        public string component_id { get; set; }
        public string component_name { get; set; }
        public double? aqi_value { get; set; }
        public DateTime? datetime_shooting { get; set; }
    }
}
