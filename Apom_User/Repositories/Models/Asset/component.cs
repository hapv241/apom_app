﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models.Asset
{
    [Table("a_component", Schema = "public")]
    public class component
    {
        [Key]
        [MaxLength(50)]
        public string id { get; set; }
        [MaxLength(255)]
        public string name { get; set; }
        [MaxLength(255)]
        public string name_en { get; set; }
        [MaxLength(500)]
        public string des { get; set; }
        [MaxLength(500)]
        public string des_en { get; set; }
        [MaxLength(50)]
        public string icon { get; set; }
        [MaxLength(50)]
        public string legend_img { get; set; }
        [MaxLength(500)]
        public string legend_des { get; set; }
        [MaxLength(500)]
        public string legend_des_en { get; set; }

        [MaxLength(50)]
        public string user_id { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
    }
}
