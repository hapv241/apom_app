﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models.Asset
{
    [Table("a_group_component", Schema = "public")]
    public class group_component
    {
        [Key]
        [MaxLength(50)]
        public string id { get; set; }
        [MaxLength(50)]
        public string group_id { get; set; }
        [MaxLength(50)]
        public string component_id { get; set; }
        public bool? isshow { get; set; }
        public bool? isactive { get; set; }
        public int? ordercomponent { get; set; }

        [MaxLength(50)]
        public string user_id { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? modifydate { get; set; }
    }

    public class group_component_short
    {
        public string group_id { get; set; }
        public string component_id { get; set; }
        public int? ordercomponent { get; set; }
    }
}
