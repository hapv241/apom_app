﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models
{
    public class NotificationModel<T>
    {
        public string title { get; set; }
        public List<T> lMessage { get; set; }
    }

    public class MsgModel: NotificationModel<MsgShowModel>
    {
    }

    public class ErrorModel: NotificationModel<ErrorShowModel>
    {
    }

    public class MsgShowModel
    {
        public string msgPath { get; set; }
        public string msgMessage { get; set; }
    }

    public class ErrorShowModel
    {
        public string errorPath { get; set; }
        public string errorMessage { get; set; }
    }
}
