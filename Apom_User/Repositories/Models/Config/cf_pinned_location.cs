﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Repositories.Models.Config
{
    [Table("cf_pinned_location", Schema = "public")]
    public class cf_pinned_location
    {
        [Required]
        public long oid { get; set; }

        [MaxLength(50)]
        public string province_id { get; set; }

        [MaxLength(50)]
        public string user_id { get; set; }
    }

    [Table("cf_pinned_location_district", Schema = "public")]
    public class cf_pinned_location_district
    {
        [Required]
        public long oid { get; set; }

        [MaxLength(50)]
        public string district_id { get; set; }

        [MaxLength(50)]
        public string user_id { get; set; }
    }

    public class pinned_location
    {

        [MaxLength(50)]
        public string province_id { get; set; }
        
        public string province_name { get; set; }

        public double? aqi { get; set; }
    }
}
