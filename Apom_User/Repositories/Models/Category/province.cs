﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Category
{
    [Table("v_administrative_province", Schema = "public")]
    public class province
    {
        //gid, province_id, district_id, commune_id, name_vi as name, name_en, type_vi as type, type_en        
        [Required]
        public long gid { get; set; }

        [MaxLength(50)]
        public string province_id { get; set; }

        [MaxLength(150)]
        public string name { get; set; }

        [MaxLength(150)]
        public string name_en { get; set; }

        [MaxLength(50)]
        public string type { get; set; }

        [MaxLength(50)]
        public string type_en { get; set; }
    }

    public class extent
    {
        public string id { get; set; }
        public string name { get; set; }
        public double minx { get; set; }
        public double miny { get; set; }
        public double maxx { get; set; }
        public double maxy { get; set; }
    }
    public class administrative_base
    {
        [MaxLength(50)]
        public string id { get; set; }

        [MaxLength(150)]
        public string name { get; set; }

        [MaxLength(50)]
        public string type { get; set; }
    }
}
