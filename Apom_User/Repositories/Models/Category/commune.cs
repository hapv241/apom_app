﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Category
{
    [Table("v_administrative_commune", Schema = "public")]
    public class commune
    {
        //gid, province_id, district_id, commune_id, name_vi as name, name_en, type_vi as type, type_en        
        [Required]
        public long gid { get; set; }

        [MaxLength(50)]
        public string province_id { get; set; }

        [MaxLength(50)]
        public string district_id { get; set; }

        [MaxLength(50)]
        public string commune_id { get; set; }

        [MaxLength(150)]
        public string name { get; set; }

        [MaxLength(150)]
        public string name_en { get; set; }

        [MaxLength(50)]
        public string type { get; set; }

        [MaxLength(50)]
        public string type_en { get; set; }
    }
}
