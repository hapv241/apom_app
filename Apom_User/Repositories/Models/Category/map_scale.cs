﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Repositories.Models.Category
{
    [Table("c_map_scale", Schema = "public")]
    public class map_scale
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long oid { get; set; }
        public double? scale_val { get; set; }
        [MaxLength(50)]
        public string scale_alias { get; set; }
    }
}
