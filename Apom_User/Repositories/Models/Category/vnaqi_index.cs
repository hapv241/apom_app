﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Repositories.Models.Category
{
    [Table("c_vnaqi_index", Schema = "public")]
    public class vnaqi_index
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long oid { get; set; }
        public double? aqi_from { get; set; }
        public double? aqi_to { get; set; }
        public double? aqi_pm25_from { get; set; }
        public double? aqi_pm25_to { get; set; }
        [MaxLength(50)]
        public string aqi_statusname { get; set; }
        [MaxLength(50)]
        public string aqi_statusname_en { get; set; }
        [MaxLength(500)]
        public string aqi_des { get; set; }
        [MaxLength(500)]
        public string aqi_des_en { get; set; }
        [MaxLength(50)]
        public string bg_color { get; set; }
        [MaxLength(50)]
        public string font_color { get; set; }

    }

    public class vnaqi_index_short
    {
        public long oid { get; set; }
        public double? from { get; set; }
        public double? to { get; set; }
        public double? pm25_from { get; set; }
        public double? pm25_to { get; set; }
        public string statusname { get; set; }
        public string status { get; set; }
        public string des { get; set; }
        public string bg_color { get; set; }
        public string font_color { get; set; }
    }
}
