﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models
{
    public class jsonResponse
    {
        public static int CODE_OK = 0;
        public static int CODE_ERROR = -1;

        public string Message { get; set; }
        //public int Code { get; set; }
        public object Data { get; set; }
        public object Error { get; set; }
        public System.Net.HttpStatusCode Code { get; set; }
    }
    
    public class JsonRequest
    {
        public string paraI { get; set; }
        public string time_request { get; set; }
        public string data { get; set; }
    }
}
