﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Models.Dashboard
{
    public class statistic_region_count_aqi
    {
        /// <summary>
        /// so quan/huyen ko o nhiem
        /// </summary>
        public int unpolluted { get; set; } = 0;

        /// <summary>
        /// so quan/huyen o nhiem
        /// </summary>
        public int polluted { get; set; } = 0;
        
        /// <summary>
        /// so ngay o nhiem
        /// </summary>
        public int polluted_days { get; set; } = 0;
    }

    public class statistic_region_count_aqi_detail
    {
        public int unpolluted { get; set; } = 0;
        public int medium_pollution { get; set; } = 0;
        public int very_polluted { get; set; } = 0;
        public int extremely_polluted { get; set; } = 0;
    }

    public class statistic_region_aqi
    {
        public string type { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public double val { get; set; }
    }

    public class statistic_region_aqi_chart
    {
        public double val { get; set; }
        public DateTime dateshooting { get; set; }
    }
}
