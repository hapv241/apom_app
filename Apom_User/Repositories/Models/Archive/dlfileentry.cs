﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Archive
{
    [Table("a_dlfileentry", Schema = "public")]
    public class dlfileentry
    {
        public dlfileentry()
        {
        }

        [Key]
        [Required]
        [Display(Name = "oid", Description = "ObjectID")]
        public long oid { get; set; }

        [Display(Name = "fileentryid", Description = "fileentryid")]
        public string fileentryid { get; set; }

        [Display(Name = "repositoryid", Description = "repositoryid")]
        public string repositoryid { get; set; }

        [Display(Name = "folderid", Description = "folderid")]
        public string folderid { get; set; }

        [Display(Name = "treepath", Description = "treepath")]
        public string treepath { get; set; }

        [Display(Name = "name", Description = "name")]
        public string name { get; set; }

        [Display(Name = "extension", Description = "extension")]
        public string extension { get; set; }

        [Display(Name = "mimetype", Description = "mimetype")]
        public string mimetype { get; set; }

        [Display(Name = "title", Description = "title")]
        public string title { get; set; }

        [Display(Name = "size", Description = "size")]
        public double size { get; set; }

        [Display(Name = "version", Description = "version")]
        public string version { get; set; }

        [Display(Name = "classname", Description = "classname")]
        public string classname { get; set; }

        [Display(Name = "classpk", Description = "classpk")]
        public string classpk { get; set; }

        [Display(Name = "host", Description = "host")]
        public string host { get; set; }

        [Display(Name = "userid", Description = "userid")]
        public string userid { get; set; }

        [Display(Name = "createdate", Description = "createdate")]
        public DateTime? createdate { get; set; }

        [Display(Name = "modifydate", Description = "modifydate")]
        public DateTime? modifydate { get; set; }


    }
    public class title
    {
        public string tab_id { get; set; }
        public string href { get; set; }
        public string name { get; set; }
        public string title_file { get; set; }
    }

    public class fileEntryInfo
    {
        public string fileentryid { get; set; }
        /// <summary>
        /// base64 || treepath_filename
        /// </summary>
        public string img_content { get; set; }
        public System.Drawing.Bitmap bmp { get; set; }
        public byte[] bytes { get; set; }
    }
}
