﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Models.Archive
{
    [Table("a_dlfolder", Schema = "public")]
    public class dlfolder
    {
        public dlfolder()
        {
        }

        [Key]
        [Required]
        [Display(Name = "oid", Description = "ObjectID")]
        public long oid { get; set; }

        [Display(Name = "folderid", Description = "folderid")]
        public string folderid { get; set; }

        [Display(Name = "parentfolderid", Description = "parentfolderid")]
        public string parentfolderid { get; set; }

        [Display(Name = "treepath", Description = "treepath")]
        public string treepath { get; set; }

        [Display(Name = "repositoryid", Description = "repositoryid")]
        public string repositoryid { get; set; }

        [Display(Name = "name", Description = "name")]
        public string name { get; set; }

        [Display(Name = "description", Description = "description")]
        public string description { get; set; }

        [Display(Name = "userid", Description = "userid")]
        public string userid { get; set; }

        [Display(Name = "createdate", Description = "createdate")]
        public DateTime? createdate { get; set; }

        [Display(Name = "modifydate", Description = "modifydate")]
        public DateTime? modifydate { get; set; }
    }
}
