﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class Constant
    {
        /// <summary>
        /// default: vi
        /// </summary>
        public static string LANGUAGE_DEFAULT = "vi"; // default: vietnamese
        public static string REPOSITORY_ROOT = "/FileStore";

        public class Member
        {
            public string val { get; set; }
            public string text { get; set; }
        }

        public static class Cache
        {
            // minute
            private static int EXPRIRATION_SHORT = 1440; // 1 day
            private static int EXPRIRATION_MEDIUM = 10080; // 1 week
            private static int EXPRIRATION_LONG = 40320; // 4 week
            private static int EXPRIRATION_VERY_LONG = 161280; // 16 week

            public static Member FILE_ENTRY = new Member
            {
                text = "FILE_ENTRY",
                val = EXPRIRATION_LONG.ToString(),
            };

            public static Member FILE_ENTRY_INFO = new Member
            {
                text = "FILE_ENTRY_INFO",
                val = EXPRIRATION_LONG.ToString(),
            };

            public static Member USER_INFO = new Member
            {
                text = "USER_INFO",
                val = EXPRIRATION_LONG.ToString(),
            };

            public static Member USERS = new Member
            {
                text = "USERS",
                val = EXPRIRATION_LONG.ToString(),
            };


            public static Member RANKING_TABLE_LIST = new Member
            {
                text = "RANKING_TABLE_LIST",
                val = EXPRIRATION_MEDIUM.ToString(),
            };

            public static Member LIST_VAL_IDENTIFY_ADMINISTRATIVE = new Member
            {
                text = "LIST_VAL_IDENTIFY_PROVINCE",
                val = EXPRIRATION_VERY_LONG.ToString(),
            };


            public static Member PINNED_LOCATION_DEFAULT_LIST = new Member
            {
                text = "PINNED_LOCATION_DEFAULT_LIST",
                val = EXPRIRATION_MEDIUM.ToString(),
            };


            public static Member PINNED_LOCATION_LIST = new Member
            {
                text = "PINNED_LOCATION_LIST",
                val = EXPRIRATION_MEDIUM.ToString(),
            };


            public static Member ANALYSIS_STATISTIC_AQI = new Member
            {
                text = "ANALYSIS_STATISTIC_AQI",
                val = EXPRIRATION_LONG.ToString(),
            };

            public static Member PROVINCES = new Member
            {
                text = "PROVINCES",
                val = EXPRIRATION_LONG.ToString(),
            };

            public static Member DISTRICTS = new Member
            {
                text = "DISTRICTS",
                val = EXPRIRATION_LONG.ToString(),
            };
        }
    }
}
