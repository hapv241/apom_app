﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class HttpStatusCode
    {
        /// <summary>
        /// 200
        /// </summary>
        public static int SUCCESS = 200;

        /// <summary>
        /// 400
        /// </summary>
        public static int BAD_REQUEST = 400;

        /// <summary>
        /// 401
        /// </summary>
        public static int UNAUTHENTICATION = 401;

        /// <summary>
        /// 403
        /// </summary>
        public static int UNAUTHORIZATION = 403;

        /// <summary>
        /// 404
        /// </summary>
        public static int NOTFOUND = 404;

        /// <summary>
        /// 500
        /// </summary>
        public static int ERROR = 500;
    }
}
