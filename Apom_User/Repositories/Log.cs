﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Repositories
{
    public class Log
    {
        private static string getFileNameLog(DateTime dateLog)
        {
            string fileName = "apom.api." + dateLog.Year.ToString() + "." + dateLog.Month.ToString() + "." + dateLog.Day + ".log";
            return fileName;
        }

        public static void writeLog(string msg)
        {
            string fileName = @"C:\APOM_APP\Logs" + "\\" + getFileNameLog(DateTime.Now);
            if (!File.Exists(fileName))
            {
                // auto refresh object
            }
            try
            {
                StreamWriter sw = File.AppendText(fileName);
                sw.WriteLine(msg);
                sw.Flush();
                sw.Close();
            }
            catch { }
        }
    }
}
