﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;

using Libs.Utils;
using Repositories.Models;
using Repositories.Models.Administrative;
using Repositories.Services.Administrative;

namespace Apom_User.Common.ActionFilter
{
    public class CustomAdminActionFilter : CustomActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            HttpContextBase httpContext = filterContext.Controller.ControllerContext.RequestContext.HttpContext;

            //string requestQueryString = httpContext.Request.Form["data"];
            //requestQueryString = requestQueryString.Substring(1);
            //requestQueryString = requestQueryString.Remove(requestQueryString.Length - 1);
            //JsonRequest deserializedRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<JsonRequest>(requestQueryString);
            //string paraI = deserializedRequest.paraI;
            //string time_request = deserializedRequest.time_request;
            string email = "";
            try
            {
                email = SessionContext.UserProfile.emailaddress; //Encrypts.Decrypt(String.Concat(time_request, "_", Constant.WebConfig.KEYENDE), paraI);
                user _user = userLocalServiceUtil.Instance.getUserbyEmailPwd(email, null);
                if (_user == null)
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                        { "controller", "Bug" },
                        { "action", "Error400" },
                        { "area", "" }
                        }
                    );
                }
                else
                {
                    bool isRoleAdmin = userroleLocalServiceUtil.Instance.isAdmin(_user.userid);       
                    if (!isRoleAdmin)
                    {
                        filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary
                            {
                                { "controller", "Bug" },
                                { "action", "Error403" },
                                { "area", "" }
                            }
                        );
                    }
                }
            }
            catch(Exception ex)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                    { "controller", "Bug" },
                    { "action", "Error403" },
                    { "area", "" }
                    }
                );
            }
        }
    }
}