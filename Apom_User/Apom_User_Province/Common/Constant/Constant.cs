﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

using Repositories.Models.Administrative;
using Repositories.Services.Administrative;

namespace Apom_User.Common
{
    public class Constant
    {
        public static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string LANGUAGE_DEFAULT = "vi"; // default: vietnamese {vi, en}

        private static general_setting _general_setting = null;
        public static general_setting GENERAL_SETTING
        {
            get
            {
                if (_general_setting == null)
                {
                    List<general_setting> lst = generalSettingLocalServiceUtil.Instance.getAll();
                    _general_setting= lst.Count > 0 ? lst[0] : new general_setting();
                    return _general_setting;
                }
                return _general_setting;
            }
        }

        public enum Page
        {
            home,
            data_analysis,
            news,
            about,
            setting,
            user_profile,
            user_list
        }

        public static class Theme
        {
            public static string APOM = "apom";
            public static string STACK = "stack";
        }

        public static class APIs
        {
            public static string administrative_province_districts = "/api/administrative/administrative_province_district";
            public static string administrative_districts = "/api/administrative/districts";
            public static string administrative_province_extent = "/api/administrative/province_extent";
            public static string administrative_district_extent = "/api/administrative/district_extent";
            public static string administrative_statistic = "/api/administrative/statistic";
            public static string administrative_info_by_coor = "/api/administrative/info_by_coor";

            public static string notification_SendNotification = "/api/notification/SendNotification";

            public static string compstation_daily = "/api/compstation/daily";

            public static string groupcomp_compbygroup = "/api/groupcomp/compbygroup";

            public static string station_daily_short = "/api/station/daily_short";
            public static string station_daily_short_newest = "/api/station/daily_short_newest";

            public static string componentgeotiffservice_getbyserviceinfo = "/api/componentgeotiffservice/getbyserviceinfo";

            public static string analysis_district_statistic = "/api/analysis/district_statistic";
            public static string analysis_province_daily_statistic = "/api/analysis/province_daily_statistic";
            public static string analysis_district_daily_statistic = "/api/analysis/district_daily_statistic";
            public static string analysis_district_avg_statistic = "/api/analysis/district_avg_statistic";

            public static string componentgeotiffdaily_ranking = "/api/componentgeotiffdaily/ranking";
            public static string componentgeotiffdaily_rankingvn = "/api/componentgeotiffdaily/rankingvn";
            public static string componentgeotiffdaily_rankingprovince = "/api/componentgeotiffdaily/rankingprovince";
            public static string componentgeotiffdaily_identify_geotiff = "/api/componentgeotiffdaily/identify_geotiff";
            public static string componentgeotiffdaily_identify_list_geotiff = "/api/componentgeotiffdaily/identify_list_geotiff";
            // by coorx, coory
            public static string componentgeotiffdaily_identify_province_list_geotiff = "/api/componentgeotiffdaily/identify_province_list_geotiff";
            // by province_id
            public static string componentgeotiffdaily_identify_province_id_list_geotiff = "/api/componentgeotiffdaily/identify_province_id_list_geotiff";
            // by district_id
            public static string componentgeotiffdaily_identify_distict_id_list_geotiff = "/api/componentgeotiffdaily/identify_district_id_list_geotiff";

            public static string group_info = "/api/group/info";

            public static string pinnedlocation_update = "/api/pinnedlocation/update";
            
            public static string pinnedlocation_default = "/api/pinnedlocation/default";

            //public static string a = "";
            //public static string a = "";

            public class Messages
            {
                public static string USER_INVALID_DATA = "Thông tin người dùng không hợp lệ";
                public static string USER_INVALID_DATA_EN = "Thông tin người dùng không hợp lệ";

                public static string USER_INVALID_AUTHENTICATION = "Thông tin đăng nhập không chính xác";

                public static string TOKEN_MISSING = "Không tồn tại thông tin xác thực";
                public static string TOKEN_INVALID_AUTHORIZED = "Thông tin xác thực không chính xác";
            }

            #region claim in token
            public static string CLAIM_TYPES_UID = "uid";
            public static string CLAIM_TYPES_PWD = "password";
            public static string CLAIM_TYPES_LANG = "language";
            #endregion
        }

        public static class WebConfig
        {
            public static string LANGUAGE = "language";

            #region cookie
            public static string COOKIE_LOGIN = "login";
            public static string COOKIE_URL = "login";
            public static string COOKIE_HUB = "apom_hub_user";

            internal static double COOKIE_TIMEOUT = 14;// nhớ 2 tuần

            public static int TIMEOUT = 30;
            #endregion

            #region token
            static string authToken = WebConfigurationManager.AppSettings["auth_token"];
            public static string ACCESS_TOKEN = "access_token";
            public static bool AUTH_TOKEN = String.IsNullOrEmpty(authToken) ? false : (authToken.Trim() == "0" ? false : true);
            public static string SECRET_TOKEN = WebConfigurationManager.AppSettings["secret_token"];
            #endregion

            #region repository
            public static string REPOSITORY_ASSET = "/Asset";
            public static string REPOSITORY_DOCUMENT = "/Document";
            public static string REPOSITORY_REPORT = "/Report";
            public static string REPOSITORY_HELP = "/Help";
            #endregion


            #region resize img info
            public static double PERCENTAGE_RESIZE_IMG = WebConfigurationManager.AppSettings["percentage_resize_img"] == null ? 0.5 : double.Parse(WebConfigurationManager.AppSettings["percentage_resize_img"].ToString());
            public static double RESIZE_IMG_WIDTH = WebConfigurationManager.AppSettings["resize_img_width"] == null ? 0.5 : double.Parse(WebConfigurationManager.AppSettings["resize_img_width"].ToString());
            public static double RESIZE_IMG_HEIGHT = WebConfigurationManager.AppSettings["resize_img_height"] == null ? 0.5 : double.Parse(WebConfigurationManager.AppSettings["resize_img_height"].ToString());
            public static string RESIZE_IMG_TYPE = WebConfigurationManager.AppSettings["resize_img_type"];
            public static int SIZE_IMG_AVATAR = WebConfigurationManager.AppSettings["size_img_avatar_max"] == null ? 1024 : int.Parse(WebConfigurationManager.AppSettings["size_img_avatar_max"].ToString());
            
            #endregion

            #region ftp info
            public static string FTP_IP = WebConfigurationManager.AppSettings["ftpServerIP"];
            public static string FTP_UID = Libs.Utils.Encrypts.Decrypt(Common.Constant.WebConfig.KEYENDE, WebConfigurationManager.AppSettings["ftpUserID"]);
            public static string FTP_PWD = Libs.Utils.Encrypts.Decrypt(Common.Constant.WebConfig.KEYENDE, WebConfigurationManager.AppSettings["ftpPassword"]);
            #endregion

            #region web.config
            public static int PWD_LENGTH_MIN = GET_CONFIG("pwd_length_min");
            public static int PWD_LENGTH_MAX = GET_CONFIG("pwd_length_max");

            #region supper admin
            public static string SEMAIL = WebConfigurationManager.AppSettings["semail"];
            public static string SNAME = WebConfigurationManager.AppSettings["sname"];
            public static string SPWD = WebConfigurationManager.AppSettings["spwd"];
            #endregion

            public static int EXPIRE_REGISTER_ACCOUNT = GET_CONFIG("expire_register_account");
            public static int PORT_ADMIN = GET_CONFIG("port_admin");

            public static string KEYENDE = WebConfigurationManager.AppSettings["KeyEnDe"];
            public static int NUMBER_IN_PAGE = GET_CONFIG("numberInPage");
            public static string PROVINCE_DEFAULT = WebConfigurationManager.AppSettings["province_default"] ?? "";
            public static string HOST_API = WebConfigurationManager.AppSettings["host_api"];
            public static string HOST_GEOSERVER = WebConfigurationManager.AppSettings["host_geoserver"];

            #region get all language
            public static List<string> LIST_LANGUAGE = WebConfigurationManager.AppSettings["language"].Split(',').ToList();
            #endregion

            #region notification
            public static int NOTI_LIMIT_NO_RECORD = GET_CONFIG("notification_limit_no_record");
            private static double d = NUMBER_IN_PAGE == 0 ? 0 : Convert.ToDouble(NOTI_LIMIT_NO_RECORD) / NUMBER_IN_PAGE;
            public static int NOTI_LIMIT_PAGE_NUM = Convert.ToInt32(Math.Ceiling(d));

            #endregion
            public static int SCALE_SHOW_STATION_LABEL = GET_CONFIG("scale_show_station_label");
            
            #endregion

            private static int GET_CONFIG(string appName)
            {
                int value = 0;
                if (int.TryParse(WebConfigurationManager.AppSettings[appName].ToString(), out value))
                {
                    return value;
                }

                return value;

            }

            private static string _connectionStr = "";
            public static string CONNECTIONSTR
            {
                get
                {
                    if (String.IsNullOrEmpty(_connectionStr))
                    {
                        Models.DataApomAppConnection ApomAppConnection = new Models.DataApomAppConnection();
                        _connectionStr = ApomAppConnection.Database.Connection.ConnectionString;
                    }
                    return _connectionStr;
                }
            }
        }

        public static class SessionKey
        {
            public static string THEME_APOM = "THEME_APOM"; // default: empty || "apom", old theme: "stack"
            public static string SESSION_LANGUAGE = "SESSION_LANGUAGE";
            public static string SESSION_USER = "SESSION_USER";
            public static string SESSION_GENERAL_SETTING = "SESSION_GENERAL_SETTING";
        }

        public static class REGEXP
        {
            public static string special_character = "/[!@#$%^*?\"{}|<>'~`=+]/g";
            //tối thiểu tám ký tự, ít nhất một ký tự hoa, một ký tự viết thường, một số và một ký tự đặc biệt
            //minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
            public static string password = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})";
        }

        public class user_hub
        {
            public string user_id { get; set; }

            public string device_tokens { get; set; }

            private HashSet<string> _connectionIds;

            public HashSet<string> ConnectionIds
            {
                get
                {
                    if (!String.IsNullOrEmpty(this.device_tokens))
                    {
                        string[] arr = this.device_tokens.Split(';');
                        this._connectionIds = new HashSet<string>(arr);
                    }
                    else
                    {
                        this._connectionIds = new HashSet<string>();
                    }
                    return _connectionIds;
                }
                set
                {
                    this._connectionIds = value;
                    this.device_tokens = string.Join(";", this._connectionIds);
                }
            }
        }

        public static List<user_hub> USER_HUB = new List<user_hub>();
    }
}