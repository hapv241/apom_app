﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Linq.Expressions;
using System.Text;

namespace Apom_User.Common.Handler
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString RadioButtonForSelectList<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> listOfValues)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var sb = new StringBuilder();

            if (listOfValues != null)
            {
                // Create a radio button for each item in the list 
                foreach (SelectListItem item in listOfValues)
                {
                    // Generate an id to be given to the radio button field 
                    var id = string.Format("{0}_{1}", metaData.PropertyName, item.Value);

                    // Create and populate a radio button using the existing html helpers 
                    //var label = htmlHelper.Label(id, item.Text);

                    var attributes = new object();
                    if (item.Selected)
                        attributes = new { id = id, @checked = "checked" };
                    else
                        attributes = new { id = id };
                    var label = item.Text;
                    var radio = htmlHelper.RadioButtonFor(expression, item.Value, attributes).ToHtmlString();

                    // Create the html string that will be returned to the client 
                    // e.g. <input data-val="true" data-val-required="You must select an option" id="TestRadio_1" name="TestRadio" type="radio" value="1" /><label for="TestRadio_1">Line1</label> 
                    sb.AppendFormat("<div class=\"radio\"><label>{0}{1}</label></div>", radio, label);
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString CheckBoxList<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> listOfValues, string name)
        {
            var sb = new StringBuilder();

            if (listOfValues != null)
            {

                foreach (var item in listOfValues)
                {
                    var attributes = new object();
                    if (item.Selected)
                        attributes = new { id = item.Value, value = item.Value, @checked = "checked", text = name, @class = name };
                    else
                        attributes = new { id = item.Value, value = item.Value, @class = name, text = item.Text };

                    var label = item.Text;
                    var checkbox = htmlHelper.CheckBoxSimple(name, attributes).ToHtmlString();
                    sb.AppendFormat("<div class=\"checkbox\"><label>{0}{1}</label></div>", checkbox, label);
                }
            }
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString CheckBoxSimple(this HtmlHelper htmlHelper, string name, object htmlAttributes)
        {
            string checkBoxWithHidden = htmlHelper.CheckBox(name, htmlAttributes).ToHtmlString().Trim();
            string pureCheckBox = checkBoxWithHidden.Substring(0, checkBoxWithHidden.IndexOf("<input", 1));
            return new MvcHtmlString(pureCheckBox);
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string src, string altText, string height, string test)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", src);
            builder.MergeAttribute("alt", altText);
            builder.MergeAttribute("height", height);
            builder.MergeAttribute("object-fit", height);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }
    }
}