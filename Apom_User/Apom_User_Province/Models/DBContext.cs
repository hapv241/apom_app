﻿using System;
using System.Data.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using System.Text;

using System.Reflection;
using System.EnterpriseServices;
using System.Globalization;
using System.Web.Security;
using System.Data.Entity.ModelConfiguration.Conventions;

using Repositories.Models.Administrative;
using Repositories.Models.Asset;
using Repositories.Models.Archive;
using Repositories.Models.Notification;

namespace Apom_User.Models
{
    public class DataApomAppConnection : DbContext
    {
        public DataApomAppConnection() : base("DataApomAppConnection")
        {
        }
        public DbSet<general_setting> GeneralSetting { get; set; }
        public DbSet<user> User { get; set; }
        public DbSet<role> Role { get; set; }
        public DbSet<user_role> UserRole { get; set; }
        public DbSet<component> Component { get; set; }
        public DbSet<component_geotiff_service> ComponentGeoTiffService { get; set; }
        public DbSet<component_station_daily> ComponentStationDaily { get; set; }
        public DbSet<group> Group { get; set; }
        public DbSet<group_component> GroupComponent { get; set; }

        #region archive
        public DbSet<dlfolder> Folder { get; set; }
        public DbSet<dlfileversion> FileVersion { get; set; }
        public DbSet<dlfileentry> FileEntry { get; set; }
        #endregion

        #region notifycation
        public DbSet<notification> Notifycation { get; set; }
        public DbSet<notification_user> NotifycationUser { get; set; }
        #endregion
    }
}