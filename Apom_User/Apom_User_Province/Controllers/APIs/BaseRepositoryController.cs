﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Data.Entity;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using Apom_User.Common;

namespace Apom_User.Controllers.APIs
{
    public class BaseRepositoryController : ApiController
    {
        protected int page_num = 1;
        protected int page_size = Constant.WebConfig.NUMBER_IN_PAGE == 0 ? 10 : Constant.WebConfig.NUMBER_IN_PAGE;
        protected string user_id { get; set; }
        protected string user_name { get; set; }
        protected string displayname { get; set; }
        protected string email_address { get; set; }
    }
}