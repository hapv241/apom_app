﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Http.Cors;
using System.Text;
using Newtonsoft.Json;

using Repositories.Models;
using Repositories.Models.Config;
using Repositories.Models.Notification;
using Repositories.Services.Config;
using Repositories.Services.Notification;

using Apom_User.Common;
using Repositories.Services.Administrative;
using Repositories.Models.Administrative;
using System.Threading.Tasks;

namespace Apom_User.Controllers.APIs
{
    /// <summary>
    /// Làm việc với push notification
    /// </summary>
    public class notificationController : BaseRepositoryController
    {
        private async Task<List<notif_info>> LoadNotifDetailLatest(string userId, string langId, int page_num, int page_size = 0)
        {
            if (page_size == 0) page_size = Constant.WebConfig.NUMBER_IN_PAGE;
            List<notif_info> lst = await notificationuserLocalServiceUtil.Instance.getNotificationsIsReadAsync(userId, langId, page_num, page_size);
            return lst;
        }

        [TokenValidation]
        [HttpGet]
        public IHttpActionResult testToken()
        {
            try
            {
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.NotFound,
                    Message = "No data",
                    Data = null
                });
            }
            catch(Exception ex)
            {
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// lấy danh sách thông báo của người dùng đăng nhập
        /// </summary>
        /// <param name="page_num"></param>
        /// <returns></returns>
        //[TokenValidation]
        [HttpGet]
        //[Route("api/notification/get_notification")]
        public async Task<IHttpActionResult> getNotificationDetail(int page_num)
        {
            try
            {
                if (HttpContext.Current != null)
                {
                    var hubCookie = HttpContext.Current.Request.Cookies[Common.Constant.WebConfig.COOKIE_HUB];
                    if (!String.IsNullOrEmpty(hubCookie.Value))
                    {
                        string userId = hubCookie.Values[Common.Constant.WebConfig.COOKIE_HUB];
                        var langId = HttpContext.Current.Request.Cookies[Constant.WebConfig.LANGUAGE] != null ? HttpContext.Current.Request.Cookies[Constant.WebConfig.LANGUAGE].Value : "";

                        #region get nuber record at last page
                        int page_size = Constant.WebConfig.NUMBER_IN_PAGE;
                        int notify_limit_page_num = Constant.WebConfig.NOTI_LIMIT_PAGE_NUM;
                        int noRecordLastPage = page_size;

                        // is last page
                        if (page_num == notify_limit_page_num)
                        {
                            int notify_limit_no_record = Constant.WebConfig.NOTI_LIMIT_NO_RECORD;
                            noRecordLastPage = notify_limit_no_record - (notify_limit_page_num - 1) * page_size; // cause: notify_limit_page_num is Math.Ceiling
                        }
                        else if (page_num > notify_limit_page_num)
                        {
                            noRecordLastPage = -1;
                        }
                        #endregion
                        List<notif_info> lst = noRecordLastPage != -1 ? await this.LoadNotifDetailLatest(userId, langId, page_num, noRecordLastPage) : new List<notif_info>();
                        var response = new HttpResponseMessage(HttpStatusCode.OK);
                        response.Content = new StringContent(JsonConvert.SerializeObject(lst));
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        return Ok(new jsonResponse
                        {
                            Code = HttpStatusCode.OK,
                            Message = "",
                            Data = new { comps = lst }
                        });
                    }
                }

                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.NotFound,
                    Message = "No data",
                    Data = null
                });
            }
            catch (Exception ex)
            {
                return Ok(new jsonResponse
                {
                    Code = HttpStatusCode.BadRequest,
                    Message = ex.Message,
                    Data = null
                });
            }
        }

        /// <summary>
        /// gửi thông báo tới người dùng các chất (pm25, aqi, ...) mà người dùng quan tâm bị vượt ngưỡng
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        //[TokenValidation]
        [HttpPost]
        //[Route("api/notification/sendnotification")]
        //[ResponseType(typeof(IEnumerable<object>))]
        public async Task<HttpResponseMessage> SendNotification(List<notification> lst)
        {
            try
            {
                Apom_User.Models.DataApomAppConnection db = new Models.DataApomAppConnection();
                NotificationHub objNotifHub = new NotificationHub();
                //if(Common.SessionContext.UserProfile == null) Request.CreateResponse(HttpStatusCode.BadRequest);
                for (int i = 0; i < lst.Count; i++)
                {
                    notification obj = lst[i];

                    notification objNotif = new notification()
                    {
                        title_vi = obj.title_vi,
                        title_en = obj.title_en,
                        content_vi = obj.content_vi,
                        content_en = obj.content_en,
                        notification_type = obj.notification_type,
                        createdate = DateTime.Now,
                        province_id = obj.province_id,
                        notification_code = obj.notification_code,
                        notification_date = obj.notification_date
                    };

                    #region get user_ids from province in cf_pinned_location table
                    List<string> lstUserId = new List<string>();
                    List<cf_pinned_location> lstPinnedLocation = await cf_pinned_locationLocalServiceUtil.Instance.getByProvinceIdAsync(objNotif.province_id);
                    bool isSendNotify = false;
                    for (int j = 0; j < lstPinnedLocation.Count; j++)
                    {
                        if (!String.IsNullOrEmpty(lstPinnedLocation[j].user_id))
                        {
                            string uid = lstPinnedLocation[j].user_id;
                            // check user get notify?
                            user u =  await userLocalServiceUtil.Instance.getUserAsync(uid);
                            if (u != null && u.is_get_notify)
                            {
                                isSendNotify = true;
                                lstUserId.Add(lstPinnedLocation[j].user_id);
                            }
                        }
                    }
                    //lstUserId.Add("686fe50b-4f18-45ee-ab8f-bdb572e89b98");
                    //lstUserId.Add("7b848e70-7dd1-4e5b-ac41-dd3335f97e97");
                    #endregion
                    if (isSendNotify)
                    {
                        await notificationLocalServiceUtil.Instance.addAsync(db, objNotif);

                        for (int j = 0; j < lstUserId.Count; j++)
                        {
                            notification_user objNotifUser = new notification_user()
                            {
                                user_logged = lstUserId[j],
                                notification_id = objNotif.notification_id,
                                is_read = false,
                                is_delete = false,
                                is_reminder = true
                            };
                            await notificationuserLocalServiceUtil.Instance.addAsync(db, objNotifUser);

                            #region send notification
                            await objNotifHub.SendNotificationAsync(objNotifUser.user_logged);
                            #endregion
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error SendNotification: " + ex.ToString());
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }



    }
}