﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Apom_User.Models;
using Apom_User.Common;
using Common;

using System.Threading;

using Repositories.Models.Administrative;
using Repositories.Services.Administrative;
using RestSharp;
using Newtonsoft.Json;
using System.Net;

namespace Apom_User.Controllers
{
    //[Authorize]
    //[Route("/acc")]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
            var container = "v" + Guid.NewGuid().ToString();
            ViewBag.container = container;

            ViewBag.PageNum = 1;
            ViewBag.PageSize = 10;
            ViewBag.LayoutLogin = "~/Views/Shared/stack-admin/_Layout_stack_login_new.cshtml";
            if (SessionContext.Theme == Constant.Theme.STACK)
            {
                ViewBag.LayoutLogin = "~/Views/Shared/stack-admin/_Layout_stack_login.cshtml";
            }
            //ViewBag.Layout = "~/Views/Shared/clip-tow/_LayoutAdmin.cshtml";
            ViewBag.Layout = "~/Views/Shared/stack-admin/_Layout_stack.cshtml";
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public void Language()
        {
            string culture = Common.Constant.LANGUAGE_DEFAULT;
            var _language = Request.Cookies[Common.Constant.WebConfig.LANGUAGE] != null ? Request.Cookies[Common.Constant.WebConfig.LANGUAGE].Value : "";
            if (!string.IsNullOrEmpty(_language))
            {
                culture = _language;
            }
            SessionContext.Language = culture;
            // create cookies language
            var languageCookie = new System.Web.HttpCookie(Common.Constant.WebConfig.LANGUAGE, culture);
            languageCookie.Expires = DateTime.Now.AddDays(14);
            HttpContext.Response.Cookies.Add(languageCookie);

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }

        //
        // GET: /Account/Login
        //[AllowAnonymous]
        [Route("/dang-nhap")]
        public async Task<ActionResult> Login(string themeid, string returnUrl, string msgInfo, string msgError, string msgSuccess)
        {
            Language();

            SessionContext.Theme = String.IsNullOrEmpty(themeid) ? SessionContext.Theme : (themeid == "apom" ? "" : themeid);
            LoginViewModel model = new LoginViewModel
            {
                msg_info = msgInfo,
                msg_error = msgError,
                msg_success = msgSuccess,
                redirect_url = returnUrl
            };
            //return View(model);

            var cookies = Request.Cookies[Constant.WebConfig.COOKIE_LOGIN] != null ? Request.Cookies[Constant.WebConfig.COOKIE_LOGIN] : null;
            if (cookies != null)
            {
                var _email = Libs.Utils.Encrypts.Decrypt(Constant.WebConfig.KEYENDE, cookies.Values["paraI"]);
                var is_check_remember = cookies.Values["paraII"];
                if (is_check_remember == "1")
                {
                    user userRemember = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(_email, "", true);
                    if (userRemember != null)
                    {
                        string pass = Libs.Utils.Encrypts.Decrypt(String.Concat(userRemember.emailaddress, "_", Common.Constant.WebConfig.KEYENDE), userRemember.password);
                        model.user_email = userRemember.emailaddress;
                        model.user_password = pass;
                        model.user_rememberme = "on";
                    }
                }
            }

            if (String.IsNullOrEmpty(SessionContext.Theme))
            {
                ViewBag.LayoutLogin = "~/Views/Shared/stack-admin/_Layout_stack_login_new.cshtml";
                return View("Login1", model);
            }
            else if (SessionContext.Theme == Constant.Theme.STACK)
            {
                ViewBag.LayoutLogin = "~/Views/Shared/stack-admin/_Layout_stack_login.cshtml";
                return View(model);
            }
            return View("Index_Theme_No_Support");
        }

        //
        // POST: /Account/Login
        [HttpPost]
        //[AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("/dang-nhap")]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                if (SessionContext.UserProfile != null)
                {
                    return RedirectToAction("Index", "Home");
                }
                apom apomUser = new apom();

                Language();
                //returnUrl = "/";
                string _returnURL = Request.Form["txtRedirectURL"] ?? "";
                ViewBag.ReturnUrl = _returnURL;
                if (!ModelState.IsValid)
                {
                    //var errors = ModelState.Values.SelectMany(v => v.Errors);
                    //var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                    //Log.LOG4NET.Info("Invalid model: " + message);
                    //model.msg_error = message;
                    var emailAddressAttribute = new EmailAddressAttribute();
                    bool isEmail = emailAddressAttribute.IsValid(model.user_email);
                    if (!isEmail)
                    {
                        model.msg_error = Resources.User.validate_is_not_email;
                    }
                    else if (model.user_password.Length < Common.Constant.WebConfig.PWD_LENGTH_MIN)
                    {
                        model.msg_error = Resources.User.password_require_num_char_min;
                        model.msg_error = model.msg_error.Replace(@"@num", Constant.WebConfig.PWD_LENGTH_MIN.ToString());
                    }
                    else if (model.user_password.Length > Common.Constant.WebConfig.PWD_LENGTH_MAX)
                    {
                        model.msg_error = Resources.User.password_require_num_char_max;
                        model.msg_error = model.msg_error.Replace(@"@num", Constant.WebConfig.PWD_LENGTH_MAX.ToString());
                    }

                    if (String.IsNullOrEmpty(SessionContext.Theme))
                    {
                        return View("Login1", model);
                        //return View("Home", model);
                    }
                    else if (SessionContext.Theme == Constant.Theme.STACK)
                    {
                        //return Redirect(_returnURL);
                        return View("Login", model);
                    }
                    else
                    {
                        //return Redirect(_returnURL);
                        return View("Index_Theme_No_Support");
                    }
                }

                string pass = model.user_password;
                string email = model.user_email;

                //pass = Libs.Utils.Encrypts.Encrypt(String.Concat("registry.pop.system@gmail.com", "_", Common.Constant.WebConfig.KEYENDE), "geoi@1111qaz"); // zrBScbKVSS69tQnqLyHIbw==
                //pass = Libs.Utils.Encrypts.Encrypt(String.Concat("registry.pop.system@gmail.com", "_", Common.Constant.WebConfig.KEYENDE), "gcgsykoygmfzfwtz"); // RE7Rt7++SWbfPhtE1CHZPhSmDnldEl8y
                //pass = Libs.Utils.Encrypts.Decrypt(String.Concat("tuannh@vidagis.com", "_", Common.Constant.WebConfig.KEYENDE), "sXql7k7cudlS3za0HHxrsg==");
                // apom@123

                //var db = new DataTennantSlaveConnection();
                string key = String.Concat(email, "_", Common.Constant.WebConfig.KEYENDE);
                pass = Libs.Utils.Encrypts.Encrypt(key, pass);

                //string s = Libs.Utils.Encrypts.Decrypt(String.Concat(email, "_", Common.Constant.WebConfig.KEYENDE), "sXql7k7cudkkFCfHIl6tKA==");
                user userLogin = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(email, pass);
                if (userLogin != null)
                {
                    if (userLogin.isactive.Value == false || userLogin.isactive == null)
                    {
                        model.msg_error = Resources.Administrator.login_account_inactive;

                        if (String.IsNullOrEmpty(SessionContext.Theme))
                        {
                            return Redirect(_returnURL);
                            //return View("Login", model);
                        }
                        else if (SessionContext.Theme == Constant.Theme.STACK)
                        {
                            return Redirect(_returnURL);
                            //return View(model);
                        }
                        else return View("Index_Theme_No_Support");
                    }

                    SessionContext.UserProfile = userLogin;
                    //SessionContext.UserProfile.SessionID = Session.SessionID;

                    #region get token to call API
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    string hostAPI = Constant.WebConfig.HOST_API;
                    string endpoint = String.Concat(hostAPI, "api/auth/login");
                    var client = new RestClient(endpoint);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    var body = @"{"
                            + "\n" + $"\"auth_email\":\"{userLogin.emailaddress}\","
                            + "\n" + $"\"auth_password\":\"{model.user_password}\","
                            + "\n" + $"\"auth_langid\":\"{SessionContext.Language}\""
                            + "\n" +
                                @"}";
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = await client.ExecuteAsync(request);
                    string content = response.Content;
                    auth_user json = JsonConvert.DeserializeObject<auth_user>(content);
                    SessionContext.UserProfile.access_token = json.access_token;
                    #endregion


                    #region cookie
                    var _email = Libs.Utils.Encrypts.Encrypt(Constant.WebConfig.KEYENDE, email);
                    bool isRemember = String.IsNullOrEmpty(model.user_rememberme) ? false : model.user_rememberme.Equals("on", StringComparison.OrdinalIgnoreCase);

                    // create cookies
                    var UsernameCookie = HttpContext.Response.Cookies[Constant.WebConfig.COOKIE_LOGIN] == null ? new System.Web.HttpCookie(Constant.WebConfig.COOKIE_LOGIN) : HttpContext.Response.Cookies[Constant.WebConfig.COOKIE_LOGIN];
                    if (isRemember)
                    {
                        UsernameCookie.Values.Add("paraI", _email);

                        UsernameCookie.Values.Add("paraII", "1"); // remember?
                        UsernameCookie.Values.Add("paraIII", "0"); // call logOut?
                        UsernameCookie.Expires = DateTime.Now.AddDays(Constant.WebConfig.COOKIE_TIMEOUT);
                        HttpContext.Response.Cookies.Add(UsernameCookie);
                    }
                    else
                    {
                        UsernameCookie.Expires = DateTime.Now.AddDays(-1);
                    }

                    #region save cookie userid to use apomHub
                    // create cookies
                    var HubCookie = HttpContext.Response.Cookies[Constant.WebConfig.COOKIE_HUB] == null ? new System.Web.HttpCookie(Constant.WebConfig.COOKIE_HUB) : HttpContext.Response.Cookies[Constant.WebConfig.COOKIE_HUB];

                    HubCookie.Values.Add(Constant.WebConfig.COOKIE_HUB, SessionContext.UserProfile.userid);
                    HubCookie.Expires = DateTime.Now.AddDays(Constant.WebConfig.COOKIE_TIMEOUT);
                    HttpContext.Response.Cookies.Add(HubCookie);
                    #endregion
                    #endregion

                    Log.LOG4NET.Info($"Login: {SessionContext.UserProfile.username}");
                    //HttpContext.Response.Cookies.Add(UsernameCookie);
                    if (!string.IsNullOrEmpty(_returnURL) && !_returnURL.Contains("LogOff"))
                        return Redirect(_returnURL);
                    else
                    {
                        // save last time login
                        userLocalServiceUtil.Instance.updateLastLoginTime(userLogin.userid, DateTime.Now);
                        return Redirect(_returnURL);
                        //return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    model.msg_error = Resources.Administrator.login_aunthen_invalid;

                    if (String.IsNullOrEmpty(SessionContext.Theme))
                    {
                        return View("Login1", model);
                    }
                    else if (SessionContext.Theme == Constant.Theme.STACK)
                    {
                        return View("Login", model);
                    }
                    else return View("Index_Theme_No_Support");
                }
            }
            catch (Exception ex)
            {
                Log.LOG4NET.Error("Error: " + ex.ToString());
            }
            return Redirect("https://popgis.vnu.edu.vn");
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            Language();
            if (SessionContext.UserProfile != null)
            {
                return RedirectToAction("Index", "Home");
            }

            if (String.IsNullOrEmpty(SessionContext.Theme))
            {
                return View("Register1");
            }
            else if (SessionContext.Theme == Constant.Theme.STACK)
            {
                return View();
            }
            else return View("Index_Theme_No_Support");
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model, LoginViewModel login)
        {
            Language();
            if (ModelState.IsValid)
            {
                bool isOK = false;
                bool isSendOK = false;

                #region check exist email
                int iExistEmail = await userLocalServiceUtil.Instance.isExistemailAsync(model.user_email);
                if (iExistEmail > 0)
                {
                    #region account is active?
                    user registerUser = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(model.user_email);
                    bool isActive = registerUser.isactive == null ? false : registerUser.isactive.Value;
                    #endregion
                    if (!isActive)
                    {
                        // cho de kich hoat
                        model.msg_error = Resources.Administrator.register_active_waiting.Replace("{0}", model.user_email);
                    }
                    else
                    {
                        // ton tai
                        model.msg_error = Resources.Administrator.register_email_exist.Replace("{0}", model.user_email);
                    }

                    if (String.IsNullOrEmpty(SessionContext.Theme))
                    {
                        return View("Register1", model);
                    }
                    else if (SessionContext.Theme == Constant.Theme.STACK)
                    {
                        return View(model);
                    }
                    else return View("Index_Theme_No_Support");
                }
                #endregion

                #region create new user
                string userId = Guid.NewGuid().ToString();
                DateTime date1970 = new DateTime(1970, 1, 1, 0, 0, 0);
                DateTime date = DateTime.Now;
                long time = date.Ticks / 1000;
                try
                {
                    user newUser = new user
                    {
                        userid = userId,
                        username = model.user_name,
                        emailaddress = model.user_email,
                        password = Libs.Utils.Encrypts.Encrypt(String.Concat(model.user_email, "_", Common.Constant.WebConfig.KEYENDE), model.user_password),
                        passwordencrypted = true,
                        isactive = false,
                        time_register = time
                    };
                    DataApomAppConnection db = new DataApomAppConnection();
                    newUser = userLocalServiceUtil.Instance.add(db, newUser);
                    isOK = newUser == null ? false : true;
                }
                catch (Exception ex)
                {
                    isOK = false;
                    model.msg_error = ex.Message;
                }
                #endregion

                #region send email to active account
                if (isOK)
                {
                    try
                    {
                        string userIdEncrypt = Libs.Utils.Encrypts.Encrypt(String.Concat(time.ToString(), "_", Common.Constant.WebConfig.KEYENDE), userId);
                        //string varifyUrl = Url.Action("Active", "Account", new { t = time.ToString(), u = userIdEncrypt }, protocol: Request.Url.Scheme);
                        System.Web.Routing.RouteValueDictionary d = new System.Web.Routing.RouteValueDictionary();
                        d.Add("t", time.ToString());
                        d.Add("u", userIdEncrypt);
                        //string varifyUrl = Url.Action("Active", "Account", new { t = time.ToString(), u = userIdEncrypt }, "https", "popgissvc.duckdns.org");
                        string varifyUrl = Url.Action("Active", "Account", d, "https", "popgis.vnu.edu.vn");

                        string body = Resources.Administrator.register_active_link;
                        body = body.Replace("{0}", model.user_name).Replace("{1}", "<a href='" + varifyUrl + "'>" + varifyUrl + "</a> ");
                        body = body.Replace("{2}", Common.Constant.WebConfig.EXPIRE_REGISTER_ACCOUNT.ToString());
                        if (Common.Constant.WebConfig.EXPIRE_REGISTER_ACCOUNT > 1) body = body.Replace("{3}", "s");
                        else body = body.Replace("{3}", "");

                        Email.EmailModel emailModel = new Email.EmailModel
                        {
                            smtp = Common.Constant.GENERAL_SETTING.email_server_pop,
                            port = Common.Constant.GENERAL_SETTING.email_gate.Value,
                            mail_from = Common.Constant.GENERAL_SETTING.email_address,
                            mail_to = model.user_email,
                            credentials_user = Common.Constant.GENERAL_SETTING.email_address,
                            credentials_password = Libs.Utils.Encrypts.Decrypt(String.Concat(Common.Constant.GENERAL_SETTING.email_address, "_", Common.Constant.WebConfig.KEYENDE), Common.Constant.GENERAL_SETTING.email_password),
                            subject = String.Concat("[", Resources.Administrator.sys_name, "]: ", Resources.Administrator.register),
                            body = body,
                            enableSsl = Common.Constant.GENERAL_SETTING.ssl.Value,
                            bodyIsHTML = true
                        };
                        isSendOK = await Email.SendMail(emailModel);
                    }
                    catch (Exception ex)
                    {
                        isSendOK = false;
                        model.msg_error = ex.Message;
                    }
                }
                else
                {
                    // delete account
                    userLocalServiceUtil.Instance.delUser(userId);
                    return RedirectToAction("Login", "Account", new { msgError = Resources.Administrator.error_register_account });
                }
                #endregion

                if (isOK && isSendOK)
                {
                    return RedirectToAction("Login", "Account", new { msgSuccess = Resources.Administrator.check_email_active_account });
                }

                //var user = new ApplicationUser { UserName = model.user_email, Email = model.user_email };
                //var result = await UserManager.CreateAsync(user, model.user_password);
                //if (result.Succeeded)
                //{
                //    await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

                //    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //    // Send an email with this link
                //    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                //    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                //    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                //    return RedirectToAction("Index", "Home");
                //}
                //AddErrors(result);
            }
            else
            {
                //var errors = ModelState.Select(x => x.Value.Errors)
                //                       .Where(y => y.Count > 0)
                //                       .ToList();
                //var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                if (model.user_password.Length < Common.Constant.WebConfig.PWD_LENGTH_MIN)
                {
                    model.msg_error = Resources.User.password_require_num_char;
                    model.msg_error = model.msg_error.Replace(@"@num", Constant.WebConfig.PWD_LENGTH_MIN.ToString());
                }
                else if (model.user_password.Length > Common.Constant.WebConfig.PWD_LENGTH_MAX)
                {
                    model.msg_error = Resources.User.password_require_num_char_max;
                    model.msg_error = model.msg_error.Replace(@"@num", Constant.WebConfig.PWD_LENGTH_MAX.ToString());
                }
                else if (!model.user_password.Equals(model.user_confirmpassword, StringComparison.OrdinalIgnoreCase))
                {
                    model.msg_error = Resources.Administrator.require_pwd_match;
                }
            }

            if (String.IsNullOrEmpty(SessionContext.Theme))
            {
                return View("Register1", model);
            }
            else if (SessionContext.Theme == Constant.Theme.STACK)
            {
                return View(model);
            }
            else return View("Index_Theme_No_Support");
        }

        [AllowAnonymous]
        public async Task<ActionResult> VerifyPassword(string t, string u)
        {
            Language();

            int expireResetPassword = Common.Constant.WebConfig.EXPIRE_REGISTER_ACCOUNT * 60;
            long time = Convert.ToInt64(t);
            string userId = Libs.Utils.Encrypts.Decrypt(String.Concat(time.ToString(), "_", Common.Constant.WebConfig.KEYENDE), u);
            user user = await userLocalServiceUtil.Instance.getUserAsync(userId);
            if (user == null) user = new user();
            time = user.time_reset_pwd == null ? 0 : user.time_reset_pwd.Value;
            DateTime dtRegister = new DateTime(time * 1000);
            TimeSpan ts = DateTime.Now - dtRegister;
            bool expiredResetPassword = ts.TotalMinutes < expireResetPassword ? false : true;
            bool requestResetPassword = user.password_reset == true ? true : false;

            ResetPasswordViewModel model = new ResetPasswordViewModel
            {
                user_email = user.emailaddress,
                expired_reset_password = expiredResetPassword,
                request_reset_password = requestResetPassword
            };

            if (user.isactive.Value == false || user.isactive == null)
            {
                model.msg_error = Resources.Administrator.login_account_inactive;
            }

            if (String.IsNullOrEmpty(SessionContext.Theme))
            {
                return View("VerifyPassword1", model);
            }
            else if (SessionContext.Theme == Constant.Theme.STACK)
            {
                return View(model);
            }
            else return View("Index_Theme_No_Support");
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPassword(ResetPasswordViewModel model)
        {
            Language();
            if (ModelState.IsValid)
            {
                #region check exist email
                int iExistEmail = await userLocalServiceUtil.Instance.isExistemailAsync(model.user_email);
                if (iExistEmail == 0)
                {
                    // ton tai
                    model.msg_error = Resources.Administrator.email_not_exist.Replace("{0}", model.user_email);

                    if (String.IsNullOrEmpty(SessionContext.Theme))
                    {
                        return View("VerifyPassword1", model);
                    }
                    else if (SessionContext.Theme == Constant.Theme.STACK)
                    {
                        return View(model);
                    }
                    else return View("Index_Theme_No_Support");
                }
                #endregion

                user user = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(model.user_email);
                if (user.isactive.Value == false || user.isactive == null)
                {
                    model.msg_error = Resources.Administrator.login_account_inactive;
                    model.request_reset_password = true;
                    if (String.IsNullOrEmpty(SessionContext.Theme))
                    {
                        return View("VerifyPassword1", model);
                    }
                    else if (SessionContext.Theme == Constant.Theme.STACK)
                    {
                        return View(model);
                    }
                    else return View("Index_Theme_No_Support");
                }

                #region update pass
                string newPass = model.user_password;
                newPass = Libs.Utils.Encrypts.Encrypt(String.Concat(model.user_email, "_", Common.Constant.WebConfig.KEYENDE), newPass);
                userLocalServiceUtil.Instance.updatePassword(model.user_email, newPass);

                return RedirectToAction("Login", "Account", new { msgSuccess = Resources.Administrator.reset_password_success });
                #endregion
            }
            else
            {
                //var errors = ModelState.Select(x => x.Value.Errors)
                //                       .Where(y => y.Count > 0)
                //                       .ToList();
                //var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                if (model.user_password.Length < Common.Constant.WebConfig.PWD_LENGTH_MIN)
                {
                    model.msg_error = Resources.User.password_require_num_char;
                    model.msg_error = model.msg_error.Replace(@"@num", Constant.WebConfig.PWD_LENGTH_MIN.ToString());
                }
                else if (model.user_password.Length > Common.Constant.WebConfig.PWD_LENGTH_MAX)
                {
                    model.msg_error = Resources.User.password_require_num_char_max;
                    model.msg_error = model.msg_error.Replace(@"@num", Constant.WebConfig.PWD_LENGTH_MAX.ToString());
                }
                else if (!model.user_password.Equals(model.user_confirmpassword, StringComparison.OrdinalIgnoreCase))
                {
                    model.msg_error = Resources.Administrator.require_pwd_match;
                }
            }


            if (String.IsNullOrEmpty(SessionContext.Theme))
            {
                return View("VerifyPassword1", model);
            }
            else if (SessionContext.Theme == Constant.Theme.STACK)
            {
                return View(model);
            }
            else return View("Index_Theme_No_Support");
        }

        [AllowAnonymous]
        public async Task<ActionResult> Active(string t, string u)
        {
            Language();

            int expireRegister = Common.Constant.WebConfig.EXPIRE_REGISTER_ACCOUNT * 60;
            long time = Convert.ToInt64(t);
            string userId = Libs.Utils.Encrypts.Decrypt(String.Concat(time.ToString(), "_", Common.Constant.WebConfig.KEYENDE), u);
            user user = await userLocalServiceUtil.Instance.getUserAsync(userId);
            if (user == null) user = new user();
            bool isActive = user.isactive == null ? false : user.isactive.Value;
            if (isActive)
            {
                return RedirectToAction("Login", "Account");
            }

            time = user.time_register == null ? 0 : user.time_register.Value;
            DateTime dtRegister = new DateTime(time * 1000);
            TimeSpan ts = DateTime.Now - dtRegister;
            bool expiredRegister = ts.TotalMinutes < expireRegister ? false : true;

            if (expiredRegister)
            {
                // truong hop link het han -> delete account
                userLocalServiceUtil.Instance.delUser(userId);
            }
            else
            {
                // nguoc lai thi cap nhat lai truong isActive
                userLocalServiceUtil.Instance.activeUser(userId);
            }
            Apom_User.Models.RegisterViewModel model = new RegisterViewModel
            {
                expired_register = expiredRegister
            };

            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            Language();
            if (SessionContext.UserProfile != null)
            {
                return RedirectToAction("Index", "Home");
            }
            if (String.IsNullOrEmpty(SessionContext.Theme))
            {
                return View("ForgotPassword1");
            }
            else if (SessionContext.Theme == Constant.Theme.STACK)
            {
                return View();
            }
            else return View("Index_Theme_No_Support");
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            Language();
            if (ModelState.IsValid)
            {
                bool isSendOK = false;

                #region check exist email
                int iExistEmail = await userLocalServiceUtil.Instance.isExistemailAsync(model.user_email);
                if (iExistEmail == 0)
                {
                    // ko ton tai
                    model.msg_error = Resources.Administrator.email_not_exist.Replace("{0}", model.user_email);

                    // If we got this far, something failed, redisplay form
                    if (String.IsNullOrEmpty(SessionContext.Theme))
                    {
                        return View("ForgotPassword1", model);
                    }
                    else if (SessionContext.Theme == Constant.Theme.STACK)
                    {
                        return View(model);
                    }
                    else return View("Index_Theme_No_Support");
                }
                #endregion

                #region send email to active account
                try
                {
                    DateTime date1970 = new DateTime(1970, 1, 1, 0, 0, 0);
                    DateTime date = DateTime.Now;
                    long time = date.Ticks / 1000;

                    #region update reset password time
                    userLocalServiceUtil.Instance.updateResetPasswordTime(model.user_email, time);
                    #endregion

                    user userRequest = await userLocalServiceUtil.Instance.getUserbyEmailPwdAsync(model.user_email, null);
                    string userIdEncrypt = Libs.Utils.Encrypts.Encrypt(String.Concat(time.ToString(), "_", Common.Constant.WebConfig.KEYENDE), userRequest.userid);
                    string varifyUrl = Url.Action("VerifyPassword", "Account", new { t = time.ToString(), u = userIdEncrypt }, protocol: Request.Url.Scheme);

                    string body = Resources.Administrator.reset_password_link;
                    body = body.Replace("{0}", userRequest.username).Replace("{1}", Resources.Administrator.sys_name).Replace("{2}", "<a href='" + varifyUrl + "'>" + varifyUrl + "</a> ");
                    body = body.Replace("{3}", Common.Constant.WebConfig.EXPIRE_REGISTER_ACCOUNT.ToString());
                    if (Common.Constant.WebConfig.EXPIRE_REGISTER_ACCOUNT > 1) body = body.Replace("{4}", "s");
                    else body = body.Replace("{4}", "");

                    Email.EmailModel emailModel = new Email.EmailModel
                    {
                        smtp = Common.Constant.GENERAL_SETTING.email_server_pop,
                        port = Common.Constant.GENERAL_SETTING.email_gate.Value,
                        mail_from = Common.Constant.GENERAL_SETTING.email_address,
                        mail_to = model.user_email,
                        credentials_user = Common.Constant.GENERAL_SETTING.email_address,
                        credentials_password = Libs.Utils.Encrypts.Decrypt(String.Concat(Common.Constant.GENERAL_SETTING.email_address, "_", Common.Constant.WebConfig.KEYENDE), Common.Constant.GENERAL_SETTING.email_password),
                        subject = String.Concat("[", Resources.Administrator.sys_name, "]: ", Resources.User.pwd_reset),
                        body = body,
                        enableSsl = Common.Constant.GENERAL_SETTING.ssl.Value,
                        bodyIsHTML = true
                    };
                    isSendOK = await Email.SendMail(emailModel);
                }
                catch (Exception ex)
                {
                    isSendOK = false;
                    model.msg_error = ex.Message;
                }
                #endregion

                if (isSendOK)
                {
                    return RedirectToAction("Login", "Account", new { msgSuccess = Resources.Administrator.check_email_reset_password });
                }

                //var user = await UserManager.FindByNameAsync(model.user_email);
                //if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                //{
                //    // Don't reveal that the user does not exist or is not confirmed
                //    return View("ForgotPasswordConfirmation");
                //}

                //// For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //// Send an email with this link
                //// string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                //// var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                //// await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                //// return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }
            else
            {
                var emailAddressAttribute = new EmailAddressAttribute();
                bool isEmail = emailAddressAttribute.IsValid(model.user_email);
                if (!isEmail)
                {
                    model.msg_error = Resources.User.validate_is_not_email;
                }
            }

            // If we got this far, something failed, redisplay form
            if (String.IsNullOrEmpty(SessionContext.Theme))
            {
                return View("ForgotPassword1", model);
            }
            else if (SessionContext.Theme == Constant.Theme.STACK)
            {
                return View(model);
            }
            else return View("Index_Theme_No_Support");
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.user_email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.user_password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Log.LOG4NET.Info("Logout");
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            string redirectURL = Request.Params["redirect"] ?? "";

            //HttpContext.Response.Cookies[Constant.WebConfig.login].Expires = DateTime.Now.AddDays(-1);

            #region clear session
            Common.SessionContext.ClearLogout();
            Common.SessionContext.ClearAllSession();
            #endregion

            #region clear cookie
            //var cookies = HttpContext.Response.Cookies[Constant.WebConfig.COOKIE_LOGIN] == null ? new HttpCookie(Constant.WebConfig.COOKIE_LOGIN) : HttpContext.Response.Cookies[Constant.WebConfig.COOKIE_LOGIN];
            var cookies = Request.Cookies[Constant.WebConfig.COOKIE_LOGIN] != null ? Request.Cookies[Constant.WebConfig.COOKIE_LOGIN] : null;
            if (cookies != null)
            {
                cookies.Values.Remove("paraIII");
                cookies.Values.Add("paraIII", "1"); // call logOut?
                HttpContext.Response.Cookies.Add(cookies);
            }
            #endregion

            // clear cache: role
            #region update user online
            //var luser = new List<Users>((List<Users>)HttpContext.Application[Common.Constant.WebConfig.NUMBER_ONLINE_USER]);
            //var user = new Users();
            //for (var i = 0; i < luser.Count; i++)
            //{
            //    if (luser[i].SessionID == Session.SessionID)
            //    {
            //        user = luser[i];
            //        break;
            //    }
            //}
            //if (user.oid > 0) luser.Remove(user);
            //HttpContext.Application.Lock();
            //HttpContext.Application[Common.Constant.WebConfig.NUMBER_ONLINE_USER] = new List<Users>(luser);
            //HttpContext.Application.UnLock();
            #endregion

            #region clear cache
            //CacheManager<Repositories.VidaGISCloud.Models.DBTenant.Administrative.RoleGroup>.Instance.ClearAllCache();
            //CacheManager<Repositories.VidaGISCloud.Models.DBTenant.Administrative.RolePermission>.Instance.ClearAllCache();
            //CacheManager<Repositories.VidaGISCloud.Models.DBTenant.Administrative.RoleUser>.Instance.ClearAllCache();
            #endregion

            if (String.IsNullOrEmpty(redirectURL)) return RedirectToAction("Index", "Home");
            else return Redirect(redirectURL);
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}