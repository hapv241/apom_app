﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Apom_User.Controllers
{
    public class CommonController : Controller
    {
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChangeLanguage(string url, string culture)
        {
            if (!Common.Constant.WebConfig.LIST_LANGUAGE.Contains(culture)) culture = Common.Constant.LANGUAGE_DEFAULT;
            string urlRedirect = url;
            
            //// check authen
            //if (SessionContext.UserProfile == null) urlRedirect = "/Account/Login";
            //else
            //{
            //    // check: system have contain controller, action of url?
            //    Uri uri = new Uri(url);
            //    string localPath = uri.LocalPath;
            //    string[] localPaths = localPath.Split('/');

            //    bool bAuthen = false;
            //    // in case of: /GIS/MainMap || /Asset
            //    string controller = (String.IsNullOrEmpty(localPaths[localPaths.Length - 1]) ? "Home" : localPaths[localPaths.Length - 1]) + "Controller";
            //    string action = "Index"; // action default 
            //    if (Common.Constant.CONTROLLER_ACTION_ALL.ContainsKey(controller) && Common.Constant.CONTROLLER_ACTION_ALL[controller].Contains(action))
            //    {
            //        bAuthen = true;
            //    }
            //    else
            //    {
            //        // in case of: /Asset/Info
            //        controller = localPaths[localPaths.Length - 2] + "Controller";
            //        action = localPaths[localPaths.Length - 1];
            //        if (Common.Constant.CONTROLLER_ACTION_ALL.ContainsKey(controller) && Common.Constant.CONTROLLER_ACTION_ALL[controller].Contains(action))
            //        {
            //            bAuthen = true;
            //        }
            //    }
            //    if (!bAuthen) urlRedirect = "/Home";
            //}

            // create cookies language
            var languageCookie = new HttpCookie(Common.Constant.WebConfig.LANGUAGE, culture);
            languageCookie.Expires = DateTime.Now.AddDays(Common.Constant.WebConfig.COOKIE_TIMEOUT);

            HttpContext.Response.Cookies.Add(languageCookie);

            Common.SessionContext.Language = culture;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);

            return Redirect(urlRedirect);
        }
    }
}