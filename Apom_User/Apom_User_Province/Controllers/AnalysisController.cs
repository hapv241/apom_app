﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Repositories.Models;
using Repositories.Models.Asset;
using Repositories.Models.Category;
using Repositories.Services.Asset;
using Repositories.Services.Category;

namespace Apom_User.Controllers
{
    public class AnalysisController : BaseController
    {
        // GET: Analysis
        [Route("/analysis_data")]
        public ActionResult Index()
        {
            Language();
            ViewBag.use_datatable = true;
            ViewBag.use_map = true;
            ViewBag.use_chart = true;
            //ViewBag.use_switch = true;
            //ViewBag.use_baseslider = true;
            ViewBag.page = Common.Constant.Page.data_analysis;

            //List<group_tree> lstGroupTree = groupLocalServiceUtil.Instance.buildGroupStationTree(Common.SessionContext.Language);

            List<province> lstProvince = provinceLocalServiceUtil.Instance.getAll(Common.SessionContext.Language);
            List<vnaqi_index_short> lstAQIIndexShort = vnaqiindexLocalServiceUtil.Instance.getInfoShort(Common.SessionContext.Language);

            analysis_model_view modelView = new analysis_model_view
            {
                Provinces = lstProvince,
                VNAQIIndex = lstAQIIndexShort
            };
            return View(modelView);
        }
    }
}