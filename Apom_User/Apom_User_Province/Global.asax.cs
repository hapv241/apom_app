﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using ServiceBase;

namespace Apom_User
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ObjectLocalServiceUtil<object>.CONNSTR = Common.Constant.WebConfig.CONNECTIONSTR;

            Repositories.Models.Administrative.user sa = new Repositories.Models.Administrative.user
            {
                userid = "apomsupperadmin",
                emailaddress = Apom_User.Common.Constant.WebConfig.SEMAIL,
                username = Apom_User.Common.Constant.WebConfig.SNAME,
                password = Apom_User.Common.Constant.WebConfig.SPWD,
                isactive = true
            };
            sa.password = Libs.Utils.Encrypts.Encrypt(String.Concat(sa.emailaddress, "_", Common.Constant.WebConfig.KEYENDE), sa.password);
            Repositories.Services.Administrative.userLocalServiceUtil.Instance.SupperAdminDefault = sa;
        }

        protected void Application_Init()
        {
            Apom_User.Common.SessionContext.Language = Common.Constant.LANGUAGE_DEFAULT;
        }
    }
}
