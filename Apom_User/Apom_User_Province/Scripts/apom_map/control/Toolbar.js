﻿apomGis.Controls.ToolbarClass = function (options) {
    if (!(options.map instanceof ol.Map)) {
        throw new Error('Please provide a valid OpenLayers 3 map object.');
    }
    this.map = options.map;

    this.class_parent = options.class_map ? options.class_map : ".vgis-map";

    this.lst_type_button = {
        // 2 action on/off
        'toogle': 'toogle',
        // 2 action (on/off co the giong nhau hoac chi co on, ko co class active)
        'press': 'press',
        // ko co action nao
        'freeze': 'freeze'
    };

    this.zoom = function (obj) {
        var map = obj.data.map;
        var delta = obj.delta === undefined ? 1 : obj.delta;
        var duration = obj.duration === undefined ? 250 : obj.duration;
        map.parent.coreImpl.Zoom(delta, duration);
    };

    this.fullExtent = function (obj) {
        var map = obj.data.map;
        map.parent.coreImpl.zoomToExtent(apomGis.Config.initExtent.full);
    };

    this.backExtent = function (obj) {
        var map = obj.data.map;
        map.parent.coreImpl.backExtent();
    };

    this.nextExtent = function (obj) {
        var map = obj.data.map;
        map.parent.coreImpl.nextExtent();
    };

    this.fullScrennMap = function (obj) {
        var map = obj.data.map;
        map.parent.coreImpl.fullScrennMap();
    };

    this.closeFullscreenMap = function (obj) {
        var map = obj.data.map;
        map.parent.coreImpl.closeFullscreenMap();
    };
    
    this.lst_buttons = [
        // fix zoom in
        {
            classname: '.fix-zoom-in',
            type: this.lst_type_button.press,
            group: 'zoom',
            data: {
                map: this.map
            },
            callback_on: this.zoom
        },
        // fix zoom out
        {
            classname: '.fix-zoom-out',
            type: this.lst_type_button.press,
            group: 'zoom',
            data: {
                map: this.map
            },
            callback_on: this.zoom
        },

        // full extent
        {
            classname: '.full-extent',
            type: this.lst_type_button.press,
            group: 'extent',
            data: {
                map: this.map
            },
            callback_on: this.fullExtent,
        },
        
        // back extent
        {
            classname: '.back-extent',
            type: this.lst_type_button.press,
            group: 'extent',
            data: {
                map: this.map
            },
            callback_on: this.backExtent,
        },
        // next extent
        {
            classname: '.next-extent',
            type: this.lst_type_button.press,
            group: 'extent',
            data: {
                map: this.map
            },
            callback_on: this.nextExtent,
        },
        // full screen
        {
            classname: '.full-screen',
            active: {
                classname: 'active'
            },
            type: this.lst_type_button.press,
            data: {
                map: this.map
            },
            callback_on: this.fullScrennMap,
            callback_off: this.closeFullscreenMap
        },
    ];

    // remove class which has type button is toogle
    this.removeActiveToogleItem = function () {
        for (var index in this.lst_buttons) {
            var _this = this;
            var item = this.lst_buttons[index];
            var type = item.type;
            var classname = this.class_parent + ' ' + item.classname;
            if (type === this.lst_type_button.toogle) {
                $(classname).removeClass("active");
            }
        }
    };

    for (var index in this.lst_buttons) {
        var _this = this;
        var item = this.lst_buttons[index];
        var type = item.type;
        var classname = this.class_parent + ' ' + item.classname;
        // toogle
        if (type === this.lst_type_button.toogle) {
            item.deactive = function () {
                $(classname).removeClass("active");
            };
            item.data = item.data || [];
            item.data.toolbar_item = item;

            $(classname).click(function () {
                // xac dinh control
                var itemControl = null;
                for (var i = 0; i < _this.lst_buttons.length; i++) {
                    itemControl = _this.lst_buttons[i];
                    var className = itemControl.classname.substr(1);
                    if ($(this).hasClass(className)) {
                        _index = i;
                        break;
                    }
                }
                if (itemControl != null) {
                    var callbackOn = itemControl.callback_on ? itemControl.callback_on : null;
                    var callbackOff = itemControl.callback_off ? itemControl.callback_off : null;
                    _this.removeActiveToogleItem();
                    if ($(classname).hasClass("active")) {
                        $(this).removeClass("active");
                        if (callbackOff && typeof callbackOff == "function") {
                            var obj = {
                                data: itemControl.data,
                            };
                            callbackOff(obj);
                        }
                    }
                    else {
                        // add class to the one we clicked
                        $(classname).removeClass("active");
                        $(this).addClass("active");
                        if (callbackOn && typeof callbackOn == "function") {
                            var obj = {
                                data: itemControl.data,
                            };
                            callbackOn(obj);
                        }
                    }
                }
            });
        }
        // press
        else if (type === this.lst_type_button.press) {
            item.data = item.data || [];
            item.data.press = false;

            $(classname).click(function () {
                // zoom params
                var delta = $(this).data('delta') === undefined ? 1 : parseFloat($(this).data('delta'));
                var duration = $(this).data('duration') === undefined ? 250 : parseFloat($(this).data('duration'));
                // xac dinh control
                var itemControl = null;
                for (var i = 0; i < _this.lst_buttons.length; i++) {
                    var itemControl = _this.lst_buttons[i];
                    var className = itemControl.classname.substr(1);
                    if ($(this).hasClass(className)) {
                        _index = i;
                        break;
                    }
                }
                if (itemControl != null) {
                    var callbackOn = itemControl.callback_on ? itemControl.callback_on : null;
                    var callbackOff = itemControl.callback_off ? itemControl.callback_off : null;
                    var press = itemControl.data.press === undefined ? false : itemControl.data.press;
                    if (press === true) {
                        itemControl.data.press = false;
                        if (callbackOff && typeof callbackOff == "function") {
                            var obj = {
                                data: itemControl.data,
                                delta: delta,
                                duration: duration
                            };
                            callbackOff(obj);
                        }
                    }
                    else {
                        if (callbackOff != null) itemControl.data.press = true;
                        if (callbackOn && typeof callbackOn == "function") {
                            var obj = {
                                data: itemControl.data,
                                delta: delta,
                                duration: duration
                            };
                            callbackOn(obj);
                        }
                    }
                }
            });
        }
    };

};