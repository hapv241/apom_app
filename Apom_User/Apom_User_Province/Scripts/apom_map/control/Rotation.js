﻿apomGis.Controls.RotationClass = function (options) {
    if (options.target) {
        this.className = apomGis.Config.position.className;
        this.map = options.map; // map.getView
        this.target = options.target; // number of rotation

        this.destroy = function () {

        };

        this.create = function () {
            var map = this.map;

            var rotation = map.getView().getRotation(); // radian
            rotation = Math.round(rotation * 180 / Math.PI); // angle
            $('#' + this.target).val(rotation);

            $('#' + this.target).change(function () {
                var rotation = parseInt($(this).val()); // angle
                rotation = Math.PI * rotation / 180; // radian
                map.getView().setRotation(rotation);
            });

            map.on('moveend', this.onMoveEnd);
        };

        this.onMoveEnd = function (evt) {
            var map = evt.map;
            var rotation = map.getView().getRotation(); // radian
            rotation = Math.round(rotation * 180 / Math.PI); // angle
            while (rotation > 360) {
                rotation = rotation - 360;
            }
            $('#' + options.target).val(rotation);
        };
    }
    else {
        throw new Error('Invalid parameter(s) provided.');
    }
}