﻿function ControlClass(map) {
    this.map = map;
};

ControlClass.prototype.getMap = function () {
    return this.map;
};

ControlClass.prototype.isContainInteraction = function () {
    if (this.map == undefined) return false;
    var bContain = false;
    var collectionInteraction = this.map.getInteractions();
    var i = 0, len = collectionInteraction.getArray().length;
    for (i = 0; i < len; i++) {
        var item = collectionInteraction.getArray()[i];
        if (item == this.control) {
            bContain = true;
            break;
        };
    };
    return bContain;
};

ControlClass.prototype.create = function () {
};

ControlClass.prototype.destroy = function () {
    this.control = null;
};