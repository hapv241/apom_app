﻿apomGis.Controls.PositionClass = function (options) {
    if (options.target) {
        this.className = apomGis.Config.position.className;
        this.control = null;
        this.target = options.target;
        this.digit = apomGis.Config.position.digit,
        this.projection = options.projection,
        this.coordinateCurrent = [0.00000000001, 0.00000000001];

        this.updateCoordinateCurrent = function (coordinates) {
            this.coordinateCurrent = ol.coordinate.toStringXY(coordinates, this.digit);
            if (this.control) this.control.undefinedHTML_ = this.coordinateCurrent;
        };

        this.create = function () {
            this.updateCoordinateCurrent(this.coordinateCurrent);
            var digit = this.digit;
            this.control = new ol.control.MousePosition({
                //coordinateFormat: ol.coordinate.createStringXY(this.digit),
                coordinateFormat: function (coordinates) {
                    var coord_x = coordinates[0].toFixed(digit);
                    var coord_y = coordinates[1].toFixed(digit);
                    // 
                    return coord_x + ', ' + coord_y;
                },
                projection: this.projection,
                // comment the following two lines to have the mouse position be placed within the map.
                className: this.className,
                target: document.getElementById(this.target),
                undefinedHTML: this.coordinateCurrent,
                placeholder: this.coordinateCurrent
            });
            this.control.parent = this;
        };
    }
    else {
        throw new Error('Invalid parameter(s) provided.');
    }
}