﻿apomGis.Interactions.ExtentClass = function (map) {
    this.map = map;

    this.create = function () {
    };

    /****
    create a extent fromo point(x, y)
    ****/
    this.createExtentByPoint = function (x, y, tolerance) {
        var delta = 3;
        if (tolerance != undefined) delta = tolerance;
        var xMin = x - delta;
        var xMax = x + delta;
        var yMin = y - delta;
        var yMax = y + delta;
        var newExtent = [xMin, yMin, xMax, yMax];
        return newExtent;
    };

    /****
    full extent
    ****/
    this.fullExtent = function () {
        var extent = this.map.parent.extent;
        var view = null;
        if (vGis.Config.initExtent.isCenter) {
            view = new ol.View({
                // the view's initial state
                center: extent['center'],
                zoom: extent['level']
            });
            this.map.setView(view);
        }
        else {
            view = new ol.View({
                // the view's initial state
                center: extent['full'],
                //zoom: extent['level']
            });
            //map.setView(view);
            this.map.getView().fit(extent.full, this.map.getSize());
        }
    };

    /****
    back extent
    ****/
    this.backExtent = function () {
        var mapClass = this.map.parent;
        if (mapClass.historyNow > 0) {
            mapClass.isSaveHistory = true;
            mapClass.historyNow--;
            this.map.getView().setProperties(mapClass.NavigationHistory[mapClass.historyNow]);
            setTimeout(function () {
                mapClass.isSaveHistory = false;
            }, 500);
        };
    };

    /****
    next extent
    ****/
    this.nextExtent = function () {
        var mapClass = this.map.parent;
        if (mapClass.historyNow < (mapClass.NavigationHistory.length - 1)) {
            mapClass.isSaveHistory = true;
            mapClass.historyNow++;
            this.map.getView().setProperties(mapClass.NavigationHistory[mapClass.historyNow]);
            setTimeout(function () {
                mapClass.isSaveHistory = false;
            }, 500);
        };
    };
};