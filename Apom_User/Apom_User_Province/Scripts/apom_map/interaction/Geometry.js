﻿apomGis.Interactions.GeometryClass = function (map) {
    this.map = map;
    /*****
    tao geometry
    *****/
    // point
    this.createPoint = function (x, y) {
        return new ol.geom.Point([x, y]);
    };

    // line
    this.createPolyline = function (coordinates) {
        return new ol.geom.LineString(coordinates);
    };

    // polygon
    this.createPolygon = function (coordinates) {
        return new ol.geom.Polygon(coordinates);
    };

    // circle
    this.createCircle = function (center, radius) {
        return new ol.geom.Circle(center, radius);
    };

    // convert from lonlat coordinate
    this.coordinateFromLonlat = function (longtitude, latitude) {
        return new ol.proj.fromLonLat([longtitude, latitude])
    };
};