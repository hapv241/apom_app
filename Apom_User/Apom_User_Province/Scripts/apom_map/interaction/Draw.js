﻿apomGis.Interactions.DrawClass = function (map) {
    if (!(map instanceof ol.Map)) {
        throw new Error('Please provide a valid OpenLayers 3 map object.');
    }
    this.map = map;
    /*****
    source of draw control
    *****/
    this.source = null;
    this.type = apomGis.Config.geometryType.none;
    this.drawType = apomGis.Config.drawType.none;
    
    this.destroy = function () {
    };
    
    // callBack: callBackEnd
    // callBack: callbackStart
    // callBack: callBackChangeStatus
    this.create = function (geometryType, sourceVector, callBackEnd, callbackStart, callBackChangeStatus) {
        this.destroy();
        this.source = sourceVector || new ol.source.Vector({ wrapX: true });

        this.type = geometryType;
        if (this.type == undefined) return;
        if (this.type == apomGis.Config.geometryType.none) return;
        var geometryFunction, maxPoints;
        if (this.type === apomGis.Config.geometryType.point) {
        }
        else if (this.type === apomGis.Config.geometryType.linestring) {
            //maxPoints = 2;
        }
        else if (this.type === apomGis.Config.geometryType.polygon) {
        }
        else if (this.type === apomGis.Config.geometryType.square) {
            this.type = apomGis.Config.geometryType.circle;
            geometryFunction = ol.interaction.Draw.createRegularPolygon(4);
        }
        else if (this.type === apomGis.Config.geometryType.box) {
            this.type = apomGis.Config.geometryType.linestring;
            maxPoints = 2;
            geometryFunction = function (coordinates, geometry) {
                if (!geometry) {
                    geometry = new ol.geom.Polygon(null);
                }
                var start = coordinates[0];
                var end = coordinates[1];
                geometry.setCoordinates([
                  [start, [start[0], end[1]], end, [end[0], start[1]], start]
                ]);
                return geometry;
            };
        }
    };

};