﻿//jQuery(function ($) {
var singleFile = (typeof apomGis == "object" && apomGis.singleFile);

/**
* Relative path of this script.
*/
var scriptName = "main.js";
window.apomGis = {
    /**
    * Method: _getScriptLocation
    * Return the path to this script. This is also implemented in
    * OpenLayers/SingleFile.js
    *
    * Returns:
    * {String} Path to this script
    */
    _getScriptLocation: (function () {
        var r = new RegExp("(^|(.*?\\/))(" + scriptName + ")(\\?|$)"),
            s = document.getElementsByTagName('script'),
            src, m, l = "";
        for (var i = 0, len = s.length; i < len; i++) {
            src = s[i].getAttribute('src');
            if (src) {
                var m = src.match(r);
                if (m) {
                    l = m[1];
                    break;
                }
            }
        }
        //            alert("l1: " + l);
        //            l = l + "lib/";
        //            alert("l2: " + l);
        return (function () { return l; });
    })()
};

var JsFiles = [
    "Config",
    "Core",
    "Layer",
    "Map",
    "Style",
    "layer/basemap",
    "layer/vectortile",
    "layer/wms",
    "interaction/Draw",
    "interaction/Extent",
    "interaction/Geometry",
    "interaction/Zoom",
    "control/Control",
    "control/Position",
    "control/Rotation",
    "control/Scale",
    "control/Toolbar",
];

var scriptTags = new Array(JsFiles.length);
var host = apomGis._getScriptLocation();
for (var i = 0, len = JsFiles.length; i < len; i++) {
    scriptTags[i] = "<script src='" + host + JsFiles[i] + ".js'></script>";
}
if (scriptTags.length > 0) {
    document.write(scriptTags.join(""));
}

/**
 * Constant: VERSION_NUMBER
 * 1.0
**/
apomGis.VERSION_NUMBER = "1.0";
apomGis.Controls = {};
apomGis.Interactions = {};
apomGis.Layers = {};

apomGis.supportLS = window.localStorage ? true : false;

apomGis.mainClass = function (opt_options) {
    this.options = opt_options || {};
    this.mapId = this.options.mapid ? this.options.mapid : 'map';
    this.hostAPI = this.options.hostapi ? this.options.hostapi : '';
    this.langId = this.options.langid ? this.options.langid : 'vi';

    this.nameStatusLayerList = 'apom_status_layers';
    
    this.mapImpl = null;

    this.listGroupLayers = []; // danh sach nhom tren ban do, 2 thanh phan: id & active    

    this.layerModelActive = null;
    this.vtStationTimeSeries = null;
    this.vtStation = null;

    this.getTitle = function () {
        return '';
    },

    this.callbackAttr = function () {
        return [];
    },

    this.initApomGis = function (extent) {
        this.extent = extent ? extent : apomGis.Config.initExtent.full;
        // new instance mapClass
        this.mapImpl = new apomGis.MapClass({
            mapid: this.mapId,
            fitExtent: this.extent,
            hostapi: this.hostAPI,
            langid: this.langId
        });
        this.mapImpl.loadExtentFromLS = extent ? false : true;
        this.mapImpl.create();
        //show popup feature
        this.mapImpl.showPopupFeature({
            map: this.mapImpl.control,
            callbackTitle: identifyTitle ? identifyTitle : this.getTitle, // des in index.cshtml
            callbackAttr: identifyCallbackAttr ? identifyCallbackAttr : this.callbackAttr, // des in index.cshtml
            idenitfyDataGeoTiff: idenitfyDataGeoTiff ? idenitfyDataGeoTiff : null // des in index.cshtml
        });

        /***
        core gis tool
        ***/
        this.mapImpl.coreImpl = new apomGis.CoreClass(this.mapImpl.control);
        /***
        register prototype
        ***/
        this.mapImpl.coreImpl.registerPrototype();

        /*  basemap */
        var base = new apomGis.Layers.BasemapClass();
        var osmLayer = base.createOSM({
            initExtent: { 'extent': this.extent },
            visible: true,
        });
        this.mapImpl.setMultyBasemap(osmLayer, true);
        
        let layerName = '', projCode = CONFIG_SYSTEMT.PROJECTION, url = '';

        apomGis.Config.Services = apomGis.Config.Services ? apomGis.Config.Services : [];
        let wmslayers = [], vtlayers = [];
        for (let i = 0; i < apomGis.Config.Services.length; i++) {
            let service = apomGis.Config.Services[i];
            let visible = service.visible === undefined ? true : service.visible;
            let featureBase = service.featurebase;
            let featureType = service.featuretype;
            if (service.type === apomGis.Config.layerType.layer) {
                let mapService = service.urlservice;
                let timeFilter = moment(new Date()).format('YYYY-MM-DD');
                let wmsTile = new apomGis.Layers.WMSClass();
                let tiledLayer = wmsTile.createTileWMS({
                    mapservice: mapService,
                    featurebase: featureBase,
                    featuretype: featureType,
                    projection: projCode,
                    time: timeFilter,
                    visible: visible
                });
                tiledLayer.set('type', service.type);
                tiledLayer.set('groupid', service.group_id);
                tiledLayer.set('parentid', service.parent_id);
                wmsTile.create({
                    layer: tiledLayer,
                    type: service.type
                });
                this.mapImpl.addLayer(tiledLayer);

                // using in Index.cshtml: filter station group
                this.tileLayer = tiledLayer;
                wmslayers.push(wmsTile);
            }
            else if (service.type === apomGis.Config.layerType.station) {
                /*  station layer */
                let layerName = featureBase + ':' + featureType;
                let mapService = service.urlservice + "/" + layerName + "@" + projCode + "@pbf/{z}/{x}/{-y}.pbf";
                let tileLoadFunction = service.timeslider ? tileLoadFunctionStationTimeSeries : tileLoadFunctionStation;
                let styleMapFunction = service.styleMap ? service.styleMap : this.mapImpl.styleImpl.setStyleForMap; // define in apomGis.StyleClass
                let vtStation = new apomGis.Layers.VTClass();
                let layerStation = vtStation.createVectorTile_TMS({
                    url: mapService,
                    projCode: projCode,
                    visible: visible,
                    tileLoadFunc: tileLoadFunction, // define in Index.cshtml
                });
                layerStation.set('type', service.type);
                layerStation.setStyle(styleMapFunction);
                vtStation.create({
                    layer: layerStation
                });
                this.mapImpl.addLayer(layerStation);

                // using in Index.cshtml: filter station group
                if (service.timeslider) this.vtStationTimeSeries = vtStation;
                else this.vtStation = vtStation;
                vtlayers.push(vtStation);
            }
        }
        
        this.mapImpl.wmslayers = wmslayers;
        this.mapImpl.vtlayers = vtlayers;
        this.mapImpl.control.getView().fit(this.extent, this.mapImpl.control.getSize());

        // toolbar
        this.mapImpl.toolbarImpl = new apomGis.Controls.ToolbarClass({
            map: this.mapImpl.control,
            class_map: '.map-zoom'
        });
    },

    this.initApomGisAnalysis = function (extent) {
        this.extent = extent ? extent : apomGis.Config.initExtent.full;
        // new instance mapClass
        this.mapImpl = new apomGis.MapClass({
            mapid: this.mapId,
            fitExtent: this.extent,
            cookie: 'apom_map_analysis',
            hostapi: this.hostAPI,
            langid: this.langId
        });
        this.mapImpl.loadExtentFromLS = extent ? false : true;
        this.mapImpl.create();

        /***
        core gis tool
        ***/
        this.mapImpl.coreImpl = new apomGis.CoreClass(this.mapImpl.control);
        /***
        register prototype
        ***/
        this.mapImpl.coreImpl.registerPrototype();

        /*  basemap */
        var base = new apomGis.Layers.BasemapClass();
        var osmLayer = base.createOSM({
            initExtent: { 'extent': this.extent },
            visible: true,
        });
        this.mapImpl.setMultyBasemap(osmLayer, true);

        let layerName = '', projCode = CONFIG_SYSTEMT.PROJECTION, url = '';

        apomGis.Config.Services = apomGis.Config.Services ? apomGis.Config.Services : [];
        let wmslayers = [], vtlayers = [];
        for (let i = 0; i < apomGis.Config.Services.length; i++) {
            let service = apomGis.Config.Services[i];
            let visible = service.visible === undefined ? true : service.visible;
            let featureBase = service.featurebase;
            let featureType = service.featuretype;
            if (service.type === apomGis.Config.layerType.layer) {
                let mapService = service.urlservice;
                let timeFilter = moment(new Date()).format('YYYY-MM-DD');
                let wmsTile = new apomGis.Layers.WMSClass();
                let tiledLayer = wmsTile.createTileWMS({
                    mapservice: mapService,
                    featurebase: featureBase,
                    featuretype: featureType,
                    projection: projCode,
                    time: timeFilter,
                    visible: visible
                });
                tiledLayer.set('type', service.type);
                tiledLayer.set('groupid', service.group_id);
                tiledLayer.set('parentid', service.parent_id);
                wmsTile.create({
                    layer: tiledLayer,
                    type: service.type
                });
                this.mapImpl.addLayer(tiledLayer);

                // using in Index.cshtml: filter station group
                this.tileLayer = tiledLayer;
                wmslayers.push(wmsTile);
            }
            else if (service.type === apomGis.Config.layerType.vectortile) {
                /*  vectortile layer */
                let layerName = featureBase + ':' + featureType;
                let mapService = service.urlservice + "/" + layerName + "@" + projCode + "@pbf/{z}/{x}/{-y}.pbf";
                let tileLoadFunction = service.tileLoadFunction ? service.tileLoadFunction : undefined;
                let styleMapFunction = service.styleMap ? service.styleMap : this.mapImpl.styleImpl.setStyleForMap; // define in apomGis.StyleClass
                let minZoom = service.minZoom ? service.minZoom : '';
                let maxZoom = service.maxZoom ? service.maxZoom : '';
                let vtLayer = new apomGis.Layers.VTClass();
                let layer = vtLayer.createVectorTile_TMS({
                    url: mapService,
                    projCode: projCode,
                    visible: visible,
                    minZoom: minZoom,
                    maxZoom: maxZoom,
                    tileLoadFunc: tileLoadFunction, // define in _header_layer.cshtml
                });
                layer.set('type', service.type);
                layer.setStyle(styleMapFunction);
                vtLayer.create({
                    layer: layer
                });
                this.mapImpl.addLayer(layer);

                // using in Index.cshtml: filter station group
                // using in Index.cshtml: filter station group
                if (featureType === 'gis_administrative_province') this.vtProvince = vtLayer;
                if (featureType === 'gis_administrative_district') this.vtDistrict = vtLayer;
                vtlayers.push(vtLayer);
            }
        }

        this.mapImpl.wmslayers = wmslayers;
        this.mapImpl.vtlayers = vtlayers;
        this.mapImpl.control.getView().fit(this.extent, this.mapImpl.control.getSize());

        // toolbar
        this.mapImpl.toolbarImpl = new apomGis.Controls.ToolbarClass({
            map: this.mapImpl.control,
            class_map: '.map-zoom'
        });
    },

    this.getStatusLayers = function () {
        if(apomGis.supportLS){
            var groupLayersStatus = [];
            if (localStorage.getItem(this.nameStatusLayerList)) {
                groupLayersStatus = JSON.parse(localStorage.getItem(this.nameStatusLayerList));
            }
            this.listGroupLayers = groupLayersStatus;
            return groupLayersStatus;
        }
        return [];
    }

    this.saveStatusLayers = function (groupLayers) {
        if (apomGis.supportLS) {
            localStorage.setItem(this.nameStatusLayerList, JSON.stringify(groupLayers));
        }
    }
};