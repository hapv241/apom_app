﻿//jQuery(function ($) {
var singleFile = (typeof apomGis == "object" && apomGis.singleFile);

/**
* Relative path of this script.
*/
var scriptName = "main_new.js";
window.apomGis = {
    /**
    * Method: _getScriptLocation
    * Return the path to this script. This is also implemented in
    * OpenLayers/SingleFile.js
    *
    * Returns:
    * {String} Path to this script
    */
    _getScriptLocation: (function () {
        var r = new RegExp("(^|(.*?\\/))(" + scriptName + ")(\\?|$)"),
            s = document.getElementsByTagName('script'),
            src, m, l = "";
        for (var i = 0, len = s.length; i < len; i++) {
            src = s[i].getAttribute('src');
            if (src) {
                var m = src.match(r);
                if (m) {
                    l = m[1];
                    break;
                }
            }
        }
        //            alert("l1: " + l);
        //            l = l + "lib/";
        //            alert("l2: " + l);
        return (function () { return l; });
    })()
};

var JsFiles = [
    "Config",
    "Core",
    "Layer",
    "Map",
    "Style",
    "layer/basemap",
    "layer/graphic",
    "layer/vectortile",
    "layer/wms",
    "interaction/Draw",
    "interaction/Extent",
    "interaction/Geometry",
    "interaction/Zoom",
    "interaction/IdentifyFeature",
    "control/Control",
    "control/Position",
    "control/Rotation",
    "control/Scale",
    "control/SearchAddr",
    "control/Toolbar",
];

var scriptTags = new Array(JsFiles.length);
var host = apomGis._getScriptLocation();
for (var i = 0, len = JsFiles.length; i < len; i++) {
    scriptTags[i] = "<script src='" + host + JsFiles[i] + ".js'></script>";
}
if (scriptTags.length > 0) {
    document.write(scriptTags.join(""));
}

/**
 * Constant: VERSION_NUMBER
 * 1.0
**/
apomGis.VERSION_NUMBER = "1.0";
apomGis.Controls = {};
apomGis.Interactions = {};
apomGis.Layers = {};

apomGis.supportLS = window.localStorage ? true : false;

apomGis.mainClass = function (opt_options) {
    this.options = opt_options || {};
    this.mapId = this.options.mapid ? this.options.mapid : 'map';
    this.hostAPI = this.options.hostapi ? this.options.hostapi : '';
    this.endpoinIdentify = this.options.endpoint_identify ? this.options.endpoint_identify : '';
    this.langId = this.options.langid ? this.options.langid : 'vi';

    this.nameStatusLayerList = 'apom_status_layers';

    this.mapImpl = null;

    this.listGroupLayers = []; // danh sach nhom tren ban do, 2 thanh phan: id & active    

    this.layerModelActive = null;
    this.vtStationTimeSeries = null;
    //this.vtStation = null;

    this.timeSlider = null;

    this.layerNameProvince = 'gis_administrative_province';
    this.layerNameDistrict = 'gis_administrative_district';

    this.getTitle = function () {
        return '';
    },

        this.callbackAttr = function () {
            return [];
        },

        this.initApomGis = function (options) {
            var _options = options ? options : [];
            var extent = _options.extent ? options.extent : undefined;
            var isShowBassemap = _options.show_bm ? options.show_bm : true;
            this.extent = extent ? extent : apomGis.Config.initExtent.full;
            this.fitExtent = _options.fit_extent ? _options.fit_extent : this.extent;
            // new instance mapClass
            this.mapImpl = new apomGis.MapClass({
                mapid: this.mapId,
                fitExtent: this.fitExtent,
                hostapi: this.hostAPI,
                langid: this.langId
            });
            this.mapImpl.loadExtentFromLS = extent ? false : true;
            this.mapImpl.create();

            /***
            core gis tool
            ***/
            this.mapImpl.coreImpl = new apomGis.CoreClass(this.mapImpl.control);
            /***
            register prototype
            ***/
            this.mapImpl.coreImpl.registerPrototype();

            /*  basemap */
            //isShowBassemap = !this.isAuthenticate;
            var base = new apomGis.Layers.BasemapClass();
            var osmLayer = base.createOSM({
                //initExtent: { 'extent': this.extent },
                visible: isShowBassemap,
            });
            this.mapImpl.setMultyBasemap(osmLayer, true);
            //var satelliteLayer = base.createSatellite({
            //    //initExtent: { 'extent': this.extent },
            //    title: 'Nền ảnh vệ tinh',
            //    mapservice: 'http://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
            //    featurebase: 'base',
            //    featuretype: 'satellite',
            //    projection: 'EPSG:4326|degrees',
            //    visible: isShowBassemap,
            //});
            //this.mapImpl.setMultyBasemap(satelliteLayer, true);

            let layerName = '', projCode = apomGis.Config.projection.code, unit = apomGis.Config.projection.unit, url = '';

            apomGis.Config.Services = apomGis.Config.Services ? apomGis.Config.Services : [];
            let wmslayers = [], vtlayers = [];
            let opacityWMSLayer = this.isAuthenticate ? 1 : apomGis.Config.layers.opacity_wms;

            this.listGroupLayers = this.getStatusLayers();
            for (let i = 0; i < apomGis.Config.Services.length; i++) {
                let service = apomGis.Config.Services[i];
                let featureBase = service.featurebase;
                let featureType = service.featuretype;

                let layerFromLS = this.listGroupLayers.find(x => x.id === service.group_id);
                let visible = service.visible === undefined ? true : service.visible;

                if (service.type === apomGis.Config.layerType.image) {
                    let mapService = service.urlservice;
                    let wmsTile = new apomGis.Layers.WMSClass();
                    let tiledLayer = wmsTile.createImageWMS({
                        mapservice: mapService,
                        featurebase: featureBase,
                        featuretype: featureType,
                        projection: projCode,
                        opacity: opacityWMSLayer,
                        visible: visible,
                        zIndex: 1
                    });
                    tiledLayer.set('type', service.type);
                    tiledLayer.set('type_slider', service.typeslider);
                    tiledLayer.set('groupid', service.group_id);
                    wmsTile.create({
                        layer: tiledLayer,
                        type: service.type
                    });
                    this.mapImpl.addLayer(tiledLayer);
                    wmslayers.push(wmsTile);
                }
                else if (service.type === apomGis.Config.layerType.layer) {
                    let mapService = service.urlservice;
                    let isUseTime = (service.timeslider === undefined || service.timeslider === null) ? false : service.timeslider;
                    let timeFilter = '';
                    //let timeFilter = isUseTime ? moment('2022-06-09').format('YYYY-MM-DD') : '';
                    if (service.typeslider === 'hour') timeFilter = isUseTime ? (moment(new Date()).format('YYYY-MM-DDThh:00:00') + 'Z') : '';
                    else timeFilter = isUseTime ? moment(new Date()).format('YYYY-MM-DD') : '';
                    let wmsTile = new apomGis.Layers.WMSClass();
                    let tiledLayer = wmsTile.createTileWMS({
                        mapservice: mapService,
                        featurebase: featureBase,
                        featuretype: featureType,
                        projection: projCode,
                        time: timeFilter,
                        opacity: opacityWMSLayer,
                        visible: visible,
                        zIndex: 2
                    });
                    tiledLayer.set('type', service.type);
                    tiledLayer.set('type_slider', service.typeslider);
                    tiledLayer.set('groupid', service.group_id);
                    wmsTile.create({
                        layer: tiledLayer,
                        type: service.type
                    });
                    this.mapImpl.addLayer(tiledLayer);

                    // using in Index.cshtml: filter station group
                    this.tileLayer = tiledLayer;
                    wmslayers.push(wmsTile);
                }
                else if (service.type === apomGis.Config.layerType.station || service.type === apomGis.Config.layerType.vectortile) {
                    /*  station layer */
                    let layerName = featureBase + ':' + featureType;
                    let tileLoadFunction = service.timeslider ? tileLoadFunctionStationTimeSeries : this.tileLoadFunction;
                    let styleMapFunction = service.styleMap ? service.styleMap : this.mapImpl.styleImpl.setStyleForMap; // define in apomGis.StyleClass
                    let vtStation = new apomGis.Layers.VTClass();
                    //let mapService = service.urlservice + "/" + layerName + "@" + projCode + "@pbf/{z}/{x}/{-y}.pbf";
                    //let layerStation = vtStation.createVectorTile_TMS({
                    //    url: mapService,
                    //    projCode: projCode,
                    //    unit: unit,
                    //    visible: visible,
                    //    //tileLoadFunc: tileLoadFunction, // define in Index.cshtml
                    //});
                    let mapService = service.urlservice;
                    let layerStation = vtStation.createVectorTile_WMTS({
                        url: mapService,
                        layerName: layerName,
                        projCode: projCode,
                        unit: unit,
                        visible: visible,
                        tileLoadFunc: tileLoadFunction, // define in Index.cshtml
                        zIndex: 4
                    });
                    layerStation.set('type', service.type);
                    layerStation.setStyle(styleMapFunction);
                    vtStation.create({
                        layer: layerStation
                    });
                    this.mapImpl.addLayer(layerStation);

                    // using in Index.cshtml: filter station group
                    this.vtStationTimeSeries = vtStation;
                    //if (service.timeslider) this.vtStationTimeSeries = vtStation;
                    //else this.vtStation = vtStation;
                    vtlayers.push(vtStation);
                }
            }

            var mapImpl = this.mapImpl;

            // Selection
            if (this.vtStationTimeSeries != null) {
                this.mapImpl.selectionLayerVT = new ol.layer.VectorTile({
                    map: this.mapImpl.control,
                    renderMode: 'vector',
                    source: this.vtStationTimeSeries.layer.getSource(),
                    zIndex: 5,
                    style: function (feature) {
                        if (feature.getProperties().gid in mapImpl.selectionFeatureVT) {
                            let aqi_pm25 = feature.getProperties().aqi_pm25;
                            let colorOut = returnColorAQIPM25(aqi_pm25, 0.4);
                            let colorIn = returnColorAQIPM25(aqi_pm25);
                            let styleAQIOut = apomGis.style.styleAQIOut(apomGis.style.AQIOut.radius_selected, colorOut, 100);
                            let styleAQIIn = apomGis.style.styleAQIIn(apomGis.style.AQIIn.radius_selected, colorIn, aqi_pm25.toString(), apomGis.style.default.textFill, apomGis.style.default.textStroke, 100);
                            return [styleAQIOut, styleAQIIn];
                        }
                    },
                });
            }

            // graphiclayer
            this.mapImpl.graphicLayer = new apomGis.Layers.GraphicLayerClass();
            this.mapImpl.graphicLayer.create(this.mapImpl.graphicLayer.createGraphicLayer({
                default_style: apomGis.style.default.flag,
                zIndex: 6
            }), apomGis.Config.layerType.graphic);
            this.mapImpl.addLayer(this.mapImpl.graphicLayer.layer);

            this.mapImpl.wmslayers = wmslayers;
            this.mapImpl.vtlayers = vtlayers;
            this.mapImpl.control.getView().fit(this.extent, this.mapImpl.control.getSize());

            // toolbar
            this.mapImpl.toolbarImpl = new apomGis.Controls.ToolbarClass({
                map: this.mapImpl.control,
                class_map: '.map-zoom'
            });

            // identify interaction
            this.mapImpl.identifyImpl = new apomGis.Interactions.IdentifyClass({
                map: this.mapImpl.control,
                endpoint_identify: this.endpoinIdentify,
                callback: identifyAction ? identifyAction : null, // des in index1.cshtml,
            });

            //if (this.isAuthenticate) {
            var format = 'image/png';
            var mapserviceAdministrative = this.hostGSV + '/wms';
            var administrativeLabel = new ol.layer.Tile({
                visible: true,
                source: new ol.source.TileWMS({
                    url: mapserviceAdministrative,
                    params: {
                        'FORMAT': format,
                        'VERSION': '1.1.1',
                        tiled: true,
                        "STYLES": '',
                        "LAYERS": 'v_administrative',
                        "exceptions": 'application/vnd.ogc.se_inimage',
                        tilesOrigin: 102.10796356201172 + "," + 8.306297302246094
                    }
                }),
                zIndex: 3,
            });
            this.mapImpl.addLayer(administrativeLabel);
            //}
        },

        this.tileLoadFunction = function (tile, url) {
            var format = tile.getFormat();
            var tileCoord = tile.getTileCoord();
            tile.setLoader(function (extent, resolution, projection) {
                fetch(url).then(function (response) {
                    response.arrayBuffer().then(function (data) {
                        const format = tile.getFormat() // ol/format/MVT configured as source format
                        const projection1 = format.readProjection(data);
                        if (tile.setProjection) tile.setProjection(projection1);
                        var features = [];
                        try {
                            //features = format.readFeatures(data, {});
                            features = format.readFeatures(data, {
                                extent: tile.extent,
                                featureProjection: tile.projection
                            });
                        }
                        catch (ex) {
                            //console.log('error read feature: ' + ex);
                            features = [];
                        }
                        tile.setFeatures(features);
                    });
                });
            });
        },

        this.initApomGisAnalysis = function (options) {
            var _options = options ? options : [];
            var isShowBassemap = _options.show_bm ? options.show_bm : false;
            var extent = _options.extent ? options.extent : undefined;
            this.extent = extent ? extent : apomGis.Config.initExtent.full;
            this.fitExtent = _options.fit_extent ? _options.fit_extent : this.extent;
            // new instance mapClass
            this.mapImpl = new apomGis.MapClass({
                mapid: this.mapId,
                fitExtent: this.fitExtent,
                cookie: 'apom_map_analysis',
                hostapi: this.hostAPI,
                langid: this.langId
            });
            this.mapImpl.loadExtentFromLS = extent ? false : true;
            this.mapImpl.create();

            /***
            core gis tool
            ***/
            this.mapImpl.coreImpl = new apomGis.CoreClass(this.mapImpl.control);
            /***
            register prototype
            ***/
            this.mapImpl.coreImpl.registerPrototype();

            /*  basemap */
            var base = new apomGis.Layers.BasemapClass();
            var osmLayer = base.createOSM({
                //initExtent: { 'extent': this.extent },
                visible: isShowBassemap,
            });
            if (isShowBassemap) this.mapImpl.setMultyBasemap(osmLayer, true);

            let layerName = '', projCode = apomGis.Config.projection.code, unit = apomGis.Config.projection.unit, url = '';
            //projCode = 'EPSG:4326';

            apomGis.Config.Services = apomGis.Config.Services ? apomGis.Config.Services : [];
            let wmslayers = [], vtlayers = [];

            this.listGroupLayers = this.getStatusLayers();
            for (let i = 0; i < apomGis.Config.Services.length; i++) {
                let service = apomGis.Config.Services[i];
                let featureBase = service.featurebase;
                let featureType = service.featuretype;

                let layerFromLS = this.listGroupLayers.find(x => x.id === service.group_id);
                let visible = service.visible === undefined ? true : service.visible;
                let orderService = service.orderservice == null ? 1 : service.orderservice;

                if (service.type === apomGis.Config.layerType.layer) {
                    let mapService = service.urlservice;
                    let isUseTime = (service.timeslider === undefined || service.timeslider === null) ? false : service.timeslider;
                    let timeFilter = '';
                    //let timeFilter = isUseTime ? moment('2022-06-09').format('YYYY-MM-DD') : '';
                    if (service.typeslider === 'hour') timeFilter = isUseTime ? (moment(new Date()).format('YYYY-MM-DDThh:00:00') + 'Z') : '';
                    else timeFilter = isUseTime ? moment(new Date()).format('YYYY-MM-DD') : '';
                    let wmsTile = new apomGis.Layers.WMSClass();
                    let tiledLayer = wmsTile.createTileWMS({
                        mapservice: mapService,
                        featurebase: featureBase,
                        featuretype: featureType,
                        projection: projCode,
                        time: timeFilter,
                        zIndex: orderService,
                        visible: visible
                    });
                    tiledLayer.set('type', service.type);
                    tiledLayer.set('type_slider', service.typeslider);
                    tiledLayer.set('groupid', service.group_id);
                    wmsTile.create({
                        layer: tiledLayer,
                        type: service.type
                    });
                    this.mapImpl.addLayer(tiledLayer);

                    if (visible) apomGis.mainCurrentImpl.layerModelActive = tiledLayer;

                    // using in Index.cshtml: filter station group
                    this.tileLayer = tiledLayer;
                    wmslayers.push(wmsTile);
                }
                else if (service.type === apomGis.Config.layerType.station || service.type === apomGis.Config.layerType.vectortile) {
                    /*  vectortile layer */
                    let layerName = featureBase + ':' + featureType;
                    let tileLoadFunction = service.tileLoadFunction ? service.tileLoadFunction : undefined;
                    let styleMapFunction = service.styleMap ? service.styleMap : this.mapImpl.styleImpl.setStyleForMap; // define in apomGis.StyleClass
                    let minZoom = service.minZoom ? service.minZoom : '';
                    let maxZoom = service.maxZoom ? service.maxZoom : '';
                    let vtLayer = new apomGis.Layers.VTClass();
                    // https://popgissvc.duckdns.org/geoserver/gwc/service/tms/1.0.0/
                    // https://popgissvc.duckdns.org/geoserver/gwc/service/wmts
                    //let mapService = service.urlservice + "/" + layerName + "@" + projCode + "@pbf/{z}/{x}/{-y}.pbf";
                    //let layer = vtLayer.createVectorTile_TMS({
                    //    url: mapService,
                    //    projCode: projCode,
                    //    unit: unit,
                    //    visible: visible,
                    //    minZoom: minZoom,
                    //    maxZoom: maxZoom,
                    //    tileLoadFunc: tileLoadFunction, // define in _header_layer.cshtml
                    //});
                    let mapService = service.urlservice;
                    let layer = vtLayer.createVectorTile_WMTS({
                        url: mapService,
                        layerName: layerName,
                        projCode: projCode,
                        unit: unit,
                        visible: visible,
                        zIndex: orderService,
                        //minZoom: minZoom,
                        //maxZoom: maxZoom,
                        tileLoadFunc: tileLoadFunction, // define in Index.cshtml
                    });
                    layer.set('type', service.type);
                    layer.setStyle(styleMapFunction);
                    vtLayer.create({
                        layer: layer
                    });
                    this.mapImpl.addLayer(layer);

                    // using in Index.cshtml: filter station group
                    if (featureType === this.layerNameProvince) this.vtProvince = vtLayer;
                    if (featureType === this.layerNameDistrict) this.vtDistrict = vtLayer;
                    vtlayers.push(vtLayer);
                }
            }

            // graphiclayer
            this.mapImpl.graphicLayer = new apomGis.Layers.GraphicLayerClass();
            this.mapImpl.graphicLayer.create(this.mapImpl.graphicLayer.createGraphicLayer({
                default_style: apomGis.style.default.flag,
                //default_style: apomGis.style.draw.all,
                zIndex: 4
            }), apomGis.Config.layerType.graphic);
            this.mapImpl.addLayer(this.mapImpl.graphicLayer.layer);

            this.mapImpl.wmslayers = wmslayers;
            this.mapImpl.vtlayers = vtlayers;
            this.mapImpl.control.getView().fit(this.extent, this.mapImpl.control.getSize());

            // toolbar
            this.mapImpl.toolbarImpl = new apomGis.Controls.ToolbarClass({
                map: this.mapImpl.control,
                class_map: '.map-zoom'
            });
        },

        this.initApomGisMobile = function (options) {
            var _options = options ? options : [];
            var extent = _options.extent ? options.extent : undefined;
            var isShowBassemap = _options.show_bm ? options.show_bm : true;
            this.extent = extent ? extent : apomGis.Config.initExtent.full;
            this.fitExtent = _options.fit_extent ? _options.fit_extent : this.extent;
            // new instance mapClass
            this.mapImpl = new apomGis.MapClass({
                mapid: this.mapId,
                fitExtent: this.fitExtent,
                hostapi: this.hostAPI,
                langid: this.langId
            });
            this.mapImpl.loadExtentFromLS = extent ? false : true;
            this.mapImpl.create();

            /***
            core gis tool
            ***/
            this.mapImpl.coreImpl = new apomGis.CoreClass(this.mapImpl.control);
            /***
            register prototype
            ***/
            this.mapImpl.coreImpl.registerPrototype();

            /*  basemap */
            //isShowBassemap = !this.isAuthenticate;
            var base = new apomGis.Layers.BasemapClass();
            var osmLayer = base.createOSM({
                //initExtent: { 'extent': this.extent },
                visible: isShowBassemap,
            });
            this.mapImpl.setMultyBasemap(osmLayer, true);

            let layerName = '', projCode = apomGis.Config.projection.code, unit = apomGis.Config.projection.unit, url = '';

            apomGis.Config.Services = apomGis.Config.Services ? apomGis.Config.Services : [];
            let wmslayers = [], vtlayers = [];
            let opacityWMSLayer = this.isAuthenticate ? 1 : apomGis.Config.layers.opacity_wms;

            this.listGroupLayers = this.getStatusLayers();
            for (let i = 0; i < apomGis.Config.Services.length; i++) {
                let service = apomGis.Config.Services[i];
                let featureBase = service.featurebase;
                let featureType = service.featuretype;

                let layerFromLS = this.listGroupLayers.find(x => x.id === service.group_id);
                let visible = service.visible === undefined ? true : service.visible;

                if (service.type === apomGis.Config.layerType.image) {
                    let mapService = service.urlservice;
                    let wmsTile = new apomGis.Layers.WMSClass();
                    let tiledLayer = wmsTile.createImageWMS({
                        mapservice: mapService,
                        featurebase: featureBase,
                        featuretype: featureType,
                        projection: projCode,
                        opacity: opacityWMSLayer,
                        visible: visible
                    });
                    tiledLayer.set('type', service.type);
                    tiledLayer.set('type_slider', service.typeslider);
                    tiledLayer.set('groupid', service.group_id);
                    wmsTile.create({
                        layer: tiledLayer,
                        type: service.type
                    });
                    this.mapImpl.addLayer(tiledLayer);
                    wmslayers.push(wmsTile);
                }
                else if (service.type === apomGis.Config.layerType.layer) {
                    let mapService = service.urlservice;
                    let isUseTime = (service.timeslider === undefined || service.timeslider === null) ? false : service.timeslider;
                    let timeFilter = '';
                    //let timeFilter = isUseTime ? moment('2022-06-09').format('YYYY-MM-DD') : '';
                    if (service.typeslider === 'hour') timeFilter = isUseTime ? (moment(new Date()).format('YYYY-MM-DDThh:00:00') + 'Z') : '';
                    else timeFilter = isUseTime ? moment(new Date()).format('YYYY-MM-DD') : '';
                    let wmsTile = new apomGis.Layers.WMSClass();
                    let tiledLayer = wmsTile.createTileWMS({
                        mapservice: mapService,
                        featurebase: featureBase,
                        featuretype: featureType,
                        projection: projCode,
                        time: timeFilter,
                        opacity: opacityWMSLayer,
                        visible: visible
                    });
                    tiledLayer.set('type', service.type);
                    tiledLayer.set('type_slider', service.typeslider);
                    tiledLayer.set('groupid', service.group_id);
                    wmsTile.create({
                        layer: tiledLayer,
                        type: service.type
                    });
                    this.mapImpl.addLayer(tiledLayer);

                    // using in Index.cshtml: filter station group
                    this.tileLayer = tiledLayer;
                    wmslayers.push(wmsTile);
                }
            }

            var mapImpl = this.mapImpl;

            // graphiclayer
            this.mapImpl.graphicLayer = new apomGis.Layers.GraphicLayerClass();
            this.mapImpl.graphicLayer.create(this.mapImpl.graphicLayer.createGraphicLayer({
                default_style: apomGis.style.default.flag,
                zIndex: 5
            }), apomGis.Config.layerType.graphic);
            this.mapImpl.addLayer(this.mapImpl.graphicLayer.layer);

            this.mapImpl.wmslayers = wmslayers;
            this.mapImpl.vtlayers = vtlayers;
            this.mapImpl.control.getView().fit(this.extent, this.mapImpl.control.getSize());

            //// toolbar
            //this.mapImpl.toolbarImpl = new apomGis.Controls.ToolbarClass({
            //    map: this.mapImpl.control,
            //    class_map: '.map-zoom'
            //});

            //// identify interaction
            //this.mapImpl.identifyImpl = new apomGis.Interactions.IdentifyClass({
            //    map: this.mapImpl.control,
            //    endpoint_identify: this.endpoinIdentify,
            //    callback: identifyAction ? identifyAction : null, // des in index1.cshtml,
            //});

            var format = 'image/png';
            var mapserviceAdministrative = this.hostGSV + '/wms';
            var administrativeLabel = new ol.layer.Tile({
                visible: true,
                source: new ol.source.TileWMS({
                    url: mapserviceAdministrative,
                    params: {
                        'FORMAT': format,
                        'VERSION': '1.1.1',
                        tiled: true,
                        "STYLES": '',
                        "LAYERS": 'v_administrative',
                        "exceptions": 'application/vnd.ogc.se_inimage',
                        tilesOrigin: 102.10796356201172 + "," + 8.306297302246094
                    }
                }),
                zIndex: 9999,
            });
            this.mapImpl.addLayer(administrativeLabel);
        },

        this.getStatusLayers = function () {
            if (apomGis.supportLS) {
                var groupLayersStatus = [];
                if (localStorage.getItem(this.nameStatusLayerList)) {
                    groupLayersStatus = JSON.parse(localStorage.getItem(this.nameStatusLayerList));
                }
                return groupLayersStatus;
            }
            return [];
        }

    this.saveStatusLayers = function (groupLayers) {
        if (apomGis.supportLS) {
            localStorage.setItem(this.nameStatusLayerList, JSON.stringify(groupLayers));
        }
    }
};