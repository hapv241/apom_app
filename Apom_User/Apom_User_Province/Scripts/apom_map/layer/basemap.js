﻿apomGis.Layers.BasemapClass = function () {
    this.map = null;
    this.layer = null;
    this.projection = '';
    this.initExtent = [];
    this.isSelected = false;
    this.title = '';
    this.type = apomGis.Config.layerType.none;

    this.create = function (options) {
        this.layer = options.layer;
        this.projection = options.projection;
        this.initExtent = options.initExtent;
        this.type = options.type;
        this.isSelected = options.isSelected;
        this.title = options.title;
        this.layer.basemap = options.isSelected;
    };
};