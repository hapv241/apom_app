﻿// graphic layer
// draw 1 graphic on map, type: point, line, polygon
// highlight feature
apomGis.Layers.GraphicLayerClass = function (map) {
    this.map = map;
    this.layer = null;
    this.type = apomGis.Config.layerType.none;
    this.geometry = null;

    this.featureHighlights = [];
    this.featureSelections = [];

    this.create = function (layer, type) {
        this.type = type;
        this.layer = layer;
        this.geometry = new apomGis.Interactions.GeometryClass();
    };

    this.createGraphic = function (options) {
        var geom = options.geom;
        var style = options.style;
        // attrs: include id
        var attrs = options.attrs == undefined ? [] : options.attrs;
        var newFeature = new ol.Feature(geom);
        if (style != undefined) newFeature.setStyle(style);
        newFeature.setProperties(attrs);
        this.layer.getSource().addFeature(newFeature);
    };

    this.removeFeature = function (feature) {
        this.layer.getSource().removeFeature(feature);
    };

    this.removeAllFeature = function () {
        this.layer.getSource().clear();
    };
};