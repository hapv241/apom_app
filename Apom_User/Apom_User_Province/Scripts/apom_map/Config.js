﻿apomGis.Config =
{
    layers:
    {
        opacity_wms: 1,
    },
    layerType:
    {
        none: 'none',
        statellite: 'statelliteImage',
        osm: 'osm',
        base: 'base',
        group: 'group',
        heatmap: 'heatmap',
        image: 'image',
        layer: 'layer',
        tile: 'tile',
        verctor: 'vector',
        vectortile: 'vectorTile',
        graphic: 'graphicLayer',
        station: 'station'
    },

    geometryType:
    {
        none: 'None',
        point: 'Point',
        multipoint: 'MultiPoint',
        linestring: 'LineString',
        multilinestring: 'MultiLineString',
        polygon: 'Polygon',
        multipolygon: 'MultiPolygon',
        circle: 'Circle',
        square: 'Square',
        box: 'Box',
        polyline: 'Polyline'
    },

    stGeometry:
    {
        none: 'None',
        point: 'ST_Point',
        multipoint: 'ST_MultiPoint',
        linestring: 'ST_LineString',
        multilinestring: 'ST_MultiLineString',
        polygon: 'ST_Polygon',
        multipolygon: 'ST_MultiPolygon',
        collection: 'ST_GeometryCollection',
    },

    action:
    {
        pan: 'pan',
        zoom: 'zoom',
        measure: 'measure',
        identify: 'identify',
        buffer: 'buffer',
        add: 'add',
        update: 'update',
        update_attr: 'update attribute',
        update_geometry: 'update geometry',
        update_vertext: 'update vertext',
        del: 'delete feature',
        split: 'split feature',
        selection_wms: 'selection wms',
        danh_gia_tai_san: 'danh_gia_tai_san',
        selection_rectangle: 'selection rectangle',
        selection_polygon: 'selection polygon',
        selection_circle: 'selection circle',
        selection_table_attributes: 'selection table attibutes',
        clearselection: 'clear selection',
        draw_search_asset: 'draw search asset'
    },

    drawType:
    {
        none: 0,
        zoom_in: 1,
        zoom_out: 2,
        add: 3,
        selection: 4,
        del: 5,
    },

    projection: {
        code: 'EPSG:3405',
        unit: 'm',
        gridNames: {
            'EPSG:4326': ['EPSG:4326:0', 'EPSG:4326:1', 'EPSG:4326:2', 'EPSG:4326:3', 'EPSG:4326:4', 'EPSG:4326:5', 'EPSG:4326:6', 'EPSG:4326:7', 'EPSG:4326:8', 'EPSG:4326:9', 'EPSG:4326:10', 'EPSG:4326:11', 'EPSG:4326:12', 'EPSG:4326:13', 'EPSG:4326:14', 'EPSG:4326:15', 'EPSG:4326:16', 'EPSG:4326:17', 'EPSG:4326:18', 'EPSG:4326:19', 'EPSG:4326:20', 'EPSG:4326:21'],
            'EPSG:3857': [],
            'EPSG:900913': []
        },
        resolutions: {
            'EPSG:4326': [0.703125, 0.3515625, 0.17578125, 0.087890625, 0.0439453125, 0.02197265625, 0.010986328125, 0.0054931640625, 0.00274658203125, 0.001373291015625, 6.866455078125E-4, 3.4332275390625E-4, 1.71661376953125E-4, 8.58306884765625E-5, 4.291534423828125E-5, 2.1457672119140625E-5, 1.0728836059570312E-5, 5.364418029785156E-6, 2.682209014892578E-6, 1.341104507446289E-6, 6.705522537231445E-7, 3.3527612686157227E-7],
            'EPSG:3857': [],
            'EPSG:900913': []
        },
        tileSize: {
            'EPSG:4326': [256, 256],
            'EPSG:3857': [],
            'EPSG:900913': []
        },
        origin: {
            'EPSG:4326': [-180.0, 90.0],
            'EPSG:3857': [],
            'EPSG:900913': []
        }
    },

    initExtent:
    {
        isCenter: true,
        fromLonlat: true,
        center:
        {
            x: 11760989.91,
            y: 1894425.175
        },
        level: 5,
        extent: [10800000, 900000, 13300000, 2700000],
        //full: [10800000, 900000, 13300000, 2700000]
        //full: [8000000, 100000, 17300000, 3500000]
        full: [90, 3, 122, 30]
    },
    acess_token: '',

    rotation:
    {
        show: true,
        target: 'rotation-view'
    },
    scale:
    {
        show: true,
        default_unit: 'metric',
        unit:
        {
            degrees: 'degrees',
            imperial_inch: 'imperial',
            us_inch: 'us',
            nautical_mile: 'nautical',
            metric: 'metric'
        },
        target_line: 'scale-line',
        target_bar: 'scale-bar',
        show_label: 100000000  // 100,000,000
    },
    position:
    {
        show: false,
        digit: 2,
        className: 'ol-mouse-position',
        target: 'mouse-position'
    },
    zoom:
    {
        show: false, // show default zoom control
        className: 'ol-zoom',
        target: 'zooom-map',
        zoomInTipLabel: 'Zoom in',
        zoomOutTipLabel: 'Zoom out',
        view: {
            minZoom: 5,
            maxZoom: 14
        },
        edit: {
            minZoom: 0,
            maxZoom: 29
        }
    },
    timeslider:
    {
        prev_current: 14,
        next_currrent: 7
    },
    //services:
    //[
    //    {
    //        featurebase: 'hydroalp',
    //        featuretype: 'pm25_wrf',
    //        mapservice: 'http://112.137.129.248:8080/geoserver/hydroalp/wms',
    //        groupcompid: '63',
    //        groupid: 'wrf_aqi_pm25',
    //        componentid: 'pm25',
    //        visible: true,
    //        type: 'layer',
    //        timeslider: true,
    //        typeslider: 'hour',
    //        title: 'WRF-Chem AQI -> PM2.5'
    //    },
    //    {
    //        featurebase: 'hydroalp',
    //        featuretype: 'pm25',
    //        mapservice: 'http://112.137.129.248:8080/geoserver/hydroalp/wms',
    //        groupcompid: '63',
    //        groupid: 'satellite_aqi_pm25',
    //        componentid: 'pm25',
    //        visible: true,
    //        type: 'layer',
    //        timeslider: true,
    //        typeslider: 'day',
    //        title: 'Satellite AQI -> PM2.5'
    //    },
    //    {
    //        featurebase: 'hydroalp',
    //        featuretype: 'gis_a_station_days_aqi_pm25',
    //        mapservice: 'http://112.137.129.248:8080/geoserver/gwc/service/tms/1.0.0',
    //        visible: false,
    //        type: 'station',
    //        timeslider: true,
    //        typeslider: 'hour',
    //        title: 'station'
    //    },
    //    {
    //        featurebase: 'hydroalp',
    //        featuretype: 'gis_a_station_aqi_pm25',
    //        mapservice: 'http://112.137.129.248:8080/geoserver/gwc/service/tms/1.0.0',
    //        visible: true,
    //        type: 'station',
    //        timeslider: false
    //    }
    //],
    formatImg: 'image/png',
    noData: -9999,
};

apomGis.Params =
{
    map: null, // current mapClass
    baselayers: [], // danh sach cac base layer - basemapClass
    grouplayers: [], // danh sach group layer - groupclass
    wmtslayers: [], // danh sach wmts layer - wmtsClass
    wmslayers: [], // danh sach wms layer - wmsClass
    vtlayers: [], // danh sach vectortile layer - vtClass
    wfslayers: [], // danh sach wfs layer - wfsClass
    dialogIDs: [], // danh sach dialog de on/off
}