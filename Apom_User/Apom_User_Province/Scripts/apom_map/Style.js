﻿/**
style
*/
apomGis.style = [];
// style default
apomGis.style.default = [];
// style when user draw
apomGis.style.draw = [];

/// default
apomGis.style.default = {
    imageDefault: new ol.style.Circle({
        radius: 10,
        stroke: new ol.style.Stroke({
            //color: 'white',
            //width: 2,
            color: 'rgba(255,0,0,1.0)',
            width: 1,
        }),
        fill: new ol.style.Fill({
            //color: 'yellow'
            color: 'rgba(255,255,255,1)'
        })
    }),
    imageDefaultTransparent: new ol.style.Circle({
        radius: 10,
        stroke: new ol.style.Stroke({
            //color: 'white',
            //width: 2,
            color: 'rgba(255,0,0,0)',
            width: 1,
        }),
        fill: new ol.style.Fill({
            //color: 'yellow'
            color: 'rgba(255,255,255,0)'
        })
    }),
    image: new ol.style.Icon({
        src: 'https://openlayers.org/en/v3.20.1/examples/data/icon.png'
    }),

    imageFlag: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 15],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            opacity: 1.0,
            //src: '/FileStore/img/flag.png'
            src: '/theme/apom/images/Location-16x16@2x.png'
    })),

    stroke: new ol.style.Stroke({
        width: 2,
        //color: 'rgba(255,255,0,1.0)',
        color: 'rgba(0,102,255,1.0)',
        //lineDash: [4, 10]
    }),
    fill: new ol.style.Fill({
        //color: [107, 243, 234, 0.1],
        color: [107, 243, 234, 0.5],
        opacity: 0.7
    }),
    textFill: new ol.style.Fill({
        color: '#fff'
    }),
    textStroke: new ol.style.Stroke({
        color: 'rgba(0, 0, 0, 0.6)',
        width: 3
    })
}

apomGis.style.default.flag = new ol.style.Style({
    image: apomGis.style.default.imageFlag
});

apomGis.style.default.point = new ol.style.Style({
    image: apomGis.style.default.imageDefault,
    zIndex: 100
});

apomGis.style.default.point_fix = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 2,
        stroke: new ol.style.Stroke({
            color: 'rgba(0,0,0,1.0)',
            width: 1,
        }),
        fill: new ol.style.Fill({
            color: 'rgba(0,128,255,1)'
        })
    })
});

apomGis.style.default.polyline = new ol.style.Style({
    fill: apomGis.style.default.fill,
    stroke: apomGis.style.default.stroke,
    zIndex: 100
});
apomGis.style.default.polygon = new ol.style.Style({
    fill: apomGis.style.default.fill,
    stroke: apomGis.style.default.stroke,
    zIndex: 100
});
apomGis.style.default.all = new ol.style.Style({
    fill: apomGis.style.default.fill,
    stroke: apomGis.style.default.stroke,
    image: apomGis.style.default.imageDefault,
    zIndex: 100
});
apomGis.style.default.alltransparent = new ol.style.Style({
    fill: apomGis.style.default.fill,
    stroke: apomGis.style.default.stroke,
    image: apomGis.style.default.imageDefaultTransparent,
    zIndex: 100
});


/// draw
apomGis.style.draw = {
    image: new ol.style.Circle({
        radius: 6,
        stroke: new ol.style.Stroke({
            color: 'rgba(0,0,0,1.0)',
            //color: 'black',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: [107, 243, 234, 0.1],
            opacity: 0.7
        })
    }),
    stroke: new ol.style.Stroke({
        width: 2,
        //color: [40, 40, 40, 1],
        color: [255, 40, 40, 1],
        lineDash: [4, 10]
    }),
    fill: new ol.style.Fill({
        //color: [107, 243, 234, 0.1],
        color: [255, 255, 0, 0.3],
        opacity: 0.5
    })
}
apomGis.style.draw.point = new ol.style.Style({
    image: apomGis.style.draw.image,
    zIndex: 100
});
apomGis.style.draw.polyline = new ol.style.Style({
    fill: apomGis.style.draw.fill,
    stroke: apomGis.style.draw.stroke,
    zIndex: 100
});
apomGis.style.draw.polygon = new ol.style.Style({
    fill: apomGis.style.draw.fill,
    stroke: apomGis.style.draw.stroke,
    zIndex: 100
});
apomGis.style.draw.all = new ol.style.Style({
    fill: apomGis.style.draw.fill,
    stroke: apomGis.style.draw.stroke,
    image: apomGis.style.draw.image,
    zIndex: 100
});

function returnColorPM25(val, opacity) {
    let color = '';
    if (val <= 25) {
        if (opacity) color = 'rgba(27,190,88,' + opacity + ')';
        else color = 'rgba(27,190,88,1)';
    }
    else if (val <= 50) {
        if (opacity) color = 'rgba(255,214,0,' + opacity + ')';
        else color = 'rgba(255,214,0,1)';
    }
    else if (val <= 80) {
        if (opacity) color = 'rgba(255,126,0,' + opacity + ')';
        else color = 'rgba(255,126,0,1)';
    }
    else if (val <= 150) {
        if (opacity) color = 'rgba(213,40,39,' + opacity + ')';
        else color = 'rgba(213,40,39,1)';
    }
    else if (val <= 250) {
        if (opacity) color = 'rgba(213,40,39,' + opacity + ')';
        else color = 'rgba(213,40,39,1)';
    }
    else {
        if (opacity) color = 'rgba(126,0,35,' + opacity + ')';
        else color = 'rgba(126,0,35,1)';
    }
    return color;
}

function returnColorAQIPM25(val, opacity) 
{
    let color = '';
    if (val <= 50) {
        if (opacity) color = 'rgba(27,190,88,' + opacity + ')';
        else color = 'rgba(27,190,88,1)';
    }
    else if (val <= 100) {
        if (opacity) color = 'rgba(255,214,0,' + opacity + ')';
        else color = 'rgba(255,214,0,1)';
    }
    else if (val <= 150) {
        if (opacity) color = 'rgba(255,126,0,' + opacity + ')';
        else color = 'rgba(255,126,0,1)';
    }
    else if (val <= 200) {
        if (opacity) color = 'rgba(213,40,39,' + opacity + ')';
        else color = 'rgba(213,40,39,1)';
    }
    else if (val <= 300) {
        if (opacity) color = 'rgba(213,40,39,' + opacity + ')';
        else color = 'rgba(213,40,39,1)';
    }
    else {
        if (opacity) color = 'rgba(126,0,35,' + opacity + ')';
        else color = 'rgba(126,0,35,1)';
    }
    return color;
};

apomGis.style.AQIOut = {
    radius: 18,
    radius_selected: 30
};

apomGis.style.AQIIn = {
    radius: 15,
    radius_selected: 26
}

apomGis.style.styleAQIOut = function (radius, color, zIndex) {
    let style = new ol.style.Style({
        image: new ol.style.Circle({
            radius: radius,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,0)',
                width: 1,
            }),
            fill: new ol.style.Fill({
                color: color
            })
        }),
        zIndex: zIndex
    });
    return style;
};

apomGis.style.styleAQIIn = function (radius, color, textVal, fillText, strokeText, zIndex) {
    let style = new ol.style.Style({
        image: new ol.style.Circle({
            radius: radius,
            stroke: new ol.style.Stroke({
                color: 'white',
                //width: 2,
                //color: 'rgba(0,0,0,0)',
                width: 3,
            }),
            fill: new ol.style.Fill({
                //color: 'yellow'
                color: color
            }),
        }),
        text: new ol.style.Text({
            text: textVal,
            fill: fillText,
            //stroke: strokeText,
            scale: 1
        }),
        zIndex: zIndex
    });
    return style;
};

apomGis.StyleClass = function (map) {
    this.map = map;

    // e: resolution
    this.setStyleForMap = function (feature, e) {
        /*var _model_style = new ol.style.Style({
            image: apomGis.style.default.all.image
        });

        return [_model_style];*/
        // get scale
        let dpi = 25.4 / 0.28;
        let mpu = ol.proj.Units.METERS_PER_UNIT[apomGis.Config.projection.unit];
        let scaleMap = e * mpu * 39.37 * dpi;        
        // only style pm25
        if (feature.getProperties().aqi_pm25) {
            let aqi_pm25 = feature.getProperties().aqi_pm25;
            let colorOut = returnColorAQIPM25(aqi_pm25, 0.4);
            let colorIn = returnColorAQIPM25(aqi_pm25);
            let styleAQIOut = apomGis.style.styleAQIOut(apomGis.style.AQIOut.radius, colorOut, 100);
            
            let textVal = '';
            if (scaleMap <= apomGis.Config.scale.show_label) textVal = aqi_pm25.toString();
            let styleAQIIn = apomGis.style.styleAQIIn(apomGis.style.AQIIn.radius, colorIn, textVal, apomGis.style.default.textFill, apomGis.style.default.textStroke, 100);
            return [styleAQIOut, styleAQIIn];
            //return [styleAQIOut];
            //return [styleAQIIn];
            //return [apomGis.style.default.all];
        }
        else return [apomGis.style.default.all];
    }
};